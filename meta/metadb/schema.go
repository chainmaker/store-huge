package metadb

// 元数据系统，用于管理 区块文件。
// 一个节点，包含 在线文件系统群组 和 归档文件系统群组
// 在线文件系统群组，包含L个文件系统，一个文件系统，包含 N个 区块文件，一个区块文件 包含M个 区块，一个区块 包含K个 交易
// 归档文件系统群组，包含L个文件系统，一个文件系统，包含 N个 区块文件，一个区块文件 包含M个 区块，一个区块 包含K个 交易
//
/* meta配置信息 结构化
*  为了存储方便，添加公共前缀 m,在db中，为了方便，下面的描述中，暂把m去了
*	/v1/onlinefilesystems/ "/home/online1,/home/online2,/home/online3"
*	/v1/archivefilesystems/ "/home/archive1,/home/archive2,/home/archive3"
*	/versions/{v1,v2,v3,v4}   配置文件是要保留历史版本的配置的，因为配置文件可能会有多次修改
*                             版本号，包含主版本和次版本号，主版本号用户配置，次版本号自动生成
*                             major,minor
*                             1,1
*                             1,2
*                             2,1
*	/allfiles/{"f$filename" : value}  从公共前缀  '/allfiles/f' 返回 所有已归档的文件名， 再根据文件名 找块高，文件系统
*					fwal.0001.bfdb: {
*										onlineFileSystem:  /home/online1,
*										isArchived:        0/1/2/3,    0 未归档，1开始归档，2x拷贝数据(21开始,22结束)，3x删除数据(31开始,32结束)，4完成归档
*                                       archiveFileSystem: /home/archive1,
*                                       startHeight:       0,
*										endHeignt:         100,
*									}
*   /doArchive/{"o$id": value }  执行一个归档，将本次 归档操作记录下来
*                 "o00001" : {
*                            startTime: xxxxxxxx,
*                            endTime: xxxxxx,
*                            status: 0/1/2/3,     0 未归档，1开始归档，2x拷贝数据(21开始,22结束)，3x删除数据(31开始,32结束)，4完成归档
*                            startHeignt:  51,
*                            endHeignt: 60,
*                            files: [wal.0001.bfdb,
* 									 wal.0002.bfdb,
*									 wal.0003.bfdb,
*                                   ],
*                          }
 */
// 文件系统群 空间均衡 策略
//    在线文件系统，按照文件系统的顺序写入。第一个写满，写第二个。一般情况，在线文件系统可能只有1-2个。
//    归档文件系统，按照 可用空间 ，均衡写入。

// 发起归档的过程:
//	1.调用归档接口，返回回调参数;异步处理;
//	2.通过回调，获得归档的结果;

// 元数据系统:
//	1.配置文件变更 要更新元数据系统;
//	2.wal创建新文件要将文件名上报到元数据系统;
//	3.文件包含的块高范围startHeight,endHeignt，要更新到文件系统中;

// 归档影响的区块:
//  1.归档是 左闭右开。归档要指定startHeight,endHeight。实际归档时，endHeight 所在的文件，如果正是当前online文件系统 正在写的文件.
//  这时，不能 对endHeight 对应的文件进行归档，也就说最新的，最近写的在线文件不能归档。
//  2.归档是 连续区间。如果目前区块是，0-100，如果首次归档 是 50-80，是不允许的，因为<50的还未归档，必须从0号区块开始归档。
//  3.配置区块归档。配置区块会单独再存到db中。同时配置区块作为普通区块也会和非配置区块一起写到对应文件和索引中。
