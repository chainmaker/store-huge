#创建文件系统 用于测试使用
#创建了 文件系统 $1 为 目录名， $2 为volume 名
#主要用于测试 多文件系统环境下 metadb 和 bfdb 的功能
#在 /tmp 目录下创建
testdir="$1"
volume="$2"
currdir=""
function make_fs() {
    currdir=`pwd`
    cd "/tmp"
    mkdir -p $testdir
    cd $testdir

    filename=""
    #安装juicefs
    if [ "$(uname)" == "Darwin" ];then
      # Mac OS X 操作系统
      filename="juicefs-1.0.0-beta3-darwin-amd64"
    elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ];then
      filename="juicefs-1.0.0-beta3-linux-amd64"
    fi
    #存在则不安装
    if [ ! -f juicefs ];then
      #JFS_LATEST_TAG=$(curl -s https://api.github.com/repos/juicedata/juicefs/releases/latest | grep 'tag_name' | cut -d '"' -f 4 | tr -d 'v')
      #wget "https://github.com/juicedata/juicefs/releases/download/v${JFS_LATEST_TAG}/juicefs-${JFS_LATEST_TAG}-linux-amd64.tar.gz"
      echo "---------------- ${filename}"
      wget -q "https://github.com/juicedata/juicefs/releases/download/v1.0.0-beta3/${filename}.tar.gz"
      tar -zxf "${filename}.tar.gz"
    fi

    #创建 文件系统 可用空间1G
    ./juicefs format sqlite3://myjfs_${volume}.db ${volume} --capacity=1

    #挂载 到 /tmp/metadb/mnt_ext4
    #./juicefs mount sqlite3://myjfs_${volume}.db mnt_${volume} -d
    ./juicefs mount sqlite3://myjfs_${volume}.db ${volume} -d

    cd $currdir
}
make_fs

