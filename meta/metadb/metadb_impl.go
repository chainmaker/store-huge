/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0

*/

// Package metadb package
package metadb

import (
	//pbgo "chainmaker.org/chainmaker/ pb-go/v3"
	"errors"
	"fmt"
	"io"
	"math"
	"os"
	"sort"
	"strings"
	"sync"
	"time"

	storePb "chainmaker.org/chainmaker/pb-go/v3/store"
	"chainmaker.org/chainmaker/protocol/v3"

	//bufferCRC "chainmaker.org/chainmaker/store-huge/v3/blockdb/blockfiledb"
	"chainmaker.org/chainmaker/store-huge/v3/conf"
	"chainmaker.org/chainmaker/store-huge/v3/meta"
	"chainmaker.org/chainmaker/store-huge/v3/types"
	"github.com/google/uuid"
	"github.com/shirou/gopsutil/v3/disk"
)

// 元数据系统，用于管理 区块文件。
// 一个节点，包含 在线文件系统群组 和 归档文件系统群组
// 在线文件系统群组，包含L个文件系统，一个文件系统，包含 N个 区块文件，一个区块文件 包含M个 区块，一个区块 包含K个 交易
// 归档文件系统群组，包含L个文件系统，一个文件系统，包含 N个 区块文件，一个区块文件 包含M个 区块，一个区块 包含K个 交易
//
/* meta配置信息 结构化
*  为了存储方便，添加公共前缀 m,在db中，为了方便，下面的描述中，暂把m去了
*	/v1/onlinefilesystems/ "/home/online1,/home/online2,/home/online3"
*	/v1/archivefilesystems/ "/home/archive1,/home/archive2,/home/archive3"
*	/versions/{v1,v2,v3,v4}   配置文件是要保留历史版本的配置的，因为配置文件可能会有多次修改
*                             版本号，包含主版本和次版本号，主版本号用户配置，次版本号自动生成
*                             major,minor
*                             v1.0.0,20211201190000
*                             v1.0.0,20220108200000
*                             v2.0.0,20220907300000
*	/allfiles/{"f$filename" : value}  从公共前缀  '/allfiles/f' 返回 所有已归档的文件名， 再根据文件名 找块高，文件系统
*					fwal.0001.bfdb: {
*										onlineFileSystem:  /home/online1,
*										status:        0/1/2/3,    0 未归档，1开始归档，2拷贝数据，3删除数据，4完成归档
*                                       archiveFileSystem: /home/archive1,
*                                       startHeight:       0,
*										endHeignt:         100,
*									}
*   /doArchive/{"o$id": value }  执行一个归档，将本次 归档操作记录下来
*                 "o00001" : {
*                            startTime: xxxxxxxx,
*                            endTime: xxxxxx,
*                            status: 0/1/2/3,     0 未归档，1开始归档，2拷贝数据，3删除数据，4完成归档
*                            startHeignt:  51,
*                            endHeignt: 60,
*                            files: [wal.0001.bfdb,
* 									 wal.0002.bfdb,
*									 wal.0003.bfdb,
*                                   ],
*                          }
 */
// 文件系统群 空间均衡 策略
//    在线文件系统，按照文件系统的顺序写入。第一个写满，写第二个。一般情况，在线文件系统可能只有1-2个。
//    归档文件系统，按照 可用空间 ，均衡写入。

// 发起归档的过程:
//	1.调用归档接口，返回回调参数;异步处理;
//	2.通过回调，获得归档的结果;

// 元数据系统:
//	1.配置文件变更 要更新元数据系统;
//	2.wal创建新文件要将文件名上报到元数据系统;
//	3.文件包含的块高范围startHeight,endHeight，要更新到文件系统中;

// 归档影响的区块:
//  1.归档是 左闭右开。归档要指定startHeight,endHeight。实际归档时，endHeight 所在的文件，如果正是当前online文件系统 正在写的文件.
//  这时，不能 对endHeight 对应的文件进行归档，也就说最新的，最近写的在线文件不能归档。
//  2.归档是 连续区间。如果目前区块是，0-100，如果首次归档 是 50-80，是不允许的，因为<50的还未归档，必须从0号区块开始归档。
//  3.配置区块归档。配置区块会单独再存到db中。同时配置区块作为普通区块也会和非配置区块一起写到对应文件和索引中。
//nolint
const (
	maxBlockHeight = math.MaxUint64
	//metaKeyPrefix     = 'm'
	metaFSGKey        = "mmetaFSG"
	metaVersionsKey   = "mversions"
	metaAllFiles      = "mallfiles"
	metaArchivePrefix = "mdoArchive"

	archiveStatusNotStart  = 0
	archiveStatusBegin     = 1
	archiveStatusCopyStart = 21
	archiveStatusCopyEnd   = 22
	archiveStatusDelStart  = 31
	archiveStatusDelEnd    = 32
	archiveStatusFinish    = 4

	BUFFERSIZE       = 1000
	ReserveSpaceSize = 1024 * 1024 * 256

	fileStatusNotArchived = 0
	fileStatusBegin       = 1
	fileStatusCopyStart   = 21
	fileStatusCopyEnd     = 22
	fileStatusDelStart    = 31
	fileStatusDelEnd      = 32
	fileStatusEnd         = 4
)

//nolint
var (
	//errConfigBfdbFile          = errors.New("bfdb file system config error")
	errConfigFileSystem        = errors.New("file system config error")
	errConfigArchiveRepeated   = errors.New("config archive file system repeated")
	errConfigOnlineRepeated    = errors.New("config online file system repeated")
	errConfigArchiveModify     = errors.New("the old archive file system configuration cannot be modified, only added")
	errConfigOnlineModify      = errors.New("the old online file system configuration cannot be modified, only added")
	errConfigVersionMajor      = errors.New("major version is smaller than origin")
	errConfigVersionMinor      = errors.New("minor version is smaller than origin")
	errCreateNewFile           = errors.New("file exist")
	errFileNotExist            = errors.New("file not exist")
	errFileIsInUse             = errors.New("file is in use")
	errWriteFile               = errors.New("incomplete write file")
	errArchiveFSSpaceNotEnough = errors.New("archive file system not enough free space")
	errArchiveStatusErr        = errors.New("archive status error")
	errFileStatusErr           = errors.New("file status error")

	ErrOnlineFSSpaceNotEnough = errors.New("online FS disk full error")
)

// MetaDataDB manage meta data of block file ,
// include create meta info of block file ,
// and separate from  hot and cold data ,
// and read data from block file which has been separated.
// Notice 1,if the block file is not be separated ,than read data from block file  directly,
// rather than read from meta data db.
// Notice 2, the startHeight and endHeight of MetaFileInfo are not the block height,
// they are the entry index of block file. The entry index of block file is 1 less than the block height.
// Example :
//	MetaFileSystem {
//        startHeight:3,
//        endHeight:8,
//	}
// it means this segment or file contains [3,8] entries .A entry is a block.
// And it means this segment contains the block height range is  [2,7] rather than [3,8].
//  @Description:
type MetaDataDB struct {
	metaFSG   *storePb.MetaFileSystem
	bfdbFiles map[string]storePb.MetaFileInfo
	//archiveFiles []FileInfo
	multVersions *storePb.MetaMultVersions
	chainID      string
	metaStoreDB  protocol.DBHandle
	logger       protocol.Logger
	archiveChan  chan string
	sync.RWMutex
	//todo 需要确认 无锁 读写map、重复加锁、死锁的 并发问题
}

// NewMetaDataDB create MetaData
//  @Description:
//  @param storeConfig
//  @param chainID
//  @param dbHandle
//  @param logger
//  @return MetaData
//  @return error
func NewMetaDataDB(config conf.StorageConfig, chainID string, dbHandle protocol.DBHandle,
	logger protocol.Logger) (meta.MetaData, error) {
	m := &MetaDataDB{
		bfdbFiles:   make(map[string]storePb.MetaFileInfo),
		chainID:     chainID,
		logger:      logger,
		metaStoreDB: dbHandle,
		archiveChan: make(chan string, 1000),
	}
	if err := m.initMeta(config); err != nil {
		m.logger.Errorf("init meta error :[%s]", err)
		return nil, err
	}

	if err := m.mkdirOnlineArchiveFS(); err != nil {
		m.logger.Errorf("mkdir onlineFS or archiveFS error :[%s]", err)
		return nil, err
	}

	//重新推送一次未完成的归档任务
	if err := m.publishArchiveJob(); err != nil {
		m.logger.Errorf("publish archive job error :[%s]", err)
		return nil, err
	}

	//启动 归档后台协程
	go func() {
		m.consumeArchiveJob()
	}()

	return m, nil
}

// initMeta 先从db中读取配置，再用配置文件刷新
// initMeta read info from config file,then update meta db with config data
// meta support multiple versions
//  @Description:
//  @receiver m
//  @param storeConfig
//  @return error
func (m *MetaDataDB) initMeta(config conf.StorageConfig) error {
	m.logger.Infof("start initMeta")
	defer m.logger.Infof("finish initMeta")

	var metaFS *storePb.MetaFileSystem
	var multVersions *storePb.MetaMultVersions

	metaFS = &storePb.MetaFileSystem{}

	if config.BlockFileConfig == nil {
		return errConfigFileSystem
	}

	onlineFS := strings.Split(config.BlockFileConfig.OnlineFileSystem, ",")
	archiveFS := strings.Split(config.BlockFileConfig.ArchiveFileSystem, ",")
	sort.Strings(onlineFS)
	sort.Strings(archiveFS)

	m.logger.Infof("config BlockFileConfig OnlineFileSystem =[%s]", onlineFS)
	m.logger.Infof("config BlockFileConfig ArchiveFileSystem =[%s]", archiveFS)

	//如果配置中有重复，则报错
	if repeated := CheckRepeated(archiveFS); repeated {
		return errConfigArchiveRepeated
	}
	if repeated := CheckRepeated(onlineFS); repeated {
		return errConfigOnlineRepeated
	}

	//从db中获得所有版本
	multVersions, err := m.GetVersionFromDB()
	if err != nil {
		return err
	}

	//首次启动,db中没有元数据，则用配置文件，写入一个
	if multVersions == nil {
		m.logger.Debugf("setMetaFirstStart")
		return m.setMetaFirstStart(config)
	}

	//非首次启动
	//db中有配置，获得最新配置，如果新配置有变化，要更新到meta中
	//初始化metaFS
	metaFS.OnlineFileSystem = onlineFS
	metaFS.ArchiveFileSystem = archiveFS
	m.logger.Debugf("setMetaUseConfigAndDB")
	return m.setMetaUseConfigAndDB(config, multVersions, metaFS)

}

// mkdirOnlineArchiveFS 归档系统创建目录,支持多链
// mkdirOnlineArchiveFS create directory of archive file system, support multiple chains
//  @Description:
//  @receiver m
//  @return error
func (m *MetaDataDB) mkdirOnlineArchiveFS() error {
	m.logger.Infof("start mkdirArchiveFS")
	defer m.logger.Infof("finish mkdirArchiveFS")
	for i := 0; i < len(m.metaFSG.ArchiveFileSystem); i++ {
		archivePath := m.metaFSG.ArchiveFileSystem[i]
		archivePath = archivePath + "/" + m.chainID + "/" + "bfdb/"
		m.logger.Debugf("marchiveFS:[%s]", archivePath)
		if err := os.MkdirAll(archivePath, 0777); err != nil {
			m.logger.Errorf("mkdir marchiveFS:[%s], error:[%s]", archivePath, err)
			return err
		}
	}
	for i := 0; i < len(m.metaFSG.OnlineFileSystem); i++ {
		onlinePath := m.metaFSG.OnlineFileSystem[i]
		onlinePath = onlinePath + "/" + m.chainID + "/" + "bfdb/"
		m.logger.Debugf("monlineFS:[%s]", onlinePath)
		if err := os.MkdirAll(onlinePath, 0777); err != nil {
			m.logger.Errorf("mkdir monlineFS:[%s], error:[%s]", onlinePath, err)
			return err
		}
	}
	return nil
}

// GetVersionFromDB 从db中获得所有版本
// GetVersionFromDB get all versions of meta from db, the meta support multiple version
//  @Description:
//  @receiver m
//  @param storeConfig
//  @return error
func (m *MetaDataDB) GetVersionFromDB() (*storePb.MetaMultVersions, error) {
	versions, err := m.metaStoreDB.Get([]byte(metaVersionsKey))
	if err != nil {
		return nil, err
	}
	if versions == nil {
		return nil, nil
	}
	multVersions := &storePb.MetaMultVersions{}
	if err := multVersions.Unmarshal(versions); err != nil {
		return nil, errors.New("unmarshal versions error")
	}
	return multVersions, nil
}

// setMetaFirstStart 元数据系统,首次启动时,db中没有，需要从配置文件中初始化一个
///todo 如果是升级，需要扫描所有文件，把文件信息 也写到db中，目前还没做
// 如果不是 升级，节点新启动的，没问题
// setMetaFirstStart  set meta data when the node is started up for the first time.
//  @Description:
//  @receiver m
//  @param storeConfig
//  @return error
func (m *MetaDataDB) setMetaFirstStart(config conf.StorageConfig) error {
	m.logger.Infof("set meta first start ,begin")
	defer m.logger.Infof("set meta first start ,finish")
	onlineFS := strings.Split(config.BlockFileConfig.OnlineFileSystem, ",")
	archiveFS := strings.Split(config.BlockFileConfig.ArchiveFileSystem, ",")

	sort.Strings(onlineFS)
	sort.Strings(archiveFS)

	metaFS := &storePb.MetaFileSystem{
		OnlineFileSystem:  onlineFS,
		ArchiveFileSystem: archiveFS,
	}
	multVersions := &storePb.MetaMultVersions{
		Version: []*storePb.MetaVersion{
			{
				MajorVersion: config.ConfigVersion.Major,
				MinorVersion: config.ConfigVersion.Minor,
				UpdateTime:   time.Now().Unix(),
			},
		},
	}
	marshalFS, err := metaFS.Marshal()
	if err != nil {
		return err
	}
	marshalMultVersion, err := multVersions.Marshal()
	if err != nil {
		return err
	}
	batcher := types.NewUpdateBatch()

	versionPrefix, err := multVersions.Version[0].Marshal()
	if err != nil {
		return err
	}
	metaFSGKeyName := append(append([]byte{'m'}, versionPrefix...), metaFSGKey...)
	batcher.Put(metaFSGKeyName, marshalFS)
	batcher.Put([]byte(metaVersionsKey), marshalMultVersion)

	//写配置文件的配置到metadb中
	if err := m.metaStoreDB.WriteBatch(batcher, true); err != nil {
		return err
	}

	//初始化数据到内存中
	m.metaFSG = metaFS
	m.multVersions = multVersions
	return nil
}

// setMetaUseConfigAndDB db中有配置，需要结合配置文件中的配置和db中的旧的配置，来完成初始化
// setMetaUseConfigAndDB  set metadata combine the data in configuration file and meta db.
//  @Description:
//  @receiver m
//  @param storeConfig
//  @param multiVersions
//  @param metaFS
//  @return error
func (m *MetaDataDB) setMetaUseConfigAndDB(config conf.StorageConfig,
	multiVersions *storePb.MetaMultVersions, metaFS *storePb.MetaFileSystem) error {
	var lastVersion *storePb.MetaVersion
	var dbMetaFS *storePb.MetaFileSystem

	//1.获得最后最新的版本号
	lastVersion = m.getLastVersion(multiVersions)

	//2.得到metadb中存储的最新的版本
	dbMetaFS, err2 := m.getMetaFSByVersion(config, lastVersion)
	if err2 != nil {
		return err2
	}

	//3.对比配置文件中的配置和dbMetaFS是否一致，是否被错误修改
	m.logger.Debugf("metaFS:[%s],dbMetaFS:[%s]", metaFS, dbMetaFS)
	diffCountOnline, diffCountArchive, err := m.checkConfigDiff(metaFS, dbMetaFS)
	if err != nil {
		m.logger.Debugf("configArchiveFS:[%s],dbArchiveFS:[%s]", metaFS.ArchiveFileSystem, dbMetaFS.ArchiveFileSystem)
		m.logger.Debugf("configOnlineFS:[%s],dbOnlineFS:[%s]", metaFS.OnlineFileSystem, dbMetaFS.OnlineFileSystem)
		return err
	}

	//4.如果配置没变，db中和配置文件中关于元数据信息相同，则不用更新db中的元数据
	if diffCountOnline == 0 && diffCountArchive == 0 &&
		lastVersion.MajorVersion == config.ConfigVersion.Major &&
		lastVersion.MinorVersion == config.ConfigVersion.Minor {

		m.metaFSG = metaFS
		m.multVersions = multiVersions

		//文件信息加载到内存中
		return m.loadMetaFileInfo()

	}
	//5.配置变了，需要生成一个新的版本的元数据，写到db中
	if err := m.setNewVersion(config, multiVersions, metaFS); err != nil {
		return err
	}
	// 新配置，初始化数据到内存中
	m.metaFSG = metaFS
	m.multVersions = multiVersions

	//文件信息加载到内存中
	return m.loadMetaFileInfo()

}

// getLastVersion 获得最后最新的版本号
//主版本号、次版本号、更新时间 最大的
// getLastVersion  get the last version
// Follow the principle of major version number, minor version number,
// and update time in order to get the latest version number.
//  @Description:
//  @receiver m
//  @param multiVersions
//  @return MetaVersion
func (m *MetaDataDB) getLastVersion(multiVersions *storePb.MetaMultVersions) *storePb.MetaVersion {
	var lastVersion *storePb.MetaVersion
	j := 0
	for i := 1; i < len(multiVersions.Version); i++ {
		//主版本小
		if multiVersions.Version[j].MajorVersion < multiVersions.Version[i].MajorVersion {
			j = i
			continue
		}
		//主版本相同，次版本小
		if multiVersions.Version[j].MajorVersion == multiVersions.Version[i].MajorVersion &&
			multiVersions.Version[j].MinorVersion < multiVersions.Version[i].MinorVersion {
			j = i
			continue
		}
		//主版本相同，次版本相同，对比时间
		if multiVersions.Version[j].MajorVersion == multiVersions.Version[i].MajorVersion &&
			multiVersions.Version[j].MinorVersion == multiVersions.Version[i].MinorVersion &&
			multiVersions.Version[j].UpdateTime < multiVersions.Version[i].UpdateTime {
			j = i
			continue
		}
	}
	lastVersion = multiVersions.Version[j]
	return lastVersion
}

// getMetaFSByVersion 获得metadb中存储的指定版本的元数据
// getMetaFSByVersion  get the meta file system by version
//  @Description:
//  @receiver m
//  @param storageConfig
//  @param lastVersion
//  @return MetaFileSystem
//  @return error
func (m *MetaDataDB) getMetaFSByVersion(config conf.StorageConfig, lastVersion *storePb.MetaVersion) (
	*storePb.MetaFileSystem, error) {
	dbMetaFS := &storePb.MetaFileSystem{}

	//对比配置文件中的版本号与metadb中的版本号是不是更大
	//主版本号更小了，则配置错误
	if config.ConfigVersion.Major < lastVersion.MajorVersion {
		return dbMetaFS, errConfigVersionMajor
	}
	//主版本号相同，次版本号更小了，则配置错误
	if config.ConfigVersion.Major == lastVersion.MajorVersion &&
		config.ConfigVersion.Minor < lastVersion.MinorVersion {
		return dbMetaFS, errConfigVersionMinor
	}

	//获得版本号
	versionPrefix, err := lastVersion.Marshal()
	if err != nil {
		return dbMetaFS, err
	}

	//拼接 metaFSG 在db中的key
	metaFSGKeyName := append(append([]byte{'m'}, versionPrefix...), metaFSGKey...)
	//读取
	dbMetaFSData, err := m.metaStoreDB.Get(metaFSGKeyName)
	if err != nil {
		return dbMetaFS, err
	}
	if err := dbMetaFS.Unmarshal(dbMetaFSData); err != nil {
		return dbMetaFS, err
	}
	return dbMetaFS, nil
}

// GetMetaFS 获得metadb中的元数据,返回最新版本的FS
// GetMetaFS  get the meta file system
//  @Description:
//  @receiver m
//  @return MetaFileSystem
//  @return error
func (m *MetaDataDB) GetMetaFS() (storePb.MetaFileSystem, error) {
	dbMetaFS := storePb.MetaFileSystem{}
	dbMetaFS.OnlineFileSystem = m.metaFSG.GetOnlineFileSystem()
	dbMetaFS.ArchiveFileSystem = m.metaFSG.GetArchiveFileSystem()
	return dbMetaFS, nil
}

// GetChainID 获得metadb中的chainID
// GetChainID  get the chainID from metadb
//  @Description:
//  @receiver m
//  @return chainID
func (m *MetaDataDB) GetChainID() string {
	return m.chainID
}

// loadMetaFileInfo 文件信息加载到内存中
////todo: 当文件越来越多时，后期应该 加缓存，不能把所有fileInfo 都放内存中，map太大了
// loadMetaFileInfo  load meta info into memory from db
//  @Description:
//  @receiver m
//  @return error
func (m *MetaDataDB) loadMetaFileInfo() error {
	m.Lock()
	defer m.Unlock()

	iter, err := m.metaStoreDB.NewIteratorWithPrefix([]byte(metaAllFiles))
	defer iter.Release()
	if err != nil {
		return err
	}
	for iter.Next() {
		v := iter.Value()
		k := iter.Key()
		fileInfo := storePb.MetaFileInfo{}
		err := fileInfo.Unmarshal(v)
		if err != nil {
			return err
		}
		//文件名不带前缀
		fileName := string(k)[len(metaAllFiles):]
		m.bfdbFiles[fileName] = fileInfo
		m.logger.Debugf("loadMetaFileInfo,fileName:[%s],fileInfo:[%v]", fileName, fileInfo)
	}
	return nil
}

// setNewVersion 新的配置信息，将配置信息从配置文件中抽取出来
//作为一个新的版本，写到db中，更新元数据
// setNewVersion  create a new version
//  @Description:
//  @receiver m
//  @param storageConfig
//  @param multiVersions
//  @param MetaFileSystem
//  @return error
func (m *MetaDataDB) setNewVersion(config conf.StorageConfig, multiVersions *storePb.MetaMultVersions,
	metaFS *storePb.MetaFileSystem) error {

	newVersion := &storePb.MetaVersion{
		MajorVersion: config.ConfigVersion.Major,
		MinorVersion: config.ConfigVersion.Minor,
		UpdateTime:   time.Now().Unix(),
	}
	multiVersions.Version = append(multiVersions.Version, newVersion)
	marshalMultVersion, err := multiVersions.Marshal()

	if err != nil {
		return err
	}
	batcher := types.NewUpdateBatch()
	//写版本信息
	batcher.Put([]byte(metaVersionsKey), marshalMultVersion)

	versionPrefix, err := newVersion.Marshal()
	if err != nil {
		return err
	}
	metaFSGKeyName := append(append([]byte{'m'}, versionPrefix...), metaFSGKey...)
	marshalFS, err := metaFS.Marshal()
	if err != nil {
		return err
	}
	//写metaFSG
	batcher.Put(metaFSGKeyName, marshalFS)

	//写配置文件的配置到metadb中
	return m.metaStoreDB.WriteBatch(batcher, true)

}

// Get 按照指定块高，返回对应块高的归属的文件
// Get  get meta info by height
//  @Description:
//  @receiver m
//  @param height
//  @return MetaFileInfo
//  @return error
func (m *MetaDataDB) Get(index uint64) (storePb.MetaFileInfo, error) {
	m.RLock()
	defer m.RUnlock()
	var fileInfo storePb.MetaFileInfo
	for _, v := range m.bfdbFiles {
		if index >= v.StartHeight && index <= v.EndHeight {
			return v, nil
		}
	}
	return fileInfo, nil
}

// GetFileInfoByHeight return the filename,file info,file exist or not, error
func (m *MetaDataDB) GetFileInfoByHeight(index uint64) (string, storePb.MetaFileInfo, bool, error) {
	m.RLock()
	defer m.RUnlock()
	var fileInfo storePb.MetaFileInfo
	for k, v := range m.bfdbFiles {
		if index >= v.StartHeight && index <= v.EndHeight {
			return k, v, true, nil
		}
	}
	return "", fileInfo, false, nil

}

// CreateNewFile 创建一个文件，在元数据系统中
// CreateNewFile  create a new file internal meta info, it is just a meta data not an os file
//  @Description:
//  @receiver m
//  @param fileName
//  @param fileSystem
//  @param startHeight
//  @return error
func (m *MetaDataDB) CreateNewFile(fileName string, fileSystem string, startIndex uint64) error {
	m.RLock()
	defer m.RUnlock()
	var fileInfo storePb.MetaFileInfo
	k := metaAllFiles + fileName

	if _, exist := m.bfdbFiles[fileName]; exist {
		return errCreateNewFile
	}

	fileInfo = storePb.MetaFileInfo{
		OnlineFileSystem:  fileSystem,
		Status:            0,
		ArchiveFileSystem: "",
		StartHeight:       startIndex,
		EndHeight:         maxBlockHeight,
	}

	batcher := types.NewUpdateBatch()
	marshalData, err := fileInfo.Marshal()
	if err != nil {
		return err
	}
	//写db，文件名带前缀
	batcher.Put([]byte(k), marshalData)
	err = m.metaStoreDB.WriteBatch(batcher, true)
	if err != nil {
		return err
	}
	//内存中，文件名 不带前缀
	m.bfdbFiles[fileName] = fileInfo

	return nil
}

// SetFileEndHeight 设置file的结束区块的块高
// SetFileEndHeight  set the end height of file
//  @Description:
//  @receiver m
//  @param fileName
//  @param endHeight
//  @return error
func (m *MetaDataDB) SetFileEndHeight(fileName string, endHeight uint64) error {
	m.Lock()
	defer m.Unlock()
	var fileInfo storePb.MetaFileInfo
	k := metaAllFiles + fileName

	if _, exist := m.bfdbFiles[fileName]; !exist {
		return errFileNotExist
	}

	//用旧的值替换新的
	old := m.bfdbFiles[fileName]

	fileInfo = storePb.MetaFileInfo{
		OnlineFileSystem:  old.OnlineFileSystem,
		Status:            old.Status,
		ArchiveFileSystem: old.ArchiveFileSystem,
		StartHeight:       old.StartHeight,
		EndHeight:         endHeight,
	}

	batcher := types.NewUpdateBatch()
	marshalData, err := fileInfo.Marshal()
	if err != nil {
		return err
	}
	batcher.Put([]byte(k), marshalData)
	//更新到db
	err = m.metaStoreDB.WriteBatch(batcher, true)
	if err != nil {
		return err
	}
	//更新到内存，内存中文件名不带前缀
	m.bfdbFiles[fileName] = fileInfo

	return nil

}

// GetFileInfoByFileName 获得文件信息
// GetFileInfoByFileName  get file info by file name
//  @Description:
//  @receiver m
//  @param fileName
//  @return MetaFileInfo
//  @return error
func (m *MetaDataDB) GetFileInfoByFileName(fileName string) (storePb.MetaFileInfo, error) {
	m.RLock()
	defer m.RUnlock()
	//fileInfo := storePb.MetaFileInfo{}
	//k := metaAllFiles + fileName

	fileInfo, exist := m.bfdbFiles[fileName]
	if !exist {
		return fileInfo, errFileNotExist
	}
	m.logger.Debugf("file info in metadb [%v]", fileInfo)

	return fileInfo, nil

}

// IsArchive return true or false if file is archived or not
//  @Description:
//  @receiver m
//  @param fileName
//  @return bool
//  @return error
func (m *MetaDataDB) IsArchive(fileName string) (bool, error) {
	name, err := m.GetFileInfoByFileName(fileName)
	if err != nil {
		m.logger.Errorf("IsArchive,fileName:[%s],errInfo:[%s]", fileName, err)
		return false, err
	}
	m.logger.Debugf("check file is archived ,meta info:[%#v]", name)
	//如果已归档，调归档系统，读已归档文件的数据
	if name.Status == fileStatusCopyEnd || name.Status == fileStatusDelStart ||
		name.Status == fileStatusDelEnd || name.Status == fileStatusEnd {
		return true, nil
	}
	return false, nil
}

// GetLastFileInfo 获得最后生成的文件 信息,如果没有 返回false
// GetLastFileInfo  get last file info
//  @Description:
//  @receiver m
//  @return MetaFileInfo
//  @return bool
//  @return error
func (m *MetaDataDB) GetLastFileInfo() (string, storePb.MetaFileInfo, bool, error) {
	m.RLock()
	defer m.RUnlock()
	fileInfo := storePb.MetaFileInfo{}
	//k := metaAllFiles + fileName

	//没有一个文件信息
	if len(m.bfdbFiles) == 0 {
		return "", fileInfo, false, nil
	}
	maxFileName := ""

	//遍历一遍，找最大的字符串
	for key, value := range m.bfdbFiles {
		if key > maxFileName {
			maxFileName = key
			fileInfo = value
		}
	}

	return maxFileName, fileInfo, true, nil

}

// ReadArchiveFileDataByOffset 读取已归档的文件数据
// ReadArchiveFileDataByOffset  read data by offset from archive file which file is cold
//  @Description:
//  @receiver m
//  @param fileName
//  @param b    input []byte, the data is saved to b
//  @param offset      read position of file
//  @return int        data length that read
//  @return error
func (m *MetaDataDB) ReadArchiveFileDataByOffset(fileName string, b []byte, offset int64) (int, error) {
	m.RLock()
	defer m.RUnlock()
	//fileInfo := storePb.MetaFileInfo{}

	fileInfo, exist := m.bfdbFiles[fileName]
	if !exist {
		return 0, errFileNotExist
	}

	path := m.absolutePath(fileName, fileInfo.ArchiveFileSystem)

	f, err := os.Open(path)
	defer func() {
		if err = f.Close(); err != nil {
			m.logger.Errorf("close db error :[%s]", err)
		}
	}()
	if err != nil {
		return 0, err
	}
	n, err := f.ReadAt(b, offset)
	if err != nil {
		return 0, err
	}
	return n, nil

}

// setFileStatus 设置file的状态
// setFileStatus  set file status
//  @Description:
//  @receiver m
//  @param fileName
//  @param newStatus
//  @return error
func (m *MetaDataDB) setFileStatus(fileName string, newStatus int32) error {
	m.Lock()
	defer m.Unlock()
	var fileInfo storePb.MetaFileInfo
	k := metaAllFiles + fileName

	if _, exist := m.bfdbFiles[fileName]; !exist {
		return errFileNotExist
	}

	//用旧的值替换新的
	old := m.bfdbFiles[fileName]

	fileInfo = storePb.MetaFileInfo{
		OnlineFileSystem:  old.OnlineFileSystem,
		Status:            newStatus,
		ArchiveFileSystem: old.ArchiveFileSystem,
		StartHeight:       old.StartHeight,
		EndHeight:         old.EndHeight,
	}

	batcher := types.NewUpdateBatch()
	marshalData, err := fileInfo.Marshal()
	if err != nil {
		return err
	}
	batcher.Put([]byte(k), marshalData)
	//更新到db
	err = m.metaStoreDB.WriteBatch(batcher, true)
	if err != nil {
		return err
	}
	//更新到内存,文件名不带前缀
	m.bfdbFiles[fileName] = fileInfo

	return nil

}

// setFileArchiveFS 更新file对应的归档文件系统
// setFileArchiveFS  set the archive file system of file
//  @Description:
//  @receiver m
//  @param fileName
//  @param archiveFS
//  @return error
func (m *MetaDataDB) setFileArchiveFS(fileName string, archiveFS string) error {
	m.Lock()
	defer m.Unlock()
	var fileInfo storePb.MetaFileInfo
	k := metaAllFiles + fileName

	if _, exist := m.bfdbFiles[fileName]; !exist {
		return errFileNotExist
	}

	//用旧的值替换新的
	old := m.bfdbFiles[fileName]

	fileInfo = storePb.MetaFileInfo{
		OnlineFileSystem:  old.OnlineFileSystem,
		Status:            old.Status,
		ArchiveFileSystem: archiveFS,
		StartHeight:       old.StartHeight,
		EndHeight:         old.EndHeight,
	}

	batcher := types.NewUpdateBatch()
	marshalData, err := fileInfo.Marshal()
	if err != nil {
		return err
	}
	batcher.Put([]byte(k), marshalData)
	//更新到db
	err = m.metaStoreDB.WriteBatch(batcher, true)
	if err != nil {
		return err
	}
	//更新到内存,文件名不带前缀
	m.bfdbFiles[fileName] = fileInfo

	return nil

}

// DoHotColdDataSeparation 发起一次冷热数据分离
// startHeight,endHeight 需要冷热分离的区块区间块高
// DoHotColdDataSeparation  do a job for separate hot data and cold data by start height and end height of block
//  @Description:
//  @receiver m
//  @param startHeight
//  @param endHeight
//  @return string   return a job id ,that client can get job status by job id.
//  @return error
func (m *MetaDataDB) DoHotColdDataSeparation(startHeight uint64, endHeight uint64) (string, error) {
	//1.保证至少最近写入的10个区块，不能做归档
	//奔溃恢复时db数据要从bf中补齐，最多读10个区块，不能从归档中读，否则会读不到。对应BF的ReadLastSegSection()函数
	_, metaFileInfo, b, err2 := m.GetLastFileInfo()
	if err2 != nil {
		return "", err2
	}
	if !b {
		return "", errFileNotExist
	}
	if metaFileInfo.StartHeight-endHeight < 10 {
		return "", errFileIsInUse
	}
	//2.进行归档
	return m.DoArchive(startHeight, endHeight)
}

// DoArchive 发起一次归档操作,异步执行,回调结果
///todo:是否要强制要求必须保证 [0,start-1]区间的已经归档了，才能 让 [start,end] 进行归档
//目前未限制
///todo:归档完成后，被归档的文件是否要通知 blockfiledb 和 resultfiledb 去重新打开
//目前看不需要
///todo: 当前最新的wal文件，后缀名带了 .END ,不能归档
//如果归档时，要对最新的wal文件进行归档，不允许
//最近10个块，不允许归档
// DoArchive  do a job for separate hot data and cold data by start height and end height of block
//  @Description:
//  @receiver m
//  @param startHeight
//  @param endHeight
//  @return string   return a job id ,that client can get job status by job id.
//  @return error
func (m *MetaDataDB) DoArchive(startHeight uint64, endHeight uint64) (string, error) {
	var archiveJob storePb.ArchiveJob

	//1.产生归档任务ID
	jobID := GenerateUniqueID()
	//2.归档任务写到db中
	doArchive := []byte(metaArchivePrefix + jobID)

	//扫描出影响的文件列表,如果归档文件包含最新写的wal文件，则返回错误
	filenameArr, err := m.getFileByHeightRange(startHeight, endHeight)
	if err != nil {
		return jobID, err
	}
	archiveJob = storePb.ArchiveJob{
		StartTime:   time.Now().Unix(),
		Status:      archiveStatusNotStart,
		StartHeight: startHeight,
		EndHeight:   endHeight,
		Files:       filenameArr,
	}
	marshalData, err := archiveJob.Marshal()
	if err != nil {
		return jobID, err
	}
	if err := m.metaStoreDB.Put(doArchive, marshalData); err != nil {
		return jobID, err
	}

	//3.push一个msg到channel
	m.archiveChan <- string(doArchive)

	//4.返回归档任务id
	return jobID, nil

}

// publishArchiveJob 重启时,重新推送所有未完成的归档任务，归档做到 中断自动恢复
//一般 只有1个归档任务在运行
// publishArchiveJob  publish the message of archive job  to archiveChan when the node restarted
//  @Description:
//  @receiver m
//  @return error
func (m *MetaDataDB) publishArchiveJob() error {
	m.logger.Infof("start publishArchiveJob")
	defer m.logger.Infof("finish publishArchiveJob")
	var archiveJob storePb.ArchiveJob

	//扫描所有归档任务
	doArchive := []byte(metaArchivePrefix)
	iter, err := m.metaStoreDB.NewIteratorWithPrefix(doArchive)
	if err != nil {
		return err
	}
	for iter.Next() {
		jobName := iter.Key()
		v := iter.Value()
		err := archiveJob.Unmarshal(v)
		if err != nil {
			return err
		}
		//归档任务未完成，则重新推送，进行归档
		if archiveJob.Status != archiveStatusFinish {
			m.logger.Infof("archive job is not finished")
			//todo: 这里如果待归档任务超过 chan 大小，启动时会阻塞，不能成功
			m.archiveChan <- string(jobName)
			m.logger.Infof("we will try again do archive [%s]", string(jobName))
		}
	}
	return nil
}

// updateArchiveJobStatus 修改归档任务的状态和结束时间
// updateArchiveJobStatus  update the status of archive job
//  @Description:
//  @receiver m
//  @param jobID
//  @param newStatus
//  @return error
func (m *MetaDataDB) updateArchiveJobStatus(jobID string, newStatus int32) error {
	var archiveJob storePb.ArchiveJob

	//读出归档任务
	archiveJob, err2 := m.getArchiveJobByID(jobID)
	if err2 != nil {
		return err2
	}

	//修改归档任务状态
	archiveJob.Status = newStatus

	//修改归档任务结束时间
	archiveJob.EndTime = time.Now().Unix()

	//回写db
	return m.setArchiveJobByID(jobID, archiveJob)

}

// GetArchiveJobByID 从db读一个归档任务
// GetArchiveJobByID  get archive job info by job ID
//  @Description:
//  @receiver m
//  @param jobID
//  @return archiveJob
//  @return error
func (m *MetaDataDB) GetArchiveJobByID(jobID string) (storePb.ArchiveJob, error) {
	return m.getArchiveJobByID(jobID)
}

// getArchiveJobByID 从db读一个归档任务
// getArchiveJobByID  get archive job info by job ID
//  @Description:
//  @receiver m
//  @param jobID
//  @return archiveJob
//  @return error
func (m *MetaDataDB) getArchiveJobByID(jobID string) (storePb.ArchiveJob, error) {
	var archiveJob storePb.ArchiveJob
	jobName := metaArchivePrefix + jobID

	//读出归档任务
	getData, err := m.metaStoreDB.Get([]byte(jobName))
	if err != nil {
		return archiveJob, err
	}
	if err := archiveJob.Unmarshal(getData); err != nil {
		return archiveJob, err
	}
	return archiveJob, nil
}

// setArchiveJobByID 写一个归档任务到db
// setArchiveJobByID  set archive job info by jobID
//  @Description:
//  @receiver m
//  @param jobID
//  @param archiveJob
//  @return error
func (m *MetaDataDB) setArchiveJobByID(jobID string, archiveJob storePb.ArchiveJob) error {
	//var archiveJob storePb.ArchiveJob
	jobName := metaArchivePrefix + jobID

	//回写db
	marshalData, err := archiveJob.Marshal()
	if err != nil {
		return err
	}
	err = m.metaStoreDB.Put([]byte(jobName), marshalData)
	if err != nil {
		return err
	}
	return nil
}

// getFileByHeightRange 获得区间块高，对应的文件
// getFileByHeightRange  get file name list by  start ,end height
//  @Description:
//  @receiver m
//  @param start
//  @param end
//  @return []string  files name
//  @return error
func (m *MetaDataDB) getFileByHeightRange(start, end uint64) ([]string, error) {
	m.RLock()
	defer m.RUnlock()

	var left string
	middle := map[string]bool{}

	for k, v := range m.bfdbFiles {
		if v.StartHeight <= start && v.EndHeight >= start {
			left = k
			middle[left] = true
		}
		if v.StartHeight <= end && v.EndHeight >= end {
			//right = k
			//要查找的区块，包含了最新的正在写入的wal文件，这个文件不能归档
			if v.EndHeight == maxBlockHeight {
				return nil, errFileIsInUse
			}
			//middle[right] = true
		}
		if v.StartHeight >= start && v.EndHeight <= end {
			middle[k] = true
		}
	}

	res := []string{}
	for k := range middle {
		res = append(res, k)
	}
	m.logger.Debugf("getFileByHeightRange:[%v],fileNumber:[%d],startHeight:[%d],endHeight:[%d]",
		res, len(res), start, end)
	return res, nil

}

// copyFile 对文件进行复制操作
//抽取校验码并保存到文件中
///todo:生成crc/hash校验码，校验码再形成校验链码
// copyFile  copy file from online file system to archive file system
//  @Description:
//  @receiver m
//  @param src
//  @param onlineFS
//  @param dst
//  @param archiveFS
//  @return error
func (m *MetaDataDB) copyFile(src string, onlineFS string, dst string, archiveFS string) error {
	//srcPath := onlineFS + "/" + src
	srcPath := m.absolutePath(src, onlineFS)
	//dstPath := archiveFS + "/" + dst
	dstPath := m.absolutePath(dst, archiveFS)

	var crcInfo CRC
	crcFile := dstPath + ".crc"

	sourceFileStat, err := os.Stat(srcPath)
	if err != nil {
		return err
	}

	if !sourceFileStat.Mode().IsRegular() {
		return fmt.Errorf("%s is not a regular file", srcPath)
	}

	source, err := os.Open(srcPath)
	if err != nil {
		return err
	}
	defer func() {
		if err = source.Close(); err != nil {
			m.logger.Errorf("close file error :[%s]", err)
		}
	}()

	//创建归档目录，如果存在在不创建
	dstDirPath := dstPath[0 : len(dstPath)-len(dst)]
	if err = os.MkdirAll(dstDirPath, 0777); err != nil {
		return err
	}
	//如果文件存在，会被truncated，所以断点还能继续
	destination, err := os.Create(dstPath)

	defer func() {
		if err = destination.Close(); err != nil {
			m.logger.Errorf("close file error :[%s]", err)
		}
	}()

	if err != nil {
		return err
	}

	//读、写文件
	buf := make([]byte, BUFFERSIZE)

	for {
		n, err := source.Read(buf)
		if err != nil && err != io.EOF {
			return err
		}
		if n == 0 {
			break
		}

		crcInfo = crcInfo.Update(buf[:n])
		writeN, err := destination.Write(buf[:n])
		if err != nil {
			return err
		}
		if writeN != n {
			return errWriteFile
		}
	}
	if err := destination.Sync(); err != nil {
		return err
	}
	//写crc校验到文件中
	return NewCrcFile(crcInfo, crcFile)
}

// removeFile 对文件进行删除操作
///todo:应该对一些特定目录做保护，防止误删除
// removeFile  remove file by file name and online file system
//  @Description:
//  @receiver m
//  @param fileName
//  @param onlineFS
//  @return error
func (m *MetaDataDB) removeFile(fileName string, onlineFS string) error {
	//filePath := onlineFS + "/" + fileName
	filePath := m.absolutePath(fileName, onlineFS)
	sourceFileStat, err := os.Stat(filePath)

	//如果文件不存在，说明已经删除
	if os.IsNotExist(err) {
		return nil
	}
	//其它错误，返回
	if err != nil {
		return err
	}
	if !sourceFileStat.Mode().IsRegular() {
		return fmt.Errorf("%s is not a regular file", filePath)
	}
	err = os.Remove(filePath)
	if err != nil {
		return err
	}
	return nil
}

// FSBalance 按照磁盘空间均衡策略，返回最优文件系统
//返回值: 剩余空间最大的文件系统，可用空间，错误信息
// FSBalance  get a file system with the largest amount of free space
//  @Description:
//  @receiver m
//  @param fileSystemGroup
//  @return string   file system which free space capacity is largest
//  @return uint64   free space capacity
//  @return error
func (m *MetaDataDB) FSBalance(fileSystemGroup []string) (string, uint64, error) {
	var maxFree uint64
	var bestFS string

	for i := 0; i < len(fileSystemGroup); i++ {
		info, err := disk.Usage(fileSystemGroup[i])
		if err != nil {
			return "", 0, err
		}
		if info.Free > maxFree {
			maxFree = info.Free
			bestFS = fileSystemGroup[i]
		}
	}
	return bestFS, maxFree, nil
}

// consumeArchiveJob 消费归档任务，做归档操作
//一般 只有1个归档任务在运行
// consumeArchiveJob  consume a message of archive job, then execute archive job
//  @Description:
//  @receiver m
//归档过程中，耗时最长的是复制数据和删除数据
func (m *MetaDataDB) consumeArchiveJob() {
	defer func() {
		if err := recover(); err != nil {
			m.logger.Errorf("consumeArchiveJob err, err:[%s]", err)
		}
	}()
	var archiveJob storePb.ArchiveJob

	for {
		//1.消费,获得一个归档任务
		jobName, ok := <-m.archiveChan
		if !ok {
			m.logger.Infof("archive chan closed")
		}
		m.logger.Debugf("consume a archive job name:[%s]", jobName)

		//2.查询归档任务
		jobID := jobName[len(metaArchivePrefix):]
		getValue, err := m.metaStoreDB.Get([]byte(jobName))
		if err != nil {
			m.logger.Errorf("get data from metastoredb error :[%s]", err)
			panic(err)
		}

		if err := archiveJob.Unmarshal(getValue); err != nil {
			panic(err)
		}
		m.logger.Debugf("get a archive job name:[%s]", jobName)
		m.logger.Debugf("archive job unmarshal:[%v]", archiveJob)

		//3.放到自动机，自动完成归档
		if err := m.archiveAutomata(jobID, archiveJob); err != nil {
			m.logger.Errorf("archive automata error :[%s]", err)
			panic(err)
			//return err
		}
		m.logger.Debugf("finished a archive job name:[%s]", jobName)

	}

}

// archiveAutomata 归档状态机，根据归档任务名字和归档任务要求，进行自动归档
// archiveAutomata  execute archive job,it is a automata
//  @Description:
//  @receiver m
//  @param jobID
//  @param archiveJob
//  @return error
func (m *MetaDataDB) archiveAutomata(jobID string, archiveJob storePb.ArchiveJob) error {
	status := archiveJob.Status
	for {
		switch status {
		// 未开始
		case archiveStatusNotStart:
			{
				if err := m.updateArchiveJobStatus(jobID, archiveStatusBegin); err != nil {
					return err
				}
				status = archiveStatusBegin
				m.logger.Debugf("do archiveStatusNotStart")
				continue
			}
		case archiveStatusBegin:
			{

				if err := m.updateArchiveJobStatus(jobID, archiveStatusCopyStart); err != nil {
					return err
				}
				status = archiveStatusCopyStart
				m.logger.Debugf("do archiveStatusBegin")
				continue
			}
		case archiveStatusCopyStart:
			{
				for i := 0; i < len(archiveJob.Files); i++ {
					m.logger.Debugf("archiveAutomata archiveJob:[%v]", archiveJob)
					m.RLock()
					fileName := archiveJob.Files[i]
					fileStatus := m.bfdbFiles[fileName].Status
					m.RUnlock()
					m.logger.Debugf("archiveAutomata archiveStatusCopyStart,fileName:[%s],fileStatus:[%d]",
						fileName, fileStatus)
					if err := m.fileArchiveAutomata(int(fileStatus), fileName); err != nil {
						m.logger.Debugf("archiveAutomata archiveStatusCopyStart error:[%s]", err)
						return err
					}

				}

				if err := m.updateArchiveJobStatus(jobID, archiveStatusCopyEnd); err != nil {
					return err
				}
				status = archiveStatusCopyEnd
				m.logger.Debugf("do archiveStatusCopyStart")
				continue

			}
		case archiveStatusCopyEnd:
			{
				status = archiveStatusDelStart
				m.logger.Debugf("do archiveStatusCopyEnd")

				continue

			}
		case archiveStatusDelStart:
			{
				for i := 0; i < len(archiveJob.Files); i++ {
					var fileInfo storePb.MetaFileInfo
					fileName := archiveJob.Files[i]

					//保证内存中文件存在
					m.RLock()
					fileInfo, exist := m.bfdbFiles[fileName]
					m.RUnlock()
					if !exist {
						m.logger.Errorf("file not exist,fileName:[%s]", fileName)
						panic(errFileNotExist)
						//return errFileNotExist
					}

					status := int(fileInfo.Status)
					//删除文件
					if err := m.fileArchiveAutomata(status, fileName); err != nil {
						return err
					}
				}
				if err := m.updateArchiveJobStatus(jobID, archiveStatusDelEnd); err != nil {
					return err
				}

				status = archiveStatusDelEnd
				m.logger.Debugf("do archiveStatusDelStart")
				continue

			}
		case archiveStatusDelEnd:
			{
				if err := m.updateArchiveJobStatus(jobID, archiveStatusFinish); err != nil {
					return err
				}
				status = archiveStatusFinish
				m.logger.Debugf("do archiveStatusDelEnd")
				continue

			}
		case archiveStatusFinish:
			{
				m.logger.Debugf("do archiveStatusFinish")
				return nil
			}
		//todo: switch进入默认的，应该报错，说明没有进入预定的状态机，状态失控了
		default:
			{
				m.logger.Errorf("archive status err,archiveStatus:[%d]", status)
				return errArchiveStatusErr
				//return nil
			}
		}
	}
}

// calcArchiveFSValidity 计算文件做归档时的归档文件系统，验证剩余空间是否够用
// calcArchiveFSValidity  check file system ,make sure the free space is available
//  @Description:
//  @receiver m
//  @param fileName
//  @param fileStatus
//  @param onlineFS
//  @param availableFS
//  @return file status
//  @return onlineFS
//  @return availableFS
//  @return error
func (m *MetaDataDB) calcArchiveFSValidity(fileName string, fileStatus int, onlineFS, availableFS string) (int,
	string, string, error) {
	var fileInfo storePb.MetaFileInfo

	//1.获得文件对应文件系统,保证内存中文件存在
	m.RLock()
	fileInfo, exist := m.bfdbFiles[fileName]
	m.RUnlock()
	if !exist {
		m.logger.Errorf("file not exist,onlineFS:[%s],fileName:[%s]", onlineFS, fileName)
		panic(errFileNotExist)
	}

	onlineFS = fileInfo.OnlineFileSystem

	//2.计算可用归档文件系统及可用空间大小
	fileSystemGroup := m.metaFSG.ArchiveFileSystem
	fs, freeSize, err := m.FSBalance(fileSystemGroup)
	if err != nil {
		return fileStatus, onlineFS, availableFS, err
	}
	availableFS = fs

	//3.更新元数据中 归档文件系统的信息,如果断点,重启后，可以继续用 计算好的文件系统
	if err = m.setFileArchiveFS(fileName, availableFS); err != nil {
		return fileStatus, onlineFS, availableFS, err
	}

	//4.计算文件大小,确定归档系统的空间是否够用,不够用，则报错
	//filePath := onlineFS + "/" + fileName
	filePath := m.absolutePath(fileName, onlineFS)
	size, err := GetFileSize(filePath)
	if err != nil {
		return fileStatus, onlineFS, availableFS, err
	}
	if uint64(size) > freeSize {
		m.logger.Errorf("space not enough,archiveFS:[%s],fileName:[%s]", availableFS, fileName)
		panic(errArchiveFSSpaceNotEnough)
	}

	//5.更新文件状态，到下一个状态
	if err = m.setFileStatus(fileName, fileStatusCopyStart); err != nil {
		return fileStatus, onlineFS, availableFS, err
	}
	fileStatus = fileStatusCopyStart
	m.logger.Debugf("fileArchive fileStatusBegin")
	return fileStatus, onlineFS, availableFS, err
}

// fileArchiveAutomata 文件归档自动机，完成对一个文件，进行自动归档，包括 断点 继续运行
// fileArchiveAutomata  do file archive ,it is a automata
//  @Description:
//  @receiver m
//  @param fileStatus
//  @param fileName
//  @return error
func (m *MetaDataDB) fileArchiveAutomata(fileStatus int, fileName string) error {
	var onlineFS, availableFS string
	fileInfo, err := m.GetFileInfoByFileName(fileName)
	if err != nil {
		return err
	}
	availableFS = fileInfo.ArchiveFileSystem
	onlineFS = fileInfo.OnlineFileSystem

	m.logger.Debugf("fileArchiveAutoMeta status:[%d],fileName:[%s]", fileStatus, fileName)
	for {
		switch fileStatus {
		case fileStatusNotArchived:
			{
				if err := m.setFileStatus(fileName, fileStatusBegin); err != nil {
					return err
				}
				fileStatus = fileStatusBegin
				m.logger.Debugf("fileArchive fileStatusNotArchived")

				continue
			}

		case fileStatusBegin:
			{
				//计算文件做归档时的归档文件系统，验证剩余空间是否够用
				_, of, af, err := m.calcArchiveFSValidity(fileName, fileStatus, onlineFS, availableFS)
				if err != nil {
					return err
				}
				//fileStatus = fs
				onlineFS = of
				availableFS = af
				fileStatus = fileStatusCopyStart

				m.logger.Debugf("fileArchive fileStatusBegin")
				continue
			}
		case fileStatusCopyStart:
			{

				m.logger.Debugf("fileArchiveAutomata fileStatusCopyStart,"+
					"srcfileName:[%s],onlineFS:[%s],dstfileName:[%s],availableFS:[%s]", fileName,
					onlineFS, fileName, availableFS)
				if err := m.copyFile(fileName, onlineFS, fileName, availableFS); err != nil {
					return err
				}
				if err := m.setFileStatus(fileName, fileStatusCopyEnd); err != nil {
					return err
				}
				fileStatus = fileStatusCopyEnd
				m.logger.Debugf("fileArchiveAutomata fileStatusCopyEnd,fileName:[%s]", fileName)

				m.logger.Debugf("fileArchive fileStatusCopyStart")

			}
		//文件拷贝结束后，状态机先结束，先不进入删除流程
		case fileStatusCopyEnd:
			{
				if err := m.setFileStatus(fileName, fileStatusDelStart); err != nil {
					return err
				}
				//fileStatus = fileStatusDelStart
				m.logger.Debugf("fileArchive fileStatusCopyEnd")
				return nil

			}
		case fileStatusDelStart:
			{
				var fileInfo storePb.MetaFileInfo

				//保证内存中文件存在
				m.RLock()
				fileInfo, exist := m.bfdbFiles[fileName]
				m.RUnlock()
				if !exist {
					m.logger.Errorf("file not exist,fileName:[%s]", fileName)
					panic(errFileNotExist)
				}
				onlineFS = fileInfo.OnlineFileSystem

				//删除文件
				//todo: 要支持 重复删除，比如有的文件已经删除了，但是状态没更新，断电了
				//重启之后，又进入删除逻辑，这时会删除失败
				//任何删除失败，可以不返回错误，只记录一个info信息
				if err := m.removeFile(fileName, onlineFS); err != nil {
					m.logger.Infof("remove file error, err:[%s]", err)
				}
				if err := m.setFileStatus(fileName, fileStatusDelEnd); err != nil {
					return err
				}
				fileStatus = fileStatusDelEnd

				m.logger.Debugf("fileArchive fileStatusDelStart")
				continue

			}
		case fileStatusDelEnd:
			{
				if err := m.setFileStatus(fileName, fileStatusEnd); err != nil {
					return err
				}
				fileStatus = fileStatusEnd
				m.logger.Debugf("fileArchive fileStatusDelEnd")
				continue

			}
		case fileStatusEnd:
			{
				m.logger.Debugf("fileArchive fileStatusEnd")
				return nil
			}
		//todo: switch进入默认的，应该报错，说明没有进入预定的状态机，状态失控了
		default:
			{
				m.logger.Errorf("file status err,fileStatus:[%d]", fileStatus)
				return errFileStatusErr
			}
		}
	}

}

// absolutePath 获得绝对路径，根据文件系统和文件名
//绝对路径: $filesystem/$chainID/bfdb/$filename.fdb
// absolutePath  get absolute path,include file system, separate char, chainID,  const var ,and file name
//  @Description:
//  @receiver m
//  @param fileName
//  @param fileSystem
//  @return string
func (m *MetaDataDB) absolutePath(fileName string, fileSystem string) string {
	abs := fileSystem + "/" + m.chainID + "/" + "bfdb/" + fileName + ".fdb"
	return abs
}

// AbsolutePath 获得绝对路径，根据文件系统和文件名
//绝对路径: $filesystem/$chainID/bfdb/$filename.fdb
// AbsolutePath  get absolute path,include file system, separate char, chainID,  const var ,and file name
//  @Description:
//  @receiver m
//  @param fileName
//  @param fileSystem
//  @return string
func (m *MetaDataDB) AbsolutePath(fileName string, fileSystem string) string {
	return m.absolutePath(fileName, fileSystem)
}

// GetFileSize 获得文件大小
// GetFileSize  get file size
//  @Description:
//  @param filePath
//  @return file size
//  @return error
func GetFileSize(filePath string) (int64, error) {
	file, err := os.Open(filePath)
	defer func() {
		if err = file.Close(); err != nil {
			fmt.Printf("close file error when get file size, errinfo:[%s]", err)
		}
	}()

	if err != nil {
		return 0, err
	}
	stat, err := file.Stat()
	if err != nil {
		return 0, err
	}
	return stat.Size(), nil
}

// CheckDirIfNotExist 检查目录是否存在
// CheckDirIfNotExist  check the directory is exist or not
//  @Description:
//  @param path
//  @return error
func CheckDirIfNotExist(path string) error {
	_, err := os.Stat(path)
	if err == nil {
		return nil
	}
	if os.IsNotExist(err) {
		return err
	}
	return nil
}

// CheckRepeated 验证arr中是否有重复元素
// CheckRepeated  check input elements of array is repeated or not
//  @Description:
//  @param arr []string
//  @return bool
func CheckRepeated(arr []string) bool {
	if len(arr) == 0 {
		return false
	}
	m := make(map[string]bool)
	for i := 0; i < len(arr); i++ {
		if _, exist := m[arr[i]]; exist {
			return true
		}
		m[arr[i]] = true
	}
	return false
}

// CheckDifferent A和B进行比较，A的元素个数小于或等于B
//返回 (A+B)-(A与B交集) 的元素个数
//返回 B-A 的差集，B中有，A中没有的元素
// A=["a","b","c"],B=["b",d"] ,返回 (3,["d"])
// CheckDifferent  find the difference between arrA and arrB
// Returns the number of elements in the union of A and B minus the number of elements in the intersection of A and B
//  @Description:
//  @param arrA []string
//  @param arrB []string
//  @return the count(A union B) - count(A intersect B)
//  @return the elements of arrB minus arrA, that the elements is exist in arrB but not exist in arrA
//  example: A=["a","b","c"],B=["b",d"] , return (3,["d"])
func CheckDifferent(arrA, arrB []string) (int, []string) {
	a := make(map[string]bool)
	b := make(map[string]bool)
	//交集
	ab := make(map[string]bool)
	//合集
	aUnionb := make(map[string]bool)

	for i := 0; i < len(arrA); i++ {
		a[arrA[i]] = true
		aUnionb[arrA[i]] = true
	}
	for j := 0; j < len(arrB); j++ {
		b[arrB[j]] = true
		aUnionb[arrB[j]] = true
		//交集
		if _, exist := a[arrB[j]]; exist {
			ab[arrB[j]] = true
		}
	}
	diffCount := len(aUnionb) - len(ab)

	diffArr := []string{}

	for j := 0; j < len(arrB); j++ {
		//得到B中存在，A中不存在的元素
		if _, exist := a[arrB[j]]; !exist {
			diffArr = append(diffArr, arrB[j])
		}
	}
	return diffCount, diffArr

}

// checkConfigDiff 对比配置文件中的配置和dbMetaFS是否一致，一致说明配置文件没更新，不一致，则需要更新元数据
// 返回值: OnlineFS增加的新文件系统个数,ArchiveFS增加的新文件系统个数,错误码
// 比如 OnlineFS=["a","b","c"],dbOnlineFS=["a","b","c","d"]
//     ArchiveFS=["e"],dbOnlineFS=["e"]
// 则返回  (1,0,nil)
// checkConfigDiff  find the difference between metaFS and dbMetaFS
//  @Description:
//  @param metaFS
//  @param dbMetaFS
//  @return int  number of  online file system added
//  @return int  number of  archive file system added
//  @return error
func (m *MetaDataDB) checkConfigDiff(metaFS, dbMetaFS *storePb.MetaFileSystem) (int, int, error) {
	if len(metaFS.ArchiveFileSystem) < len(dbMetaFS.ArchiveFileSystem) ||
		len(metaFS.OnlineFileSystem) < len(dbMetaFS.OnlineFileSystem) {
		//配置的文件系统比原来的少，出错了
		return 0, 0, errConfigFileSystem
	}

	//验证是否修改了旧配置,包括 删除元素，修改元素
	diffCountOnline, diffArrOnline := CheckDifferent(dbMetaFS.OnlineFileSystem, metaFS.OnlineFileSystem)
	if len(diffArrOnline) != diffCountOnline {
		//配置文件中原有的元素被 删除或者修改了
		m.logger.Debugf("OnlineFS config error")
		return 0, 0, errConfigOnlineModify
	}
	//验证是否修改了旧配置,包括 删除元素，修改元素
	diffCountArchive, diffArrArchive := CheckDifferent(dbMetaFS.ArchiveFileSystem, metaFS.ArchiveFileSystem)
	if len(diffArrArchive) != diffCountArchive {
		m.logger.Debugf("ArchiveFS config error")
		return 0, 0, errConfigArchiveModify
	}
	return diffCountOnline, diffCountArchive, nil
}

// GenerateUniqueID  get unique id
//  @Description:
//  @return string
func GenerateUniqueID() string {
	id := uuid.New().String() + "-" + time.Now().String()
	id = strings.ReplaceAll(id, " ", "-")
	//id = 411d9d21-4e36-41db-91f7-f3938a837bd6-2022-12-14-11:05:53
	return id
}

// newCrc 创建一个crc对象
// newCrc  create a crc object
//  @Description:
//  @param b
//  @return CRC
func newCrc(b []byte) CRC {
	return NewCRC(b)
}

// NewCrcFile 创建一个保存crc的文件
///todo:处理 文件已存在的情况下，create file error
// NewCrcFile  create a crc file
//  @Description:
//  @param crc
//  @param file name
//  @return error
func NewCrcFile(c CRC, fileName string) error {
	var crcFile storePb.FileCrcInfo
	//如果文件存在，则truncated
	file, err := os.Create(fileName)
	defer func() {
		if err = file.Close(); err != nil {
			fmt.Printf("close file error when newCrcFile errinfo:[%s]", err)
		}
	}()
	if err != nil {
		return err
	}
	//序列化 校验码
	checksum := c.Value()
	crcFile.CrcData = int64(checksum)
	marshalData, err := crcFile.Marshal()
	if err != nil {
		return err
	}

	//校验码写到文件中
	count := len(marshalData)
	idx := 0
	for idx < count {
		n, err := file.Write(marshalData[idx:])
		if err != nil {
			return err
		}
		idx = idx + n
	}
	return file.Sync()
}

//验证归档的文件和校验码是否一致
//func checkCrcByFileName(c bufferCRC.CRC, fileName string) error {
//	return nil
//}
