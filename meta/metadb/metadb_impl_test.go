package metadb

import (
	"bytes"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"reflect"
	"sort"
	"sync"
	"testing"
	"time"

	"chainmaker.org/chainmaker/common/v3/crypto"
	storePb "chainmaker.org/chainmaker/pb-go/v3/store"
	"chainmaker.org/chainmaker/protocol/v3"
	"chainmaker.org/chainmaker/protocol/v3/test"
	leveldbprovider "chainmaker.org/chainmaker/store-leveldb/v3"

	//"chainmaker.org/chainmaker/store-huge/v3/blockdb/blockfiledb"
	"chainmaker.org/chainmaker/store-huge/v3/conf"
	"chainmaker.org/chainmaker/store-huge/v3/meta"
	"github.com/mitchellh/mapstructure"
)

const (
	tmpMetadbArchive = "/tmp/metadb/archive1,/tmp/metadb/archive2"
	tmpMetadbOnline  = "/tmp/metadb/online1,/tmp/metadb/online2"
	tmpMetadbOnline1 = "/tmp/metadb/online1"
	newTmpFile       = "new_100M_file"
)

var (
	globalConfig      = getSqliteConfig()
	globalTestChainID = "test-global-test-chain-id"
	testChainID       = "test-chain-id"
	testDBHandler     = getDBHandle(globalTestChainID, "test-metadb", "metadb", &test.GoLogger{}, nil)
	//metadbTest, _     = NewMetaDataDB(*globalConfig, globalTestChainID, testDBHandler, &test.GoLogger{})
	testLogger = &test.GoLogger{}

	metaFSGTest = &storePb.MetaFileSystem{
		OnlineFileSystem:  []string{"/tmp/online1,/tmp/online2"},
		ArchiveFileSystem: []string{"/tmp/archive1,/tmp/archive2"},
	}
	bfdbFilesTest = map[string]storePb.MetaFileInfo{
		"000000000000000001001": {
			OnlineFileSystem:  "/tmp/online1",
			Status:            0,
			ArchiveFileSystem: "/tmp/archive1",
			StartHeight:       1001,
			EndHeight:         1100,
		},
		"00000000000000000001101": {
			OnlineFileSystem:  "/tmp/online1",
			Status:            0,
			ArchiveFileSystem: "/tmp/archive1",
			StartHeight:       1101,
			EndHeight:         1200,
		},
	}
	multVersionsTest = &storePb.MetaMultVersions{
		Version: []*storePb.MetaVersion{
			{
				MajorVersion: 1,
				MinorVersion: 1,
				UpdateTime:   0,
			},
			//&storePb.MetaVersion{
			//	MajorVersion:2,
			//	MinorVersion: 1,
			//	UpdateTime: 0,
			//},
		},
	}
)

func getDBHandle(chainId, providerName, dbFolder string,
	logger protocol.Logger, encryptor crypto.SymmetricKey) protocol.DBHandle {
	//使用全局配置
	inputDBConfig := globalConfig.BlockDbConfig
	configMap := inputDBConfig.LevelDbConfig

	dbConfig := &leveldbprovider.LevelDbConfig{}

	err := mapstructure.Decode(configMap, dbConfig)
	if err != nil {
		return nil
	}
	input := &leveldbprovider.NewLevelDBOptions{
		Config:    dbConfig,
		Logger:    logger,
		Encryptor: encryptor,
		ChainId:   chainId,
		DbFolder:  dbFolder,
	}
	return leveldbprovider.NewLevelDBHandle(input)
}
func getSqliteConfig() *conf.StorageConfig {
	conf1, _ := conf.NewStorageConfig(nil)
	conf1.StorePath = filepath.Join(os.TempDir(), fmt.Sprintf("%d", time.Now().Nanosecond()))
	conf1.WriteBlockType = 0
	var sqlconfig = make(map[string]interface{})
	sqlconfig["sqldb_type"] = "sqlite"
	sqlconfig["dsn"] = ":memory:"

	dbConfig := &conf.DbConfig{
		Provider:    "sql",
		SqlDbConfig: sqlconfig,
	}
	statedbConfig := &conf.DbConfig{
		Provider:    "sql",
		SqlDbConfig: sqlconfig,
	}

	lvlConfig := make(map[string]interface{})
	lvlConfig["store_path"] = filepath.Join(os.TempDir(), fmt.Sprintf("%d", time.Now().Nanosecond()))

	txExistDbConfig := &conf.DbConfig{
		Provider:      "leveldb",
		LevelDbConfig: lvlConfig,
	}
	conf1.DisableBlockFileDb = true
	conf1.BlockDbConfig = dbConfig
	conf1.StateDbConfig = statedbConfig
	conf1.HistoryDbConfig = conf.NewHistoryDbConfig(dbConfig)
	conf1.ResultDbConfig = dbConfig
	conf1.ContractEventDbConfig = dbConfig
	conf1.DisableContractEventDB = true
	conf1.TxExistDbConfig = txExistDbConfig
	conf1.LogDBSegmentSize = 20
	//conf1.DisableBigFilter = true
	conf1.RollingWindowCacheCapacity = 1000000
	conf1.BlockFileConfig = &conf.BlockFileConfig{
		OnlineFileSystem:  "/tmp/online1,/tmp/online2",
		ArchiveFileSystem: "/tmp/archive1,/tmp/archive2",
	}
	conf1.ConfigVersion = &conf.StorageConfigVersion{
		Major: 1,
		Minor: 1,
	}
	return conf1
}

func resetMetaDBData(db protocol.DBHandle) {
	iter, _ := db.NewIteratorWithPrefix([]byte("m"))
	for iter.Next() {
		k := iter.Key()
		db.Delete(k)
	}

}
func TestCheckDifferent(t *testing.T) {
	type args struct {
		A []string
		B []string
	}
	tests := []struct {
		name  string
		args  args
		want  int
		want1 []string
	}{
		{
			name: "check-diff1",
			args: args{
				A: []string{"a", "b"},
				B: []string{"a", "b"},
			},
			want:  0,
			want1: []string{},
		},
		{
			name: "check-diff2",
			args: args{
				A: []string{"a", "b", "c"},
				B: []string{"b", "d"},
			},
			want:  3,
			want1: []string{"d"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1 := CheckDifferent(tt.args.A, tt.args.B)
			if got != tt.want {
				t.Errorf("CheckDifferent() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("CheckDifferent() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func TestCheckDirIfNotExist(t *testing.T) {
	type args struct {
		path string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "check-dir-if-not-exist1",
			args: args{
				path: "/tmp",
			},
			wantErr: false,
		},
		{
			name: "check-dir-if-not-exist2",
			args: args{
				path: "/tmp_not_exist",
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := CheckDirIfNotExist(tt.args.path); (err != nil) != tt.wantErr {
				t.Errorf("CheckDirIfNotExist() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestCheckRepeated(t *testing.T) {
	type args struct {
		arr []string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "check-repeated1",
			args: args{
				arr: []string{"a", "b"},
			},
			want: false,
		},
		{
			name: "check-repeated2",
			args: args{
				arr: []string{"a", "a"},
			},
			want: true,
		},
		{
			name: "check-repeated3",
			args: args{
				arr: []string{},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := CheckRepeated(tt.args.arr); got != tt.want {
				t.Errorf("CheckRepeated() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGenerateUniqueID(t *testing.T) {
	tests := []struct {
		name string
		want bool
	}{
		{
			name: "generate-unique-id",
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got1 := GenerateUniqueID()
			got2 := GenerateUniqueID()
			//连续调用2次，生成的id 不相等
			if (got1 == got2) != tt.want {
				t.Errorf("GenerateUniqueID() = %v, want %v", got1 == got2, tt.want)
			}
		})
	}
}

func TestGetFileSize(t *testing.T) {
	initFileSystem("metadb", "archive1")
	initFileSystem("metadb", "archive2")

	defer umountFileSystem("metadb", "archive1")
	defer umountFileSystem("metadb", "archive2")

	defer destroyFileSystem("metadb")

	//2.对 /tmp/metadb/archive2 写入一些数据，以便 让 archive1 可用空间大于 archive2
	cmd := exec.Command("/usr/bin/dd", "if=/dev/zero", "of=/tmp/metadb/archive2/new_100M_file", "count=1024", "bs=10")
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()
	if err != nil {
		fmt.Println("start create new_100M_file error :", err)
	}
	//outStr, errStr := string(stdout.Bytes()), string(stderr.Bytes())
	outStr, errStr := stdout.String(), stderr.String()
	fmt.Printf("out:%s\n", outStr)
	if errStr != "" {
		fmt.Printf("err:%s\n", errStr)
	}

	type args struct {
		filePath string
	}
	tests := []struct {
		name    string
		args    args
		want    int64
		wantErr bool
	}{
		{
			name: "get-file-size",
			args: args{
				filePath: "/tmp/metadb/archive2/new_100M_file",
			},
			want:    10240,
			wantErr: false,
		},
		{
			name: "get-file-size-file-not-exist",
			args: args{
				filePath: "/tmp/aw2.txt",
			},
			want:    0,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := GetFileSize(tt.args.filePath)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetFileSize() error = %v, wantErr %v, filePath %v", err, tt.wantErr, tt.args.filePath)
				return
			}
			if got != tt.want {
				t.Errorf("GetFileSize() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMetaDataDB_CreateNewFile(t *testing.T) {
	type fields struct {
		metaFSG      *storePb.MetaFileSystem
		bfdbFiles    map[string]storePb.MetaFileInfo
		multVersions *storePb.MetaMultVersions
		chainID      string
		metaStoreDB  protocol.DBHandle
		logger       protocol.Logger
		archiveChan  chan string
	}
	type args struct {
		fileName    string
		fileSystem  string
		startHeight uint64
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "create-new-file1",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  testDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1),
			},
			args: args{
				fileName:    "0000001",
				fileSystem:  "/tmp/online1",
				startHeight: 1,
			},
			wantErr: false,
		},
		{
			name: "create-new-file1-file-exist",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  testDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1),
			},
			args: args{
				fileName:    "0000001",
				fileSystem:  "/tmp/online1",
				startHeight: 1,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MetaDataDB{
				metaFSG:      tt.fields.metaFSG,
				bfdbFiles:    tt.fields.bfdbFiles,
				multVersions: tt.fields.multVersions,
				chainID:      tt.fields.chainID,
				metaStoreDB:  tt.fields.metaStoreDB,
				logger:       tt.fields.logger,
				archiveChan:  tt.fields.archiveChan,
				RWMutex:      sync.RWMutex{},
			}
			if err := m.CreateNewFile(tt.args.fileName, tt.args.fileSystem, tt.args.startHeight); (err != nil) != tt.wantErr {
				t.Errorf("CreateNewFile() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestMetaDataDB_DoArchive(t *testing.T) {
	initFileSystem("metadb", "online1")
	initFileSystem("metadb", "online2")
	initFileSystem("metadb", "archive1")
	initFileSystem("metadb", "archive2")
	defer umountFileSystem("metadb", "online1")
	defer umountFileSystem("metadb", "online2")
	defer umountFileSystem("metadb", "archive1")
	defer umountFileSystem("metadb", "archive2")
	defer destroyFileSystem("metadb")

	dbConfigTest := &leveldbprovider.LevelDbConfig{
		StorePath:       "test-metaDb",
		WriteBufferSize: 2,
	}
	levelConfig := &leveldbprovider.NewLevelDBOptions{
		ChainId:  "chain1",
		DbFolder: "test-DoArchive",
		Config:   dbConfigTest,
		Logger:   &test.GoLogger{},
	}
	db := leveldbprovider.NewLevelDBHandle(levelConfig)
	defer db.Close()
	defer resetMetaDBData(db)

	//2.修改配置文件中，文件系统的配置,使用测试配置文件
	testConfig := *getSqliteConfig()
	testConfig.BlockFileConfig.ArchiveFileSystem = tmpMetadbArchive
	testConfig.BlockFileConfig.OnlineFileSystem = tmpMetadbOnline

	//3.创建并启动元数据
	dataDb, err := NewMetaDataDB(testConfig, testChainID, db, &test.GoLogger{})
	if err != nil {
		fmt.Println("metadb.NewMetaDataDb,error =", err)
		return
	}

	//4.模拟bfdb,创建元数据
	fileName1 := "00000000000000000001"
	fileName2 := "00000000000000000101"
	CreateArchiveCase(dataDb, fileName1,
		tmpMetadbOnline1, 1, 100, archiveStatusNotStart)
	CreateArchiveCaseNoArchive(dataDb, fileName2,
		tmpMetadbOnline1, 101, 200, archiveStatusNotStart)

	type fields struct {
		metaFSG      *storePb.MetaFileSystem
		bfdbFiles    map[string]storePb.MetaFileInfo
		multVersions *storePb.MetaMultVersions
		chainID      string
		metaStoreDB  protocol.DBHandle
		logger       protocol.Logger
		archiveChan  chan string
	}
	type args struct {
		startHeight uint64
		endHeight   uint64
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "do-archive",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  testDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1),
			},
			args: args{
				startHeight: 2,
				endHeight:   30,
			},
			want:    "",
			wantErr: false,
		},
		{
			name: "do-archive-errFileIsInUse",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  testDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1),
			},
			args: args{
				startHeight: 105,
				endHeight:   200,
			},
			want:    "",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MetaDataDB{
				metaFSG:      tt.fields.metaFSG,
				bfdbFiles:    tt.fields.bfdbFiles,
				multVersions: tt.fields.multVersions,
				chainID:      tt.fields.chainID,
				metaStoreDB:  tt.fields.metaStoreDB,
				logger:       tt.fields.logger,
				archiveChan:  tt.fields.archiveChan,
				RWMutex:      sync.RWMutex{},
			}
			m = dataDb.(*MetaDataDB)
			_, err := m.DoArchive(tt.args.startHeight, tt.args.endHeight)
			if (err != nil) != tt.wantErr {
				t.Errorf("DoArchive() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			//if got != tt.want {
			//	t.Errorf("DoArchive() got = %v, want %v", got, tt.want)
			//}
		})
	}
}

func TestMetaDataDB_FSBalance(t *testing.T) {
	//1.创建文件系统，大小为1G
	//tmp/metadb/archive1  /tmp/metadb/archive2

	initFileSystem("metadb", "archive1")
	initFileSystem("metadb", "archive2")

	defer umountFileSystem("metadb", "archive1")
	defer umountFileSystem("metadb", "archive2")

	defer destroyFileSystem("metadb")

	//2.对 /tmp/metadb/archive2 写入一些数据，以便 让 archive1 可用空间大于 archive2
	cmd := exec.Command("/usr/bin/dd", "if=/dev/zero", "of=/tmp/metadb/archive2/new_100M_file", "count=102400", "bs=1024")
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()
	if err != nil {
		fmt.Println("start create new_100M_file error :", err)
	}
	//outStr, errStr := string(stdout.Bytes()), string(stderr.Bytes())
	outStr, errStr := stdout.String(), stderr.String()
	fmt.Printf("out:%s\n", outStr)
	if errStr != "" {
		fmt.Printf("err:%s\n", errStr)
	}

	type fields struct {
		metaFSG      *storePb.MetaFileSystem
		bfdbFiles    map[string]storePb.MetaFileInfo
		multVersions *storePb.MetaMultVersions
		chainID      string
		metaStoreDB  protocol.DBHandle
		logger       protocol.Logger
		archiveChan  chan string
	}
	type args struct {
		fileSystemGroup []string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		want1   uint64
		wantErr bool
	}{
		{
			name: "fs-balance-test",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  testDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1),
			},
			args: args{
				fileSystemGroup: []string{"/tmp/metadb/archive1", "/tmp/metadb/archive2"},
			},
			want:    "/tmp/metadb/archive1",
			want1:   19976359936,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MetaDataDB{
				metaFSG:      tt.fields.metaFSG,
				bfdbFiles:    tt.fields.bfdbFiles,
				multVersions: tt.fields.multVersions,
				chainID:      tt.fields.chainID,
				metaStoreDB:  tt.fields.metaStoreDB,
				logger:       tt.fields.logger,
				archiveChan:  tt.fields.archiveChan,
				RWMutex:      sync.RWMutex{},
			}
			got, _, err := m.FSBalance(tt.args.fileSystemGroup)
			if (err != nil) != tt.wantErr {
				t.Errorf("FSBalance() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("FSBalance() got = %v, want %v", got, tt.want)
			}
			//if got1 != tt.want1 {
			//	t.Errorf("FSBalance() got1 = %v, want %v", got1, tt.want1)
			//}
		})
	}
}

func TestMetaDataDB_Get(t *testing.T) {
	initFileSystem("metadb", "online1")
	initFileSystem("metadb", "online2")
	initFileSystem("metadb", "archive1")
	initFileSystem("metadb", "archive2")
	defer umountFileSystem("metadb", "online1")
	defer umountFileSystem("metadb", "online2")
	defer umountFileSystem("metadb", "archive1")
	defer umountFileSystem("metadb", "archive2")
	defer destroyFileSystem("metadb")

	dbConfigTest := &leveldbprovider.LevelDbConfig{
		StorePath:       "test-metaDb",
		WriteBufferSize: 2,
	}
	levelConfig := &leveldbprovider.NewLevelDBOptions{
		ChainId:  "chain1",
		DbFolder: "test-get",
		Config:   dbConfigTest,
		Logger:   &test.GoLogger{},
	}
	db := leveldbprovider.NewLevelDBHandle(levelConfig)
	defer db.Close()
	defer resetMetaDBData(db)

	//2.修改配置文件中，文件系统的配置,使用测试配置文件
	testConfig := *getSqliteConfig()
	testConfig.BlockFileConfig.ArchiveFileSystem = tmpMetadbArchive
	testConfig.BlockFileConfig.OnlineFileSystem = tmpMetadbOnline

	//3.创建并启动元数据
	dataDb, err := NewMetaDataDB(testConfig, testChainID, db, &test.GoLogger{})
	if err != nil {
		fmt.Println("metadb.NewMetaDataDb,error =", err)
		return
	}

	//4.模拟bfdb,创建元数据
	fileName1 := "00000000000000000001"
	fileName2 := "00000000000000000101"
	CreateArchiveCaseNoArchive(dataDb, fileName1,
		tmpMetadbOnline1, 1, 100, archiveStatusNotStart)
	CreateArchiveCaseNoArchive(dataDb, fileName2,
		tmpMetadbOnline1, 101, 200, archiveStatusNotStart)

	wantFileInfo := storePb.MetaFileInfo{
		OnlineFileSystem:  tmpMetadbOnline1,
		Status:            archiveStatusNotStart,
		ArchiveFileSystem: "",
		StartHeight:       1,
		EndHeight:         maxBlockHeight,
	}

	type fields struct {
		metaFSG      *storePb.MetaFileSystem
		bfdbFiles    map[string]storePb.MetaFileInfo
		multVersions *storePb.MetaMultVersions
		chainID      string
		metaStoreDB  protocol.DBHandle
		logger       protocol.Logger
		archiveChan  chan string
	}
	type args struct {
		height uint64
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    storePb.MetaFileInfo
		wantErr bool
	}{
		{
			name: "meta-data-db-get",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  testDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				height: 2,
			},
			want:    wantFileInfo,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MetaDataDB{
				metaFSG:      tt.fields.metaFSG,
				bfdbFiles:    tt.fields.bfdbFiles,
				multVersions: tt.fields.multVersions,
				chainID:      tt.fields.chainID,
				metaStoreDB:  tt.fields.metaStoreDB,
				logger:       tt.fields.logger,
				archiveChan:  tt.fields.archiveChan,
				RWMutex:      sync.RWMutex{},
			}
			m = dataDb.(*MetaDataDB)
			got, err := m.Get(tt.args.height)
			if (err != nil) != tt.wantErr {
				t.Errorf("Get() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Get() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMetaDataDB_GetArchiveJobByID(t *testing.T) {
	initFileSystem("metadb", "online1")
	initFileSystem("metadb", "online2")
	initFileSystem("metadb", "archive1")
	initFileSystem("metadb", "archive2")
	defer umountFileSystem("metadb", "online1")
	defer umountFileSystem("metadb", "online2")
	defer umountFileSystem("metadb", "archive1")
	defer umountFileSystem("metadb", "archive2")
	defer destroyFileSystem("metadb")

	dbConfigTest := &leveldbprovider.LevelDbConfig{
		StorePath:       "test-metaDb",
		WriteBufferSize: 2,
	}
	levelConfig := &leveldbprovider.NewLevelDBOptions{
		ChainId:  "chain1",
		DbFolder: "test-GetArchiveJobByID",
		Config:   dbConfigTest,
		Logger:   &test.GoLogger{},
	}
	db := leveldbprovider.NewLevelDBHandle(levelConfig)
	defer db.Close()
	defer resetMetaDBData(db)

	//2.修改配置文件中，文件系统的配置,使用测试配置文件
	testConfig := *getSqliteConfig()
	testConfig.BlockFileConfig.ArchiveFileSystem = tmpMetadbArchive
	testConfig.BlockFileConfig.OnlineFileSystem = tmpMetadbOnline

	//3.创建并启动元数据
	dataDb, err := NewMetaDataDB(testConfig, testChainID, db, &test.GoLogger{})
	if err != nil {
		fmt.Println("metadb.NewMetaDataDb,error =", err)
		return
	}

	//4.模拟bfdb,创建元数据，发起归档，获得归档id和归档任务
	//需要创建两个文件,因为最后写入的文件不能归档,所以只能对第一个文件进行归档
	fileName1 := "00000000000000000001"
	fileName2 := "00000000000000000101"
	_, _ = CreateArchiveCase(dataDb, fileName1,
		tmpMetadbOnline1, 1, 100, archiveStatusNotStart)
	archiveJobID, _ := CreateArchiveCase(dataDb, fileName2,
		tmpMetadbOnline1, 101, 200, archiveStatusNotStart)
	//archiveJobID, archiveJobInfo = CreateArchiveCase(dataDb, fileName1,
	//	tmpMetadbOnline1, 1, 100, archiveStatusNotStart)

	jobInfo := storePb.ArchiveJob{}
	jobInfo, err = dataDb.GetArchiveJobByID(archiveJobID)
	for {
		jobInfo, err = dataDb.GetArchiveJobByID(archiveJobID)

		if jobInfo.Status != archiveStatusFinish {
			fmt.Println("---------------------dataDb.GetArchiveJobByID, jobInfo.Status=", jobInfo.Status)
			time.Sleep(100 * time.Millisecond)
			continue
		}
		break
	}

	wantJobInfo := storePb.ArchiveJob{
		StartTime:   jobInfo.StartTime,
		EndTime:     jobInfo.EndTime,
		Status:      archiveStatusFinish,
		StartHeight: 101,
		EndHeight:   200,
		Files:       []string{fileName2},
	}
	type fields struct {
		metaFSG      *storePb.MetaFileSystem
		bfdbFiles    map[string]storePb.MetaFileInfo
		multVersions *storePb.MetaMultVersions
		chainID      string
		metaStoreDB  protocol.DBHandle
		logger       protocol.Logger
		archiveChan  chan string
	}
	type args struct {
		jobID string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    storePb.ArchiveJob
		wantErr bool
	}{
		{
			name: "get-archive-job-by-id",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  testDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				jobID: archiveJobID,
			},
			want:    wantJobInfo,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MetaDataDB{
				metaFSG:      tt.fields.metaFSG,
				bfdbFiles:    tt.fields.bfdbFiles,
				multVersions: tt.fields.multVersions,
				chainID:      tt.fields.chainID,
				metaStoreDB:  tt.fields.metaStoreDB,
				logger:       tt.fields.logger,
				archiveChan:  tt.fields.archiveChan,
				RWMutex:      sync.RWMutex{},
			}
			m = dataDb.(*MetaDataDB)
			fmt.Println("--------------------------1")
			got, err := m.GetArchiveJobByID(tt.args.jobID)
			fmt.Println("got=", got)
			fmt.Println("--------------------------1")
			if (err != nil) != tt.wantErr {
				t.Errorf("GetArchiveJobByID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetArchiveJobByID() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMetaDataDB_GetChainID(t *testing.T) {
	type fields struct {
		metaFSG      *storePb.MetaFileSystem
		bfdbFiles    map[string]storePb.MetaFileInfo
		multVersions *storePb.MetaMultVersions
		chainID      string
		metaStoreDB  protocol.DBHandle
		logger       protocol.Logger
		archiveChan  chan string
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "get-chain-id",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  testDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			want: testChainID,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MetaDataDB{
				metaFSG:      tt.fields.metaFSG,
				bfdbFiles:    tt.fields.bfdbFiles,
				multVersions: tt.fields.multVersions,
				chainID:      tt.fields.chainID,
				metaStoreDB:  tt.fields.metaStoreDB,
				logger:       tt.fields.logger,
				archiveChan:  tt.fields.archiveChan,
				RWMutex:      sync.RWMutex{},
			}
			if got := m.GetChainID(); got != tt.want {
				t.Errorf("GetChainID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMetaDataDB_GetFileInfoByFileName(t *testing.T) {
	initFileSystem("metadb", "online1")
	initFileSystem("metadb", "online2")
	initFileSystem("metadb", "archive1")
	initFileSystem("metadb", "archive2")
	defer umountFileSystem("metadb", "online1")
	defer umountFileSystem("metadb", "online2")
	defer umountFileSystem("metadb", "archive1")
	defer umountFileSystem("metadb", "archive2")
	defer destroyFileSystem("metadb")

	dbConfigTest := &leveldbprovider.LevelDbConfig{
		StorePath:       "test-metaDb",
		WriteBufferSize: 2,
	}
	levelConfig := &leveldbprovider.NewLevelDBOptions{
		ChainId:  "chain1",
		DbFolder: "test-GetFileInfoByFileName",
		Config:   dbConfigTest,
		Logger:   &test.GoLogger{},
	}
	db := leveldbprovider.NewLevelDBHandle(levelConfig)
	defer db.Close()
	defer resetMetaDBData(db)

	//2.修改配置文件中，文件系统的配置,使用测试配置文件
	testConfig := *getSqliteConfig()
	testConfig.BlockFileConfig.ArchiveFileSystem = tmpMetadbArchive
	testConfig.BlockFileConfig.OnlineFileSystem = tmpMetadbOnline

	//3.创建并启动元数据
	dataDb, err := NewMetaDataDB(testConfig, testChainID, db, &test.GoLogger{})
	if err != nil {
		fmt.Println("metadb.NewMetaDataDb,error =", err)
		return
	}

	//4.模拟bfdb,创建元数据，发起归档，获得归档id和归档任务
	//需要创建两个文件,因为最后写入的文件不能归档,所以只能对第一个文件进行归档
	fileName1 := "00000000000000000001"
	CreateArchiveCaseNoArchive(dataDb, fileName1,
		tmpMetadbOnline1, 1, 100, archiveStatusNotStart)

	wantFileInfo := storePb.MetaFileInfo{
		OnlineFileSystem:  tmpMetadbOnline1,
		Status:            archiveStatusNotStart,
		ArchiveFileSystem: "",
		StartHeight:       1,
		EndHeight:         maxBlockHeight,
	}

	type fields struct {
		metaFSG      *storePb.MetaFileSystem
		bfdbFiles    map[string]storePb.MetaFileInfo
		multVersions *storePb.MetaMultVersions
		chainID      string
		metaStoreDB  protocol.DBHandle
		logger       protocol.Logger
		archiveChan  chan string
	}
	type args struct {
		fileName string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    storePb.MetaFileInfo
		wantErr bool
	}{
		{
			name: "get-file-info-by-file-name",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  testDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				fileName: fileName1,
			},
			want:    wantFileInfo,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MetaDataDB{
				metaFSG:      tt.fields.metaFSG,
				bfdbFiles:    tt.fields.bfdbFiles,
				multVersions: tt.fields.multVersions,
				chainID:      tt.fields.chainID,
				metaStoreDB:  tt.fields.metaStoreDB,
				logger:       tt.fields.logger,
				archiveChan:  tt.fields.archiveChan,
				RWMutex:      sync.RWMutex{},
			}
			m = dataDb.(*MetaDataDB)
			got, err := m.GetFileInfoByFileName(tt.args.fileName)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetFileInfoByFileName() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetFileInfoByFileName() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMetaDataDB_GetLastFileInfo(t *testing.T) {
	initFileSystem("metadb", "online1")
	initFileSystem("metadb", "online2")
	initFileSystem("metadb", "archive1")
	initFileSystem("metadb", "archive2")
	defer umountFileSystem("metadb", "online1")
	defer umountFileSystem("metadb", "online2")
	defer umountFileSystem("metadb", "archive1")
	defer umountFileSystem("metadb", "archive2")
	defer destroyFileSystem("metadb")

	dbConfigTest := &leveldbprovider.LevelDbConfig{
		StorePath:       "test-metaDb",
		WriteBufferSize: 2,
	}
	levelConfig := &leveldbprovider.NewLevelDBOptions{
		ChainId:  "chain1",
		DbFolder: "test-GetLastFileInfo",
		Config:   dbConfigTest,
		Logger:   &test.GoLogger{},
	}
	db := leveldbprovider.NewLevelDBHandle(levelConfig)
	defer db.Close()
	defer resetMetaDBData(db)

	//2.修改配置文件中，文件系统的配置,使用测试配置文件
	testConfig := *getSqliteConfig()
	testConfig.BlockFileConfig.ArchiveFileSystem = tmpMetadbArchive
	testConfig.BlockFileConfig.OnlineFileSystem = tmpMetadbOnline

	//3.创建并启动元数据
	dataDb, err := NewMetaDataDB(testConfig, testChainID, db, &test.GoLogger{})
	if err != nil {
		fmt.Println("metadb.NewMetaDataDb,error =", err)
		return
	}

	//4.模拟bfdb,创建元数据，发起归档，获得归档id和归档任务
	//需要创建两个文件,因为最后写入的文件不能归档,所以只能对第一个文件进行归档
	fileName1 := "00000000000000000001"
	_, _ = CreateArchiveCase(dataDb, fileName1,
		tmpMetadbOnline1, 1, 100, archiveStatusNotStart)
	fileName2 := "00000000000000000101"
	CreateArchiveCaseNoArchive(dataDb, fileName2,
		tmpMetadbOnline1, 101, 200, archiveStatusNotStart)

	////createArchiveCase中修改了 endHeight,这里再改成初始状态，表示该文件还是处于在wal中正在写入的状态，还未切换到下一个文件
	//dataDb.SetFileEndHeight(fileName2, maxBlockHeight)
	wantFileInfo := storePb.MetaFileInfo{
		OnlineFileSystem:  tmpMetadbOnline1,
		Status:            archiveStatusNotStart,
		ArchiveFileSystem: "",
		StartHeight:       101,
		EndHeight:         maxBlockHeight,
	}

	//模拟元数据为 空的 情况
	bfdbFilesEmpty := map[string]storePb.MetaFileInfo{}
	noDataDBHandler := getDBHandle(testChainID, "test-metadb", "metadb/GetLastFileInfoNoData", &test.GoLogger{}, nil)
	resetMetaDBData(noDataDBHandler)
	defer noDataDBHandler.Close()

	wantFileInfo2 := storePb.MetaFileInfo{}

	type fields struct {
		metaFSG      *storePb.MetaFileSystem
		bfdbFiles    map[string]storePb.MetaFileInfo
		multVersions *storePb.MetaMultVersions
		chainID      string
		metaStoreDB  protocol.DBHandle
		logger       protocol.Logger
		archiveChan  chan string
	}
	tests := []struct {
		name    string
		fields  fields
		want    string
		want1   storePb.MetaFileInfo
		want2   bool
		wantErr bool
	}{
		{
			name: "get-last-file-info",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  testDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			want:    fileName2,
			want1:   wantFileInfo,
			want2:   true,
			wantErr: false,
		},
		{
			name: "get-last-file-info-no-last",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesEmpty,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  noDataDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			want:    "",
			want1:   wantFileInfo2,
			want2:   false,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MetaDataDB{
				metaFSG:      tt.fields.metaFSG,
				bfdbFiles:    tt.fields.bfdbFiles,
				multVersions: tt.fields.multVersions,
				chainID:      tt.fields.chainID,
				metaStoreDB:  tt.fields.metaStoreDB,
				logger:       tt.fields.logger,
				archiveChan:  tt.fields.archiveChan,
				RWMutex:      sync.RWMutex{},
			}
			//模拟空的情况
			if tt.name != "get-last-file-info-no-last" {
				m = dataDb.(*MetaDataDB)
			}

			got, got1, got2, err := m.GetLastFileInfo()
			if (err != nil) != tt.wantErr {
				t.Errorf("GetLastFileInfo() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("GetLastFileInfo() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("GetLastFileInfo() got1 = %v, want %v", got1, tt.want1)
			}
			if got2 != tt.want2 {
				t.Errorf("GetLastFileInfo() got2 = %v, want %v", got2, tt.want2)
			}
		})
	}
}

func TestMetaDataDB_GetMetaFS(t *testing.T) {
	initFileSystem("metadb", "online1")
	initFileSystem("metadb", "online2")
	initFileSystem("metadb", "archive1")
	initFileSystem("metadb", "archive2")
	defer umountFileSystem("metadb", "online1")
	defer umountFileSystem("metadb", "online2")
	defer umountFileSystem("metadb", "archive1")
	defer umountFileSystem("metadb", "archive2")
	defer destroyFileSystem("metadb")

	dbConfigTest := &leveldbprovider.LevelDbConfig{
		StorePath:       "test-metaDb",
		WriteBufferSize: 2,
	}
	levelConfig := &leveldbprovider.NewLevelDBOptions{
		ChainId:  "chain1",
		DbFolder: "test-GetMetaFS",
		Config:   dbConfigTest,
		Logger:   &test.GoLogger{},
	}
	db := leveldbprovider.NewLevelDBHandle(levelConfig)
	defer db.Close()
	defer resetMetaDBData(db)

	//2.修改配置文件中，文件系统的配置,使用测试配置文件
	testConfig := getSqliteConfig()
	testConfig.BlockFileConfig.ArchiveFileSystem = tmpMetadbArchive
	testConfig.BlockFileConfig.OnlineFileSystem = tmpMetadbOnline

	wantMetaFS := storePb.MetaFileSystem{
		OnlineFileSystem:  []string{tmpMetadbOnline1, "/tmp/metadb/online2"},
		ArchiveFileSystem: []string{"/tmp/metadb/archive1", "/tmp/metadb/archive2"},
	}

	//3.创建并启动元数据
	dataDb, err := NewMetaDataDB(*testConfig, testChainID, db, &test.GoLogger{})
	if err != nil {
		fmt.Println("metadb.NewMetaDataDb,error =", err)
		return
	}

	type fields struct {
		metaFSG      *storePb.MetaFileSystem
		bfdbFiles    map[string]storePb.MetaFileInfo
		multVersions *storePb.MetaMultVersions
		chainID      string
		metaStoreDB  protocol.DBHandle
		logger       protocol.Logger
		archiveChan  chan string
	}
	tests := []struct {
		name    string
		fields  fields
		want    storePb.MetaFileSystem
		wantErr bool
	}{
		{
			name: "get-meta-fs",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  testDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			want:    wantMetaFS,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MetaDataDB{
				metaFSG:      tt.fields.metaFSG,
				bfdbFiles:    tt.fields.bfdbFiles,
				multVersions: tt.fields.multVersions,
				chainID:      tt.fields.chainID,
				metaStoreDB:  tt.fields.metaStoreDB,
				logger:       tt.fields.logger,
				archiveChan:  tt.fields.archiveChan,
				RWMutex:      sync.RWMutex{},
			}
			m = dataDb.(*MetaDataDB)
			got, err := m.GetMetaFS()
			if (err != nil) != tt.wantErr {
				t.Errorf("GetMetaFS() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetMetaFS() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMetaDataDB_GetVersionFromDB(t *testing.T) {
	type fields struct {
		metaFSG      *storePb.MetaFileSystem
		bfdbFiles    map[string]storePb.MetaFileInfo
		multVersions *storePb.MetaMultVersions
		chainID      string
		metaStoreDB  protocol.DBHandle
		logger       protocol.Logger
		archiveChan  chan string
	}
	tests := []struct {
		name    string
		fields  fields
		want    *storePb.MetaMultVersions
		wantErr bool
	}{

		{
			name: "get-version-from-db",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  testDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			want:    multVersionsTest,
			wantErr: false,
		},
		{
			name: "get-version-from-db-nil",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  testDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			want:    nil,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MetaDataDB{
				metaFSG:      tt.fields.metaFSG,
				bfdbFiles:    tt.fields.bfdbFiles,
				multVersions: tt.fields.multVersions,
				chainID:      tt.fields.chainID,
				metaStoreDB:  tt.fields.metaStoreDB,
				logger:       tt.fields.logger,
				archiveChan:  tt.fields.archiveChan,
				RWMutex:      sync.RWMutex{},
			}
			resetMetaDBData(m.metaStoreDB)

			if tt.name == "get-version-from-db" {
				m.initMeta(*getSqliteConfig())

			}
			// 模拟没有数据时，空db的情况
			if tt.name == "get-version-from-db-nil" {
				m.metaStoreDB.Delete([]byte(metaVersionsKey))
			}
			got, err := m.GetVersionFromDB()
			if (err != nil) != tt.wantErr {
				t.Errorf("GetVersionFromDB() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			//case name:"get-version-from-db-nil",
			if tt.want == nil {
				if tt.want != got {
					t.Errorf("GetVersionFromDB() got = %v, want %v", got, tt.want)
					return
				}
				return
			}
			for i := 0; i < len(tt.want.Version); i++ {
				if tt.want.Version[i].MajorVersion != got.Version[i].MajorVersion ||
					tt.want.Version[i].MinorVersion != got.Version[i].MinorVersion {
					t.Errorf("GetVersionFromDB() got = %v, want %v", got, tt.want)
				}
			}

		})
	}
}

func TestMetaDataDB_ReadArchiveFileDataByOffset(t *testing.T) {
	initFileSystem("metadb", "online1")
	initFileSystem("metadb", "online2")
	initFileSystem("metadb", "archive1")
	initFileSystem("metadb", "archive2")
	defer umountFileSystem("metadb", "online1")
	defer umountFileSystem("metadb", "online2")
	defer umountFileSystem("metadb", "archive1")
	defer umountFileSystem("metadb", "archive2")
	defer destroyFileSystem("metadb")

	dbConfigTest := &leveldbprovider.LevelDbConfig{
		StorePath:       "test-metaDb",
		WriteBufferSize: 2,
	}
	levelConfig := &leveldbprovider.NewLevelDBOptions{
		ChainId:  "chain1",
		DbFolder: "test-ReadArchiveFileDataByOffset",
		Config:   dbConfigTest,
		Logger:   &test.GoLogger{},
	}
	db := leveldbprovider.NewLevelDBHandle(levelConfig)
	defer db.Close()
	defer resetMetaDBData(db)

	//2.修改配置文件中，文件系统的配置,使用测试配置文件
	testConfig := *getSqliteConfig()
	testConfig.BlockFileConfig.ArchiveFileSystem = tmpMetadbArchive
	testConfig.BlockFileConfig.OnlineFileSystem = tmpMetadbOnline

	//3.创建并启动元数据
	dataDb, err := NewMetaDataDB(testConfig, testChainID, db, &test.GoLogger{})
	if err != nil {
		fmt.Println("metadb.NewMetaDataDb,error =", err)
		return
	}

	//4.模拟bfdb,创建元数据，发起归档，获得归档id和归档任务
	//需要创建两个文件,因为最后写入的文件不能归档,所以只能对第一个文件进行归档
	fileName1 := "00000000000000000001"
	archiveJobID1, _ := CreateArchiveCase(dataDb, fileName1,
		tmpMetadbOnline1, 1, 100, archiveStatusNotStart)
	fileName2 := "00000000000000000101"
	_, _ = CreateArchiveCase(dataDb, fileName2,
		tmpMetadbOnline1, 101, 200, archiveStatusNotStart)

	//保证 文件已被归档，才能读出数据来
	for {
		fmt.Println("---------------------dataDb.GetArchiveJobByID-----")
		jobInfo, err := dataDb.GetArchiveJobByID(archiveJobID1)
		if err != nil {
			fmt.Println("GetArchiveJobByID error :", err)
		}
		if jobInfo.Status != archiveStatusFinish {
			time.Sleep(100 * time.Millisecond)
			continue
		}
		break
	}

	fileName3 := "file3-not-exist-in-ReadArchiveFile"
	//5.完成归档后，读一些数据

	readBuf := make([]byte, 5)
	wantBuf := []byte("2c3d4")

	type fields struct {
		metaFSG      *storePb.MetaFileSystem
		bfdbFiles    map[string]storePb.MetaFileInfo
		multVersions *storePb.MetaMultVersions
		chainID      string
		metaStoreDB  protocol.DBHandle
		logger       protocol.Logger
		archiveChan  chan string
	}
	type args struct {
		fileName string
		b        []byte
		off      int64
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		{
			name: "read-archive-file-data-by-offset",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  testDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				fileName: fileName1,
				b:        readBuf,
				off:      3,
			},
			want:    5,
			wantErr: false,
		},
		{
			name: "read-archive-file-data-by-offset-not-exist",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  testDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				fileName: fileName3,
				b:        make([]byte, 10),
				off:      3,
			},
			want:    0,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MetaDataDB{
				metaFSG:      tt.fields.metaFSG,
				bfdbFiles:    tt.fields.bfdbFiles,
				multVersions: tt.fields.multVersions,
				chainID:      tt.fields.chainID,
				metaStoreDB:  tt.fields.metaStoreDB,
				logger:       tt.fields.logger,
				archiveChan:  tt.fields.archiveChan,
				RWMutex:      sync.RWMutex{},
			}
			m = dataDb.(*MetaDataDB)
			got, err := m.ReadArchiveFileDataByOffset(tt.args.fileName, tt.args.b, tt.args.off)
			//针对case1 比对 读的字节数据，是否正确
			if tt.name == "read-archive-file-data-by-offset" {
				if string(readBuf) != string(wantBuf) {
					t.Errorf("ReadArchiveFileDataByOffset() error,readBuf = %v, want %v", readBuf, wantBuf)
				}
			}
			if (err != nil) != tt.wantErr {
				t.Errorf("ReadArchiveFileDataByOffset() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("ReadArchiveFileDataByOffset() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMetaDataDB_SetFileEndHeight(t *testing.T) {
	initFileSystem("metadb", "online1")
	initFileSystem("metadb", "online2")

	initFileSystem("metadb", "archive1")
	initFileSystem("metadb", "archive2")

	defer umountFileSystem("metadb", "online1")
	defer umountFileSystem("metadb", "online2")
	defer umountFileSystem("metadb", "archive1")
	defer umountFileSystem("metadb", "archive2")

	defer destroyFileSystem("metadb")

	dbConfigTest := &leveldbprovider.LevelDbConfig{
		StorePath:       "test-metaDb",
		WriteBufferSize: 2,
	}
	levelConfig := &leveldbprovider.NewLevelDBOptions{
		ChainId:  "chain1",
		DbFolder: "test-SetFileEndHeight",
		Config:   dbConfigTest,
		Logger:   &test.GoLogger{},
	}
	db := leveldbprovider.NewLevelDBHandle(levelConfig)
	defer db.Close()
	defer resetMetaDBData(db)

	//2.修改配置文件中，文件系统的配置,使用测试配置文件
	testConfig := *getSqliteConfig()
	testConfig.BlockFileConfig.ArchiveFileSystem = tmpMetadbArchive
	testConfig.BlockFileConfig.OnlineFileSystem = tmpMetadbOnline

	//3.创建并启动元数据
	dataDb, err := NewMetaDataDB(testConfig, testChainID, db, &test.GoLogger{})
	if err != nil {
		fmt.Println("metadb.NewMetaDataDb,error =", err)
		return
	}
	testFileName := "00000000000000000001"
	err = dataDb.CreateNewFile(testFileName, tmpMetadbOnline1, 1)
	//err := dataDb.CreateNewFile(fileName,fileSystem,start)
	if err != nil {
		fmt.Println("CreateNewFile error:", err)
	}
	//模拟bfdb，伪造一个文件
	filePath := tmpMetadbOnline1 + "/" + testChainID + "/" + "bfdb" + testFileName + ".fdb"
	_, err = os.Create(filePath)
	if err != nil {
		fmt.Println("error,when create file,errInfo:", err)
	}

	fileName2 := "file-not-exist-in-SetFileEndHeight"

	type fields struct {
		metaFSG      *storePb.MetaFileSystem
		bfdbFiles    map[string]storePb.MetaFileInfo
		multVersions *storePb.MetaMultVersions
		chainID      string
		metaStoreDB  protocol.DBHandle
		logger       protocol.Logger
		archiveChan  chan string
	}
	type args struct {
		fileName  string
		endHeight uint64
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "set-file-end-height",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  db,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				fileName:  testFileName,
				endHeight: 100,
			},
			wantErr: false,
		},
		{
			name: "set-file-end-height-file-not-exist",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  db,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				fileName:  fileName2,
				endHeight: 100,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MetaDataDB{
				metaFSG:      tt.fields.metaFSG,
				bfdbFiles:    tt.fields.bfdbFiles,
				multVersions: tt.fields.multVersions,
				chainID:      tt.fields.chainID,
				metaStoreDB:  tt.fields.metaStoreDB,
				logger:       tt.fields.logger,
				archiveChan:  tt.fields.archiveChan,
				RWMutex:      sync.RWMutex{},
			}
			m = dataDb.(*MetaDataDB)
			if err := m.SetFileEndHeight(tt.args.fileName, tt.args.endHeight); (err != nil) != tt.wantErr {
				t.Errorf("SetFileEndHeight() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestMetaDataDB_absolutePath(t *testing.T) {
	type fields struct {
		metaFSG      *storePb.MetaFileSystem
		bfdbFiles    map[string]storePb.MetaFileInfo
		multVersions *storePb.MetaMultVersions
		chainID      string
		metaStoreDB  protocol.DBHandle
		logger       protocol.Logger
		archiveChan  chan string
	}
	type args struct {
		fileName   string
		fileSystem string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		{
			name: "absolute-path",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  testDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				fileName:   "file1",
				fileSystem: "/tmp/metadb/arch1",
			},
			want: "/tmp/metadb/arch1/" + testChainID + "/" + "bfdb/" + "file1" + ".fdb",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MetaDataDB{
				metaFSG:      tt.fields.metaFSG,
				bfdbFiles:    tt.fields.bfdbFiles,
				multVersions: tt.fields.multVersions,
				chainID:      tt.fields.chainID,
				metaStoreDB:  tt.fields.metaStoreDB,
				logger:       tt.fields.logger,
				archiveChan:  tt.fields.archiveChan,
				RWMutex:      sync.RWMutex{},
			}
			if got := m.absolutePath(tt.args.fileName, tt.args.fileSystem); got != tt.want {
				t.Errorf("absolutePath() = %v, want %v", got, tt.want)
			}
		})
	}
}

//创建文件系统
//在/tmp目录下
func initFileSystem(dirName, volume string) {
	cmd := exec.Command("/bin/bash", "./mount.sh", dirName, volume)
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()
	if err != nil {
		fmt.Println("start mount fs error :", err)
	}
	//outStr, errStr := string(stdout.Bytes()), string(stderr.Bytes())
	outStr, errStr := stdout.String(), stderr.String()
	fmt.Printf("out:%s\n", outStr)
	if errStr != "" {
		fmt.Printf("err:%s\n", errStr)
	}
}

//卸载文件系统
func umountFileSystem(dirName, volume string) {
	cmd := exec.Command("/bin/bash", "./umount.sh", dirName, volume)
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()
	if err != nil {
		fmt.Println("stop umount error: ", err)
	}
	//outStr, errStr := string(stdout.Bytes()), string(stderr.Bytes())
	outStr, errStr := stdout.String(), stderr.String()
	fmt.Printf("out:%s\n", outStr)
	if errStr != "" {
		fmt.Printf("err:%s\n", errStr)
	}
}

//删除文件系统
func destroyFileSystem(dirName string) {
	cmd := exec.Command("/bin/bash", "./rmdir.sh", dirName)
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()
	if err != nil {
		fmt.Println("stop rmdir error: ", err)
	}
	//outStr, errStr := string(stdout.Bytes()), string(stderr.Bytes())
	outStr, errStr := stdout.String(), stderr.String()
	fmt.Printf("out:%s\n", outStr)
	if errStr != "" {
		fmt.Printf("err:%s\n", errStr)
	}
}

//创建不同状态的归档任务，用于测试归档自动机
//状态对应 archiveAutomata 中的状态
func CreateArchiveCase(metaDB meta.MetaData, fileName string, fileSystem string, start, end uint64, status int32) (string,
	storePb.ArchiveJob) {

	//err := metaDB.CreateNewFile("00000000000000000001",tmpMetadbOnline1,1)
	err := metaDB.CreateNewFile(fileName, fileSystem, start)
	if err != nil {
		fmt.Println("CreateNewFile error:", err)
	}

	//var stdout, stderr bytes.Buffer
	//模拟bfdb，伪造一个文件
	filePath := fileSystem + "/" + testChainID + "/" + "bfdb/" + fileName + ".fdb"
	//ofStr := "of=" + filePath
	create, err := os.Create(filePath)
	create.Close()

	if err != nil {
		fmt.Println("create file ,error,", err, filePath)
	}

	//写入一些数据
	openFile, err := os.OpenFile(filePath, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
	//openFile, err := os.OpenFile(filePath,os.)
	if err != nil {
		fmt.Println("open file  error :", err, filePath)
	}
	defer openFile.Close()
	writeN, err := openFile.Write([]byte("a1b2c3d4e5f6g7h8i9"))
	if err != nil || writeN != len("a1b2c3d4e5f6g7h8i9") {
		fmt.Println("start write a1b2c3d4e5f6g7h8i9 to new_100M_file error :", err)
	}

	fmt.Println("echo  a1b2c3d4e5f6g7h8i9 finish,file", filePath)
	//time.Sleep(100 * time.Second)

	//模拟bfdb,修改文件结束区块块高,模拟bfdb切换文件时的操作
	err = metaDB.SetFileEndHeight(fileName, end)
	if err != nil {
		fmt.Println("SetFileEndHeight error:", err)
	}

	//发起归档任务，第一次归档
	archiveJobID, err := metaDB.DoArchive(start, end)
	if err != nil {
		fmt.Println("DoArchive error :", err)
	}
	//获得待归档的任务
	time.Sleep(100 * time.Millisecond)
	jobInfo, err := metaDB.GetArchiveJobByID(archiveJobID)
	if err != nil {
		fmt.Println("GetArchiveJobByID error :", err)
	}
	jobInfo.Status = status

	return archiveJobID, jobInfo

}

//创建不同状态的归档任务，用于测试归档自动机
//状态对应 archiveAutomata 中的状态
func CreateArchiveCaseNoArchive(metaDB meta.MetaData, fileName string, fileSystem string, start, end uint64, status int32) {

	//err := metaDB.CreateNewFile("00000000000000000001",tmpMetadbOnline1,1)
	err := metaDB.CreateNewFile(fileName, fileSystem, start)
	if err != nil {
		fmt.Println("CreateNewFile error:", err)
	}

	//var stdout, stderr bytes.Buffer
	//模拟bfdb，伪造一个文件
	filePath := fileSystem + "/" + testChainID + "/" + "bfdb/" + fileName + ".fdb"
	//ofStr := "of=" + filePath
	create, err := os.Create(filePath)
	create.Close()

	if err != nil {
		fmt.Println("create file ,error,", err, filePath)
	}

	//写入一些数据
	openFile, err := os.OpenFile(filePath, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
	//openFile, err := os.OpenFile(filePath,os.)
	if err != nil {
		fmt.Println("open file  error :", err, filePath)
	}
	defer openFile.Close()
	writeN, err := openFile.Write([]byte("a1b2c3d4e5f6g7h8i9"))
	if err != nil || writeN != len("a1b2c3d4e5f6g7h8i9") {
		fmt.Println("start write a1b2c3d4e5f6g7h8i9 to new_100M_file error :", err)
	}

	fmt.Println("echo  a1b2c3d4e5f6g7h8i9 finish,file", filePath)
	//time.Sleep(100 * time.Second)
}

//创建不同状态的归档任务，用于测试归档自动机
//状态对应 archiveAutomata 中的状态
func CreateArchiveCaseSetEnd(metaDB meta.MetaData, fileName string, fileSystem string, start, end uint64, status int32) {

	//err := metaDB.CreateNewFile("00000000000000000001",tmpMetadbOnline1,1)
	err := metaDB.CreateNewFile(fileName, fileSystem, start)
	if err != nil {
		fmt.Println("CreateNewFile error:", err)
	}

	//模拟bfdb,修改文件结束区块块高,模拟bfdb切换文件时的操作
	err = metaDB.SetFileEndHeight(fileName, end)
	if err != nil {
		fmt.Println("SetFileEndHeight error:", err)
	}

	var stdout, stderr bytes.Buffer
	//模拟bfdb，伪造一个文件
	filePath := fileSystem + "/" + testChainID + "/" + "bfdb/" + fileName + ".fdb"
	//写入一些数据,100MB
	ofStr := "of=" + filePath
	cmd := exec.Command("/usr/bin/dd", "if=/dev/zero", ofStr, "count=102400", "bs=10")
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err = cmd.Run()
	if err != nil {
		fmt.Println("start create new_100M_file error :", err)
	}
	outStr, errStr := stdout.String(), stderr.String()
	//outStr, errStr := string(stdout.Bytes()), string(stderr.Bytes())
	fmt.Printf("out:%s\n", outStr)
	if errStr != "" {
		fmt.Printf("err:%s\n", errStr)
	}
}

//创建文件系统
//创建metadb
//模拟bfdb,创建文件元数据 和 创建文件
//创建归档任务
//执行归档任务
func TestMetaDataDB_archiveAutomata(t *testing.T) {
	//1.创建文件系统
	//tmp/metadb/mnt_online1  /tmp/metadb/mnt_online2
	//tmp/metadb/mnt_archive1  /tmp/metadb/mnt_archive2
	initFileSystem("metadb", "online1")
	initFileSystem("metadb", "online2")

	initFileSystem("metadb", "archive1")
	initFileSystem("metadb", "archive2")

	defer umountFileSystem("metadb", "online1")
	defer umountFileSystem("metadb", "online2")
	defer umountFileSystem("metadb", "archive1")
	defer umountFileSystem("metadb", "archive2")

	defer destroyFileSystem("metadb")

	dbConfigTest := &leveldbprovider.LevelDbConfig{
		StorePath:       "test-metaDb",
		WriteBufferSize: 2,
	}
	levelConfig := &leveldbprovider.NewLevelDBOptions{
		ChainId:  "chain1",
		DbFolder: "test-archiveAutomata",
		Config:   dbConfigTest,
		Logger:   &test.GoLogger{},
	}
	db := leveldbprovider.NewLevelDBHandle(levelConfig)
	defer db.Close()
	defer resetMetaDBData(db)

	//2.修改配置文件中，文件系统的配置,使用测试配置文件
	testConfig := *getSqliteConfig()
	testConfig.BlockFileConfig.ArchiveFileSystem = tmpMetadbArchive
	testConfig.BlockFileConfig.OnlineFileSystem = tmpMetadbOnline

	//3.创建并启动元数据
	dataDb, err := NewMetaDataDB(testConfig, testChainID, db, &test.GoLogger{})
	if err != nil {
		fmt.Println("metadb.NewMetaDataDb,error =", err)
		return
	}

	//4.模拟bfdb,创建元数据，发起归档，获得归档id和归档任务
	//模拟归档任务状态为 archiveStatusNotStart 的case
	notStartJobID, notStartJobInfo := CreateArchiveCase(dataDb, "00000000000000000001",
		tmpMetadbOnline1, 1, 100, archiveStatusNotStart)
	beginJobID, beginJobInfo := CreateArchiveCase(dataDb, "00000000000000000101",
		tmpMetadbOnline1, 101, 200, archiveStatusBegin)
	copyStartJobID, copyStartJobInfo := CreateArchiveCase(dataDb, "00000000000000000201",
		tmpMetadbOnline1, 201, 300, archiveStatusCopyStart)
	copyEndJobID, copyEndJobInfo := CreateArchiveCase(dataDb, "00000000000000000301",
		tmpMetadbOnline1, 301, 400, archiveStatusCopyEnd)
	delStartJobID, delStartJobInfo := CreateArchiveCase(dataDb, "00000000000000000401",
		tmpMetadbOnline1, 401, 500, archiveStatusDelStart)
	delEndJobID, delEndJobInfo := CreateArchiveCase(dataDb, "00000000000000000501",
		tmpMetadbOnline1, 501, 600, archiveStatusDelEnd)
	finishJobID, finishJobInfo := CreateArchiveCase(dataDb, "00000000000000000601",
		tmpMetadbOnline1, 601, 700, archiveStatusFinish)
	defaultJobID, defaultJobInfo := CreateArchiveCase(dataDb, "00000000000000000701",
		tmpMetadbOnline1, 701, 800, 5)

	//5.输入各种case

	type fields struct {
		metaFSG      *storePb.MetaFileSystem
		bfdbFiles    map[string]storePb.MetaFileInfo
		multVersions *storePb.MetaMultVersions
		chainID      string
		metaStoreDB  protocol.DBHandle
		logger       protocol.Logger
		archiveChan  chan string
	}
	type args struct {
		jobID      string
		archiveJob storePb.ArchiveJob
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "archive-automata-not-start",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  db,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				jobID:      notStartJobID,
				archiveJob: notStartJobInfo,
			},
			wantErr: false,
		},
		{
			name: "archive-automata-begin",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  db,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				jobID:      beginJobID,
				archiveJob: beginJobInfo,
			},
			wantErr: false,
		},
		{
			name: "archive-automata-copy-start",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  db,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				jobID:      copyStartJobID,
				archiveJob: copyStartJobInfo,
			},
			wantErr: false,
		},
		{
			name: "archive-automata-copy-end",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  db,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				jobID:      copyEndJobID,
				archiveJob: copyEndJobInfo,
			},
			wantErr: false,
		},
		{
			name: "archive-automata--del-start",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  db,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				jobID:      delStartJobID,
				archiveJob: delStartJobInfo,
			},
			wantErr: false,
		},
		{
			name: "archive-automata-del-end",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  db,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				jobID:      delEndJobID,
				archiveJob: delEndJobInfo,
			},
			wantErr: false,
		},
		{
			name: "archive-automata-finish",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  db,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				jobID:      finishJobID,
				archiveJob: finishJobInfo,
			},
			wantErr: false,
		},
		{
			name: "archive-automata-default",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  db,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				jobID:      defaultJobID,
				archiveJob: defaultJobInfo,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MetaDataDB{
				metaFSG:      tt.fields.metaFSG,
				bfdbFiles:    tt.fields.bfdbFiles,
				multVersions: tt.fields.multVersions,
				chainID:      tt.fields.chainID,
				metaStoreDB:  tt.fields.metaStoreDB,
				logger:       tt.fields.logger,
				archiveChan:  tt.fields.archiveChan,
				RWMutex:      sync.RWMutex{},
			}
			m = dataDb.(*MetaDataDB)
			if err := m.archiveAutomata(tt.args.jobID, tt.args.archiveJob); (err != nil) != tt.wantErr {
				t.Errorf("archiveAutomata() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestMetaDataDB_checkConfigDiff(t *testing.T) {
	configMFS1 := &storePb.MetaFileSystem{
		OnlineFileSystem:  []string{"a", "b", "c"},
		ArchiveFileSystem: []string{},
	}
	dbMFS1 := &storePb.MetaFileSystem{
		OnlineFileSystem:  []string{"a", "b"},
		ArchiveFileSystem: []string{},
	}

	configMFS2 := &storePb.MetaFileSystem{
		OnlineFileSystem:  []string{},
		ArchiveFileSystem: []string{"a", "b", "c"},
	}
	dbMFS2 := &storePb.MetaFileSystem{
		OnlineFileSystem:  []string{},
		ArchiveFileSystem: []string{"a", "b"},
	}

	configMFS3 := &storePb.MetaFileSystem{
		OnlineFileSystem:  []string{"a", "b"},
		ArchiveFileSystem: []string{},
	}
	dbMFS3 := &storePb.MetaFileSystem{
		OnlineFileSystem:  []string{"a", "c"},
		ArchiveFileSystem: []string{},
	}

	configMFS4 := &storePb.MetaFileSystem{
		OnlineFileSystem:  []string{},
		ArchiveFileSystem: []string{"a", "b"},
	}
	dbMFS4 := &storePb.MetaFileSystem{
		OnlineFileSystem:  []string{},
		ArchiveFileSystem: []string{"a", "c"},
	}

	configMFS5 := &storePb.MetaFileSystem{
		OnlineFileSystem:  []string{},
		ArchiveFileSystem: []string{"a"},
	}
	dbMFS5 := &storePb.MetaFileSystem{
		OnlineFileSystem:  []string{},
		ArchiveFileSystem: []string{"a", "b"},
	}

	type fields struct {
		metaFSG      *storePb.MetaFileSystem
		bfdbFiles    map[string]storePb.MetaFileInfo
		multVersions *storePb.MetaMultVersions
		chainID      string
		metaStoreDB  protocol.DBHandle
		logger       protocol.Logger
		archiveChan  chan string
	}
	type args struct {
		metaFS   *storePb.MetaFileSystem
		dbMetaFS *storePb.MetaFileSystem
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		want1   int
		wantErr bool
	}{
		{
			name: "check-config-diff1",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  testDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				metaFS:   configMFS1,
				dbMetaFS: dbMFS1,
			},
			want:    1,
			want1:   0,
			wantErr: false,
		},
		{
			name: "check-config-diff2",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  testDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				metaFS:   configMFS2,
				dbMetaFS: dbMFS2,
			},
			want:    0,
			want1:   1,
			wantErr: false,
		},
		{
			name: "check-config-diff3",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  testDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				metaFS:   configMFS3,
				dbMetaFS: dbMFS3,
			},
			want:    0,
			want1:   0,
			wantErr: true,
		},
		{
			name: "check-config-diff4",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  testDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				metaFS:   configMFS4,
				dbMetaFS: dbMFS4,
			},
			want:    0,
			want1:   0,
			wantErr: true,
		},
		{
			name: "check-config-diff5",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  testDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				metaFS:   configMFS5,
				dbMetaFS: dbMFS5,
			},
			want:    0,
			want1:   0,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MetaDataDB{
				metaFSG:      tt.fields.metaFSG,
				bfdbFiles:    tt.fields.bfdbFiles,
				multVersions: tt.fields.multVersions,
				chainID:      tt.fields.chainID,
				metaStoreDB:  tt.fields.metaStoreDB,
				logger:       tt.fields.logger,
				archiveChan:  tt.fields.archiveChan,
				RWMutex:      sync.RWMutex{},
			}
			got, got1, err := m.checkConfigDiff(tt.args.metaFS, tt.args.dbMetaFS)
			if (err != nil) != tt.wantErr {
				t.Errorf("checkConfigDiff() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("checkConfigDiff() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("checkConfigDiff() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func TestMetaDataDB_consumeArchiveJob(t *testing.T) {
	dbConfigTest := &leveldbprovider.LevelDbConfig{
		StorePath:       "test-metaDb",
		WriteBufferSize: 2,
	}
	levelConfig := &leveldbprovider.NewLevelDBOptions{
		ChainId:  "chain1",
		DbFolder: "test-consumeArchiveJob",
		Config:   dbConfigTest,
		Logger:   &test.GoLogger{},
	}
	db := leveldbprovider.NewLevelDBHandle(levelConfig)
	resetMetaDBData(db)
	defer db.Close()
	defer resetMetaDBData(db)

	//2.修改配置文件中，文件系统的配置,使用测试配置文件
	testConfig := *getSqliteConfig()
	testConfig.BlockFileConfig.ArchiveFileSystem = tmpMetadbArchive
	testConfig.BlockFileConfig.OnlineFileSystem = tmpMetadbOnline

	//3.创建并启动元数据
	dataDb, err := NewMetaDataDB(testConfig, testChainID, db, &test.GoLogger{})
	if err != nil {
		fmt.Println("metadb.NewMetaDataDb,error =", err)
		return
	}

	type fields struct {
		metaFSG      *storePb.MetaFileSystem
		bfdbFiles    map[string]storePb.MetaFileInfo
		multVersions *storePb.MetaMultVersions
		chainID      string
		metaStoreDB  protocol.DBHandle
		logger       protocol.Logger
		archiveChan  chan string
	}
	tests := []struct {
		name   string
		fields fields
		//wantErr bool
	}{
		{
			name: "consume-archive-job",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  testDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			//wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MetaDataDB{
				metaFSG:      tt.fields.metaFSG,
				bfdbFiles:    tt.fields.bfdbFiles,
				multVersions: tt.fields.multVersions,
				chainID:      tt.fields.chainID,
				metaStoreDB:  tt.fields.metaStoreDB,
				logger:       tt.fields.logger,
				archiveChan:  tt.fields.archiveChan,
				RWMutex:      sync.RWMutex{},
			}
			m = dataDb.(*MetaDataDB)
			fmt.Println("--start DoArchive---")
			_, _ = m.DoArchive(1, 99)
			fmt.Println("--finish DoArchive---")

			go m.consumeArchiveJob()
		})
	}
}

func TestMetaDataDB_copyFile(t *testing.T) {
	//创建文件系统，包括online和archive
	initFileSystem("metadb", "online1")
	initFileSystem("metadb", "archive1")
	defer umountFileSystem("metadb", "online1")
	defer umountFileSystem("metadb", "archive1")
	defer destroyFileSystem("metadb")

	dstFileExistName := "exist_file"
	srcFileSystem := tmpMetadbOnline1
	srcFileName := newTmpFile
	dstFileSystem := "/tmp/metadb/arhive1"
	dstFileName := newTmpFile
	srcFileNameNotExist := "not-file-exist-copy"

	//对 /tmp/metadb/online1 写入文件
	mkdir := srcFileSystem + "/" + testChainID + "/" + "bfdb/"
	cmd := exec.Command("mkdir", "-p", mkdir)
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()
	if err != nil {
		fmt.Println("start mkdir error :", err)
	}
	outStr, errStr := stdout.String(), stderr.String()
	fmt.Printf("out:%s\n", outStr)
	if errStr != "" {
		fmt.Printf("err:%s\n", errStr)
	}

	//写入一些数据
	file := srcFileSystem + "/" + testChainID + "/" + "bfdb/" + dstFileName + ".fdb"
	ofStr := "of=" + file
	cmd = exec.Command("dd", "if=/dev/zero", ofStr, "count=102400", "bs=1024")
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err = cmd.Run()
	if err != nil {
		fmt.Println("start create new 100M_file error :", err)
	}
	//outStr, errStr := string(stdout.Bytes()), string(stderr.Bytes())
	outStr, errStr = stdout.String(), stderr.String()
	fmt.Printf("out:%s\n", outStr)
	if errStr != "" {
		fmt.Printf("err:%s\n", errStr)
	}

	//模拟目标文件存在的情况
	//写入一些数据
	dstFileExist := srcFileSystem + "/" + testChainID + "/" + "bfdb/" + dstFileExistName + ".fdb"
	ofStrFileExist := "of=" + dstFileExist
	cmd = exec.Command("dd", "if=/dev/zero", ofStrFileExist, "count=102400", "bs=10")
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err = cmd.Run()
	if err != nil {
		fmt.Println("start create new 100M file error :", err)
	}
	//outStr, errStr := string(stdout.Bytes()), string(stderr.Bytes())
	outStr, errStr = stdout.String(), stderr.String()
	fmt.Printf("out:%s\n", outStr)
	if errStr != "" {
		fmt.Printf("err:%s\n", errStr)
	}

	type fields struct {
		metaFSG      *storePb.MetaFileSystem
		bfdbFiles    map[string]storePb.MetaFileInfo
		multVersions *storePb.MetaMultVersions
		chainID      string
		metaStoreDB  protocol.DBHandle
		logger       protocol.Logger
		archiveChan  chan string
	}
	type args struct {
		src       string
		onlineFS  string
		dst       string
		archiveFS string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "copy-file",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  testDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				src:       srcFileName,
				onlineFS:  srcFileSystem,
				dst:       dstFileName,
				archiveFS: dstFileSystem,
			},
			wantErr: false,
		},
		{
			//模拟目标文件已存在的情况
			name: "copy-file-dst-exist",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  testDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				src:       srcFileName,
				onlineFS:  srcFileSystem,
				dst:       dstFileExistName,
				archiveFS: dstFileSystem,
			},
			wantErr: false,
		},
		{
			//模拟源文件不存在的情况
			name: "copy-file-src-not-exist",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  testDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				src:       srcFileNameNotExist,
				onlineFS:  srcFileSystem,
				dst:       dstFileExistName,
				archiveFS: dstFileSystem,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MetaDataDB{
				metaFSG:      tt.fields.metaFSG,
				bfdbFiles:    tt.fields.bfdbFiles,
				multVersions: tt.fields.multVersions,
				chainID:      tt.fields.chainID,
				metaStoreDB:  tt.fields.metaStoreDB,
				logger:       tt.fields.logger,
				archiveChan:  tt.fields.archiveChan,
				RWMutex:      sync.RWMutex{},
			}
			if err := m.copyFile(tt.args.src, tt.args.onlineFS, tt.args.dst, tt.args.archiveFS); (err != nil) != tt.wantErr {
				t.Errorf("copyFile() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

//创建不同状态的归档任务，用于测试归档自动机
//状态对应 fileArchiveAutomata 中的状态
func CreateFileCase(metaDB meta.MetaData, fileName string, fileSystem string, start, end uint64, status int) (string,
	int) {

	//err := metaDB.CreateNewFile("00000000000000000001",tmpMetadbOnline1,1)
	err := metaDB.CreateNewFile(fileName, fileSystem, start)
	if err != nil {
		fmt.Println("CreateNewFile error:", err)
	}
	//模拟bfdb，伪造一个文件
	filePath := fileSystem + "/" + testChainID + "/" + "bfdb/" + fileName + ".fdb"
	_, err = os.Create(filePath)
	if err != nil {
		fmt.Println("error,when create file,errInfo:", err)
	}
	//模拟bfdb,修改文件结束区块块高
	err = metaDB.SetFileEndHeight(fileName, end)
	if err != nil {
		fmt.Println("SetFileEndHeight error:", err)
	}

	//发起归档任务，第一次归档
	_, err = metaDB.DoArchive(start, end)
	if err != nil {
		fmt.Println("DoArchive error :", err)
	}

	//fileInfo, err := metaDB.Get(start)
	//if err != nil {
	//	fmt.Println("get fileInfo by height error :",err)
	//}
	//
	//fileInfo.Status = status

	return fileName, status

}

func CreateFileNoArchiveCase(metaDB meta.MetaData, fileName string, fileSystem string, start, end uint64, status int) (string,
	int) {

	//err := metaDB.CreateNewFile("00000000000000000001",tmpMetadbOnline1,1)
	err := metaDB.CreateNewFile(fileName, fileSystem, start)
	if err != nil {
		fmt.Println("CreateNewFile error:", err)
	}
	//模拟bfdb，伪造一个文件
	filePath := fileSystem + "/" + testChainID + "/" + "bfdb/" + fileName + ".fdb"
	_, err = os.Create(filePath)
	if err != nil {
		fmt.Println("error,when create file,errInfo:", err)
	}
	//模拟bfdb,修改文件结束区块块高
	err = metaDB.SetFileEndHeight(fileName, end)
	if err != nil {
		fmt.Println("SetFileEndHeight error:", err)
	}

	////发起归档任务，第一次归档
	//_, err = metaDB.DoArchive(start, end)
	//if err != nil {
	//	fmt.Println("DoArchive error :", err)
	//}

	//fileInfo, err := metaDB.Get(start)
	//if err != nil {
	//	fmt.Println("get fileInfo by height error :",err)
	//}
	//
	//fileInfo.Status = status

	return fileName, status

}

func TestMetaDataDB_fileArchiveAutomata(t *testing.T) {
	//1.创建文件系统
	//tmp/metadb/mnt_online1  /tmp/metadb/mnt_online2
	//tmp/metadb/mnt_archive1  /tmp/metadb/mnt_archive2
	initFileSystem("metadb", "online1")
	initFileSystem("metadb", "online2")

	initFileSystem("metadb", "archive1")
	initFileSystem("metadb", "archive2")

	defer umountFileSystem("metadb", "online1")
	defer umountFileSystem("metadb", "online2")
	defer umountFileSystem("metadb", "archive1")
	defer umountFileSystem("metadb", "archive2")

	defer destroyFileSystem("metadb")

	dbConfigTest := &leveldbprovider.LevelDbConfig{
		StorePath:       "test-metaDb",
		WriteBufferSize: 2,
	}
	levelConfig := &leveldbprovider.NewLevelDBOptions{
		ChainId:  "chain1",
		DbFolder: "test-fileArchiveAutomata",
		Config:   dbConfigTest,
		Logger:   &test.GoLogger{},
	}
	db := leveldbprovider.NewLevelDBHandle(levelConfig)
	defer db.Close()
	defer resetMetaDBData(db)

	//2.修改配置文件中，文件系统的配置,使用测试配置文件
	testConfig := getSqliteConfig()
	fmt.Println("before testConfig.BlockFileConfig =", testConfig.BlockFileConfig)
	testConfig.BlockFileConfig.ArchiveFileSystem = tmpMetadbArchive
	testConfig.BlockFileConfig.OnlineFileSystem = tmpMetadbOnline
	fmt.Println("after testConfig.BlockFileConfig =", testConfig.BlockFileConfig)

	//3.创建并启动元数据
	dataDb, err := NewMetaDataDB(*testConfig, testChainID, db, &test.GoLogger{})
	if err != nil {
		fmt.Println("metadb.NewMetaDataDb,error =", err)
		return
	}

	//4.模拟bfdb,创建元数据，发起归档，获得归档id和归档任务
	//模拟归档任务状态为 archiveStatusNotStart 的case
	notArchivedFileName, notArchivedFileStatus := CreateFileNoArchiveCase(dataDb, "00000000000000000001",
		tmpMetadbOnline1, 1, 100, fileStatusNotArchived)
	beginFileName, beginFileStatus := CreateFileNoArchiveCase(dataDb, "00000000000000000101",
		tmpMetadbOnline1, 101, 200, fileStatusBegin)
	copyStartFileName, copyStartFileStatus := CreateFileNoArchiveCase(dataDb, "00000000000000000201",
		tmpMetadbOnline1, 201, 300, fileStatusCopyStart)
	copyEndFileName, copyEndFileStatus := CreateFileNoArchiveCase(dataDb, "00000000000000000301",
		tmpMetadbOnline1, 301, 400, fileStatusCopyEnd)
	delStartFileName, delStartFileStatus := CreateFileNoArchiveCase(dataDb, "00000000000000000401",
		tmpMetadbOnline1, 401, 500, fileStatusDelStart)
	delEndFileName, delEndFileStatus := CreateFileNoArchiveCase(dataDb, "00000000000000000501",
		tmpMetadbOnline1, 501, 600, fileStatusDelEnd)
	endFileName, endFileStatus := CreateFileNoArchiveCase(dataDb, "00000000000000000601",
		tmpMetadbOnline1, 601, 700, fileStatusEnd)
	defaultFileName, defaultFileStatus := CreateFileNoArchiveCase(dataDb, "00000000000000000701",
		tmpMetadbOnline1, 701, 800, 5)

	type fields struct {
		metaFSG      *storePb.MetaFileSystem
		bfdbFiles    map[string]storePb.MetaFileInfo
		multVersions *storePb.MetaMultVersions
		chainID      string
		metaStoreDB  protocol.DBHandle
		logger       protocol.Logger
		archiveChan  chan string
	}
	type args struct {
		fileStatus int
		fileName   string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "file-archive-auto-mata-not-archived",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  db,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				fileName:   notArchivedFileName,
				fileStatus: notArchivedFileStatus,
			},
			wantErr: false,
		},
		{
			name: "file-archive-auto-mata-begin",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  db,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				fileName:   beginFileName,
				fileStatus: beginFileStatus,
			},
			wantErr: false,
		},
		{
			name: "file-archive-auto-mata-copy-start",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  db,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				fileName:   copyStartFileName,
				fileStatus: copyStartFileStatus,
			},
			wantErr: false,
		},
		{
			name: "file-archive-auto-mata-copy-end",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  db,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				fileName:   copyEndFileName,
				fileStatus: copyEndFileStatus,
			},
			wantErr: false,
		},
		{
			name: "file-archive-auto-mata-del-start",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  db,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				fileName:   delStartFileName,
				fileStatus: delStartFileStatus,
			},
			wantErr: false,
		},
		{
			name: "file-archive-auto-mata-del-end",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  db,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				fileName:   delEndFileName,
				fileStatus: delEndFileStatus,
			},
			wantErr: false,
		},
		{
			name: "file-archive-auto-mata-end",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  db,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				fileName:   endFileName,
				fileStatus: endFileStatus,
			},
			wantErr: false,
		},
		{
			name: "file-archive-auto-mata-default",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  db,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				fileName:   defaultFileName,
				fileStatus: defaultFileStatus,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MetaDataDB{
				metaFSG:      tt.fields.metaFSG,
				bfdbFiles:    tt.fields.bfdbFiles,
				multVersions: tt.fields.multVersions,
				chainID:      tt.fields.chainID,
				metaStoreDB:  tt.fields.metaStoreDB,
				logger:       tt.fields.logger,
				archiveChan:  tt.fields.archiveChan,
				RWMutex:      sync.RWMutex{},
			}
			m = dataDb.(*MetaDataDB)
			if err := m.fileArchiveAutomata(tt.args.fileStatus, tt.args.fileName); (err != nil) != tt.wantErr {
				t.Errorf("fileArchiveAutomata() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestMetaDataDB_getArchiveJobByID(t *testing.T) {
	initFileSystem("metadb", "online1")
	initFileSystem("metadb", "online2")

	initFileSystem("metadb", "archive1")
	initFileSystem("metadb", "archive2")

	defer umountFileSystem("metadb", "online1")
	defer umountFileSystem("metadb", "online2")
	defer umountFileSystem("metadb", "archive1")
	defer umountFileSystem("metadb", "archive2")

	defer destroyFileSystem("metadb")

	dbConfigTest := &leveldbprovider.LevelDbConfig{
		StorePath:       "test-metaDb",
		WriteBufferSize: 2,
	}
	levelConfig := &leveldbprovider.NewLevelDBOptions{
		ChainId:  "chain1",
		DbFolder: "test-getArchiveJobByID",
		Config:   dbConfigTest,
		Logger:   &test.GoLogger{},
	}
	db := leveldbprovider.NewLevelDBHandle(levelConfig)
	defer db.Close()
	defer resetMetaDBData(db)

	//2.修改配置文件中，文件系统的配置,使用测试配置文件
	testConfig := *getSqliteConfig()
	testConfig.BlockFileConfig.ArchiveFileSystem = tmpMetadbArchive
	testConfig.BlockFileConfig.OnlineFileSystem = tmpMetadbOnline

	//3.创建并启动元数据
	dataDb, err := NewMetaDataDB(testConfig, testChainID, db, &test.GoLogger{})
	if err != nil {
		fmt.Println("metadb.NewMetaDataDb,error =", err)
		return
	}

	//4.模拟bfdb,创建元数据
	//模拟归档任务状态为 archiveStatusNotStart 的case
	_, _ = CreateArchiveCase(dataDb, "00000000000000000001",
		tmpMetadbOnline1, 1, 100, archiveStatusNotStart)
	_, _ = CreateArchiveCase(dataDb, "00000000000000000101",
		tmpMetadbOnline1, 101, 200, archiveStatusNotStart)

	//5.发起归档任务，第一次归档
	archiveJobID, err := dataDb.DoArchive(101, 200)
	if err != nil {
		fmt.Println("DoArchive error :", err)
	}
	jobInfo := storePb.ArchiveJob{}
	for {
		jobInfo, err = dataDb.GetArchiveJobByID(archiveJobID)

		if jobInfo.Status != fileStatusEnd {
			time.Sleep(100 * time.Millisecond)
			continue
		}
		break
	}

	wantJobInfo := storePb.ArchiveJob{
		StartTime:   jobInfo.StartTime,
		EndTime:     jobInfo.EndTime,
		StartHeight: 101,
		EndHeight:   200,
		Status:      fileStatusEnd,
		Files:       []string{"00000000000000000101"},
	}

	type fields struct {
		metaFSG      *storePb.MetaFileSystem
		bfdbFiles    map[string]storePb.MetaFileInfo
		multVersions *storePb.MetaMultVersions
		chainID      string
		metaStoreDB  protocol.DBHandle
		logger       protocol.Logger
		archiveChan  chan string
	}
	type args struct {
		jobID string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    storePb.ArchiveJob
		wantErr bool
	}{
		{
			name: "get-archive-job-by-id",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  db,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				jobID: archiveJobID,
			},
			want:    wantJobInfo,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MetaDataDB{
				metaFSG:      tt.fields.metaFSG,
				bfdbFiles:    tt.fields.bfdbFiles,
				multVersions: tt.fields.multVersions,
				chainID:      tt.fields.chainID,
				metaStoreDB:  tt.fields.metaStoreDB,
				logger:       tt.fields.logger,
				archiveChan:  tt.fields.archiveChan,
				RWMutex:      sync.RWMutex{},
			}
			m = dataDb.(*MetaDataDB)
			got, err := m.getArchiveJobByID(tt.args.jobID)

			tt.want.StartTime = got.StartTime
			tt.want.EndTime = got.EndTime

			if (err != nil) != tt.wantErr {
				t.Errorf("getArchiveJobByID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("getArchiveJobByID() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMetaDataDB_getFileByHeightRange(t *testing.T) {
	initFileSystem("metadb", "online1")
	initFileSystem("metadb", "online2")

	initFileSystem("metadb", "archive1")
	initFileSystem("metadb", "archive2")

	defer umountFileSystem("metadb", "online1")
	defer umountFileSystem("metadb", "online2")
	defer umountFileSystem("metadb", "archive1")
	defer umountFileSystem("metadb", "archive2")

	defer destroyFileSystem("metadb")

	dbConfigTest := &leveldbprovider.LevelDbConfig{
		StorePath:       "test-metaDb",
		WriteBufferSize: 2,
	}
	levelConfig := &leveldbprovider.NewLevelDBOptions{
		ChainId:  "chain1",
		DbFolder: "test-getFilebyHeightRange",
		Config:   dbConfigTest,
		Logger:   &test.GoLogger{},
	}
	db := leveldbprovider.NewLevelDBHandle(levelConfig)
	defer db.Close()
	defer resetMetaDBData(db)

	//2.修改配置文件中，文件系统的配置,使用测试配置文件
	testConfig := *getSqliteConfig()
	testConfig.BlockFileConfig.ArchiveFileSystem = tmpMetadbArchive
	testConfig.BlockFileConfig.OnlineFileSystem = tmpMetadbOnline

	//3.创建并启动元数据
	dataDb, err := NewMetaDataDB(testConfig, testChainID, db, &test.GoLogger{})
	if err != nil {
		fmt.Println("metadb.NewMetaDataDb,error =", err)
		return
	}

	//4.模拟bfdb,创建元数据
	//模拟归档任务状态为 archiveStatusNotStart 的case
	file1 := "00000000000000000001"
	file2 := "00000000000000000101"

	_, _ = CreateArchiveCase(dataDb, file1,
		tmpMetadbOnline1, 1, 100, archiveStatusNotStart)
	_, _ = CreateArchiveCase(dataDb, file2,
		tmpMetadbOnline1, 101, 200, archiveStatusNotStart)

	type fields struct {
		metaFSG      *storePb.MetaFileSystem
		bfdbFiles    map[string]storePb.MetaFileInfo
		multVersions *storePb.MetaMultVersions
		chainID      string
		metaStoreDB  protocol.DBHandle
		logger       protocol.Logger
		archiveChan  chan string
	}
	type args struct {
		start uint64
		end   uint64
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []string
		wantErr bool
	}{
		{
			name: "get-file-by-height-range",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  db,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				start: 1,
				end:   150,
			},
			//want:    []string{file1, file2},
			want:    []string{file1},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MetaDataDB{
				metaFSG:      tt.fields.metaFSG,
				bfdbFiles:    tt.fields.bfdbFiles,
				multVersions: tt.fields.multVersions,
				chainID:      tt.fields.chainID,
				metaStoreDB:  tt.fields.metaStoreDB,
				logger:       tt.fields.logger,
				archiveChan:  tt.fields.archiveChan,
				RWMutex:      sync.RWMutex{},
			}
			m = dataDb.(*MetaDataDB)
			got, err := m.getFileByHeightRange(tt.args.start, tt.args.end)
			if (err != nil) != tt.wantErr {
				t.Errorf("getFilebyHeightRange() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			//对结果排序
			sort.Strings(got)
			sort.Strings(tt.want)

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("getFilebyHeightRange() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMetaDataDB_getLastVersion(t *testing.T) {
	getLastVersionDB := getDBHandle(globalTestChainID, "test-metadb", "metadb_getLasterVerson",
		&test.GoLogger{}, nil)
	resetMetaDBData(getLastVersionDB)

	version1 := &storePb.MetaMultVersions{
		Version: []*storePb.MetaVersion{
			{
				MajorVersion: 0,
				MinorVersion: 1,
				UpdateTime:   0,
			},
			{
				MajorVersion: 1,
				MinorVersion: 1,
				UpdateTime:   0,
			},
			{
				MajorVersion: 1,
				MinorVersion: 3,
				UpdateTime:   0,
			},
			{
				MajorVersion: 1,
				MinorVersion: 4,
				UpdateTime:   0,
			},
			{
				MajorVersion: 2,
				MinorVersion: 3,
				UpdateTime:   0,
			},
			{
				MajorVersion: 2,
				MinorVersion: 3,
				UpdateTime:   1,
			},
		},
	}

	type fields struct {
		metaFSG      *storePb.MetaFileSystem
		bfdbFiles    map[string]storePb.MetaFileInfo
		multVersions *storePb.MetaMultVersions
		chainID      string
		metaStoreDB  protocol.DBHandle
		logger       protocol.Logger
		archiveChan  chan string
	}
	type args struct {
		multVersions *storePb.MetaMultVersions
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *storePb.MetaVersion
	}{
		{
			name: "get-last-version",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: version1,
				chainID:      testChainID,
				metaStoreDB:  getLastVersionDB,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				multVersions: version1,
			},
			want: version1.Version[5],
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MetaDataDB{
				metaFSG:      tt.fields.metaFSG,
				bfdbFiles:    tt.fields.bfdbFiles,
				multVersions: tt.fields.multVersions,
				chainID:      tt.fields.chainID,
				metaStoreDB:  tt.fields.metaStoreDB,
				logger:       tt.fields.logger,
				archiveChan:  tt.fields.archiveChan,
				RWMutex:      sync.RWMutex{},
			}
			if got := m.getLastVersion(tt.args.multVersions); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("getLastVersion() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMetaDataDB_getMetaFSByVersion(t *testing.T) {
	initFileSystem("metadb", "online1")
	initFileSystem("metadb", "online2")

	initFileSystem("metadb", "archive1")
	initFileSystem("metadb", "archive2")

	defer umountFileSystem("metadb", "online1")
	defer umountFileSystem("metadb", "online2")
	defer umountFileSystem("metadb", "archive1")
	defer umountFileSystem("metadb", "archive2")

	defer destroyFileSystem("metadb")

	dbConfigTest := &leveldbprovider.LevelDbConfig{
		StorePath:       "test-metaDb",
		WriteBufferSize: 2,
	}
	levelConfig := &leveldbprovider.NewLevelDBOptions{
		ChainId:  "chain1",
		DbFolder: "test-getMetaFSByVersion",
		Config:   dbConfigTest,
		Logger:   &test.GoLogger{},
	}
	db := leveldbprovider.NewLevelDBHandle(levelConfig)
	defer db.Close()
	defer resetMetaDBData(db)

	//2.修改配置文件中，文件系统的配置,使用测试配置文件
	testConfig := *getSqliteConfig()
	//testConfig.BlockFileConfig.ArchiveFileSystem = tmpMetadbArchive
	//testConfig.BlockFileConfig.OnlineFileSystem = tmpMetadbOnline

	//3.创建并启动元数据
	dataDb, err := NewMetaDataDB(testConfig, testChainID, db, &test.GoLogger{})
	if err != nil {
		fmt.Println("metadb.NewMetaDataDb,error =", err)
		return
	}

	lastVersion := multVersionsTest.Version[0]
	testVersion1 := &storePb.MetaVersion{
		MajorVersion: 2,
		MinorVersion: 1,
		UpdateTime:   0,
	}
	testVersion2 := &storePb.MetaVersion{
		MajorVersion: 1,
		MinorVersion: 2,
		UpdateTime:   0,
	}
	//因为写入的版本信息中，包含了时间，时间是服务器当前运行时间，所以无法直接获得 getMetaFSByVersion
	wantMFS := &storePb.MetaFileSystem{}
	//lastVersion.UpdateTime = 0

	type fields struct {
		metaFSG      *storePb.MetaFileSystem
		bfdbFiles    map[string]storePb.MetaFileInfo
		multVersions *storePb.MetaMultVersions
		chainID      string
		metaStoreDB  protocol.DBHandle
		logger       protocol.Logger
		archiveChan  chan string
	}
	type args struct {
		config      conf.StorageConfig
		lastVersion *storePb.MetaVersion
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *storePb.MetaFileSystem
		wantErr bool
	}{
		{
			name: "get-meta-fs-by-version",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  testDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				config:      testConfig,
				lastVersion: lastVersion,
			},
			want:    wantMFS,
			wantErr: false,
		},
		{
			//模拟主版本号小
			name: "get-meta-fs-by-version-test1",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  testDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				config:      testConfig,
				lastVersion: testVersion1,
			},
			want:    wantMFS,
			wantErr: true,
		},
		{
			//模拟次版本号小
			name: "get-meta-fs-by-version-test2",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  testDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				config:      testConfig,
				lastVersion: testVersion2,
			},
			want:    wantMFS,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MetaDataDB{
				metaFSG:      tt.fields.metaFSG,
				bfdbFiles:    tt.fields.bfdbFiles,
				multVersions: tt.fields.multVersions,
				chainID:      tt.fields.chainID,
				metaStoreDB:  tt.fields.metaStoreDB,
				logger:       tt.fields.logger,
				archiveChan:  tt.fields.archiveChan,
				RWMutex:      sync.RWMutex{},
			}
			m = dataDb.(*MetaDataDB)
			got, err := m.getMetaFSByVersion(tt.args.config, tt.args.lastVersion)
			if (err != nil) != tt.wantErr {
				t.Errorf("getMetaFSByVersion() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("getMetaFSByVersion() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMetaDataDB_initMeta(t *testing.T) {
	dBHandler := getDBHandle(globalTestChainID, "test-metadb", "metadb-initMeta", &test.GoLogger{}, nil)
	resetMetaDBData(dBHandler)

	conf1 := *getSqliteConfig()

	conf2 := *getSqliteConfig()
	conf2.BlockFileConfig = nil

	conf3 := *getSqliteConfig()
	conf3.BlockFileConfig.OnlineFileSystem = "/tmp/online1,/tmp/online1"

	conf4 := *getSqliteConfig()
	conf4.BlockFileConfig.ArchiveFileSystem = "/tmp/archive1,/tmp/archive1"

	type fields struct {
		metaFSG      *storePb.MetaFileSystem
		bfdbFiles    map[string]storePb.MetaFileInfo
		multVersions *storePb.MetaMultVersions
		chainID      string
		metaStoreDB  protocol.DBHandle
		logger       protocol.Logger
		archiveChan  chan string
	}
	type args struct {
		config conf.StorageConfig
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "init-meta-setMetaFirstStart",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  dBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				config: conf1,
			},
			wantErr: false,
		},
		{
			//模拟 配置文件中 BlockFileConfig 为nil
			name: "init-meta-BlockFileConfig-is-nil",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  dBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				config: conf2,
			},
			wantErr: true,
		},
		{
			//模拟 配置文件中 onlineFS 配置有重复
			name: "init-meta-online-fs-repeated",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  dBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				config: conf3,
			},
			wantErr: true,
		},
		{
			//模拟 配置文件中 archiveFS 配置有重复
			name: "init-meta-archive-fs-repeated",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  dBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				config: conf4,
			},
			wantErr: true,
		},
		{
			name: "init-meta-setMetaUseConfigAndDB",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  dBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				config: conf1,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			//if tt.name == "init-meta-BlockFileConfig-is-nil" {
			//	tt.args.config.BlockFileConfig = nil
			//}
			//if tt.name == "init-meta-online-fs-repeated" {
			//	tt.args.config.BlockFileConfig.OnlineFileSystem = "/tmp/online1,/tmp/online2,/tmp/online1"
			//}
			//if tt.name == "init-meta-archive-fs-repeated" {
			//	tt.args.config.BlockFileConfig.ArchiveFileSystem = "/tmp/archive1,/tmp/archive2,/tmp/archive1"
			//}
			m := &MetaDataDB{
				metaFSG:      tt.fields.metaFSG,
				bfdbFiles:    tt.fields.bfdbFiles,
				multVersions: tt.fields.multVersions,
				chainID:      tt.fields.chainID,
				metaStoreDB:  tt.fields.metaStoreDB,
				logger:       tt.fields.logger,
				archiveChan:  tt.fields.archiveChan,
				RWMutex:      sync.RWMutex{},
			}
			if err := m.initMeta(tt.args.config); (err != nil) != tt.wantErr {
				t.Errorf("initMeta() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestMetaDataDB_loadMetaFileInfo(t *testing.T) {
	initFileSystem("metadb", "online1")
	initFileSystem("metadb", "online2")

	initFileSystem("metadb", "archive1")
	initFileSystem("metadb", "archive2")

	defer umountFileSystem("metadb", "online1")
	defer umountFileSystem("metadb", "online2")
	defer umountFileSystem("metadb", "archive1")
	defer umountFileSystem("metadb", "archive2")

	defer destroyFileSystem("metadb")

	dbConfigTest := &leveldbprovider.LevelDbConfig{
		StorePath:       "test-metaDb",
		WriteBufferSize: 2,
	}
	levelConfig := &leveldbprovider.NewLevelDBOptions{
		ChainId:  "chain1",
		DbFolder: "test-loadMetaFileInfo",
		Config:   dbConfigTest,
		Logger:   &test.GoLogger{},
	}
	db := leveldbprovider.NewLevelDBHandle(levelConfig)
	defer db.Close()
	defer resetMetaDBData(db)

	//2.修改配置文件中，文件系统的配置,使用测试配置文件
	testConfig := *getSqliteConfig()
	testConfig.BlockFileConfig.ArchiveFileSystem = tmpMetadbArchive
	testConfig.BlockFileConfig.OnlineFileSystem = tmpMetadbOnline

	//3.创建并启动元数据
	dataDb, err := NewMetaDataDB(testConfig, testChainID, db, &test.GoLogger{})
	if err != nil {
		fmt.Println("metadb.NewMetaDataDb,error =", err)
		return
	}

	//4.模拟bfdb,创建元数据
	//模拟归档任务状态为 archiveStatusNotStart 的case
	file1 := "00000000000000000001"
	file2 := "00000000000000000101"

	CreateArchiveCaseNoArchive(dataDb, file1,
		tmpMetadbOnline1, 1, 100, archiveStatusNotStart)
	CreateArchiveCaseNoArchive(dataDb, file2,
		tmpMetadbOnline1, 101, 200, archiveStatusNotStart)

	type fields struct {
		metaFSG      *storePb.MetaFileSystem
		bfdbFiles    map[string]storePb.MetaFileInfo
		multVersions *storePb.MetaMultVersions
		chainID      string
		metaStoreDB  protocol.DBHandle
		logger       protocol.Logger
		archiveChan  chan string
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			name: "load-meta-file-info",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  testDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MetaDataDB{
				metaFSG:      tt.fields.metaFSG,
				bfdbFiles:    tt.fields.bfdbFiles,
				multVersions: tt.fields.multVersions,
				chainID:      tt.fields.chainID,
				metaStoreDB:  tt.fields.metaStoreDB,
				logger:       tt.fields.logger,
				archiveChan:  tt.fields.archiveChan,
				RWMutex:      sync.RWMutex{},
			}
			m = dataDb.(*MetaDataDB)
			if err := m.loadMetaFileInfo(); (err != nil) != tt.wantErr {
				t.Errorf("loadMetaFileInfo() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestMetaDataDB_publishArchiveJob(t *testing.T) {
	//1.创建文件系统
	//tmp/metadb/mnt_online1  /tmp/metadb/mnt_online2
	//tmp/metadb/mnt_archive1  /tmp/metadb/mnt_archive2
	initFileSystem("metadb", "online1")
	initFileSystem("metadb", "online2")

	initFileSystem("metadb", "archive1")
	initFileSystem("metadb", "archive2")

	defer umountFileSystem("metadb", "online1")
	defer umountFileSystem("metadb", "online2")
	defer umountFileSystem("metadb", "archive1")
	defer umountFileSystem("metadb", "archive2")

	defer destroyFileSystem("metadb")

	dbConfigTest := &leveldbprovider.LevelDbConfig{
		StorePath:       "test-metaDb",
		WriteBufferSize: 2,
	}
	levelConfig := &leveldbprovider.NewLevelDBOptions{
		ChainId:  "chain1",
		DbFolder: "test-publishArchiveJob",
		Config:   dbConfigTest,
		Logger:   &test.GoLogger{},
	}
	db := leveldbprovider.NewLevelDBHandle(levelConfig)
	defer db.Close()
	defer resetMetaDBData(db)

	//2.修改配置文件中，文件系统的配置,使用测试配置文件
	testConfig := *getSqliteConfig()
	testConfig.BlockFileConfig.ArchiveFileSystem = tmpMetadbArchive
	testConfig.BlockFileConfig.OnlineFileSystem = tmpMetadbOnline

	//3.创建并启动元数据
	dataDb, err := NewMetaDataDB(testConfig, testChainID, db, &test.GoLogger{})
	if err != nil {
		fmt.Println("metadb.NewMetaDataDb,error =", err)
		return
	}

	//4.模拟bfdb,创建元数据
	CreateArchiveCaseSetEnd(dataDb, "00000000000000000001",
		tmpMetadbOnline1, 1, 100, archiveStatusNotStart)

	CreateArchiveCaseSetEnd(dataDb, "00000000000000000001",
		tmpMetadbOnline1, 101, 200, archiveStatusCopyStart)

	CreateArchiveCaseSetEnd(dataDb, "00000000000000000001",
		tmpMetadbOnline1, 201, 300, archiveStatusCopyStart)

	type fields struct {
		metaFSG      *storePb.MetaFileSystem
		bfdbFiles    map[string]storePb.MetaFileInfo
		multVersions *storePb.MetaMultVersions
		chainID      string
		metaStoreDB  protocol.DBHandle
		logger       protocol.Logger
		archiveChan  chan string
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			name: "publisth-archive-job",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  testDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MetaDataDB{
				metaFSG:      tt.fields.metaFSG,
				bfdbFiles:    tt.fields.bfdbFiles,
				multVersions: tt.fields.multVersions,
				chainID:      tt.fields.chainID,
				metaStoreDB:  tt.fields.metaStoreDB,
				logger:       tt.fields.logger,
				archiveChan:  tt.fields.archiveChan,
				RWMutex:      sync.RWMutex{},
			}
			m = dataDb.(*MetaDataDB)
			//发起3次归档任务
			_, _ = m.DoArchive(1, 2)
			_, _ = m.DoArchive(101, 102)
			_, _ = m.DoArchive(201, 202)

			if err := m.publishArchiveJob(); (err != nil) != tt.wantErr {
				t.Errorf("publishArchiveJob() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestMetaDataDB_removeFile(t *testing.T) {
	//创建文件系统，包括online和archive
	initFileSystem("metadb", "online1")
	defer umountFileSystem("metadb", "online1")
	defer destroyFileSystem("metadb")

	//对 /tmp/metadb/online1 写入文件
	mkdir := "/tmp/metadb/online1/" + testChainID + "/" + "bfdb/"
	cmd := exec.Command("mkdir", "-p", mkdir)
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()
	if err != nil {
		fmt.Println("start mkdir error :", err)
	}
	outStr, errStr := stdout.String(), stderr.String()
	fmt.Printf("out:%s\n", outStr)
	if errStr != "" {
		fmt.Printf("err:%s\n", errStr)
	}

	//对 /tmp/metadb/online1 写入文件
	file := "/tmp/metadb/online1/" + testChainID + "/" + "bfdb/" + newTmpFile + ".fdb"
	ofStr := "of=" + file
	cmd = exec.Command("dd", "if=/dev/zero", ofStr, "count=102400", "bs=1024")
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err = cmd.Run()
	if err != nil {
		fmt.Println("start create new_100M_file error :", err)
	}
	//outStr, errStr := string(stdout.Bytes()), string(stderr.Bytes())
	outStr, errStr = stdout.String(), stderr.String()
	fmt.Printf("out:%s\n", outStr)
	if errStr != "" {
		fmt.Printf("err:%s\n", errStr)
	}

	srcFileSystem := tmpMetadbOnline1
	srcFileName := newTmpFile

	type fields struct {
		metaFSG      *storePb.MetaFileSystem
		bfdbFiles    map[string]storePb.MetaFileInfo
		multVersions *storePb.MetaMultVersions
		chainID      string
		metaStoreDB  protocol.DBHandle
		logger       protocol.Logger
		archiveChan  chan string
	}
	type args struct {
		fileName string
		onlineFS string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "remove",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  testDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				fileName: srcFileName,
				onlineFS: srcFileSystem,
			},
			wantErr: false,
		},
		{
			name: "remove-file-not-exist",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  testDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				fileName: "test-file-not-exist",
				onlineFS: srcFileSystem,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MetaDataDB{
				metaFSG:      tt.fields.metaFSG,
				bfdbFiles:    tt.fields.bfdbFiles,
				multVersions: tt.fields.multVersions,
				chainID:      tt.fields.chainID,
				metaStoreDB:  tt.fields.metaStoreDB,
				logger:       tt.fields.logger,
				archiveChan:  tt.fields.archiveChan,
				RWMutex:      sync.RWMutex{},
			}
			if err := m.removeFile(tt.args.fileName, tt.args.onlineFS); (err != nil) != tt.wantErr {
				t.Errorf("removeFile() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestMetaDataDB_setArchiveJobByID(t *testing.T) {
	//1.创建文件系统
	//tmp/metadb/mnt_online1  /tmp/metadb/mnt_online2
	//tmp/metadb/mnt_archive1  /tmp/metadb/mnt_archive2
	initFileSystem("metadb", "online1")
	initFileSystem("metadb", "online2")

	initFileSystem("metadb", "archive1")
	initFileSystem("metadb", "archive2")

	defer umountFileSystem("metadb", "online1")
	defer umountFileSystem("metadb", "online2")
	defer umountFileSystem("metadb", "archive1")
	defer umountFileSystem("metadb", "archive2")

	defer destroyFileSystem("metadb")

	dbConfigTest := &leveldbprovider.LevelDbConfig{
		StorePath:       "test-metaDb",
		WriteBufferSize: 2,
	}
	levelConfig := &leveldbprovider.NewLevelDBOptions{
		ChainId:  "chain1",
		DbFolder: "test-setArchiveJobByID",
		Config:   dbConfigTest,
		Logger:   &test.GoLogger{},
	}
	db := leveldbprovider.NewLevelDBHandle(levelConfig)
	defer db.Close()
	defer resetMetaDBData(db)

	//2.修改配置文件中，文件系统的配置,使用测试配置文件
	testConfig := *getSqliteConfig()
	testConfig.BlockFileConfig.ArchiveFileSystem = tmpMetadbArchive
	testConfig.BlockFileConfig.OnlineFileSystem = tmpMetadbOnline

	//3.创建并启动元数据
	dataDb, err := NewMetaDataDB(testConfig, testChainID, db, &test.GoLogger{})
	if err != nil {
		fmt.Println("metadb.NewMetaDataDb,error =", err)
		return
	}

	//4.模拟bfdb,创建元数据，发起归档，获得归档id和归档任务
	//模拟归档任务状态为 archiveStatusNotStart 的case
	jobID, jobInfo := CreateArchiveCase(dataDb, "00000000000000000001",
		tmpMetadbOnline1, 1, 100, archiveStatusNotStart)

	type fields struct {
		metaFSG      *storePb.MetaFileSystem
		bfdbFiles    map[string]storePb.MetaFileInfo
		multVersions *storePb.MetaMultVersions
		chainID      string
		metaStoreDB  protocol.DBHandle
		logger       protocol.Logger
		archiveChan  chan string
	}
	type args struct {
		jobID      string
		archiveJob storePb.ArchiveJob
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "set-archive-job-by-id",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  testDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				jobID:      jobID,
				archiveJob: jobInfo,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MetaDataDB{
				metaFSG:      tt.fields.metaFSG,
				bfdbFiles:    tt.fields.bfdbFiles,
				multVersions: tt.fields.multVersions,
				chainID:      tt.fields.chainID,
				metaStoreDB:  tt.fields.metaStoreDB,
				logger:       tt.fields.logger,
				archiveChan:  tt.fields.archiveChan,
				RWMutex:      sync.RWMutex{},
			}
			m = dataDb.(*MetaDataDB)
			if err := m.setArchiveJobByID(tt.args.jobID, tt.args.archiveJob); (err != nil) != tt.wantErr {
				t.Errorf("setArchiveJobByID() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestMetaDataDB_setFileStatus(t *testing.T) {
	initFileSystem("metadb", "online1")
	initFileSystem("metadb", "online2")

	initFileSystem("metadb", "archive1")
	initFileSystem("metadb", "archive2")

	defer umountFileSystem("metadb", "online1")
	defer umountFileSystem("metadb", "online2")
	defer umountFileSystem("metadb", "archive1")
	defer umountFileSystem("metadb", "archive2")

	defer destroyFileSystem("metadb")

	dbConfigTest := &leveldbprovider.LevelDbConfig{
		StorePath:       "test-metaDb",
		WriteBufferSize: 2,
	}
	levelConfig := &leveldbprovider.NewLevelDBOptions{
		ChainId:  "chain1",
		DbFolder: "test-setFileStatus",
		Config:   dbConfigTest,
		Logger:   &test.GoLogger{},
	}
	db := leveldbprovider.NewLevelDBHandle(levelConfig)
	//defer db.Close()
	defer resetMetaDBData(db)

	//2.修改配置文件中，文件系统的配置,使用测试配置文件
	testConfig := *getSqliteConfig()
	testConfig.BlockFileConfig.ArchiveFileSystem = tmpMetadbArchive
	testConfig.BlockFileConfig.OnlineFileSystem = tmpMetadbOnline

	//3.创建并启动元数据
	dataDb, err := NewMetaDataDB(testConfig, testChainID, db, &test.GoLogger{})
	if err != nil {
		fmt.Println("metadb.NewMetaDataDb,error =", err)
		return
	}

	//4.模拟bfdb,创建元数据，发起归档，获得归档id和归档任务
	//模拟归档任务状态为 archiveStatusNotStart 的case
	fileName, fileStatus := CreateFileCase(dataDb, "00000000000000000001",
		tmpMetadbOnline1, 1, 100, fileStatusCopyEnd)

	file2 := "file2-not-exist-in-set"

	type fields struct {
		metaFSG      *storePb.MetaFileSystem
		bfdbFiles    map[string]storePb.MetaFileInfo
		multVersions *storePb.MetaMultVersions
		chainID      string
		metaStoreDB  protocol.DBHandle
		logger       protocol.Logger
		archiveChan  chan string
	}
	type args struct {
		fileName  string
		newStatus int32
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "set-file-status",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  testDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				fileName:  fileName,
				newStatus: int32(fileStatus),
			},
			wantErr: false,
		},
		{
			name: "set-file-status-file-not-exist",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  testDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				fileName:  file2,
				newStatus: int32(fileStatus),
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MetaDataDB{
				metaFSG:      tt.fields.metaFSG,
				bfdbFiles:    tt.fields.bfdbFiles,
				multVersions: tt.fields.multVersions,
				chainID:      tt.fields.chainID,
				metaStoreDB:  tt.fields.metaStoreDB,
				logger:       tt.fields.logger,
				archiveChan:  tt.fields.archiveChan,
				RWMutex:      sync.RWMutex{},
			}
			m = dataDb.(*MetaDataDB)
			if err := m.setFileStatus(tt.args.fileName, tt.args.newStatus); (err != nil) != tt.wantErr {
				t.Errorf("setFileStatus() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestMetaDataDB_setMetaFirstStart(t *testing.T) {
	type fields struct {
		metaFSG      *storePb.MetaFileSystem
		bfdbFiles    map[string]storePb.MetaFileInfo
		multVersions *storePb.MetaMultVersions
		chainID      string
		metaStoreDB  protocol.DBHandle
		logger       protocol.Logger
		archiveChan  chan string
	}
	type args struct {
		config conf.StorageConfig
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "set-meta-first-start-test1",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  testDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				config: *getSqliteConfig(),
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MetaDataDB{
				metaFSG:      tt.fields.metaFSG,
				bfdbFiles:    tt.fields.bfdbFiles,
				multVersions: tt.fields.multVersions,
				chainID:      tt.fields.chainID,
				metaStoreDB:  tt.fields.metaStoreDB,
				logger:       tt.fields.logger,
				archiveChan:  tt.fields.archiveChan,
				RWMutex:      sync.RWMutex{},
			}
			//删除所有数据
			resetMetaDBData(m.metaStoreDB)

			if err := m.setMetaFirstStart(tt.args.config); (err != nil) != tt.wantErr {
				t.Errorf("setMetaFirstStart() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestMetaDataDB_setMetaUseConfigAndDB(t *testing.T) {
	setMetaUseConfigAndDBHandler := getDBHandle(globalTestChainID, "test-metadb", "metadb-setMetaUseConfigAndDB", &test.GoLogger{}, nil)
	resetMetaDBData(setMetaUseConfigAndDBHandler)

	//testConfig := getSqliteConfig()

	//修改配置，配置文件由 "/tmp/archive1,/tmp/archive2" => "/tmp/archive1,/tmp/archive3"
	testConfigErr := getSqliteConfig()
	testConfigErr.BlockFileConfig.ArchiveFileSystem = "/tmp/archive1,/tmp/archive3"

	//修改配置，让文件系统增加 配置文件由 "/tmp/archive1,/tmp/archive2" => "/tmp/archive1,archive2,archive3"
	testConfigAdd := getSqliteConfig()
	testConfigAdd.BlockFileConfig.ArchiveFileSystem = "/tmp/archive1,/tmp/archive2,/tmp/archive3"

	//修改配置，让文件系统变少 配置文件由 "/tmp/archive1,/tmp/archive2" => "/tmp/archive1"
	testConfigLess := getSqliteConfig()
	testConfigLess.BlockFileConfig.ArchiveFileSystem = "/tmp/archive1"

	type fields struct {
		metaFSG      *storePb.MetaFileSystem
		bfdbFiles    map[string]storePb.MetaFileInfo
		multVersions *storePb.MetaMultVersions
		chainID      string
		metaStoreDB  protocol.DBHandle
		logger       protocol.Logger
		archiveChan  chan string
	}
	type args struct {
		config       conf.StorageConfig
		multVersions *storePb.MetaMultVersions
		metaFS       *storePb.MetaFileSystem
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		//{
		//	//配置文件没有被错误修改
		//	name: "set-Meta-Use-Config-And-DB",
		//	fields: fields{
		//		metaFSG:      metaFSGTest,
		//		bfdbFiles:    bfdbFilesTest,
		//		multVersions: multVersionsTest,
		//		chainID:      testChainID,
		//		metaStoreDB:  setMetaUseConfigAndDBHandler,
		//		logger:       testLogger,
		//		archiveChan:  make(chan string, 1000),
		//	},
		//	args: args{
		//		config:       *testConfig,
		//		multVersions: multVersionsTest,
		//		metaFS:       metaFSGTest,
		//	},
		//	wantErr: false,
		//},
		//{
		//	//配置文件 被错误修改
		//	name: "set-Meta-Use-Config-And-DB-error",
		//	fields: fields{
		//		metaFSG:      metaFSGTest,
		//		bfdbFiles:    bfdbFilesTest,
		//		multVersions: multVersionsTest,
		//		chainID:      testChainID,
		//		metaStoreDB:  setMetaUseConfigAndDBHandler,
		//		logger:       testLogger,
		//		archiveChan:  make(chan string, 1000),
		//	},
		//	args: args{
		//		config:       *testConfigErr,
		//		multVersions: multVersionsTest,
		//		metaFS:       metaFSGTest,
		//	},
		//	wantErr: false,
		//},
		{
			//配置文件 增加了一个版本
			name: "set-Meta-Use-Config-And-DB-Add",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  setMetaUseConfigAndDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				config:       *testConfigAdd,
				multVersions: multVersionsTest,
				metaFS:       metaFSGTest,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MetaDataDB{
				metaFSG:      tt.fields.metaFSG,
				bfdbFiles:    tt.fields.bfdbFiles,
				multVersions: tt.fields.multVersions,
				chainID:      tt.fields.chainID,
				metaStoreDB:  tt.fields.metaStoreDB,
				logger:       tt.fields.logger,
				archiveChan:  tt.fields.archiveChan,
				RWMutex:      sync.RWMutex{},
			}
			if err := m.setMetaUseConfigAndDB(tt.args.config, tt.args.multVersions, tt.args.metaFS); (err != nil) != tt.wantErr {
				t.Errorf("setMetaUseConfigAndDB() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestMetaDataDB_setNewVersion(t *testing.T) {

	testConfig := *getSqliteConfig()

	type fields struct {
		metaFSG      *storePb.MetaFileSystem
		bfdbFiles    map[string]storePb.MetaFileInfo
		multVersions *storePb.MetaMultVersions
		chainID      string
		metaStoreDB  protocol.DBHandle
		logger       protocol.Logger
		archiveChan  chan string
	}
	type args struct {
		config       conf.StorageConfig
		multVersions *storePb.MetaMultVersions
		metaFS       *storePb.MetaFileSystem
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "set-new-version",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  testDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				config:       testConfig,
				multVersions: multVersionsTest,
				metaFS:       metaFSGTest,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MetaDataDB{
				metaFSG:      tt.fields.metaFSG,
				bfdbFiles:    tt.fields.bfdbFiles,
				multVersions: tt.fields.multVersions,
				chainID:      tt.fields.chainID,
				metaStoreDB:  tt.fields.metaStoreDB,
				logger:       tt.fields.logger,
				archiveChan:  tt.fields.archiveChan,
			}
			if err := m.setNewVersion(tt.args.config, tt.args.multVersions, tt.args.metaFS); (err != nil) != tt.wantErr {
				t.Errorf("setNewVersion() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestMetaDataDB_updateArchiveJobStatus(t *testing.T) {
	//1.创建文件系统
	//tmp/metadb/mnt_online1  /tmp/metadb/mnt_online2
	//tmp/metadb/mnt_archive1  /tmp/metadb/mnt_archive2
	initFileSystem("metadb", "online1")
	initFileSystem("metadb", "online2")

	initFileSystem("metadb", "archive1")
	initFileSystem("metadb", "archive2")

	defer umountFileSystem("metadb", "online1")
	defer umountFileSystem("metadb", "online2")
	defer umountFileSystem("metadb", "archive1")
	defer umountFileSystem("metadb", "archive2")

	defer destroyFileSystem("metadb")

	dbConfigTest := &leveldbprovider.LevelDbConfig{
		StorePath:       "test-metaDb",
		WriteBufferSize: 2,
	}
	levelConfig := &leveldbprovider.NewLevelDBOptions{
		ChainId:  "chain1",
		DbFolder: "test-updateArchiveJobStatus",
		Config:   dbConfigTest,
		Logger:   &test.GoLogger{},
	}
	db := leveldbprovider.NewLevelDBHandle(levelConfig)
	//defer db.Close()
	defer resetMetaDBData(db)

	//2.修改配置文件中，文件系统的配置,使用测试配置文件
	testConfig := *getSqliteConfig()
	testConfig.BlockFileConfig.ArchiveFileSystem = tmpMetadbArchive
	testConfig.BlockFileConfig.OnlineFileSystem = tmpMetadbOnline

	//3.创建并启动元数据
	dataDb, err := NewMetaDataDB(testConfig, testChainID, db, &test.GoLogger{})
	if err != nil {
		fmt.Println("metadb.NewMetaDataDb,error =", err)
		return
	}

	//4.模拟bfdb,创建元数据，发起归档，获得归档id和归档任务
	//模拟归档任务状态为 archiveStatusNotStart 的case
	jobID, _ := CreateArchiveCase(dataDb, "00000000000000000001",
		tmpMetadbOnline1, 1, 100, archiveStatusNotStart)

	type fields struct {
		metaFSG      *storePb.MetaFileSystem
		bfdbFiles    map[string]storePb.MetaFileInfo
		multVersions *storePb.MetaMultVersions
		chainID      string
		metaStoreDB  protocol.DBHandle
		logger       protocol.Logger
		archiveChan  chan string
	}
	type args struct {
		jobID     string
		newStatus int32
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "update-archive-job-status",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  testDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				jobID:     jobID,
				newStatus: archiveStatusCopyStart,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MetaDataDB{
				metaFSG:      tt.fields.metaFSG,
				bfdbFiles:    tt.fields.bfdbFiles,
				multVersions: tt.fields.multVersions,
				chainID:      tt.fields.chainID,
				metaStoreDB:  tt.fields.metaStoreDB,
				logger:       tt.fields.logger,
				archiveChan:  tt.fields.archiveChan,
				RWMutex:      sync.RWMutex{},
			}
			if err := m.updateArchiveJobStatus(tt.args.jobID, tt.args.newStatus); (err != nil) != tt.wantErr {
				t.Errorf("updateArchiveJobStatus() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestNewMetaDataDB(t *testing.T) {
	type args struct {
		config   conf.StorageConfig
		chainID  string
		dbHandle protocol.DBHandle
		logger   protocol.Logger
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			//如果从meta中能获得链id，说明成功
			name: "new-meta-data-db",
			args: args{
				config:   *getSqliteConfig(),
				chainID:  testChainID,
				dbHandle: getDBHandle(testChainID, "test", "test", testLogger, nil),
				logger:   testLogger,
			},
			want:    testChainID,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			metaDB, err := NewMetaDataDB(tt.args.config, tt.args.chainID, tt.args.dbHandle, tt.args.logger)
			got := metaDB.GetChainID()
			if (err != nil) != tt.wantErr {
				t.Errorf("NewMetaDataDB() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewMetaDataDB() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_newCrc(t *testing.T) {
	type args struct {
		b []byte
	}
	tests := []struct {
		name string
		args args
		want CRC
	}{
		{
			name: "new-crc",
			args: args{
				b: []byte("aaa"),
			},
			want: 3818383321,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := newCrc(tt.args.b); got != tt.want {
				t.Errorf("newCrc() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_NewCrcFile(t *testing.T) {
	type args struct {
		c        CRC
		fileName string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "new-crc-file",
			args: args{
				c:        3818383321,
				fileName: "test_crc_file",
			},
			wantErr: false,
		},
		{
			name: "new-crc-file-exist",
			args: args{
				c:        3818383321,
				fileName: "test_crc_file",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := NewCrcFile(tt.args.c, tt.args.fileName); (err != nil) != tt.wantErr {
				t.Errorf("newCrcFile() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestMetaDataDB_mkdirArchiveFS(t *testing.T) {
	//1.创建文件系统
	//tmp/metadb/mnt_online1  /tmp/metadb/mnt_online2
	//tmp/metadb/mnt_archive1  /tmp/metadb/mnt_archive2

	initFileSystem("metadb", "online1")
	initFileSystem("metadb", "online2")

	initFileSystem("metadb", "archive1")
	initFileSystem("metadb", "archive2")

	defer umountFileSystem("metadb", "online1")
	defer umountFileSystem("metadb", "online2")
	defer umountFileSystem("metadb", "archive1")
	defer umountFileSystem("metadb", "archive2")

	defer destroyFileSystem("metadb")

	dbConfigTest := &leveldbprovider.LevelDbConfig{
		StorePath:       "test-metaDb",
		WriteBufferSize: 2,
	}
	levelConfig := &leveldbprovider.NewLevelDBOptions{
		ChainId:  "chain1",
		DbFolder: "test-mkdirArchiveFS",
		Config:   dbConfigTest,
		Logger:   &test.GoLogger{},
	}
	db := leveldbprovider.NewLevelDBHandle(levelConfig)
	//defer db.Close()
	defer resetMetaDBData(db)

	//2.修改配置文件中，文件系统的配置,使用测试配置文件
	testConfig := *getSqliteConfig()
	testConfig.BlockFileConfig.ArchiveFileSystem = tmpMetadbArchive
	testConfig.BlockFileConfig.OnlineFileSystem = tmpMetadbOnline

	//3.创建并启动元数据
	dataDb, err := NewMetaDataDB(testConfig, testChainID, db, &test.GoLogger{})
	if err != nil {
		fmt.Println("metadb.NewMetaDataDb,error =", err)
		return
	}

	type fields struct {
		metaFSG      *storePb.MetaFileSystem
		bfdbFiles    map[string]storePb.MetaFileInfo
		multVersions *storePb.MetaMultVersions
		chainID      string
		metaStoreDB  protocol.DBHandle
		logger       protocol.Logger
		archiveChan  chan string
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			name: "mkdir-archive-fs",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  testDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			wantErr: false,
		},
		//{
		//	name: "mkdir-archive-fs-path-not-exist",
		//	fields: fields{
		//		metaFSG:      metaFSGnotExist,
		//		bfdbFiles:    bfdbFilesTest,
		//		multVersions: multVersionsTest,
		//		chainID:      testChainID,
		//		metaStoreDB:  testDBHandler,
		//		logger:       testLogger,
		//		archiveChan:  make(chan string, 1000),
		//	},
		//	wantErr: true,
		//},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MetaDataDB{
				metaFSG:      tt.fields.metaFSG,
				bfdbFiles:    tt.fields.bfdbFiles,
				multVersions: tt.fields.multVersions,
				chainID:      tt.fields.chainID,
				metaStoreDB:  tt.fields.metaStoreDB,
				logger:       tt.fields.logger,
				archiveChan:  tt.fields.archiveChan,
				RWMutex:      sync.RWMutex{},
			}
			//panic("")
			//模拟创建错误的文件夹
			if tt.name != "mkdir-archive-fs-path-not-exist" {
				m = dataDb.(*MetaDataDB)
			}
			if err := m.mkdirOnlineArchiveFS(); (err != nil) != tt.wantErr {
				t.Errorf("mkdirArchiveFS() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestMetaDataDB_setFileArchiveFS(t *testing.T) {
	//1.创建文件系统
	//tmp/metadb/mnt_online1  /tmp/metadb/mnt_online2
	//tmp/metadb/mnt_archive1  /tmp/metadb/mnt_archive2
	initFileSystem("metadb", "online1")
	initFileSystem("metadb", "online2")

	initFileSystem("metadb", "archive1")
	initFileSystem("metadb", "archive2")

	defer umountFileSystem("metadb", "online1")
	defer umountFileSystem("metadb", "online2")
	defer umountFileSystem("metadb", "archive1")
	defer umountFileSystem("metadb", "archive2")

	defer destroyFileSystem("metadb")

	dbConfigTest := &leveldbprovider.LevelDbConfig{
		StorePath:       "test-metaDb",
		WriteBufferSize: 2,
	}
	levelConfig := &leveldbprovider.NewLevelDBOptions{
		ChainId:  "chain1",
		DbFolder: "test-setFileArchiveFS",
		Config:   dbConfigTest,
		Logger:   &test.GoLogger{},
	}
	db := leveldbprovider.NewLevelDBHandle(levelConfig)
	//defer db.Close()
	defer resetMetaDBData(db)

	//2.修改配置文件中，文件系统的配置,使用测试配置文件
	testConfig := *getSqliteConfig()
	testConfig.BlockFileConfig.ArchiveFileSystem = tmpMetadbArchive
	testConfig.BlockFileConfig.OnlineFileSystem = tmpMetadbOnline

	//3.创建并启动元数据
	dataDb, err := NewMetaDataDB(testConfig, testChainID, db, &test.GoLogger{})
	if err != nil {
		fmt.Println("metadb.NewMetaDataDb,error =", err)
		return
	}
	//4.模拟bfdb,创建元数据，发起归档，获得归档id和归档任务
	//模拟归档任务状态为 archiveStatusNotStart 的case
	file1 := "00000000000000000001"
	_, _ = CreateArchiveCase(dataDb, file1,
		tmpMetadbOnline1, 1, 100, archiveStatusNotStart)
	file2 := "file-not-exist-in-set"

	type fields struct {
		metaFSG      *storePb.MetaFileSystem
		bfdbFiles    map[string]storePb.MetaFileInfo
		multVersions *storePb.MetaMultVersions
		chainID      string
		metaStoreDB  protocol.DBHandle
		logger       protocol.Logger
		archiveChan  chan string
	}
	type args struct {
		fileName  string
		archiveFS string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "set-file-archive-FS",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  testDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				fileName:  file1,
				archiveFS: "/tmp/metadb/archive1",
			},
			wantErr: false,
		},
		{
			//模拟文件不存在的情况
			name: "set-file-archive-FS-not-exist",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  testDBHandler,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				fileName:  file2,
				archiveFS: "/tmp/metadb/archive1",
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MetaDataDB{
				metaFSG:      tt.fields.metaFSG,
				bfdbFiles:    tt.fields.bfdbFiles,
				multVersions: tt.fields.multVersions,
				chainID:      tt.fields.chainID,
				metaStoreDB:  tt.fields.metaStoreDB,
				logger:       tt.fields.logger,
				archiveChan:  tt.fields.archiveChan,
				RWMutex:      sync.RWMutex{},
			}
			m = dataDb.(*MetaDataDB)
			if err := m.setFileArchiveFS(tt.args.fileName, tt.args.archiveFS); (err != nil) != tt.wantErr {
				t.Errorf("setFileArchiveFS() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestMetaDataDB_calcArchiveFSValidity(t *testing.T) {
	//1.创建文件系统
	//tmp/metadb/mnt_online1  /tmp/metadb/mnt_online2
	//tmp/metadb/mnt_archive1  /tmp/metadb/mnt_archive2
	initFileSystem("metadb", "online1")
	initFileSystem("metadb", "online2")

	initFileSystem("metadb", "archive1")
	initFileSystem("metadb", "archive2")

	defer umountFileSystem("metadb", "online1")
	defer umountFileSystem("metadb", "online2")
	defer umountFileSystem("metadb", "archive1")
	defer umountFileSystem("metadb", "archive2")

	defer destroyFileSystem("metadb")

	dbConfigTest := &leveldbprovider.LevelDbConfig{
		StorePath:       "test-metaDb",
		WriteBufferSize: 2,
	}
	levelConfig := &leveldbprovider.NewLevelDBOptions{
		ChainId:  "chain1",
		DbFolder: "test-calcArchiveFSvalidity",
		Config:   dbConfigTest,
		Logger:   &test.GoLogger{},
	}
	db := leveldbprovider.NewLevelDBHandle(levelConfig)
	defer db.Close()
	defer resetMetaDBData(db)

	//2.修改配置文件中，文件系统的配置,使用测试配置文件
	testConfig := getSqliteConfig()
	fmt.Println("before testConfig.BlockFileConfig =", testConfig.BlockFileConfig)
	testConfig.BlockFileConfig.ArchiveFileSystem = tmpMetadbArchive
	testConfig.BlockFileConfig.OnlineFileSystem = tmpMetadbOnline
	fmt.Println("after testConfig.BlockFileConfig =", testConfig.BlockFileConfig)

	//3.创建并启动元数据
	dataDb, err := NewMetaDataDB(*testConfig, testChainID, db, &test.GoLogger{})
	if err != nil {
		fmt.Println("metadb.NewMetaDataDb,error =", err)
		return
	}

	//4.模拟bfdb,创建元数据，发起归档，获得归档id和归档任务
	//模拟归档任务状态为 archiveStatusNotStart 的case
	_, _ = CreateFileNoArchiveCase(dataDb, "00000000000000000001",
		tmpMetadbOnline1, 1, 100, fileStatusNotArchived)
	beginFileName, beginFileStatus := CreateFileNoArchiveCase(dataDb, "00000000000000000101",
		tmpMetadbOnline1, 101, 200, fileStatusBegin)

	beginOnlineFS := tmpMetadbOnline1
	beginArchiveFS := "/tmp/metadb/archive1"

	type fields struct {
		metaFSG      *storePb.MetaFileSystem
		bfdbFiles    map[string]storePb.MetaFileInfo
		multVersions *storePb.MetaMultVersions
		chainID      string
		metaStoreDB  protocol.DBHandle
		logger       protocol.Logger
		archiveChan  chan string
	}
	type args struct {
		fileName    string
		fileStatus  int
		onlineFS    string
		availableFS string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		want1   string
		want2   string
		wantErr bool
	}{
		{
			name: "calcArchiveFSvalidity",
			fields: fields{
				metaFSG:      metaFSGTest,
				bfdbFiles:    bfdbFilesTest,
				multVersions: multVersionsTest,
				chainID:      testChainID,
				metaStoreDB:  db,
				logger:       testLogger,
				archiveChan:  make(chan string, 1000),
			},
			args: args{
				fileName:    beginFileName,
				fileStatus:  beginFileStatus,
				onlineFS:    beginOnlineFS,
				availableFS: beginArchiveFS,
			},
			want:    fileStatusCopyStart,
			want1:   beginOnlineFS,
			want2:   beginArchiveFS,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MetaDataDB{
				metaFSG:      tt.fields.metaFSG,
				bfdbFiles:    tt.fields.bfdbFiles,
				multVersions: tt.fields.multVersions,
				chainID:      tt.fields.chainID,
				metaStoreDB:  tt.fields.metaStoreDB,
				logger:       tt.fields.logger,
				archiveChan:  tt.fields.archiveChan,
				RWMutex:      sync.RWMutex{},
			}
			m = dataDb.(*MetaDataDB)
			got, got1, got2, err := m.calcArchiveFSValidity(tt.args.fileName, tt.args.fileStatus, tt.args.onlineFS, tt.args.availableFS)
			if (err != nil) != tt.wantErr {
				t.Errorf("calcArchiveFSvalidity() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("calcArchiveFSvalidity() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("calcArchiveFSvalidity() got1 = %v, want %v", got1, tt.want1)
			}
			if got2 != tt.want2 {
				t.Errorf("calcArchiveFSvalidity() got2 = %v, want %v", got2, tt.want2)
			}
		})
	}
}
