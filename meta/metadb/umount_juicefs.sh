#删除文件系统
#创建文件系统 用于测试使用
#创建了 文件系统 $1 为 目录名， $2 为volume 名
#主要用于测试 多文件系统环境下 metadb 和 bfdb 的功能
#在 /tmp 目录下创建
testdir="$1"
volume="$2"
currdir=""
function destroy_fs() {
    currdir=`pwd`
    cd /tmp
    cd $testdir
    #umount mnt_${volume}
    umount ${volume}

    rm -rf /var/jfs/${volume}
    rm -rf /tmp/$testdir/myjfs_${volume}.db
    cd $currdir

}
destroy_fs
