#创建文件系统 用于测试使用
#创建了 文件系统 $1 为 目录名， $2 为volume 名
#主要用于测试 多文件系统环境下 metadb 和 bfdb 的功能
#在 /tmp 目录下创建
testdir="$1"
volume="$2"
currdir=""
function make_fs() {
    currdir=`pwd`
    cd "/tmp"
    mkdir -p $testdir
    cd $testdir
    mkdir $volume
    return
}
make_fs

