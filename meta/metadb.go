package meta

import "chainmaker.org/chainmaker/pb-go/v3/store"

// MetaData provides handle to meta
// @Description:
type MetaData interface {
	// GetMetaFS return meta file system which include online file system and archive file system
	GetMetaFS() (store.MetaFileSystem, error)

	// GetChainID return meta's chain ID
	GetChainID() string

	// Get return meta file info by given entry index
	Get(index uint64) (store.MetaFileInfo, error)

	// GetFileInfoByHeight return the filename,file info,file exist or not, error by entry index
	GetFileInfoByHeight(index uint64) (string, store.MetaFileInfo, bool, error)

	// CreateNewFile  create a new wal/bfdb file ,when wal/bfdb need create new file
	CreateNewFile(fileName string, fileSystem string, startIndex uint64) error

	// SetFileEndHeight update file info, when  wal/bfdb close file and create next new file
	SetFileEndHeight(fileName string, endHeight uint64) error

	// GetFileInfoByFileName return meta file info by file name
	GetFileInfoByFileName(fileName string) (store.MetaFileInfo, error)

	// IsArchive return true or false if file is archived or not
	IsArchive(fileName string) (bool, error)

	// GetLastFileInfo return the lastest created file's info
	GetLastFileInfo() (string, store.MetaFileInfo, bool, error)

	// ReadArchiveFileDataByOffset  read archived file by offset and fileName
	ReadArchiveFileDataByOffset(fileName string, b []byte, off int64) (int, error)

	// DoHotColdDataSeparation create a new task , that move cold block data to archive file system
	DoHotColdDataSeparation(startHeight uint64, endHeight uint64) (string, error)

	// DoArchive  create a new archive task
	DoArchive(startHeight uint64, endHeight uint64) (string, error)

	// GetArchiveJobByID return archive job info
	GetArchiveJobByID(jobID string) (store.ArchiveJob, error)
}
