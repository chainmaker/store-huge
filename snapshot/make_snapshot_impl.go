package snapshot

import (
	"errors"
	"fmt"
	"os"
	"path"
	"path/filepath"
	"sync"

	"chainmaker.org/chainmaker/pb-go/v3/common"
	"chainmaker.org/chainmaker/protocol/v3"
	"chainmaker.org/chainmaker/store-huge/v3/blockdb"
	"chainmaker.org/chainmaker/store-huge/v3/conf"
	"chainmaker.org/chainmaker/store-huge/v3/contracteventdb"
	"chainmaker.org/chainmaker/store-huge/v3/meta/metadb"
	"chainmaker.org/chainmaker/store-huge/v3/statedb"
	leveldbProvider "chainmaker.org/chainmaker/store-leveldb/v3"
)

const (
	stateKeyPre           = 's'
	txIDKeyPre            = 't'
	blockTxIDIdxKeyPrefix = 'b'
	configBlockKey        = 'c'
	snapshotDBFolder      = "snapshotDB"
	snapshotDir           = "snapshot"
)

//nolint
var (
	errSnapshotHeight  = errors.New("err,snapshot height is less then current height")
	errSnapshotIsBeing = errors.New("err,a snapshot is being processed")
)

//todo: 支持多链，每个链对应一个文件快照
//todo: txidSet,configBlock,statedb keys/values use different namespace in leveldb
//  block :   'b'
//  configBlock :   'c'
//  statedb :   's'

// Snapshot a snapshot ,the implementation
//todo: 快照中要包含最后写入的区块，要利用这个区块，写到bf(wal)中，否则 wal的 segment中的entry 下标从0开始，和区块块高不能匹配了
//所以，快照的应用，要把 segment的下标，set成对应块高，所以利用快照，做apply，要把新节点的wal中的第一个文件创建出来。
//以及 各个db 的savepoint 也要做起来
type Snapshot struct {
	height uint64
	status uint64 //0 unfinish, 1 finish
	//同步写用
	storeChan chan struct{}
	//快照与异步写通信
	jobChan chan *snapshotJob

	blockdb blockdb.BlockDB
	statedb statedb.StateDB
	eventdb contracteventdb.ContractEventDB

	logger  protocol.Logger
	chainID string

	storeConfig *conf.StorageConfig

	sync.RWMutex
}

// NewSnapshot create a snapshot
func NewSnapshot(blockdb blockdb.BlockDB, statedb statedb.StateDB, eventdb contracteventdb.ContractEventDB,
	logger protocol.Logger, chainID string, storeConfig *conf.StorageConfig) LedgerSnapshot {
	s := &Snapshot{
		height:      0,
		storeChan:   make(chan struct{}, 10),
		jobChan:     make(chan *snapshotJob, 10),
		blockdb:     blockdb,
		statedb:     statedb,
		eventdb:     eventdb,
		logger:      logger,
		chainID:     chainID,
		storeConfig: storeConfig,
	}

	go s.RecevieSnapshotIterator()
	return s
}

// MakeSnapshot get a snapshot
func (s *Snapshot) MakeSnapshot(currHeight, height uint64) error {
	s.Lock()
	defer s.Unlock()
	if height <= currHeight {
		return errSnapshotHeight
	}
	if (height == s.height && s.status == 0) || (s.height != 0 && s.status == 0) {
		return errSnapshotIsBeing
	}

	// same snapshot, have finished
	if s.status == 1 && s.height == height {
		return nil
	}

	s.height = height
	s.status = 0
	return nil
}

// WaitingSnapshotJob  wait a snapshot job until snapshot job finish
func (s *Snapshot) WaitingSnapshotJob(height uint64) error {
	s.RLock()
	snapshotHeight := s.height
	s.RUnlock()
	//hasn't job
	if height > snapshotHeight || height < snapshotHeight {
		return nil
	}
	//job is running
	<-s.storeChan

	return nil
}

// SendSnapshotIterator  send a snapshot iterator to a chan, which includes db iterators
func (s *Snapshot) SendSnapshotIterator(rootDir string, height uint64) error {
	//todo: 传入的height，应该 就是 要修改的 s.height，应该先判断currentHeight和 height是不是允许 做快照
	//todo: 在同一个区块高度下，做两个快照不允许，比如2个快照，都要做到 以第9个区块为height，做快照，不允许
	//todo: 幂等，也得支持，当前已有快照，正在进行，则不允许，再做快照了
	s.RLock()
	snapshotHeight := s.height
	s.RUnlock()

	// no snapshot required
	if snapshotHeight != height {
		return nil
	}

	// snapshot required, there, need to do a new snapshotJob
	s.Lock()
	defer s.Unlock()
	//set status unfinish
	s.status = 0

	s.logger.Debugf("start send snapshot, chain:[%s], height:[%d]", s.chainID, height)

	//create new snapshot job
	leveldbFolder := fmt.Sprintf("%s_%d", snapshotDBFolder, height)
	job, err := newSnapshotJob(rootDir, s.logger, s.chainID, leveldbFolder)
	if err != nil {
		return err
	}

	// 1.configBlock data
	s.logger.Debugf("start send snapshot, getConfigBlock,chain:[%s], height:[%d]", s.chainID, height)
	block, err := s.getConfigBlock()
	//if block == nil {
	//	s.logger.Debugf("get config block is nil")
	//	panic("get config block is nil")
	//}
	//if block.Txs == nil {
	//	s.logger.Debugf("get config block txs is nil")
	//	panic("get config block txs is nil")
	//}
	//if block.Header == nil {
	//	s.logger.Debugf("get config block header is nil")
	//	panic("get config block header is nil")
	//}
	if err != nil {
		return err
	}
	job.block = block

	// 2.txidSet data iterator
	s.logger.Debugf("start send snapshot, getTxIDSetIter,chain:[%s], height:[%d]", s.chainID, height)
	txidSetIter, err := s.getTxIDSetIter()
	if err != nil {
		return err
	}
	job.txidSetIter = txidSetIter

	// 3.statedb data iterator
	s.logger.Debugf("start send snapshot, getStateIter,chain:[%s], height:[%d]", s.chainID, height)
	statedbIter, err := s.getStateIter()
	if err != nil {
		return err
	}
	job.stateIter = statedbIter

	// 4.eventdb data sql file
	if !s.storeConfig.DisableContractEventDB {
		s.logger.Debugf("start send snapshot, getEventData,chain:[%s], height:[%d]", s.chainID, height)
		eventSnapshotName, err := s.getEventData()
		if err != nil {
			return err
		}
		//rename file name
		newFileName := fmt.Sprintf("event_%d.sql", s.height)
		newFilePath := path.Join(s.eventdb.GetDumpDir(), newFileName)
		if err := os.Rename(eventSnapshotName, newFilePath); err != nil {
			return err
		}
		job.eventsSqlDBFile = newFilePath
		s.logger.Debugf("end send snapshot, getEventData,chain:[%s], height:[%d],eventSnapshotName:[%s]",
			s.chainID, height, newFilePath)
	}

	// 5.send msg to chan
	s.jobChan <- job
	s.storeChan <- struct{}{}
	s.logger.Debugf("finish send snapshot, chain:[%s], height:[%d]", s.chainID, height)

	return nil
}

// RecevieSnapshotIterator  receive a snapshot iterator from chan ,then write snapshot data to file
func (s *Snapshot) RecevieSnapshotIterator() {
	for {
		job, ok := <-s.jobChan
		if !ok {
			s.logger.Infof("jobChan is closed")
		}
		err := job.writeSnapshot(s.chainID, s.height)
		if err != nil {
			s.logger.Errorf("write snapshot data to file error,errInfo:[%s]", err)
			panic(err)
		}
		s.Lock()
		//set status finish
		s.status = 1
		s.Unlock()
	}
}

// GetSnapshotStatus returns the last snapshot job status
func (s *Snapshot) GetSnapshotStatus() uint64 {
	s.RLock()
	defer s.RUnlock()

	//there are no snapshots to make yet, so the height is 0
	if s.height == 0 {
		return 1
	}
	return s.status
}

// getConfigBlock returns the config block pointer
func (s *Snapshot) getConfigBlock() (*common.Block, error) {
	return s.blockdb.GetLastConfigBlock()
}

// getTxIDSetIter  return  iterator array of tx id set
///todo: support array iterator to improve import speed
func (s *Snapshot) getTxIDSetIter() ([]protocol.Iterator, error) {
	return s.blockdb.GetTxidSet()
}

// getStateIter return iterator array of state
//todo: support array iterator to improve import speed
func (s *Snapshot) getStateIter() ([]protocol.Iterator, error) {
	return s.statedb.GetSnapshotIterator()
}

// getEventData return file's name of mysql dump result of contract event db
func (s *Snapshot) getEventData() (string, error) {
	return s.eventdb.GetSnapshotFile()
}

type snapshotJob struct {
	block            *common.Block       // configBlock
	txidSetIter      []protocol.Iterator // txidSet iterator
	stateIter        []protocol.Iterator // stateDB iterator
	db               *leveldbProvider.LevelDBHandle
	eventsSqlDBFile  string
	snapshotChainDir string
	logger           protocol.Logger
}

// newSnapshotJob  create a snapshot job
func newSnapshotJob(rootDir string, logger protocol.Logger, chainID string, dbFolder string) (*snapshotJob, error) {
	dbConfig := &leveldbProvider.LevelDbConfig{
		StorePath:         rootDir,
		NoSync:            false,
		DisableBufferPool: true,
		DisableBlockCache: true,
		WriteBufferSize:   10,
	}
	//folder = "snapshot/snapshotDB_height"
	folder := path.Join(snapshotDir, dbFolder)
	db, err := NewLeveldb(dbConfig, logger, chainID, folder)
	if err != nil {
		return nil, err
	}

	snapshotChainDir := filepath.Join(rootDir, chainID)
	job := &snapshotJob{
		block:            &common.Block{},
		db:               db,
		snapshotChainDir: snapshotChainDir,
		logger:           logger,
	}
	return job, nil
}

// writeSnapshot that consume job from chan,then read data from iter and write data to file
func (b *snapshotJob) writeSnapshot(chainID string, height uint64) error {
	b.logger.Debugf("start write snapshot,chain:[%s], height:[%d]", chainID, height)
	var (
		stateCrc       []metadb.CRC
		txidCrc        []metadb.CRC
		configBlockCrc metadb.CRC
		sumCrcFileName string
		sumCrc         metadb.CRC
	)
	stateCrc = make([]metadb.CRC, 0)
	for i := 0; i < len(b.stateIter); i++ {
		stateCrc = append(stateCrc, metadb.NewCRC([]byte{}))
	}
	txidCrc = make([]metadb.CRC, 0)
	for i := 0; i < len(b.txidSetIter); i++ {
		txidCrc = append(txidCrc, metadb.NewCRC([]byte{}))
	}
	configBlockCrc = metadb.NewCRC([]byte{})
	sumCrcFileName = fmt.Sprintf("crc_%s_%d", chainID, height)

	// 1.save state kv
	b.logger.Debugf("start write statedb snapshot,chain:[%s], height:[%d]", chainID, height)
	wgState := &sync.WaitGroup{}
	wgState.Add(len(b.stateIter))
	stateErrChan := make(chan error, len(b.stateIter))

	for i := 0; i < len(b.stateIter); i++ {
		go func(i int) {
			defer wgState.Done()
			for b.stateIter[i].Next() {
				var newKey []byte
				key := b.stateIter[i].Key()
				value := b.stateIter[i].Value()
				newKey = append([]byte{stateKeyPre}, key...)
				stateCrc[i] = stateCrc[i].Update(newKey).Update(value)
				if err := b.db.Put(newKey, value); err != nil {
					stateErrChan <- err
				}
			}
		}(i)
	}
	wgState.Wait()

	// 2.save txid set
	b.logger.Debugf("start write txidSet snapshot,chain:[%s], height:[%d]", chainID, height)
	wgTxidSet := &sync.WaitGroup{}
	wgTxidSet.Add(len(b.txidSetIter))
	txidSetErrChan := make(chan error, len(b.txidSetIter))

	for i := 0; i < len(b.stateIter); i++ {
		go func(i int) {
			defer wgTxidSet.Done()
			for b.txidSetIter[i].Next() {
				var newKey []byte
				key := b.txidSetIter[i].Key()
				value := b.txidSetIter[i].Value()
				newKey = append([]byte{txIDKeyPre}, key...)
				txidCrc[i] = txidCrc[i].Update(newKey).Update(value)
				if err := b.db.Put(newKey, value); err != nil {
					txidSetErrChan <- err
				}
			}
		}(i)
	}
	wgTxidSet.Wait()

	// 3.save configBlock
	b.logger.Debugf("start write configBlock snapshot,chain:[%s], height:[%d]", chainID, height)
	bytes, err := b.block.Marshal()
	if err != nil {
		return err
	}
	configBlockCrc = configBlockCrc.Update(bytes)
	if err := b.db.Put([]byte{configBlockKey}, bytes); err != nil {
		return err
	}
	if err := b.db.Close(); err != nil {
		return err
	}

	// 4.merge stateCrc、txidSetCrc、configBlockCrc into sumCrc
	sumCrc = metadb.NewCRC([]byte{})
	for i := 0; i < len(stateCrc); i++ {
		sumCrc = sumCrc + metadb.CRC(stateCrc[i].Value())
	}
	for i := 0; i < len(txidCrc); i++ {
		sumCrc = sumCrc + metadb.CRC(txidCrc[i].Value())
	}
	sumCrc = sumCrc + metadb.CRC(configBlockCrc.Value())

	// 5.save crc data to file
	b.logger.Debugf("start write crc to file,chain:[%s], height:[%d], crc filename:[%s]", chainID,
		height, sumCrcFileName)
	//crcPath = $store_path/$chainID/crc_$chainID_$height
	crcPath := path.Join(b.snapshotChainDir, snapshotDir, sumCrcFileName)
	return metadb.NewCrcFile(sumCrc, crcPath)

}

// applySnapshot,that open snapshot file ,then read data from snapshot and write data to blockdb and statedb
//func (b *snapshotJob) applySnapshot(name string) error {
//	return nil
//}

// NewLeveldb return a handle of leveldb
func NewLeveldb(dbConfig *leveldbProvider.LevelDbConfig, logger protocol.Logger, chainId string,
	dbFolder string) (*leveldbProvider.LevelDBHandle, error) {

	input := &leveldbProvider.NewLevelDBOptions{
		Config:    dbConfig,
		Logger:    logger,
		Encryptor: nil,
		ChainId:   chainId,
		DbFolder:  dbFolder,
	}
	return leveldbProvider.NewLevelDBHandle(input), nil

}

// SnapshotDir returns the absolute path of the ledger snapshot
//func SnapshotDir(snapshotRootDir string, chainID string, height uint64) string {
//	return filepath.Join(snapshotRootDir, "_", chainID, "_", strconv.FormatUint(height, 10))
//}
