package snapshot

// LedgerSnapshot provide make snapshot, get snapshot status and so on.
type LedgerSnapshot interface {
	// MakeSnapshot get a snapshot task
	MakeSnapshot(currHeight, height uint64) error

	// GetSnapshotStatus returns the last snapshot job status, return 0 unfinished, 1 finish
	GetSnapshotStatus() uint64

	// WaitingSnapshotJob  wait a snapshot job until snapshot job finish
	WaitingSnapshotJob(height uint64) error

	// SendSnapshotIterator  send a snapshot iterator to a chan, which includes db iterators and configBlock
	SendSnapshotIterator(snapshotDir string, height uint64) error

	// RecevieSnapshotIterator  receive a snapshot iterator from chan ,then write snapshot data to file
	RecevieSnapshotIterator()
}
