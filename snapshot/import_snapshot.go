package snapshot

import (
	"errors"
	"fmt"
	"path"

	"chainmaker.org/chainmaker/pb-go/v3/common"
	"chainmaker.org/chainmaker/protocol/v3"
	"chainmaker.org/chainmaker/store-huge/v3/bigfilterdb"
	"chainmaker.org/chainmaker/store-huge/v3/binlog"
	"chainmaker.org/chainmaker/store-huge/v3/blockdb"
	"chainmaker.org/chainmaker/store-huge/v3/conf"
	"chainmaker.org/chainmaker/store-huge/v3/contracteventdb"
	"chainmaker.org/chainmaker/store-huge/v3/historydb"
	"chainmaker.org/chainmaker/store-huge/v3/meta"
	"chainmaker.org/chainmaker/store-huge/v3/resultdb"
	"chainmaker.org/chainmaker/store-huge/v3/statedb"
	"chainmaker.org/chainmaker/store-huge/v3/txexistdb"
	leveldbProvider "chainmaker.org/chainmaker/store-leveldb/v3"
)

//nolint
var (
	errConfigBlockIsNil = errors.New("config block is nil")
)

// StoreImportSnapshot ,import snapshot ,includes statedb,blockdb,bigfilter,txExistdb,wal(bf),metadb,resultdb,
//historydb,contractEventdb, and so on .
// @Description
type StoreImportSnapshot struct {
	chainID         string
	storeConfig     *conf.StorageConfig
	blockDB         blockdb.BlockDB
	stateDB         statedb.StateDB
	historyDB       historydb.HistoryDB
	contractEventDB contracteventdb.ContractEventDB
	resultDB        resultdb.ResultDB
	txExistDB       txexistdb.TxExistDB
	commonDB        protocol.DBHandle
	logger          protocol.Logger
	blockFileDB     binlog.BinLogger
	bigFilterDB     bigfilterdb.BigFilterDB
	meta            meta.MetaData
	//ledgerSnapshot LedgerSnapshot
}

// NewStoreImportSnapshot construct ImportSnapshot
// @Description:
// @param chainID
// @param storageConfig
// @param blockDB
// @param stateDB
// @param historyDB
// @param contractEventDB
// @param resultDB
// @param txExistDB
// @param commonDB
// @param logger
// @param bfdb
// @param bigFilterDB
// @param metaData
// @return ImportLedgerSnapshot
// @return error
func NewStoreImportSnapshot(
	chainID string,
	storeConfig *conf.StorageConfig,
	blockDB blockdb.BlockDB,
	stateDB statedb.StateDB,
	historyDB historydb.HistoryDB,
	contractEventDB contracteventdb.ContractEventDB,
	resultDB resultdb.ResultDB,
	txExistDB txexistdb.TxExistDB,
	commonDB protocol.DBHandle,
	logger protocol.Logger,
	bfdb binlog.BinLogger,
	bigFilterDB bigfilterdb.BigFilterDB,
	meta meta.MetaData) (protocol.ImportLedgerSnapshot, error) {

	blockStore := &StoreImportSnapshot{
		chainID:         chainID,
		blockDB:         blockDB,
		stateDB:         stateDB,
		historyDB:       historyDB,
		contractEventDB: contractEventDB,
		resultDB:        resultDB,
		txExistDB:       txExistDB,
		blockFileDB:     bfdb,
		bigFilterDB:     bigFilterDB,
		meta:            meta,
		commonDB:        commonDB,
		logger:          logger,
		storeConfig:     storeConfig,
	}
	return blockStore, nil
}

// ImportSnapshot  import snapshot data into storage
// @Description
// @receiver i
// @param chainID
// @param snapshotPath
// @param height
// @return error
func (i *StoreImportSnapshot) ImportSnapshot(chainID string, snapshotPath string, height uint64) error {
	rootDir := snapshotPath
	//crcName := fmt.Sprintf("crc_%s_%d",chainID,height)
	//crcFile := path.Join(rootDir,crcName)
	//
	//eventName := fmt.Sprintf("event_%d",height)
	//eventFile := path.Join(rootDir,eventName)
	//
	//kvdbSnapshot := path.Join(rootDir,fmt.Sprintf("snapshot_%d",height))

	dbConfig := &leveldbProvider.LevelDbConfig{
		StorePath:         rootDir,
		NoSync:            false,
		DisableBufferPool: true,
		DisableBlockCache: true,
		WriteBufferSize:   10,
	}
	leveldbFolder := fmt.Sprintf("%s_%d", snapshotDBFolder, height)
	folder := path.Join(snapshotDir, leveldbFolder)
	db, err := NewLeveldb(dbConfig, i.logger, chainID, folder)
	if err != nil {
		return err
	}
	snapshotDB := db

	//1.读取快照元数据信息

	//2.导入block file
	//在 NewStoreImportSnapshot之前，创建bf时，已经调用了 blockfiledb.ImportOpen() 完成了bf的重置
	//blockfiledb.ImportOpen()

	//3.tx id 导入blockdb, bigfilter, txExistdb

	prefix := []byte{txIDKeyPre, blockTxIDIdxKeyPrefix}
	txidSet, err := snapshotDB.NewIteratorWithPrefix(prefix)
	if err != nil {
		return err
	}
	for txidSet.Next() {
		//key remove prefix
		k := txidSet.Key()[2:]
		v := txidSet.Value()
		if err = i.blockdbImport(k, v); err != nil {
			return err
		}
		if i.storeConfig.TxExistDbConfig != nil {
			if err = i.txExistdbImport(k, v); err != nil {
				return err
			}
		}
		if i.storeConfig.EnableBigFilter {
			if err = i.bigfilterImport(k, v); err != nil {
				return err
			}
		}
	}

	//4.导入配置区块
	//获得配置区块
	var configBlock *common.Block
	if configBlock, err = i.getConfigBlock(snapshotDB); err != nil {
		return err
	}
	//导入
	if err = i.configBlockImport(configBlock); err != nil {
		return err
	}

	//5.导入statedb 包括 state k/v
	statedbPrefix := []byte{stateKeyPre}
	statedbIter, err := snapshotDB.NewIteratorWithPrefix(statedbPrefix)
	if err != nil {
		return err
	}

	for statedbIter.Next() {
		// key remove prefix
		stateK := statedbIter.Key()[1:]
		stateV := statedbIter.Value()
		// import
		if err = i.statedbImport(stateK, stateV); err != nil {
			return err
		}
	}

	//5.导入eventdb
	//需要利用 mysql client 导入  , 例如 mysql -uuser -ppassword -h host -Pport < event_2000.sql

	//6.配置savePoint，对 historydb、resultdb、blockdb、statedb、eventdb、bigfilter、txExistdb
	if err = i.resetSavePoint(height); err != nil {
		return err
	}

	//返回
	return nil
}

// statedbImport  import snapshot data into statedb
// @Description
// @receiver i
// @param key
// @param value
// @return error
func (i *StoreImportSnapshot) statedbImport(key, value []byte) error {
	return i.stateDB.ImportSnapshot(key, value)
}

// resetSavePoint  reset save point
// @Description
// @receiver i
// @param height
// @return error
func (i *StoreImportSnapshot) resetSavePoint(height uint64) error {
	var err error
	if err = i.stateDB.ImportSetSavePoint(height); err != nil {
		return err
	}
	if err = i.blockDB.ImportSetSavePoint(height); err != nil {
		return err
	}
	if !i.storeConfig.DisableHistoryDB {
		if err = i.historyDB.ImportSetSavePoint(height); err != nil {
			return err
		}
	}
	if !i.storeConfig.DisableResultDB {
		if err = i.resultDB.ImportSetSavePoint(height); err != nil {
			return err
		}
	}
	if i.storeConfig.EnableBigFilter {
		if err = i.bigFilterDB.ImportSetSavePoint(height); err != nil {
			return err
		}
	}
	if i.storeConfig.TxExistDbConfig != nil {
		if err = i.txExistDB.ImportSetSavePoint(height); err != nil {
			return err
		}
	}
	//eventdb 不需要

	return nil
}

// blockdbImport  import snapshot data into blockdb
// @Description
// @receiver i
// @param key
// @param value
// @return error
func (i *StoreImportSnapshot) blockdbImport(key, value []byte) error {
	return i.blockDB.ImportTxID(key, value)
}

// configBlockImport  import config block data into block db
// @Description
// @receiver i
// @param config block
// @return error
func (i *StoreImportSnapshot) configBlockImport(block *common.Block) error {
	return i.blockDB.ConfigBlockImport(block)
}

// bigfilterImport  import snapshot data into bigfilter
// @Description
// @receiver i
// @param txid []byte
// @param value []byte
// @return error
func (i *StoreImportSnapshot) bigfilterImport(txid, value []byte) error {
	return i.bigFilterDB.ImportTxID(txid, value)
}

// txExistdbImport  import snapshot data into tx exist db
// @Description
// @receiver i
// @param txid []byte
// @param value []byte
// @return error
func (i *StoreImportSnapshot) txExistdbImport(txid, value []byte) error {
	return i.txExistDB.ImportTxID(txid, value)
}

// getConfigBlock  get config block from snapshot db
// @Description
// @receiver i
// @param snapshot db
// @return *Block
// @return error
func (i *StoreImportSnapshot) getConfigBlock(snapshotDB protocol.DBHandle) (*common.Block, error) {
	vBytes, err := snapshotDB.Get([]byte{configBlockKey})
	if err != nil {
		return nil, err
	}
	block := &common.Block{}
	if err := block.Unmarshal(vBytes); err != nil {
		return nil, err
	}
	return block, nil

}
