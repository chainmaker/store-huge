//+build windows

package snapshot

import (
	"chainmaker.org/chainmaker/common/v3/crypto"
	acPb "chainmaker.org/chainmaker/pb-go/v3/accesscontrol"
	commonPb "chainmaker.org/chainmaker/pb-go/v3/common"
	storePb "chainmaker.org/chainmaker/pb-go/v3/store"
	"chainmaker.org/chainmaker/pb-go/v3/syscontract"
	"chainmaker.org/chainmaker/protocol/v3"
	"chainmaker.org/chainmaker/protocol/v3/test"
	badgerdbprovider "chainmaker.org/chainmaker/store-badgerdb/v3"
	"chainmaker.org/chainmaker/store-huge/v3/historydb"
	"chainmaker.org/chainmaker/store-huge/v3/historydb/historykvdb"
	"chainmaker.org/chainmaker/store-huge/v3/resultdb"
	"chainmaker.org/chainmaker/store-huge/v3/resultdb/resultkvdb"
	"chainmaker.org/chainmaker/store-huge/v3/serialization"
	"chainmaker.org/chainmaker/store-huge/v3/txexistdb"
	"chainmaker.org/chainmaker/store-huge/v3/txexistdb/txexistkvdb"
	leveldbprovider "chainmaker.org/chainmaker/store-leveldb/v3"
	rawsqlprovider "chainmaker.org/chainmaker/store-sqldb/v3"
	tikvdbprovider "chainmaker.org/chainmaker/store-tikv/v3"
	"reflect"
	"time"

	//"chainmaker.org/chainmaker/store-huge/v3"

	//"chainmaker.org/chainmaker/store-huge/v3"
	//"store"
	"chainmaker.org/chainmaker/store-huge/v3/binlog"
	"chainmaker.org/chainmaker/store-huge/v3/blockdb"
	"chainmaker.org/chainmaker/store-huge/v3/blockdb/blockfiledb"
	"chainmaker.org/chainmaker/store-huge/v3/conf"
	"chainmaker.org/chainmaker/store-huge/v3/contracteventdb"
	"chainmaker.org/chainmaker/store-huge/v3/contracteventdb/eventsqldb"
	"chainmaker.org/chainmaker/store-huge/v3/meta"
	"chainmaker.org/chainmaker/store-huge/v3/meta/metadb"
	"chainmaker.org/chainmaker/store-huge/v3/statedb"
	"chainmaker.org/chainmaker/store-huge/v3/statedb/statekvdb"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"github.com/mitchellh/mapstructure"
	"path/filepath"
	"strings"

	"sync"
	"testing"
)

var defaultSysContractName = syscontract.SystemContract_CHAIN_CONFIG.String()

var cJson = `{
"StorePath":"./ledgerData1",
"DbPrefix":"","WriteBufferSize":0,
"BloomFilterBits":0,
"BlockWriteBufferSize":0,
"DisableContractEventDB":true,
"LogDBWriteAsync":false,
"BlockFileConfig":{
	"OnlineFileSystem":"/tmp/online1,/tmp/online2",
	"ArchiveFileSystem":"/tmp/archive1,/tmp/archive2"
},
"ConfigVersion":{
	"Major": 1,
	"Minor": 0
},
"BlockDbConfig":{
	"Provider":"leveldb",
	"LevelDbConfig":{
		"store_path":"./ledgerData1/data/org1/blocks"
	},
	"BadgerDbConfig":null,
	"SqlDbConfig":null
},
"StateDbConfig":{
	"Provider":"leveldb",
	"LevelDbConfig":{"store_path":"./ledgerData1/data/org1/statedb"},
	"BadgerDbConfig":null,"SqlDbConfig":null
},
"TxExistDbConfig":{
	"Provider":"leveldb",
	"LevelDbConfig":{
		"store_path":"./ledgerData1/data/org1/txexist"
	},
	"BadgerDbConfig":null,
	"SqlDbConfig":null
},
"HistoryDbConfig":{
	"Provider":"leveldb",
	"LevelDbConfig":{"store_path":"./ledgerData1/data/org1/history"},
	"BadgerDbConfig":null,"SqlDbConfig":null
},
"ResultDbConfig":{
	"Provider":"leveldb",
	"LevelDbConfig":{"store_path":"./ledgerData1/data/org1/result"},
	"BadgerDbConfig":null,
	"SqlDbConfig":null
},
"UnArchiveBlockHeight":0,
"Encryptor":"sm4",
"EncryptKey":"1234567890123456"
}`

//var cJson2 = `{
//"StorePath":"./ledgerData1",
//"DbPrefix":"","WriteBufferSize":0,
//"BloomFilterBits":0,
//"BlockWriteBufferSize":0,
//"DisableHistoryDB":true,
//"DisableResultDB":true,
//"DisableContractEventDB":true,
//"LogDBWriteAsync":false,
//"BlockFileConfig":{
//	"OnlineFileSystem":"/tmp/online1,/tmp/online2",
//	"ArchiveFileSystem":"/tmp/archive1,/tmp/archive2"
//},
//"ConfigVersion":{
//	"Major": 1,
//	"Minor": 0
//},
//"BlockDbConfig":{
//	"Provider":"leveldb",
//	"LevelDbConfig":{
//		"store_path":"./ledgerData1/data/org1/blocks"
//	},
//	"BadgerDbConfig":null,
//	"SqlDbConfig":null
//},
//"TxExistDbConfig":{
//	"Provider":"leveldb",
//	"LevelDbConfig":{
//		"store_path":"./ledgerData1/data/org1/txexist"
//	},
//	"BadgerDbConfig":null,
//	"SqlDbConfig":null
//},
//"StateDbConfig":{
//	"Provider":"leveldb",
//	"LevelDbConfig":{"store_path":"./ledgerData1/data/org1/statedb"},
//	"BadgerDbConfig":null,"SqlDbConfig":null
//},
//"HistoryDbConfig":{
//	"Provider":"leveldb",
//	"LevelDbConfig":{"store_path":"./ledgerData1/data/org1/history"},
//	"BadgerDbConfig":null,"SqlDbConfig":null
//},
//"ResultDbConfig":{
//	"Provider":"leveldb",
//	"LevelDbConfig":{"store_path":"./ledgerData1/data/org1/result"},
//	"BadgerDbConfig":null,
//	"SqlDbConfig":null
//},
//"ContractEventDbConfig":{
//	"Provider":"sql",
//	"LevelDbConfig":null,
//	"BadgerDbConfig":null,
//	"SqlDbConfig":{
//		"dsn":"sqlkv:sqlkvpass@tcp(127.0.0.1:33666)/",
//		"sqldb_type":"mysql"
//	}
//},
//"UnArchiveBlockHeight":0,
//"Encryptor":"sm4",
//"EncryptKey":"1234567890123456"
//}`

func GetConfig(cJson string) *conf.StorageConfig {
	config := &conf.StorageConfig{}

	//config.DisableBigFilter = true
	err := json.Unmarshal([]byte(cJson), config)
	if err != nil {
		panic(err)
	}
	return config
}

func GetDBHandle(chainId, providerName, dbFolder string,
	config map[string]interface{}, logger protocol.Logger,
	encryptor crypto.SymmetricKey) (protocol.DBHandle, error) {
	providerName = strings.ToLower(providerName)
	switch providerName {
	case conf.DbconfigProviderLeveldb:
		dbConfig := &leveldbprovider.LevelDbConfig{}
		err := mapstructure.Decode(config, dbConfig)
		if err != nil {
			return nil, err
		}
		input := &leveldbprovider.NewLevelDBOptions{
			Config:    dbConfig,
			Logger:    logger,
			Encryptor: encryptor,
			ChainId:   chainId,
			DbFolder:  dbFolder,
		}
		return leveldbprovider.NewLevelDBHandle(input), nil
	case conf.DbconfigProviderBadgerdb:
		dbConfig := &badgerdbprovider.BadgerDbConfig{}
		err := mapstructure.Decode(config, dbConfig)
		if err != nil {
			return nil, err
		}
		input := &badgerdbprovider.NewBadgerDBOptions{
			Config:    dbConfig,
			Logger:    logger,
			Encryptor: encryptor,
			ChainId:   chainId,
			DbFolder:  dbFolder,
		}
		return badgerdbprovider.NewBadgerDBHandle(input), nil
	case conf.DbconfigProviderTikvdb:
		dbConfig := &tikvdbprovider.TiKVDbConfig{}
		err := mapstructure.Decode(config, dbConfig)
		if err != nil {
			return nil, err
		}
		input := &tikvdbprovider.NewTikvDBOptions{
			Config:    dbConfig,
			Logger:    logger,
			Encryptor: encryptor,
			ChainId:   chainId,
			DbName:    dbFolder,
		}
		return tikvdbprovider.NewTiKVDBHandle(input), nil
	case conf.DbconfigProviderSqlKV:
		dbConfig := &rawsqlprovider.SqlDbConfig{}
		err := mapstructure.Decode(config, dbConfig)
		if err != nil {
			return nil, err
		}
		input := &rawsqlprovider.NewSqlDBOptions{
			Config:    dbConfig,
			Logger:    logger,
			Encryptor: encryptor,
			ChainId:   chainId,
			DbName:    dbFolder,
		}
		return rawsqlprovider.NewKVDBHandle(input), nil
	case conf.DbconfigProviderMemdb:
		return leveldbprovider.NewMemdbHandle(), nil
	default:
		return nil, fmt.Errorf("unsupported provider:%s", providerName)

	}
}

func GetSQLDBHandle(chainId, dbName string, config map[string]interface{},
	logger protocol.Logger) protocol.SqlDBHandle {

	dbConfig := &rawsqlprovider.SqlDbConfig{}
	err := mapstructure.Decode(config, dbConfig)
	if err != nil {
		panic(err)
	}
	input := &rawsqlprovider.NewSqlDBOptions{
		Config:    dbConfig,
		Logger:    logger,
		Encryptor: nil,
		ChainId:   chainId,
		DbName:    dbName,
	}
	logger.Debugf("initial new sql db for %s", dbName)
	return rawsqlprovider.NewSqlDBHandle(input)
}

func GetMeta(chainID string, storeConfig *conf.StorageConfig, commonDB protocol.DBHandle,
	logger protocol.Logger) meta.MetaData {

	metadb, err := metadb.NewMetaDataDB(*storeConfig, chainID, commonDB, logger)
	if err != nil {
		panic(err)
	}
	return metadb
}

func GetBF(chainID string, storeConfig *conf.StorageConfig, metadb meta.MetaData,
	logger protocol.Logger) binlog.BinLogger {
	var (
		err error
		bf  *blockfiledb.BlockFile
	)
	blockFilePath := "bfdb"

	opts := blockfiledb.DefaultOptions
	opts.NoCopy = true
	opts.NoSync = storeConfig.LogDBSegmentAsync
	if storeConfig.LogDBSegmentSize > 64 { // LogDBSegmentSize default is 64MB
		opts.SegmentSize = storeConfig.LogDBSegmentSize * 1024 * 1024
	}
	if storeConfig.DisableLogDBMmap {
		opts.UseMmap = false
	}
	bfPath := filepath.Join(storeConfig.StorePath, chainID, blockFilePath)
	bf, err = blockfiledb.Open(bfPath, opts, logger, metadb)

	if err != nil {
		panic(fmt.Sprintf("open block file db failed, path:%s, error:%s", bfPath, err))
	}
	return bf
}

func GetImportBF(chainID string, storeConfig *conf.StorageConfig, metadb meta.MetaData,
	logger protocol.Logger, height uint64) binlog.BinLogger {
	var (
		err error
		bf  *blockfiledb.BlockFile
	)
	//blockFilePath := "bfdb"

	opts := blockfiledb.DefaultOptions
	opts.NoCopy = true
	opts.NoSync = storeConfig.LogDBSegmentAsync
	if storeConfig.LogDBSegmentSize > 64 { // LogDBSegmentSize default is 64MB
		opts.SegmentSize = storeConfig.LogDBSegmentSize * 1024 * 1024
	}
	if storeConfig.DisableLogDBMmap {
		opts.UseMmap = false
	}
	//bfPath := filepath.Join(storeConfig.StorePath, chainID, blockFilePath)
	bf, err = blockfiledb.ImportOpen(opts, logger, metadb, height)

	if err != nil {
		panic(fmt.Sprintf("open block file db failed, error:%s", err))
	}
	return bf
}

func GetBlockDB(chainId string, storeConfig *conf.StorageConfig,
	bf binlog.BinLogger, blockDBHandle protocol.DBHandle, logger protocol.Logger) blockdb.BlockDB {
	//dbPrefix := storeConfig.DbPrefix
	db := blockfiledb.NewBlockFileDB(chainId, blockDBHandle, logger, storeConfig, bf)
	return db
}

func GetStateDB(chainId string, storeConfig *conf.StorageConfig, stateDBHandle protocol.DBHandle,
	logger protocol.Logger) statedb.StateDB {

	db := statekvdb.NewStateKvDB(chainId, stateDBHandle, logger, storeConfig)
	return db
}

func GetEventDB(chainId string, storeConfig *conf.StorageConfig, eventDBHandle protocol.SqlDBHandle,
	logger protocol.Logger) contracteventdb.ContractEventDB {

	dbName := getDbName(storeConfig.DbPrefix, "eventdb", chainId)

	db, err := eventsqldb.NewContractEventDB(dbName, eventDBHandle, logger, storeConfig, chainId)
	if err != nil {
		panic(err)
	}

	return db
}

func GetHistoryDB(chainId string, storeConfig *conf.StorageConfig, historyDBHandle protocol.DBHandle,
	logger protocol.Logger) historydb.HistoryDB {

	db := historykvdb.NewHistoryKvDB(chainId, storeConfig.HistoryDbConfig, historyDBHandle, logger)
	return db
}

func GetResultDB(chainId string, storeConfig *conf.StorageConfig, resultDBHandle protocol.DBHandle,
	logger protocol.Logger) resultdb.ResultDB {

	db := resultkvdb.NewResultKvDB(chainId, resultDBHandle, logger, storeConfig)
	return db
}

func GetTxExistDB(chainId string, storeConfig *conf.StorageConfig, txExistDBHandle protocol.DBHandle,
	logger protocol.Logger) txexistdb.TxExistDB {

	db := txexistkvdb.NewTxExistKvDB(chainId, txExistDBHandle, logger)
	return db
}

//为了支持多链和多个db
func getDbName(dbPrefix, dbName, chainId string) string {
	return dbPrefix + dbName + "_" + chainId
}

//func N() {
//	f := store.NewFactory()
//
//	err2 := f.CreateAll()
//	f.
//	store, err := f.NewStore()
//	store.
//
//}

func TestNewSnapshot(t *testing.T) {
	type args struct {
		blockdb     blockdb.BlockDB
		statedb     statedb.StateDB
		eventdb     contracteventdb.ContractEventDB
		logger      protocol.Logger
		chainID     string
		storeConfig *conf.StorageConfig
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "test-new-snapshot",
			args: args{
				blockdb:     nil,
				statedb:     nil,
				eventdb:     nil,
				logger:      test.GoLogger{},
				chainID:     "test-chain-id",
				storeConfig: GetConfig(cJson),
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewSnapshot(tt.args.blockdb, tt.args.statedb, tt.args.eventdb,
				tt.args.logger, tt.args.chainID, tt.args.storeConfig)
			if got == nil {
				t.Errorf("NewSnapshot() = %v, want %v", got == nil, tt.want)
			}
		})
	}
}

func TestSnapshot_MakeSnapshot(t *testing.T) {
	type fields struct {
		height      uint64
		status      uint64
		storeChan   chan struct{}
		jobChan     chan *snapshotJob
		blockdb     blockdb.BlockDB
		statedb     statedb.StateDB
		eventdb     contracteventdb.ContractEventDB
		logger      protocol.Logger
		chainID     string
		storeConfig *conf.StorageConfig
	}
	type args struct {
		currHeight uint64
		height     uint64
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "test-make-snapshot",
			fields: fields{
				height:      0,
				status:      0,
				storeChan:   make(chan struct{}, 0),
				jobChan:     make(chan *snapshotJob, 10),
				blockdb:     nil,
				statedb:     nil,
				eventdb:     nil,
				logger:      test.GoLogger{},
				chainID:     "test1",
				storeConfig: nil,
			},
			args: args{
				currHeight: 0,
				height:     1,
			},
			wantErr: false,
		},
		{
			name: "test-make-snapshot-err-snapshot-height",
			fields: fields{
				height:      0,
				status:      0,
				storeChan:   make(chan struct{}, 0),
				jobChan:     make(chan *snapshotJob, 10),
				blockdb:     nil,
				statedb:     nil,
				eventdb:     nil,
				logger:      test.GoLogger{},
				chainID:     "test1",
				storeConfig: nil,
			},
			args: args{
				currHeight: 1,
				height:     1,
			},
			wantErr: true,
		},
		{
			name: "test-make-snapshot-err-snapshot-is-being",
			fields: fields{
				height:      2,
				status:      0,
				storeChan:   make(chan struct{}, 0),
				jobChan:     make(chan *snapshotJob, 10),
				blockdb:     nil,
				statedb:     nil,
				eventdb:     nil,
				logger:      test.GoLogger{},
				chainID:     "test1",
				storeConfig: nil,
			},
			args: args{
				currHeight: 1,
				height:     2,
			},
			wantErr: true,
		},
		{
			name: "test-make-snapshot-err-snapshot-is-being-2",
			fields: fields{
				height:      2,
				status:      0,
				storeChan:   make(chan struct{}, 0),
				jobChan:     make(chan *snapshotJob, 10),
				blockdb:     nil,
				statedb:     nil,
				eventdb:     nil,
				logger:      test.GoLogger{},
				chainID:     "test1",
				storeConfig: nil,
			},
			args: args{
				currHeight: 1,
				height:     3,
			},
			wantErr: true,
		},
		{
			name: "test-make-snapshot-snapshot-finish",
			fields: fields{
				height:      2,
				status:      1,
				storeChan:   make(chan struct{}, 0),
				jobChan:     make(chan *snapshotJob, 10),
				blockdb:     nil,
				statedb:     nil,
				eventdb:     nil,
				logger:      test.GoLogger{},
				chainID:     "test1",
				storeConfig: nil,
			},
			args: args{
				currHeight: 1,
				height:     2,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &Snapshot{
				height:      tt.fields.height,
				status:      tt.fields.status,
				storeChan:   tt.fields.storeChan,
				jobChan:     tt.fields.jobChan,
				blockdb:     tt.fields.blockdb,
				statedb:     tt.fields.statedb,
				eventdb:     tt.fields.eventdb,
				logger:      tt.fields.logger,
				chainID:     tt.fields.chainID,
				storeConfig: tt.fields.storeConfig,
				RWMutex:     sync.RWMutex{},
			}
			if err := s.MakeSnapshot(tt.args.currHeight, tt.args.height); (err != nil) != tt.wantErr {
				t.Errorf("MakeSnapshot() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestSnapshot_WaitingSnapshotJob(t *testing.T) {
	type fields struct {
		height      uint64
		status      uint64
		storeChan   chan struct{}
		jobChan     chan *snapshotJob
		blockdb     blockdb.BlockDB
		statedb     statedb.StateDB
		eventdb     contracteventdb.ContractEventDB
		logger      protocol.Logger
		chainID     string
		storeConfig *conf.StorageConfig
	}
	type args struct {
		height uint64
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "test-waiting-snapshot-job",
			fields: fields{
				height:      0,
				status:      0,
				storeChan:   make(chan struct{}, 10),
				jobChan:     make(chan *snapshotJob, 10),
				blockdb:     nil,
				statedb:     nil,
				eventdb:     nil,
				logger:      test.GoLogger{},
				chainID:     "test1",
				storeConfig: nil,
			},
			args: args{
				height: 1,
			},
			wantErr: false,
		},
		{
			name: "test-waiting-snapshot-job-2",
			fields: fields{
				height:      1,
				status:      0,
				storeChan:   make(chan struct{}, 10),
				jobChan:     make(chan *snapshotJob, 10),
				blockdb:     nil,
				statedb:     nil,
				eventdb:     nil,
				logger:      test.GoLogger{},
				chainID:     "test1",
				storeConfig: nil,
			},
			args: args{
				height: 1,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &Snapshot{
				height:      tt.fields.height,
				status:      tt.fields.status,
				storeChan:   tt.fields.storeChan,
				jobChan:     tt.fields.jobChan,
				blockdb:     tt.fields.blockdb,
				statedb:     tt.fields.statedb,
				eventdb:     tt.fields.eventdb,
				logger:      tt.fields.logger,
				chainID:     tt.fields.chainID,
				storeConfig: tt.fields.storeConfig,
				RWMutex:     sync.RWMutex{},
			}
			s.storeChan <- struct{}{}
			if err := s.WaitingSnapshotJob(tt.args.height); (err != nil) != tt.wantErr {
				t.Errorf("WaitingSnapshotJob() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

// TestSnapshot_MakeSnapshotAndImportSnapshot send snapshot then write to file from chainA and import snapshot into chainB
// first, send snapshot iterator from chainA
// second, write snapshot data into files
// third, import snapshot files into chainB
// finally, start chainB ,check the difference between  chainB and chainA ,then write a new block into chainB
func TestSnapshot_MakeSnapshotAndImportSnapshot(t *testing.T) {
	var (
		snapshotHeight    uint64 = 1
		sourceChainID            = "test_chainA"
		destChainID              = "test_chainB"
		provider                 = "leveldb"
		dbFolderBlockDB          = "blockdb"
		dbFolderStateDB          = "statedb"
		dbFolderCommonDB         = "commondb"
		dbFolderTxExistDB        = "txexistb"
		dbFolderHistoryDB        = "historydb"
		dbFolderResultDB         = "resultdb"
	)

	blockDBConfig := GetConfig(cJson).BlockDbConfig.GetDbConfig()
	stateDBConfig := GetConfig(cJson).StateDbConfig.GetDbConfig()
	commonDBConfig := GetConfig(cJson).GetDefaultDBConfig().GetDbConfig()
	txExistDBConfig := GetConfig(cJson).TxExistDbConfig.GetDbConfig()
	resultDBConfig := GetConfig(cJson).ResultDbConfig.GetDbConfig()
	historyDBConfig := GetConfig(cJson).HistoryDbConfig.GetDbConfig()

	// 1.create a source chain's object ,include sourceBlockDB and sourceStateDB
	//blockdb handle.
	sourceBlockDBHandle, err := GetDBHandle(sourceChainID, provider, dbFolderBlockDB, blockDBConfig, test.GoLogger{}, nil)
	if err != nil {
		panic(err)
	}
	//statedb handle
	sourceStateDBHandle, err := GetDBHandle(sourceChainID, provider, dbFolderStateDB, stateDBConfig, test.GoLogger{}, nil)
	if err != nil {
		panic(err)
	}
	//common/localdb handle
	sourceCommonDBHandle, err := GetDBHandle(sourceChainID, provider, dbFolderCommonDB, commonDBConfig, test.GoLogger{}, nil)
	if err != nil {
		panic(err)
	}
	//txExistdb handle
	sourceTxExistDBHandle, err := GetDBHandle(sourceChainID, provider, dbFolderTxExistDB, txExistDBConfig, test.GoLogger{}, nil)
	if err != nil {
		panic(err)
	}
	//historydb handle
	sourceHistoryDBHandle, err := GetDBHandle(sourceChainID, provider, dbFolderHistoryDB, historyDBConfig, test.GoLogger{}, nil)
	if err != nil {
		panic(err)
	}
	//resultdb handle
	sourceResultDBHandle, err := GetDBHandle(sourceChainID, provider, dbFolderResultDB, historyDBConfig, test.GoLogger{}, nil)
	if err != nil {
		panic(err)
	}

	//meta
	sourceMetadb := GetMeta(sourceChainID, GetConfig(cJson), sourceCommonDBHandle, test.GoLogger{})

	sourceBF := GetBF(sourceChainID, GetConfig(cJson), sourceMetadb, test.GoLogger{})

	sourceBlockDB := GetBlockDB(sourceChainID, GetConfig(cJson), sourceBF, sourceBlockDBHandle, test.GoLogger{})
	sourceStateDB := GetStateDB(sourceChainID, GetConfig(cJson), sourceStateDBHandle, test.GoLogger{})
	sourceTxExistDB := GetTxExistDB(sourceChainID, GetConfig(cJson), sourceTxExistDBHandle, test.GoLogger{})
	sourceHistoryDB := GetHistoryDB(sourceChainID, GetConfig(cJson), sourceHistoryDBHandle, test.GoLogger{})
	sourceResultDB := GetResultDB(sourceChainID, GetConfig(cJson), sourceResultDBHandle, test.GoLogger{})

	// 2.create import ledger for chainB
	//blockdb handle.
	destBlockDBHandle, err := GetDBHandle(destChainID, provider, dbFolderBlockDB, blockDBConfig, test.GoLogger{}, nil)
	if err != nil {
		panic(err)
	}
	//statedb handle
	destStateDBHandle, err := GetDBHandle(destChainID, provider, dbFolderStateDB, stateDBConfig, test.GoLogger{}, nil)
	if err != nil {
		panic(err)
	}
	//common/localdb handle
	destCommonDBHandle, err := GetDBHandle(destChainID, provider, dbFolderCommonDB, commonDBConfig, test.GoLogger{}, nil)
	if err != nil {
		panic(err)
	}
	//txExistdb handle
	destTxExistDBHandle, err := GetDBHandle(destChainID, provider, dbFolderTxExistDB, txExistDBConfig, test.GoLogger{}, nil)
	if err != nil {
		panic(err)
	}
	//historydb handle
	destHistoryDBHandle, err := GetDBHandle(destChainID, provider, dbFolderHistoryDB, historyDBConfig, test.GoLogger{}, nil)
	if err != nil {
		panic(err)
	}
	//resultdb handle
	destResultDBHandle, err := GetDBHandle(destChainID, provider, dbFolderResultDB, resultDBConfig, test.GoLogger{}, nil)
	if err != nil {
		panic(err)
	}

	//meta
	destMetadb := GetMeta(destChainID, GetConfig(cJson), destCommonDBHandle, test.GoLogger{})

	//snapshot height is 1
	destBF := GetImportBF(destChainID, GetConfig(cJson), destMetadb, test.GoLogger{}, snapshotHeight)

	destBlockDB := GetBlockDB(destChainID, GetConfig(cJson), destBF, destBlockDBHandle, test.GoLogger{})
	destStateDB := GetStateDB(destChainID, GetConfig(cJson), destStateDBHandle, test.GoLogger{})
	destTxExistDB := GetTxExistDB(destChainID, GetConfig(cJson), destTxExistDBHandle, test.GoLogger{})
	destHistoryDB := GetHistoryDB(destChainID, GetConfig(cJson), destHistoryDBHandle, test.GoLogger{})
	destResultDB := GetResultDB(destChainID, GetConfig(cJson), destResultDBHandle, test.GoLogger{})

	chainBstoreImport, err := NewStoreImportSnapshot(destChainID, GetConfig(cJson), destBlockDB, destStateDB,
		destHistoryDB, nil, destResultDB, destTxExistDB, destCommonDBHandle,
		test.GoLogger{}, destBF, nil, destMetadb)
	if err != nil {
		panic(err)
	}

	// 3. write block0 ,block1 into chainA

	block0 := createConfBlock(sourceChainID, 0)
	txRWSets0 := getTxRWSets()

	block1 := createConfBlock(sourceChainID, 1)
	txRWSets1 := getTxRWSets()

	// 3.1 block0
	//序列化
	blockWithRWSet0 := &storePb.BlockWithRWSet{
		Block:    block0,
		TxRWSets: txRWSets0,
	}
	blockBytes, blockWithSerializedInfo, err := serialization.SerializeBlock(blockWithRWSet0)
	if err != nil {
		panic(err)
	}

	// write wal
	// wal log, index increase from 1, while blockHeight increase form 0
	fileName, offset, bytesLen, err := sourceBF.Write(1, blockBytes)
	if err != nil {
		panic(err)
	}
	blockIndex := &storePb.StoreInfo{
		FileName: fileName,
		Offset:   offset,
		ByteLen:  bytesLen,
	}
	blockWithSerializedInfo.Index = blockIndex

	// write blockDB stateDB txExistDB historyDB resultDB
	if err := sourceBlockDB.CommitBlock(blockWithSerializedInfo, true); err != nil {
		panic(err)
	}
	if err := sourceBlockDB.CommitBlock(blockWithSerializedInfo, false); err != nil {
		panic(err)
	}

	if err := sourceStateDB.CommitBlock(blockWithSerializedInfo, true); err != nil {
		panic(err)
	}
	if err := sourceStateDB.CommitBlock(blockWithSerializedInfo, false); err != nil {
		panic(err)
	}

	if err := sourceTxExistDB.CommitBlock(blockWithSerializedInfo, false); err != nil {
		panic(err)
	}

	if err := sourceHistoryDB.CommitBlock(blockWithSerializedInfo, true); err != nil {
		panic(err)
	}
	if err := sourceHistoryDB.CommitBlock(blockWithSerializedInfo, false); err != nil {
		panic(err)
	}

	if err := sourceResultDB.CommitBlock(blockWithSerializedInfo, true); err != nil {
		panic(err)
	}
	if err := sourceResultDB.CommitBlock(blockWithSerializedInfo, false); err != nil {
		panic(err)
	}

	// 3.2 block1
	//序列化
	blockWithRWSet1 := &storePb.BlockWithRWSet{
		Block:    block1,
		TxRWSets: txRWSets1,
	}
	blockBytes, blockWithSerializedInfo, err = serialization.SerializeBlock(blockWithRWSet1)
	if err != nil {
		panic(err)
	}
	// write wal
	// wal log, index increase from 1, while blockHeight increase form 0
	fileName, offset, bytesLen, err = sourceBF.Write(2, blockBytes)
	if err != nil {
		panic(err)
	}

	blockIndex = &storePb.StoreInfo{
		FileName: fileName,
		Offset:   offset,
		ByteLen:  bytesLen,
	}
	blockWithSerializedInfo.Index = blockIndex

	// write blockDB stateDB
	if err := sourceBlockDB.CommitBlock(blockWithSerializedInfo, true); err != nil {
		panic(err)
	}
	if err := sourceBlockDB.CommitBlock(blockWithSerializedInfo, false); err != nil {
		panic(err)
	}
	if err := sourceStateDB.CommitBlock(blockWithSerializedInfo, true); err != nil {
		panic(err)
	}
	if err := sourceStateDB.CommitBlock(blockWithSerializedInfo, false); err != nil {
		panic(err)
	}

	if err := sourceTxExistDB.CommitBlock(blockWithSerializedInfo, false); err != nil {
		panic(err)
	}

	if err := sourceHistoryDB.CommitBlock(blockWithSerializedInfo, true); err != nil {
		panic(err)
	}
	if err := sourceHistoryDB.CommitBlock(blockWithSerializedInfo, false); err != nil {
		panic(err)
	}

	if err := sourceResultDB.CommitBlock(blockWithSerializedInfo, true); err != nil {
		panic(err)
	}
	if err := sourceResultDB.CommitBlock(blockWithSerializedInfo, false); err != nil {
		panic(err)
	}

	type fields struct {
		height      uint64
		status      uint64
		storeChan   chan struct{}
		jobChan     chan *snapshotJob
		blockdb     blockdb.BlockDB
		statedb     statedb.StateDB
		eventdb     contracteventdb.ContractEventDB
		logger      protocol.Logger
		chainID     string
		storeConfig *conf.StorageConfig
	}
	type args struct {
		rootDir string
		height  uint64
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "test-send-snapshot-iterator-and-import-snapshot-into-another-chain",
			fields: fields{
				height:    snapshotHeight,
				status:    0,
				storeChan: make(chan struct{}, 10),
				jobChan:   make(chan *snapshotJob, 10),
				//blockdb:     nil,
				blockdb: sourceBlockDB,
				//statedb:     nil,
				statedb:     sourceStateDB,
				eventdb:     nil,
				logger:      test.GoLogger{},
				chainID:     sourceChainID,
				storeConfig: GetConfig(cJson),
			},
			args: args{
				rootDir: GetConfig(cJson).StorePath,
				height:  snapshotHeight,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &Snapshot{
				height:      tt.fields.height,
				status:      tt.fields.status,
				storeChan:   tt.fields.storeChan,
				jobChan:     tt.fields.jobChan,
				blockdb:     tt.fields.blockdb,
				statedb:     tt.fields.statedb,
				eventdb:     tt.fields.eventdb,
				logger:      tt.fields.logger,
				chainID:     tt.fields.chainID,
				storeConfig: tt.fields.storeConfig,
				RWMutex:     sync.RWMutex{},
			}
			//1.send snapshot iterator
			if err := s.SendSnapshotIterator(tt.args.rootDir, tt.args.height); (err != nil) != tt.wantErr {
				t.Errorf("SendSnapshotIterator() error = %v, wantErr %v", err, tt.wantErr)
			}
			//2.write snapshot data into file
			go s.RecevieSnapshotIterator()

			//3.make sure write file has finished
			time.Sleep(3 * time.Second)

			//return

			//4.import chainA's snapshot data from files into chainB
			if err := chainBstoreImport.ImportSnapshot(
				sourceChainID,
				GetConfig(cJson).StorePath,
				snapshotHeight); (err != nil) != tt.wantErr {
				t.Errorf("importSnapshot error = %v, wantErr %v", err, tt.wantErr)
			}

			//5.check difference between chainA and chainB
			//if tx id ,state kv, config block are the  same , the result is normal
			// 5.1 check tx id
			setA, err2 := sourceBlockDB.GetTxidSet()
			if err2 != nil {
				t.Errorf("importSnapshot error = %v, wantErr %v", err, tt.wantErr)
			}
			setB, err2 := destBlockDB.GetTxidSet()
			if err2 != nil {
				t.Errorf("importSnapshot error = %v, wantErr %v", err, tt.wantErr)
			}
			if len(setA) != len(setB) {
				t.Errorf("importSnapshot error,setA is not equal setB")
			}
			for i := 0; i < len(setA); i++ {
				for setA[i].Next() && setB[i].Next() {
					if !reflect.DeepEqual(setA[i].Key(), setB[i].Key()) {
						t.Errorf("importSnapshot error,setA is not equal setB")
					}
				}
			}

			// 5.2 check state kv
			stateIterA, err2 := sourceStateDB.GetSnapshotIterator()
			if err2 != nil {
				t.Errorf("importSnapshot error = %v, wantErr %v", err, tt.wantErr)
			}
			stateIterB, err2 := destStateDB.GetSnapshotIterator()
			if err2 != nil {
				t.Errorf("importSnapshot error = %v, wantErr %v", err, tt.wantErr)
			}
			if len(stateIterA) != len(stateIterB) {
				t.Errorf("importSnapshot error = %v, wantErr %v", err, tt.wantErr)
			}
			for i := 0; i < len(stateIterA); i++ {
				for stateIterA[i].Next() && stateIterB[i].Next() {
					if !reflect.DeepEqual(stateIterA[i].Key(), stateIterB[i].Key()) {
						t.Errorf("importSnapshot error = %v, wantErr %v", err, tt.wantErr)
					}
					if !reflect.DeepEqual(stateIterA[i].Value(), stateIterB[i].Value()) {
						t.Errorf("importSnapshot error = %v, wantErr %v", err, tt.wantErr)
					}
				}
			}

			// 5.3 check config block
			blockConfigA, err2 := sourceBlockDB.GetLastConfigBlock()
			if err2 != nil {
				t.Errorf("importSnapshot error = %v, wantErr %v", err, tt.wantErr)
			}
			blockConfigB, err2 := destBlockDB.GetLastConfigBlock()
			if err2 != nil {
				t.Errorf("importSnapshot error = %v, wantErr %v", err, tt.wantErr)
			}
			marshalA, err2 := blockConfigA.Marshal()
			if err2 != nil {
				t.Errorf("importSnapshot error = %v, wantErr %v", err, tt.wantErr)
			}
			marshalB, err2 := blockConfigB.Marshal()
			if err2 != nil {
				t.Errorf("importSnapshot error = %v, wantErr %v", err, tt.wantErr)
			}
			if !reflect.DeepEqual(marshalA, marshalB) {
				t.Errorf("importSnapshot error = %v, wantErr %v", err, tt.wantErr)
			}

			// 6.write a block into chainB
			// write block2 to chainB
			block2 := createConfBlock(sourceChainID, 2)
			txRWSets2 := getTxRWSets()

			// 6.1 block2
			//序列化
			blockWithRWSet2 := &storePb.BlockWithRWSet{
				Block:    block2,
				TxRWSets: txRWSets2,
			}
			blockBytes, blockWithSerializedInfo, err := serialization.SerializeBlock(blockWithRWSet2)
			if err != nil {
				t.Errorf("importSnapshot error = %v, wantErr %v", err, tt.wantErr)
				//panic(err)
			}

			// 6.2 write wal
			// wal log, index increase from 3, while blockHeight increase from 2
			fileName, offset, bytesLen, err := destBF.Write(3, blockBytes)
			if err != nil {
				t.Errorf("importSnapshot error = %v, wantErr %v", err, tt.wantErr)
				//panic(err)
			}
			blockIndex := &storePb.StoreInfo{
				FileName: fileName,
				Offset:   offset,
				ByteLen:  bytesLen,
			}
			blockWithSerializedInfo.Index = blockIndex

			// 6.3 write blockDB stateDB
			if err := destBlockDB.CommitBlock(blockWithSerializedInfo, true); err != nil {
				t.Errorf("importSnapshot error = %v, wantErr %v", err, tt.wantErr)
				//panic(err)
			}
			if err := destBlockDB.CommitBlock(blockWithSerializedInfo, false); err != nil {
				t.Errorf("importSnapshot error = %v, wantErr %v", err, tt.wantErr)
				//panic(err)
			}
			if err := destStateDB.CommitBlock(blockWithSerializedInfo, true); err != nil {
				t.Errorf("importSnapshot error = %v, wantErr %v", err, tt.wantErr)
				//panic(err)
			}
			if err := destStateDB.CommitBlock(blockWithSerializedInfo, false); err != nil {
				t.Errorf("importSnapshot error = %v, wantErr %v", err, tt.wantErr)
				//panic(err)
			}

		})
	}
}

func TestSnapshot_GetSnapshotStatus(t *testing.T) {
	type fields struct {
		height      uint64
		status      uint64
		storeChan   chan struct{}
		jobChan     chan *snapshotJob
		blockdb     blockdb.BlockDB
		statedb     statedb.StateDB
		eventdb     contracteventdb.ContractEventDB
		logger      protocol.Logger
		chainID     string
		storeConfig *conf.StorageConfig
	}
	tests := []struct {
		name   string
		fields fields
		want   uint64
	}{
		{
			name: "test-get-snapshot-status-0",
			fields: fields{
				height:      0,
				status:      0,
				storeChan:   make(chan struct{}, 0),
				jobChan:     make(chan *snapshotJob, 10),
				blockdb:     nil,
				statedb:     nil,
				eventdb:     nil,
				logger:      test.GoLogger{},
				chainID:     "test1",
				storeConfig: nil,
			},
			want: 1,
		},
		{
			name: "test-get-snapshot-status-1",
			fields: fields{
				height:      1,
				status:      0,
				storeChan:   make(chan struct{}, 0),
				jobChan:     make(chan *snapshotJob, 10),
				blockdb:     nil,
				statedb:     nil,
				eventdb:     nil,
				logger:      test.GoLogger{},
				chainID:     "test1",
				storeConfig: nil,
			},
			want: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &Snapshot{
				height:      tt.fields.height,
				status:      tt.fields.status,
				storeChan:   tt.fields.storeChan,
				jobChan:     tt.fields.jobChan,
				blockdb:     tt.fields.blockdb,
				statedb:     tt.fields.statedb,
				eventdb:     tt.fields.eventdb,
				logger:      tt.fields.logger,
				chainID:     tt.fields.chainID,
				storeConfig: tt.fields.storeConfig,
				RWMutex:     sync.RWMutex{},
			}
			if got := s.GetSnapshotStatus(); got != tt.want {
				t.Errorf("GetSnapshotStatus() = %v, want %v", got, tt.want)
			}
		})
	}
}

func createConfBlock(chainId string, height uint64) *commonPb.Block {
	block := &commonPb.Block{
		Header: &commonPb.BlockHeader{
			ChainId:     chainId,
			BlockHeight: height,
			Proposer: &acPb.Member{
				OrgId:      "org1",
				MemberInfo: []byte("User1"),
			},
		},
		Txs: []*commonPb.Transaction{
			{
				Payload: &commonPb.Payload{
					ChainId:      chainId,
					TxType:       commonPb.TxType_INVOKE_CONTRACT,
					TxId:         generateTxId(chainId, height, 0),
					ContractName: syscontract.SystemContract_CHAIN_CONFIG.String(),
				},
				Sender: &commonPb.EndorsementEntry{
					Signer: &acPb.Member{
						OrgId:      "org1",
						MemberInfo: []byte("User1"),
					},
					Signature: []byte("signature1"),
				},
				Result: &commonPb.Result{
					Code: commonPb.TxStatusCode_SUCCESS,
					ContractResult: &commonPb.ContractResult{
						Result: []byte("ok"),
					},
				},
			},
		},
	}

	block.Header.BlockHash = generateBlockHash(chainId, height)
	block.Txs[0].Payload.TxId = generateTxId(chainId, height, 0)

	return block
}

func generateTxId(chainId string, height uint64, index int) string {
	txIdBytes := sha256.Sum256([]byte(fmt.Sprintf("%s-%d-%d", chainId, height, index)))
	return hex.EncodeToString(txIdBytes[:])
}
func generateBlockHash(chainId string, height uint64) []byte {
	blockHash := sha256.Sum256([]byte(fmt.Sprintf("%s-%d", chainId, height)))
	return blockHash[:]
}
func initGenesis(s protocol.BlockchainStore, chainId string) {
	genesis := createConfigBlock(chainId, 0)
	g := &storePb.BlockWithRWSet{Block: genesis, TxRWSets: getTxRWSets()}
	_ = s.InitGenesis(g)
}
func createConfigBlock(chainId string, height uint64) *commonPb.Block {
	block := &commonPb.Block{
		Header: &commonPb.BlockHeader{
			ChainId:     chainId,
			BlockHeight: height,
			Proposer: &acPb.Member{
				OrgId:      "org1",
				MemberInfo: []byte("User1"),
			},
		},
		Txs: []*commonPb.Transaction{
			{
				Payload: &commonPb.Payload{
					ChainId:      chainId,
					TxType:       commonPb.TxType_INVOKE_CONTRACT,
					ContractName: syscontract.SystemContract_CHAIN_CONFIG.String(),
					TxId:         generateTxId(chainId, height, 0),
				},
				Sender: &commonPb.EndorsementEntry{
					Signer: &acPb.Member{
						OrgId:      "org1",
						MemberInfo: []byte("User1"),
					},
					Signature: []byte("signature1"),
				},
				Result: &commonPb.Result{
					Code: commonPb.TxStatusCode_SUCCESS,
					ContractResult: &commonPb.ContractResult{
						Result: []byte("ok"),
					},
				},
			},
		},
	}

	block.Header.BlockHash = generateBlockHash(chainId, height)
	block.Txs[0].Payload.TxId = generateTxId(chainId, height, 0)
	return block
}
func getTxRWSets() []*commonPb.TxRWSet {
	return []*commonPb.TxRWSet{
		{
			//TxId: "abcdefg",
			TxWrites: []*commonPb.TxWrite{
				{
					Key:          []byte("key1"),
					Value:        []byte("value1"),
					ContractName: defaultSysContractName,
				},
				{
					Key:          []byte("key2"),
					Value:        []byte("value2"),
					ContractName: defaultSysContractName,
				},
				{
					Key:          []byte("key3"),
					Value:        nil,
					ContractName: defaultSysContractName,
				},
			},
		},
	}
}
