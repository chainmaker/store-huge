#!/bin/bash
#create user sqlkv@'%' identified by sqlkvpass
docker exec -i mysql33666 sh -c 'exec mysql -uroot -pmysqlroot' <<EOF
CREATE USER 'sqlkv'@'%' IDENTIFIED BY 'sqlkvpass';
grant all privileges on *.* to 'sqlkv'@'%';
flush privileges;
exit
EOF