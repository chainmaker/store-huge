#!/bin/bash
count=`docker ps -a | grep mysql33666|grep -v grep | wc -l`;
if [ $count -gt 0 ];
then
  echo 'mysql docker mysql33666 exists,we will stop it and rm it,then we run a new docker container of mysql for unit testing';
  docker stop mysql33666
  docker rm   mysql33666
fi
# use mysql:5.7 replace with mysql:8.0
docker run -d -p 33666:3306 --name mysql33666 -e MYSQL_ROOT_PASSWORD=mysqlroot -d mysql:5.7