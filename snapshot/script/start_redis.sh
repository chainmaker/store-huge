#!/bin/bash
# 根据配置，启动一组redis
portPre=751

# 先关闭
for i in `seq 0 2`;
do
  for containId in `docker ps -a |grep  redis-redisbloom${portPre}${i} |grep ${portPre} | awk '{print $1}'|grep -v grep | head -n 1`
  do
    if [[ $containId != "" ]];then
      docker stop $containId
      docker rm $containId
    fi
  done
done

# 再开启
for i in `seq 0 2`;
do
  containId=`docker run -d -p ${portPre}${i}:6379 --name redis-redisbloom${portPre}$i redislabs/rebloom:latest`
  if [[ $? -eq 0 ]];then
    continue
  else
    exit 2
  fi
done

echo "finish start all redisbloom docker container"
exit 0

