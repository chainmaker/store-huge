#!/bin/bash
# 关闭 一组 redis
portPre=751

for i in `seq 0 2`;
do
  for containId in `docker ps -a |grep  redis-redisbloom${portPre}${i} |grep ${portPre} | awk '{print $1}'`
  do
    if [[ $containId != "" ]];then
      docker stop $containId
      docker rm $containId
    fi
  done
done
echo "finish stop and rm all redisbloom docker container"
exit 0