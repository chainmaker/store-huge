CREATE USER 'sqlkv'@'%' IDENTIFIED BY 'sqlkvpass';

grant all privileges on *.* to 'sqlkv'@'%';

flush privileges;