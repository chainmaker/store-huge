//+build !windows

package snapshot

import (
	"bytes"
	"errors"
	"reflect"
	"time"

	"chainmaker.org/chainmaker/common/v3/crypto"
	acPb "chainmaker.org/chainmaker/pb-go/v3/accesscontrol"
	commonPb "chainmaker.org/chainmaker/pb-go/v3/common"
	storePb "chainmaker.org/chainmaker/pb-go/v3/store"
	"chainmaker.org/chainmaker/pb-go/v3/syscontract"
	"chainmaker.org/chainmaker/protocol/v3"
	"chainmaker.org/chainmaker/protocol/v3/test"
	badgerdbprovider "chainmaker.org/chainmaker/store-badgerdb/v3"
	"chainmaker.org/chainmaker/store-huge/v3/bigfilterdb"
	"chainmaker.org/chainmaker/store-huge/v3/bigfilterdb/bigfilterkvdb"
	"chainmaker.org/chainmaker/store-huge/v3/historydb"
	"chainmaker.org/chainmaker/store-huge/v3/historydb/historykvdb"
	"chainmaker.org/chainmaker/store-huge/v3/resultdb"
	"chainmaker.org/chainmaker/store-huge/v3/resultdb/resultkvdb"
	"chainmaker.org/chainmaker/store-huge/v3/serialization"
	"chainmaker.org/chainmaker/store-huge/v3/txexistdb"
	"chainmaker.org/chainmaker/store-huge/v3/txexistdb/txexistkvdb"
	leveldbprovider "chainmaker.org/chainmaker/store-leveldb/v3"
	rawsqlprovider "chainmaker.org/chainmaker/store-sqldb/v3"
	tikvdbprovider "chainmaker.org/chainmaker/store-tikv/v3"

	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"os/exec"
	"path/filepath"
	"strings"

	"chainmaker.org/chainmaker/store-huge/v3/binlog"
	"chainmaker.org/chainmaker/store-huge/v3/blockdb"
	"chainmaker.org/chainmaker/store-huge/v3/blockdb/blockfiledb"
	"chainmaker.org/chainmaker/store-huge/v3/conf"
	"chainmaker.org/chainmaker/store-huge/v3/contracteventdb"
	"chainmaker.org/chainmaker/store-huge/v3/contracteventdb/eventsqldb"
	"chainmaker.org/chainmaker/store-huge/v3/meta"
	"chainmaker.org/chainmaker/store-huge/v3/meta/metadb"
	"chainmaker.org/chainmaker/store-huge/v3/statedb"
	"chainmaker.org/chainmaker/store-huge/v3/statedb/statekvdb"
	"github.com/mitchellh/mapstructure"

	"sync"
	"testing"
)

var (
	defaultSysContractName = syscontract.SystemContract_CHAIN_CONFIG.String()
	globalConfig           *conf.StorageConfig
	storeTmpDir            string
	once                   = sync.Once{}
)

var cJson = `{
"StorePath":"./ledgerData1",
"DbPrefix":"preA",
"WriteBufferSize":0,
"BloomFilterBits":0,
"BlockWriteBufferSize":0,
"LogDBWriteAsync":false,
"BlockFileConfig":{
	"OnlineFileSystem":"/tmp/online1,/tmp/online2",
	"ArchiveFileSystem":"/tmp/archive1,/tmp/archive2"
},
"ConfigVersion":{
	"Major": 1,
	"Minor": 0
},
"BlockDbConfig":{
	"Provider":"leveldb",
	"LevelDbConfig":{
		"store_path":"./ledgerData1/data/org1/blocks"
	},
	"BadgerDbConfig":null,
	"SqlDbConfig":null
},
"TxExistDbConfig":{
	"Provider":"leveldb",
	"LevelDbConfig":{
		"store_path":"./ledgerData1/data/org1/txexist"
	},
	"BadgerDbConfig":null,
	"SqlDbConfig":null
},
"StateDbConfig":{
	"Provider":"leveldb",
	"LevelDbConfig":{"store_path":"./ledgerData1/data/org1/statedb"},
	"BadgerDbConfig":null,"SqlDbConfig":null
},
"HistoryDbConfig":{
	"Provider":"leveldb",
	"LevelDbConfig":{"store_path":"./ledgerData1/data/org1/history"},
	"BadgerDbConfig":null,"SqlDbConfig":null
},
"ResultDbConfig":{
	"Provider":"leveldb",
	"LevelDbConfig":{"store_path":"./ledgerData1/data/org1/result"},
	"BadgerDbConfig":null,
	"SqlDbConfig":null
},
"ContractEventDbConfig":{
	"Provider":"sql",
	"LevelDbConfig":null,
	"BadgerDbConfig":null,
	"SqlDbConfig":{
		"dsn":"root:mysqlroot@tcp(127.0.0.1:33666)/",
		"sqldb_type":"mysql"
	}
},
"EnableBigFilter":true,
"BigFilter":{
	"RedisHosts":"127.0.0.1:7510,127.0.0.1:7511,127.0.0.1:7512",
    "Pass":null,
    "TxCapacity":1000000000,
    "FpRate":0.000000001
},
"UnArchiveBlockHeight":0,
"Encryptor":"sm4",
"EncryptKey":"1234567890123456"
}`

func GetConfig(cJson string) (*conf.StorageConfig, string) {
	once.Do(func() {
		globalConfig = &conf.StorageConfig{}
		err := json.Unmarshal([]byte(cJson), globalConfig)
		if err != nil {
			panic(err)
		}

		unixTime := time.Now().Unix()
		//create tmpDir for Store path
		storeTmpDir = fmt.Sprintf("tmp_ledgerData1_%d", unixTime)
		globalConfig.StorePath = storeTmpDir

		//create tmpDir for online file system path and archive file system path
		onlineFSTmpDir := fmt.Sprintf("/tmp/online1_%d,/tmp/online2_%d", unixTime, unixTime)
		archiveFSTmpDir := fmt.Sprintf("/tmp/archive1_%d,/tmp/archive2_%d", unixTime, unixTime)
		globalConfig.BlockFileConfig.OnlineFileSystem = onlineFSTmpDir
		globalConfig.BlockFileConfig.ArchiveFileSystem = archiveFSTmpDir
	})
	return globalConfig, storeTmpDir
	/*
		config := &conf.StorageConfig{}

		//config.DisableBigFilter = true
		err := json.Unmarshal([]byte(cJson), config)
		if err != nil {
			panic(err)
		}
		return config

	*/
}

func GetDBHandle(chainId, providerName, dbFolder string,
	config map[string]interface{}, logger protocol.Logger,
	encryptor crypto.SymmetricKey) (protocol.DBHandle, error) {
	providerName = strings.ToLower(providerName)
	switch providerName {
	case conf.DbconfigProviderLeveldb:
		dbConfig := &leveldbprovider.LevelDbConfig{}
		err := mapstructure.Decode(config, dbConfig)
		if err != nil {
			return nil, err
		}
		input := &leveldbprovider.NewLevelDBOptions{
			Config:    dbConfig,
			Logger:    logger,
			Encryptor: encryptor,
			ChainId:   chainId,
			DbFolder:  dbFolder,
		}
		return leveldbprovider.NewLevelDBHandle(input), nil
	case conf.DbconfigProviderBadgerdb:
		dbConfig := &badgerdbprovider.BadgerDbConfig{}
		err := mapstructure.Decode(config, dbConfig)
		if err != nil {
			return nil, err
		}
		input := &badgerdbprovider.NewBadgerDBOptions{
			Config:    dbConfig,
			Logger:    logger,
			Encryptor: encryptor,
			ChainId:   chainId,
			DbFolder:  dbFolder,
		}
		return badgerdbprovider.NewBadgerDBHandle(input), nil
	case conf.DbconfigProviderTikvdb:
		dbConfig := &tikvdbprovider.TiKVDbConfig{}
		err := mapstructure.Decode(config, dbConfig)
		if err != nil {
			return nil, err
		}
		input := &tikvdbprovider.NewTikvDBOptions{
			Config:    dbConfig,
			Logger:    logger,
			Encryptor: encryptor,
			ChainId:   chainId,
			DbName:    dbFolder,
		}
		return tikvdbprovider.NewTiKVDBHandle(input), nil
	case conf.DbconfigProviderSqlKV:
		dbConfig := &rawsqlprovider.SqlDbConfig{}
		err := mapstructure.Decode(config, dbConfig)
		if err != nil {
			return nil, err
		}
		input := &rawsqlprovider.NewSqlDBOptions{
			Config:    dbConfig,
			Logger:    logger,
			Encryptor: encryptor,
			ChainId:   chainId,
			DbName:    dbFolder,
		}
		return rawsqlprovider.NewKVDBHandle(input), nil
	case conf.DbconfigProviderMemdb:
		return leveldbprovider.NewMemdbHandle(), nil
	default:
		return nil, fmt.Errorf("unsupported provider:%s", providerName)

	}
}

func GetSQLDBHandle(chainId, dbName string, config map[string]interface{},
	logger protocol.Logger) protocol.SqlDBHandle {

	dbConfig := &rawsqlprovider.SqlDbConfig{}
	err := mapstructure.Decode(config, dbConfig)
	if err != nil {
		panic(err)
	}
	input := &rawsqlprovider.NewSqlDBOptions{
		Config:    dbConfig,
		Logger:    logger,
		Encryptor: nil,
		ChainId:   chainId,
		DbName:    dbName,
	}
	logger.Debugf("initial new sql db for %s", dbName)
	return rawsqlprovider.NewSqlDBHandle(input)
}

func GetMeta(chainID string, storeConfig *conf.StorageConfig, commonDB protocol.DBHandle,
	logger protocol.Logger) meta.MetaData {

	metadb, err := metadb.NewMetaDataDB(*storeConfig, chainID, commonDB, logger)
	if err != nil {
		panic(err)
	}
	return metadb
}

func GetBF(chainID string, storeConfig *conf.StorageConfig, metadb meta.MetaData,
	logger protocol.Logger) binlog.BinLogger {
	var (
		err error
		bf  *blockfiledb.BlockFile
	)
	blockFilePath := "bfdb"

	opts := blockfiledb.DefaultOptions
	opts.NoCopy = true
	opts.NoSync = storeConfig.LogDBSegmentAsync
	if storeConfig.LogDBSegmentSize > 64 { // LogDBSegmentSize default is 64MB
		opts.SegmentSize = storeConfig.LogDBSegmentSize * 1024 * 1024
	}
	if storeConfig.DisableLogDBMmap {
		opts.UseMmap = false
	}
	bfPath := filepath.Join(storeConfig.StorePath, chainID, blockFilePath)
	bf, err = blockfiledb.Open(bfPath, opts, logger, metadb)

	if err != nil {
		panic(fmt.Sprintf("open block file db failed, path:%s, error:%s", bfPath, err))
	}
	return bf
}

func GetImportBF(chainID string, storeConfig *conf.StorageConfig, metadb meta.MetaData,
	logger protocol.Logger, height uint64) binlog.BinLogger {
	var (
		err error
		bf  *blockfiledb.BlockFile
	)
	//blockFilePath := "bfdb"

	opts := blockfiledb.DefaultOptions
	opts.NoCopy = true
	opts.NoSync = storeConfig.LogDBSegmentAsync
	if storeConfig.LogDBSegmentSize > 64 { // LogDBSegmentSize default is 64MB
		opts.SegmentSize = storeConfig.LogDBSegmentSize * 1024 * 1024
	}
	if storeConfig.DisableLogDBMmap {
		opts.UseMmap = false
	}
	//bfPath := filepath.Join(storeConfig.StorePath, chainID, blockFilePath)
	bf, err = blockfiledb.ImportOpen(opts, logger, metadb, height)

	if err != nil {
		panic(fmt.Sprintf("open block file db failed, error:%s", err))
	}
	return bf
}

func GetBlockDB(chainId string, storeConfig *conf.StorageConfig,
	bf binlog.BinLogger, blockDBHandle protocol.DBHandle, logger protocol.Logger) blockdb.BlockDB {
	//dbPrefix := storeConfig.DbPrefix
	db := blockfiledb.NewBlockFileDB(chainId, blockDBHandle, logger, storeConfig, bf)
	return db
}

func GetStateDB(chainId string, storeConfig *conf.StorageConfig, stateDBHandle protocol.DBHandle,
	logger protocol.Logger) statedb.StateDB {

	db := statekvdb.NewStateKvDB(chainId, stateDBHandle, logger, storeConfig)
	return db
}

func GetEventDB(chainId string, storeConfig *conf.StorageConfig, eventDBHandle protocol.SqlDBHandle,
	logger protocol.Logger) contracteventdb.ContractEventDB {

	dbName := getDbName(storeConfig.DbPrefix, "eventdb", chainId)

	db, err := eventsqldb.NewContractEventDB(dbName, eventDBHandle, logger, storeConfig, chainId)
	if err != nil {
		panic(err)
	}

	return db
}

func GetHistoryDB(chainId string, storeConfig *conf.StorageConfig, historyDBHandle protocol.DBHandle,
	logger protocol.Logger) historydb.HistoryDB {

	db := historykvdb.NewHistoryKvDB(chainId, storeConfig.HistoryDbConfig, historyDBHandle, logger)
	return db
}

func GetResultDB(chainId string, storeConfig *conf.StorageConfig, resultDBHandle protocol.DBHandle,
	logger protocol.Logger) resultdb.ResultDB {

	db := resultkvdb.NewResultKvDB(chainId, resultDBHandle, logger, storeConfig)
	return db
}

func GetTxExistDB(chainId string, storeConfig *conf.StorageConfig, txExistDBHandle protocol.DBHandle,
	logger protocol.Logger) txexistdb.TxExistDB {

	db := txexistkvdb.NewTxExistKvDB(chainId, txExistDBHandle, logger)
	return db
}

func GetContractEventDB(dbName string, db protocol.SqlDBHandle, logger protocol.Logger,
	storeConfig *conf.StorageConfig, chainId string) contracteventdb.ContractEventDB {
	eventdb, err := eventsqldb.NewContractEventDB(dbName, db, logger, storeConfig, chainId)
	if err != nil {
		panic(err)
	}
	return eventdb
}

func GetBigFilterDB(chainID string, bigFilterConfig *conf.BigFilterConfig, logger protocol.Logger) bigfilterdb.BigFilterDB {
	redisHosts := strings.Split(bigFilterConfig.RedisHosts, ",")
	filterNum := len(redisHosts)
	if bigFilterConfig.Pass == "" {
		b, err := bigfilterkvdb.NewBigFilterKvDB(filterNum, bigFilterConfig.TxCapacity, bigFilterConfig.FpRate,
			logger, redisHosts, nil, chainID)
		if err != nil {
			panic(err)
		}
		return b
	}
	b, err := bigfilterkvdb.NewBigFilterKvDB(filterNum, bigFilterConfig.TxCapacity, bigFilterConfig.FpRate,
		logger, redisHosts, &bigFilterConfig.Pass, chainID)
	if err != nil {
		panic(err)
	}
	return b
}

//为了支持多链和多个db
func getDbName(dbPrefix, dbName, chainId string) string {
	return dbPrefix + dbName + "_" + chainId
}

func TestNewSnapshot(t *testing.T) {
	sConfig, tmpDir := GetConfig(cJson)
	//defer os.RemoveAll(tmpDir)
	defer destroyFileSystem(tmpDir)
	type args struct {
		blockdb     blockdb.BlockDB
		statedb     statedb.StateDB
		eventdb     contracteventdb.ContractEventDB
		logger      protocol.Logger
		chainID     string
		storeConfig *conf.StorageConfig
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "test-new-snapshot",
			args: args{
				blockdb:     nil,
				statedb:     nil,
				eventdb:     nil,
				logger:      test.GoLogger{},
				chainID:     "test-chain-id",
				storeConfig: sConfig,
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewSnapshot(tt.args.blockdb, tt.args.statedb, tt.args.eventdb,
				tt.args.logger, tt.args.chainID, tt.args.storeConfig)
			if got == nil {
				t.Errorf("NewSnapshot() = %v, want %v", got == nil, tt.want)
			}
		})
	}
}

func TestSnapshot_MakeSnapshot(t *testing.T) {
	type fields struct {
		height      uint64
		status      uint64
		storeChan   chan struct{}
		jobChan     chan *snapshotJob
		blockdb     blockdb.BlockDB
		statedb     statedb.StateDB
		eventdb     contracteventdb.ContractEventDB
		logger      protocol.Logger
		chainID     string
		storeConfig *conf.StorageConfig
	}
	type args struct {
		currHeight uint64
		height     uint64
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "test-make-snapshot",
			fields: fields{
				height:      0,
				status:      0,
				storeChan:   make(chan struct{}),
				jobChan:     make(chan *snapshotJob, 10),
				blockdb:     nil,
				statedb:     nil,
				eventdb:     nil,
				logger:      test.GoLogger{},
				chainID:     "test1",
				storeConfig: nil,
			},
			args: args{
				currHeight: 0,
				height:     1,
			},
			wantErr: false,
		},
		{
			name: "test-make-snapshot-err-snapshot-height",
			fields: fields{
				height:      0,
				status:      0,
				storeChan:   make(chan struct{}),
				jobChan:     make(chan *snapshotJob, 10),
				blockdb:     nil,
				statedb:     nil,
				eventdb:     nil,
				logger:      test.GoLogger{},
				chainID:     "test1",
				storeConfig: nil,
			},
			args: args{
				currHeight: 1,
				height:     1,
			},
			wantErr: true,
		},
		{
			name: "test-make-snapshot-err-snapshot-is-being",
			fields: fields{
				height:      2,
				status:      0,
				storeChan:   make(chan struct{}),
				jobChan:     make(chan *snapshotJob, 10),
				blockdb:     nil,
				statedb:     nil,
				eventdb:     nil,
				logger:      test.GoLogger{},
				chainID:     "test1",
				storeConfig: nil,
			},
			args: args{
				currHeight: 1,
				height:     2,
			},
			wantErr: true,
		},
		{
			name: "test-make-snapshot-err-snapshot-is-being-2",
			fields: fields{
				height:      2,
				status:      0,
				storeChan:   make(chan struct{}),
				jobChan:     make(chan *snapshotJob, 10),
				blockdb:     nil,
				statedb:     nil,
				eventdb:     nil,
				logger:      test.GoLogger{},
				chainID:     "test1",
				storeConfig: nil,
			},
			args: args{
				currHeight: 1,
				height:     3,
			},
			wantErr: true,
		},
		{
			name: "test-make-snapshot-snapshot-finish",
			fields: fields{
				height:      2,
				status:      1,
				storeChan:   make(chan struct{}),
				jobChan:     make(chan *snapshotJob, 10),
				blockdb:     nil,
				statedb:     nil,
				eventdb:     nil,
				logger:      test.GoLogger{},
				chainID:     "test1",
				storeConfig: nil,
			},
			args: args{
				currHeight: 1,
				height:     2,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &Snapshot{
				height:      tt.fields.height,
				status:      tt.fields.status,
				storeChan:   tt.fields.storeChan,
				jobChan:     tt.fields.jobChan,
				blockdb:     tt.fields.blockdb,
				statedb:     tt.fields.statedb,
				eventdb:     tt.fields.eventdb,
				logger:      tt.fields.logger,
				chainID:     tt.fields.chainID,
				storeConfig: tt.fields.storeConfig,
				RWMutex:     sync.RWMutex{},
			}
			if err := s.MakeSnapshot(tt.args.currHeight, tt.args.height); (err != nil) != tt.wantErr {
				t.Errorf("MakeSnapshot() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestSnapshot_WaitingSnapshotJob(t *testing.T) {
	type fields struct {
		height      uint64
		status      uint64
		storeChan   chan struct{}
		jobChan     chan *snapshotJob
		blockdb     blockdb.BlockDB
		statedb     statedb.StateDB
		eventdb     contracteventdb.ContractEventDB
		logger      protocol.Logger
		chainID     string
		storeConfig *conf.StorageConfig
	}
	type args struct {
		height uint64
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "test-waiting-snapshot-job",
			fields: fields{
				height:      0,
				status:      0,
				storeChan:   make(chan struct{}, 10),
				jobChan:     make(chan *snapshotJob, 10),
				blockdb:     nil,
				statedb:     nil,
				eventdb:     nil,
				logger:      test.GoLogger{},
				chainID:     "test1",
				storeConfig: nil,
			},
			args: args{
				height: 1,
			},
			wantErr: false,
		},
		{
			name: "test-waiting-snapshot-job-2",
			fields: fields{
				height:      1,
				status:      0,
				storeChan:   make(chan struct{}, 10),
				jobChan:     make(chan *snapshotJob, 10),
				blockdb:     nil,
				statedb:     nil,
				eventdb:     nil,
				logger:      test.GoLogger{},
				chainID:     "test1",
				storeConfig: nil,
			},
			args: args{
				height: 1,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &Snapshot{
				height:      tt.fields.height,
				status:      tt.fields.status,
				storeChan:   tt.fields.storeChan,
				jobChan:     tt.fields.jobChan,
				blockdb:     tt.fields.blockdb,
				statedb:     tt.fields.statedb,
				eventdb:     tt.fields.eventdb,
				logger:      tt.fields.logger,
				chainID:     tt.fields.chainID,
				storeConfig: tt.fields.storeConfig,
				RWMutex:     sync.RWMutex{},
			}
			s.storeChan <- struct{}{}
			if err := s.WaitingSnapshotJob(tt.args.height); (err != nil) != tt.wantErr {
				t.Errorf("WaitingSnapshotJob() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func initMysqlDocker() error {
	//destroyMysqlDocker()

	cmdStart := exec.Command("/bin/bash", "./script/start_mysql_docker.sh")
	var stdout, stderr bytes.Buffer
	cmdStart.Stdout = &stdout
	cmdStart.Stderr = &stderr
	err := cmdStart.Run()
	if err != nil {
		fmt.Printf("start mysql error :[%s]\n", err)
		panic(err)
	}
	outStr, errStr := stdout.String(), stderr.String()
	fmt.Printf("cmdStart out:[%s]\n", outStr)
	if errStr != "" && !strings.Contains(errStr, "Warning") {
		fmt.Printf("cmdStart err:[%s]\n", errStr)
		panic(err)
	}

	time.Sleep(100 * time.Second)
	//cmdCreateUser := exec.Command("/bin/bash", "./script/create_mysql_user.sh")
	//cmdCreateUser.Stdout = &stdout
	//cmdCreateUser.Stderr = &stderr
	//err = cmdCreateUser.Run()
	//if err != nil {
	//	fmt.Printf("create mysql user error :[%s]\n", err)
	//	panic(err)
	//}
	//outStr, errStr = stdout.String(), stderr.String()
	//fmt.Printf("cmdCreateUser out:[%s]\n", outStr)
	//if (errStr != "") && (!strings.Contains(errStr, "Warning")) {
	//	fmt.Printf("cmdCreateUser std error,err:[%s]\n", errStr)
	//	panic(errStr)
	//}
	return nil
}

func destroyMysqlDocker() {
	cmd := exec.Command("/bin/bash", "./script/stop_mysql_docker.sh")
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()
	if err != nil {
		fmt.Printf("stop mysql error :[%s]", err)
	}
	//outStr, errStr := string(stdout.Bytes()), string(stderr.Bytes())
	outStr, errStr := stdout.String(), stderr.String()
	fmt.Printf("stop mysql out:[%s]\n", outStr)
	if errStr != "" {
		fmt.Printf("stop mysql err:[%s]\n", errStr)
	}
}

// TestSnapshot_MakeSnapshotAndImportSnapshot send snapshot then write to file from chainA and import snapshot into chainB
// first, send snapshot iterator from chainA
// second, write snapshot data into files
// third, import snapshot files into chainB
// finally, start chainB ,check the difference between  chainB and chainA ,then write a new block into chainB
func TestSnapshot_MakeSnapshotAndImportSnapshot(t *testing.T) {
	var (
		snapshotHeight        uint64 = 1
		sourceChainID                = "test_chainA"
		destChainID                  = "test_chainB"
		provider                     = "leveldb"
		dbFolderBlockDB              = "blockdb"
		dbFolderStateDB              = "statedb"
		dbFolderCommonDB             = "commondb"
		dbFolderTxExistDB            = "txexistb"
		dbFolderHistoryDB            = "historydb"
		dbFolderResultDB             = "resultdb"
		dbNameContractEventDB        = "contractEventdb"
		err                   error
	)
	initMysqlDocker()
	initRedisHosts()

	defer destroyRedisHosts()
	defer destroyMysqlDocker()

	sConfig, tmpDir := GetConfig(cJson)
	//defer os.RemoveAll(tmpDir)
	defer destroyFileSystem(tmpDir)

	// 1,2,3  create chainA,chainB ,and write block0 ,block1 into chainA
	sourceBlockDB, sourceStateDB, _, _, sourceContractEventDB,
		_, _, _, _,
		destBlockDB, destStateDB, _, _, _, _, _, destBF, _,
		chainBStoreImport := prepareForMakeSnapshotTest(
		sConfig, sourceChainID, destChainID, provider, dbFolderBlockDB, dbFolderStateDB, dbFolderCommonDB,
		dbFolderTxExistDB, dbFolderHistoryDB, dbFolderResultDB, dbNameContractEventDB, snapshotHeight)

	// 4. make snapshot
	wantErr := false
	s := &Snapshot{
		height:      snapshotHeight,
		status:      0,
		storeChan:   make(chan struct{}, 10),
		jobChan:     make(chan *snapshotJob, 10),
		blockdb:     sourceBlockDB,
		statedb:     sourceStateDB,
		eventdb:     sourceContractEventDB,
		logger:      test.GoLogger{},
		chainID:     sourceChainID,
		storeConfig: sConfig,
		RWMutex:     sync.RWMutex{},
	}
	// 4.1.send snapshot iterator
	if err = s.SendSnapshotIterator(sConfig.StorePath, snapshotHeight); (err != nil) != wantErr {
		t.Errorf("SendSnapshotIterator() error = %v, wantErr %v", err, wantErr)
	}
	// 4.2.write snapshot data into file
	go s.RecevieSnapshotIterator()

	// 4.3.make sure write file has finished
	time.Sleep(3 * time.Second)

	// 5.import chainA's snapshot data from files into chainB
	if err = chainBStoreImport.ImportSnapshot(
		sourceChainID,
		sConfig.StorePath,
		snapshotHeight); (err != nil) != wantErr {
		t.Errorf("importSnapshot error = %v, wantErr %v", err, wantErr)
	}

	// 6.check difference between chainA and chainB
	//if tx id ,state kv, config block are the  same , the result is normal
	if err = checkDifferentChainAChainB(
		t, wantErr, sourceBlockDB, destBlockDB, sourceStateDB, destStateDB); (err != nil) != wantErr {
		t.Errorf("importSnapshot error = %v, wantErr %v", err, wantErr)
	}

	// 7.write a block into chainB
	// write block2 which is a new block to chainB,
	// if there is no problem, prove that chainB is import data successful and available
	block2 := createConfBlock(sourceChainID, 2)
	txRWSets2 := getTxRWSets()

	if err = commitBlockToImportLedger(
		t, wantErr, block2, txRWSets2, destBlockDB, destStateDB, destBF, 2+1); (err != nil) != wantErr {
		t.Errorf("importSnapshot error = %v, wantErr %v", err, wantErr)
	}
}

func prepareForMakeSnapshotTest(
	sConfig *conf.StorageConfig, sourceChainID, destChainID string,
	provider string, dbFolderBlockDB, dbFolderStateDB, dbFolderCommonDB,
	dbFolderTxExistDB, dbFolderHistoryDB, dbFolderResultDB, dbNameContractEventDB string,
	snapshotHeight uint64) (blockdb.BlockDB, statedb.StateDB,
	resultdb.ResultDB, historydb.HistoryDB,
	contracteventdb.ContractEventDB, bigfilterdb.BigFilterDB,
	txexistdb.TxExistDB, binlog.BinLogger, meta.MetaData,

	blockdb.BlockDB, statedb.StateDB,
	resultdb.ResultDB, historydb.HistoryDB,
	contracteventdb.ContractEventDB, bigfilterdb.BigFilterDB,
	txexistdb.TxExistDB, binlog.BinLogger, meta.MetaData,
	protocol.ImportLedgerSnapshot) {

	blockDBConfig := sConfig.BlockDbConfig.GetDbConfig()
	stateDBConfig := sConfig.StateDbConfig.GetDbConfig()
	commonDBConfig := sConfig.GetDefaultDBConfig().GetDbConfig()
	txExistDBConfig := sConfig.TxExistDbConfig.GetDbConfig()
	resultDBConfig := sConfig.ResultDbConfig.GetDbConfig()
	historyDBConfig := sConfig.HistoryDbConfig.GetDbConfig()
	contractEventDBConfig := sConfig.ContractEventDbConfig.GetDbConfig()

	// 1.create a source chain's object ,include sourceBlockDB and sourceStateDB
	//blockdb handle.
	sourceBlockDBHandle, err := GetDBHandle(sourceChainID, provider, dbFolderBlockDB, blockDBConfig, test.GoLogger{}, nil)
	if err != nil {
		panic(err)
	}
	//statedb handle
	sourceStateDBHandle, err := GetDBHandle(sourceChainID, provider, dbFolderStateDB, stateDBConfig, test.GoLogger{}, nil)
	if err != nil {
		panic(err)
	}
	//common/localdb handle
	sourceCommonDBHandle, err := GetDBHandle(sourceChainID, provider, dbFolderCommonDB, commonDBConfig, test.GoLogger{}, nil)
	if err != nil {
		panic(err)
	}
	//txExistdb handle
	sourceTxExistDBHandle, err := GetDBHandle(sourceChainID, provider, dbFolderTxExistDB, txExistDBConfig, test.GoLogger{}, nil)
	if err != nil {
		panic(err)
	}
	//historydb handle
	sourceHistoryDBHandle, err := GetDBHandle(sourceChainID, provider, dbFolderHistoryDB, historyDBConfig, test.GoLogger{}, nil)
	if err != nil {
		panic(err)
	}
	//resultdb handle
	sourceResultDBHandle, err := GetDBHandle(sourceChainID, provider, dbFolderResultDB, historyDBConfig, test.GoLogger{}, nil)
	if err != nil {
		panic(err)
	}
	//contractEventDB handle(sql)

	dbNameEventSQLDB := getDbName(sConfig.DbPrefix, dbNameContractEventDB, sourceChainID)
	sourceContractEventDBHandle := GetSQLDBHandle(sourceChainID, dbNameEventSQLDB, contractEventDBConfig, test.GoLogger{})

	//meta
	sourceMetadb := GetMeta(sourceChainID, sConfig, sourceCommonDBHandle, test.GoLogger{})

	sourceBF := GetBF(sourceChainID, sConfig, sourceMetadb, test.GoLogger{})

	sourceBlockDB := GetBlockDB(sourceChainID, sConfig, sourceBF, sourceBlockDBHandle, test.GoLogger{})
	sourceStateDB := GetStateDB(sourceChainID, sConfig, sourceStateDBHandle, test.GoLogger{})
	sourceTxExistDB := GetTxExistDB(sourceChainID, sConfig, sourceTxExistDBHandle, test.GoLogger{})
	sourceHistoryDB := GetHistoryDB(sourceChainID, sConfig, sourceHistoryDBHandle, test.GoLogger{})
	sourceResultDB := GetResultDB(sourceChainID, sConfig, sourceResultDBHandle, test.GoLogger{})
	sourceContractEventDB := GetContractEventDB(dbNameEventSQLDB, sourceContractEventDBHandle,
		test.GoLogger{}, sConfig, sourceChainID)

	sourceBigfilterDB := GetBigFilterDB(sourceChainID, sConfig.BigFilter, test.GoLogger{})

	// 2.create import ledger for chainB
	//blockdb handle.
	destBlockDBHandle, err := GetDBHandle(destChainID, provider, dbFolderBlockDB, blockDBConfig, test.GoLogger{}, nil)
	if err != nil {
		panic(err)
	}
	//statedb handle
	destStateDBHandle, err := GetDBHandle(destChainID, provider, dbFolderStateDB, stateDBConfig, test.GoLogger{}, nil)
	if err != nil {
		panic(err)
	}
	//common/localdb handle
	destCommonDBHandle, err := GetDBHandle(destChainID, provider, dbFolderCommonDB, commonDBConfig, test.GoLogger{}, nil)
	if err != nil {
		panic(err)
	}
	//txExistdb handle
	destTxExistDBHandle, err := GetDBHandle(destChainID, provider, dbFolderTxExistDB, txExistDBConfig, test.GoLogger{}, nil)
	if err != nil {
		panic(err)
	}
	//historydb handle
	destHistoryDBHandle, err := GetDBHandle(destChainID, provider, dbFolderHistoryDB, historyDBConfig, test.GoLogger{}, nil)
	if err != nil {
		panic(err)
	}
	//resultdb handle
	destResultDBHandle, err := GetDBHandle(destChainID, provider, dbFolderResultDB, resultDBConfig, test.GoLogger{}, nil)
	if err != nil {
		panic(err)
	}

	//meta
	destMetadb := GetMeta(destChainID, sConfig, destCommonDBHandle, test.GoLogger{})

	//snapshot height is 1
	destBF := GetImportBF(destChainID, sConfig, destMetadb, test.GoLogger{}, snapshotHeight)

	destBlockDB := GetBlockDB(destChainID, sConfig, destBF, destBlockDBHandle, test.GoLogger{})
	destStateDB := GetStateDB(destChainID, sConfig, destStateDBHandle, test.GoLogger{})
	destTxExistDB := GetTxExistDB(destChainID, sConfig, destTxExistDBHandle, test.GoLogger{})
	destHistoryDB := GetHistoryDB(destChainID, sConfig, destHistoryDBHandle, test.GoLogger{})
	destResultDB := GetResultDB(destChainID, sConfig, destResultDBHandle, test.GoLogger{})
	destBigfilterDB := GetBigFilterDB(destChainID, sConfig.BigFilter, test.GoLogger{})

	chainBStoreImport, err := NewStoreImportSnapshot(destChainID, sConfig, destBlockDB, destStateDB,
		destHistoryDB, nil, destResultDB, destTxExistDB, destCommonDBHandle,
		test.GoLogger{}, destBF, destBigfilterDB, destMetadb)
	if err != nil {
		panic(err)
	}

	// 3. write block0 ,block1 into chainA

	block0 := createConfBlock(sourceChainID, 0)
	txRWSets0 := getTxRWSets()

	block1 := createConfBlock(sourceChainID, 1)
	txRWSets1 := getTxRWSets()

	// 3.1 block0
	commitBlock(block0, txRWSets0, sourceBF, sourceBlockDB, sourceStateDB, sourceTxExistDB, sourceHistoryDB,
		sourceResultDB, sourceContractEventDB, sourceBigfilterDB)
	// 3.2 block1
	commitBlock(block1, txRWSets1, sourceBF, sourceBlockDB, sourceStateDB, sourceTxExistDB, sourceHistoryDB,
		sourceResultDB, sourceContractEventDB, sourceBigfilterDB)

	// 4. return chainA and chainB
	//blockdb.BlockDB,statedb.StateDB,
	//	resultdb.ResultDB,historydb.HistoryDB,
	//	contracteventdb.ContractEventDB,bigfilterdb.BigFilterDB,
	//	txexistdb.TxExistDB,binlog.BinLogger,metadb.MetaDataDB,
	return sourceBlockDB, sourceStateDB, sourceResultDB, sourceHistoryDB, sourceContractEventDB,
		sourceBigfilterDB, sourceTxExistDB, sourceBF, sourceMetadb,
		destBlockDB, destStateDB, destResultDB, destHistoryDB, nil, destBigfilterDB, destTxExistDB, destBF, destMetadb,
		chainBStoreImport

}

func commitBlock(block *commonPb.Block,
	txRWSets []*commonPb.TxRWSet,
	bf binlog.BinLogger,
	blockDB blockdb.BlockDB,
	stateDB statedb.StateDB,
	txExistDB txexistdb.TxExistDB,
	historyDB historydb.HistoryDB,
	resultDB resultdb.ResultDB,
	contractEventDB contracteventdb.ContractEventDB,
	bigfilterDB bigfilterdb.BigFilterDB) {
	// 3.1 block0
	//序列化
	blockWithRWSet0 := &storePb.BlockWithRWSet{
		Block:    block,
		TxRWSets: txRWSets,
	}
	blockBytes, blockWithSerializedInfo, err := serialization.SerializeBlock(blockWithRWSet0)
	if err != nil {
		panic(err)
	}

	// write wal
	// wal log, index increase from 1, while blockHeight increase form 0
	fileName, offset, bytesLen, err := bf.Write(1+block.Header.BlockHeight, blockBytes)
	if err != nil {
		panic(err)
	}
	blockIndex := &storePb.StoreInfo{
		FileName: fileName,
		Offset:   offset,
		ByteLen:  bytesLen,
	}
	blockWithSerializedInfo.Index = blockIndex

	// write blockDB stateDB txExistDB historyDB resultDB bigFilterDB
	if err = blockDB.CommitBlock(blockWithSerializedInfo, true); err != nil {
		panic(err)
	}
	if err = blockDB.CommitBlock(blockWithSerializedInfo, false); err != nil {
		panic(err)
	}

	if err = stateDB.CommitBlock(blockWithSerializedInfo, true); err != nil {
		panic(err)
	}
	if err = stateDB.CommitBlock(blockWithSerializedInfo, false); err != nil {
		panic(err)
	}

	if err = txExistDB.CommitBlock(blockWithSerializedInfo, false); err != nil {
		panic(err)
	}

	if err = historyDB.CommitBlock(blockWithSerializedInfo, true); err != nil {
		panic(err)
	}
	if err = historyDB.CommitBlock(blockWithSerializedInfo, false); err != nil {
		panic(err)
	}

	if err = resultDB.CommitBlock(blockWithSerializedInfo, true); err != nil {
		panic(err)
	}
	if err = resultDB.CommitBlock(blockWithSerializedInfo, false); err != nil {
		panic(err)
	}

	if err = contractEventDB.CommitBlock(blockWithSerializedInfo, false); err != nil {
		panic(err)
	}

	if err = bigfilterDB.CommitBlock(blockWithSerializedInfo, true); err != nil {
		panic(err)
	}
	if err = bigfilterDB.CommitBlock(blockWithSerializedInfo, false); err != nil {
		panic(err)
	}
}

func checkDifferentChainAChainB(
	t *testing.T,
	wantErr bool,
	sourceBlockDB, destBlockDB blockdb.BlockDB,
	sourceStateDB, destStateDB statedb.StateDB) error {
	var differentError = errors.New("different between chainA and chainB")
	var err error

	//5.check difference between chainA and chainB
	//if tx id ,state kv, config block are the  same , the result is normal
	// 5.1 check tx id
	if err = checkDifferentTxidSet(t, wantErr, sourceBlockDB, destBlockDB, sourceStateDB, destStateDB); err != nil {
		t.Errorf("importSnapshot error = %v, wantErr %v", err, wantErr)
		return differentError
	}

	// 5.2 check state kv
	if err = checkDifferentStateKV(t, wantErr, sourceStateDB, destStateDB); err != nil {
		t.Errorf("importSnapshot error = %v, wantErr %v", err, wantErr)
		return differentError
	}

	// 5.3 check config block
	if err = checkDifferentConfigBlock(t, wantErr, sourceBlockDB, destBlockDB); err != nil {
		t.Errorf("importSnapshot error = %v, wantErr %v", err, wantErr)
		return differentError
	}
	return nil
}

func checkDifferentTxidSet(
	t *testing.T,
	wantErr bool,
	sourceBlockDB, destBlockDB blockdb.BlockDB,
	sourceStateDB, destStateDB statedb.StateDB) error {
	var differentError = errors.New("different between chainA and chainB")
	var err2 error

	//5.check difference between chainA and chainB
	//if tx id ,state kv, config block are the  same , the result is normal
	// 5.1 check tx id
	setA, err2 := sourceBlockDB.GetTxidSet()
	if err2 != nil {
		t.Errorf("importSnapshot error = %v, wantErr %v", err2, wantErr)
		return differentError
	}
	setB, err2 := destBlockDB.GetTxidSet()
	if err2 != nil {
		t.Errorf("importSnapshot error = %v, wantErr %v", err2, wantErr)
		return differentError
	}
	if len(setA) != len(setB) {
		t.Errorf("importSnapshot error,setA is not equal setB")
		return differentError
	}
	for i := 0; i < len(setA); i++ {
		for setA[i].Next() && setB[i].Next() {
			if !reflect.DeepEqual(setA[i].Key(), setB[i].Key()) {
				t.Errorf("importSnapshot error,setA is not equal setB")
				return differentError
			}
		}
	}

	return nil
}
func checkDifferentStateKV(
	t *testing.T,
	wantErr bool,
	sourceStateDB, destStateDB statedb.StateDB) error {
	var differentError = errors.New("different between chainA and chainB")
	var err error

	//5.check difference between chainA and chainB
	//if tx id ,state kv, config block are the  same , the result is normal

	// 5.2 check state kv
	stateIterA, err2 := sourceStateDB.GetSnapshotIterator()
	if err2 != nil {
		t.Errorf("importSnapshot error = %v, wantErr %v", err2, wantErr)
		return differentError
	}
	stateIterB, err2 := destStateDB.GetSnapshotIterator()
	if err2 != nil {
		t.Errorf("importSnapshot error = %v, wantErr %v", err2, wantErr)
		return differentError
	}
	if len(stateIterA) != len(stateIterB) {
		t.Errorf("importSnapshot error = %v, wantErr %v", err, wantErr)
		return differentError
	}
	for i := 0; i < len(stateIterA); i++ {
		for stateIterA[i].Next() && stateIterB[i].Next() {
			if !reflect.DeepEqual(stateIterA[i].Key(), stateIterB[i].Key()) {
				t.Errorf("importSnapshot error = %v, wantErr %v", err, wantErr)
				return differentError
			}
			if !reflect.DeepEqual(stateIterA[i].Value(), stateIterB[i].Value()) {
				t.Errorf("importSnapshot error = %v, wantErr %v", err, wantErr)
				return differentError
			}
		}
	}

	return nil
}
func checkDifferentConfigBlock(
	t *testing.T,
	wantErr bool,
	sourceBlockDB, destBlockDB blockdb.BlockDB) error {
	var differentError = errors.New("different between chainA and chainB")
	var err error

	//5.check difference between chainA and chainB
	//if tx id ,state kv, config block are the  same , the result is normal

	// 5.3 check config block
	blockConfigA, err2 := sourceBlockDB.GetLastConfigBlock()
	if err2 != nil {
		t.Errorf("importSnapshot error = %v, wantErr %v", err2, wantErr)
		return differentError
	}
	blockConfigB, err2 := destBlockDB.GetLastConfigBlock()
	if err2 != nil {
		t.Errorf("importSnapshot error = %v, wantErr %v", err2, wantErr)
		return differentError
	}
	marshalA, err2 := blockConfigA.Marshal()
	if err2 != nil {
		t.Errorf("importSnapshot error = %v, wantErr %v", err2, wantErr)
		return differentError
	}
	marshalB, err2 := blockConfigB.Marshal()
	if err2 != nil {
		t.Errorf("importSnapshot error = %v, wantErr %v", err2, wantErr)
		return differentError
	}
	if !reflect.DeepEqual(marshalA, marshalB) {
		t.Errorf("importSnapshot error = %v, wantErr %v", err, wantErr)
		return differentError
	}
	return nil
}
func commitBlockToImportLedger(
	t *testing.T,
	wantErr bool,
	block *commonPb.Block,
	txRWSets []*commonPb.TxRWSet,
	destBlockDB blockdb.BlockDB,
	destStateDB statedb.StateDB,
	destBF binlog.BinLogger,
	height uint64) error {
	// 6.1 block2
	//序列化
	blockWithRWSet2 := &storePb.BlockWithRWSet{
		Block:    block,
		TxRWSets: txRWSets,
	}
	blockBytes, blockWithSerializedInfo, err := serialization.SerializeBlock(blockWithRWSet2)
	if err != nil {
		t.Errorf("importSnapshot error = %v, wantErr %v", err, wantErr)
		return err
		//panic(err)
	}

	// 6.2 write wal
	// wal log, index increase from 3, while blockHeight increase from 2
	//fileName, offset, bytesLen, err := sourceBF.Write(3, blockBytes)
	fileName, offset, bytesLen, err := destBF.Write(height, blockBytes)
	if err != nil {
		t.Errorf("importSnapshot error = %v, wantErr %v", err, wantErr)
		return err
	}
	blockIndex := &storePb.StoreInfo{
		FileName: fileName,
		Offset:   offset,
		ByteLen:  bytesLen,
	}
	blockWithSerializedInfo.Index = blockIndex

	// 6.3 write blockDB stateDB
	if err = destBlockDB.CommitBlock(blockWithSerializedInfo, true); err != nil {
		t.Errorf("importSnapshot error = %v, wantErr %v", err, wantErr)
		return err
	}
	if err = destBlockDB.CommitBlock(blockWithSerializedInfo, false); err != nil {
		t.Errorf("importSnapshot error = %v, wantErr %v", err, wantErr)
		return err
	}
	if err = destStateDB.CommitBlock(blockWithSerializedInfo, true); err != nil {
		t.Errorf("importSnapshot error = %v, wantErr %v", err, wantErr)
		return err
	}
	if err = destStateDB.CommitBlock(blockWithSerializedInfo, false); err != nil {
		t.Errorf("importSnapshot error = %v, wantErr %v", err, wantErr)
		return err
	}
	return nil
}

func TestSnapshot_GetSnapshotStatus(t *testing.T) {
	type fields struct {
		height      uint64
		status      uint64
		storeChan   chan struct{}
		jobChan     chan *snapshotJob
		blockdb     blockdb.BlockDB
		statedb     statedb.StateDB
		eventdb     contracteventdb.ContractEventDB
		logger      protocol.Logger
		chainID     string
		storeConfig *conf.StorageConfig
	}
	tests := []struct {
		name   string
		fields fields
		want   uint64
	}{
		{
			name: "test-get-snapshot-status-0",
			fields: fields{
				height:      0,
				status:      0,
				storeChan:   make(chan struct{}),
				jobChan:     make(chan *snapshotJob, 10),
				blockdb:     nil,
				statedb:     nil,
				eventdb:     nil,
				logger:      test.GoLogger{},
				chainID:     "test1",
				storeConfig: nil,
			},
			want: 1,
		},
		{
			name: "test-get-snapshot-status-1",
			fields: fields{
				height:      1,
				status:      0,
				storeChan:   make(chan struct{}),
				jobChan:     make(chan *snapshotJob, 10),
				blockdb:     nil,
				statedb:     nil,
				eventdb:     nil,
				logger:      test.GoLogger{},
				chainID:     "test1",
				storeConfig: nil,
			},
			want: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &Snapshot{
				height:      tt.fields.height,
				status:      tt.fields.status,
				storeChan:   tt.fields.storeChan,
				jobChan:     tt.fields.jobChan,
				blockdb:     tt.fields.blockdb,
				statedb:     tt.fields.statedb,
				eventdb:     tt.fields.eventdb,
				logger:      tt.fields.logger,
				chainID:     tt.fields.chainID,
				storeConfig: tt.fields.storeConfig,
				RWMutex:     sync.RWMutex{},
			}
			if got := s.GetSnapshotStatus(); got != tt.want {
				t.Errorf("GetSnapshotStatus() = %v, want %v", got, tt.want)
			}
		})
	}
}

func createConfBlock(chainId string, height uint64) *commonPb.Block {
	block := &commonPb.Block{
		Header: &commonPb.BlockHeader{
			ChainId:     chainId,
			BlockHeight: height,
			Proposer: &acPb.Member{
				OrgId:      "org1",
				MemberInfo: []byte("User1"),
			},
		},
		Txs: []*commonPb.Transaction{
			{
				Payload: &commonPb.Payload{
					ChainId:      chainId,
					TxType:       commonPb.TxType_INVOKE_CONTRACT,
					TxId:         generateTxId(chainId, height, 0),
					ContractName: syscontract.SystemContract_CHAIN_CONFIG.String(),
				},
				Sender: &commonPb.EndorsementEntry{
					Signer: &acPb.Member{
						OrgId:      "org1",
						MemberInfo: []byte("User1"),
					},
					Signature: []byte("signature1"),
				},
				Result: &commonPb.Result{
					Code: commonPb.TxStatusCode_SUCCESS,
					ContractResult: &commonPb.ContractResult{
						Result: []byte("ok"),
					},
				},
			},
		},
	}

	block.Header.BlockHash = generateBlockHash(chainId, height)
	block.Txs[0].Payload.TxId = generateTxId(chainId, height, 0)

	return block
}

func generateTxId(chainId string, height uint64, index int) string {
	txIdBytes := sha256.Sum256([]byte(fmt.Sprintf("%s-%d-%d", chainId, height, index)))
	return hex.EncodeToString(txIdBytes[:])
}
func generateBlockHash(chainId string, height uint64) []byte {
	blockHash := sha256.Sum256([]byte(fmt.Sprintf("%s-%d", chainId, height)))
	return blockHash[:]
}
func initGenesis(s protocol.BlockchainStore, chainId string) {
	genesis := createConfigBlock(chainId, 0)
	g := &storePb.BlockWithRWSet{Block: genesis, TxRWSets: getTxRWSets()}
	_ = s.InitGenesis(g)
}
func createConfigBlock(chainId string, height uint64) *commonPb.Block {
	block := &commonPb.Block{
		Header: &commonPb.BlockHeader{
			ChainId:     chainId,
			BlockHeight: height,
			Proposer: &acPb.Member{
				OrgId:      "org1",
				MemberInfo: []byte("User1"),
			},
		},
		Txs: []*commonPb.Transaction{
			{
				Payload: &commonPb.Payload{
					ChainId:      chainId,
					TxType:       commonPb.TxType_INVOKE_CONTRACT,
					ContractName: syscontract.SystemContract_CHAIN_CONFIG.String(),
					TxId:         generateTxId(chainId, height, 0),
				},
				Sender: &commonPb.EndorsementEntry{
					Signer: &acPb.Member{
						OrgId:      "org1",
						MemberInfo: []byte("User1"),
					},
					Signature: []byte("signature1"),
				},
				Result: &commonPb.Result{
					Code: commonPb.TxStatusCode_SUCCESS,
					ContractResult: &commonPb.ContractResult{
						Result: []byte("ok"),
					},
				},
			},
		},
	}

	block.Header.BlockHash = generateBlockHash(chainId, height)
	block.Txs[0].Payload.TxId = generateTxId(chainId, height, 0)
	return block
}
func getTxRWSets() []*commonPb.TxRWSet {
	return []*commonPb.TxRWSet{
		{
			//TxId: "abcdefg",
			TxWrites: []*commonPb.TxWrite{
				{
					Key:          []byte("key1"),
					Value:        []byte("value1"),
					ContractName: defaultSysContractName,
				},
				{
					Key:          []byte("key2"),
					Value:        []byte("value2"),
					ContractName: defaultSysContractName,
				},
				{
					Key:          []byte("key3"),
					Value:        nil,
					ContractName: defaultSysContractName,
				},
			},
		},
	}
}

func initRedisHosts() {
	cmd := exec.Command("/bin/bash", "./script/start_redis.sh")
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()
	if err != nil {
		fmt.Println("start redis error :", err)
	}
	//outStr, errStr := string(stdout.Bytes()), string(stderr.Bytes())
	outStr, errStr := stdout.String(), stderr.String()
	fmt.Printf("out:%s\n", outStr)
	if errStr != "" {
		fmt.Printf("err:%s\n", errStr)
	}
}

func destroyRedisHosts() {
	cmd := exec.Command("/bin/bash", "./script/stop_redis.sh")
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()
	if err != nil {
		fmt.Println("stop redis error: ", err)
	}
	//outStr, errStr := string(stdout.Bytes()), string(stderr.Bytes())
	outStr, errStr := stdout.String(), stderr.String()
	fmt.Printf("out:%s\n", outStr)
	if errStr != "" {
		fmt.Printf("err:%s\n", errStr)
	}

}

//删除文件系统
func destroyFileSystem(dirName string) {
	cmd := exec.Command("/bin/bash", "./script/rmdir.sh", dirName)
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()
	if err != nil {
		fmt.Printf("stop rmdir [%s] error:[%s] ", dirName, err)
	}
	//outStr, errStr := string(stdout.Bytes()), string(stderr.Bytes())
	outStr, errStr := stdout.String(), stderr.String()
	fmt.Printf("out:%s\n", outStr)
	if errStr != "" {
		fmt.Printf("destroy tmp dir :[%s] err:%s\n", dirName, errStr)
	}
	fmt.Printf("destroy tmp test dir success tmp_test_dir[%s],out[%s]\n", dirName, outStr)
}
