VERSION=v3.0.0
VERSION_LWS=v1.1.0
gomod:
	go get chainmaker.org/chainmaker/common/v3@$(VERSION)
	go get chainmaker.org/chainmaker/pb-go/v3@$(VERSION)
	go get chainmaker.org/chainmaker/protocol/v3@$(VERSION)
	go get chainmaker.org/chainmaker/store-badgerdb/v3@$(VERSION)
	go get chainmaker.org/chainmaker/store-leveldb/v3@$(VERSION)
	go get chainmaker.org/chainmaker/store-sqldb/v3@$(VERSION)
	go get chainmaker.org/chainmaker/store-tikv/v3@$(VERSION)
	go get chainmaker.org/chainmaker/utils/v3@$(VERSION)
	go get chainmaker.org/chainmaker/lws@$(VERSION_LWS)
	go mod tidy
ut:
	go test ./...
	(gocloc --include-lang=Go --output-type=json . | jq '(.total.comment-.total.files*6)/(.total.code+.total.comment)*100')