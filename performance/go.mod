module chainmaker.org/chainmaker/store-huge/performance

go 1.15

require (
	chainmaker.org/chainmaker/localconf/v2 v2.1.1-0.20211214124610-bb7620382194
	chainmaker.org/chainmaker/logger/v2 v2.1.1-0.20211214124250-621f11b35ab0
	chainmaker.org/chainmaker/pb-go/v2 v2.1.1-0.20220125074156-6f9f1b52f915
	chainmaker.org/chainmaker/protocol/v2 v2.1.2-0.20220128021940-8fd70a8c2c75
	//chainmaker.org/chainmaker/store-huge/v2 v2.1.1
	chainmaker.org/chainmaker/utils/v2 v2.1.1-0.20220128023017-5bf8279342f1
	github.com/spf13/cobra v1.1.1
)

replace (
	//chainmaker.org/chainmaker/store-huge/v2 => ../
	google.golang.org/grpc => google.golang.org/grpc v1.26.0
)
