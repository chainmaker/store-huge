/*
 * Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 */

package store

import (
	"bytes"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"math/rand"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
	"testing"
	"time"
	"unsafe"

	acPb "chainmaker.org/chainmaker/pb-go/v3/accesscontrol"
	commonPb "chainmaker.org/chainmaker/pb-go/v3/common"
	storePb "chainmaker.org/chainmaker/pb-go/v3/store"
	"chainmaker.org/chainmaker/pb-go/v3/syscontract"
	"chainmaker.org/chainmaker/protocol/v3"
	"chainmaker.org/chainmaker/protocol/v3/test"
	"chainmaker.org/chainmaker/store-huge/v3/archive"
	"chainmaker.org/chainmaker/store-huge/v3/binlog"
	"chainmaker.org/chainmaker/store-huge/v3/conf"
	"chainmaker.org/chainmaker/store-huge/v3/serialization"
	"chainmaker.org/chainmaker/utils/v3"
	"github.com/stretchr/testify/assert"
)

//var chainId = "ut1"
//var testCchainId = "ut1"

//var dbType = types.MySQL
//var dbType = types.LevelDb
func getFactory() *Factory {
	f := NewFactory()
	_ = f.ioc.Register(func() protocol.Logger { return &test.GoLogger{} })
	_ = f.ioc.Register(func() binlog.BinLogger { return binlog.NewMemBinlog(&test.GoLogger{}) })
	return f
}

var defaultSysContractName = syscontract.SystemContract_CHAIN_CONFIG.String()
var userContractName = "contract1"

//var block0 = createConfigBlock(chainId, 0)
//var block1 = createBlock(chainId, 1, 1)
//var block5 = createBlock(chainId, 5, 1)

func getTxRWSets() []*commonPb.TxRWSet {
	return []*commonPb.TxRWSet{
		{
			//TxId: "abcdefg",
			TxWrites: []*commonPb.TxWrite{
				{
					Key:          []byte("key1"),
					Value:        []byte("value1"),
					ContractName: defaultSysContractName,
				},
				{
					Key:          []byte("key2"),
					Value:        []byte("value2"),
					ContractName: defaultSysContractName,
				},
				{
					Key:          []byte("key3"),
					Value:        nil,
					ContractName: defaultSysContractName,
				},
			},
		},
	}
}

var config1 = getSqliteConfig()

//var config3 = getlvldbConfig("")

//var configBadger = getBadgerConfig("")
func getLocalMySqlConfig() *conf.StorageConfig {
	conf1, _ := conf.NewStorageConfig(nil)
	conf1.DbPrefix = "org1_"
	conf1.StorePath = filepath.Join(os.TempDir(), fmt.Sprintf("%d", time.Now().Nanosecond()))
	conf1.WriteBlockType = 0
	var sqlconfig = make(map[string]interface{})
	sqlconfig["sqldb_type"] = "mysql"
	sqlconfig["dsn"] = "root:123@tcp(127.0.0.1:3306)/mysql"

	dbConfig := &conf.DbConfig{
		Provider:    "sql",
		SqlDbConfig: sqlconfig,
	}
	statedbConfig := &conf.DbConfig{
		Provider:    "sql",
		SqlDbConfig: sqlconfig,
	}

	lvlConfig := make(map[string]interface{})
	lvlConfig["store_path"] = filepath.Join(os.TempDir(), fmt.Sprintf("%d", time.Now().Nanosecond()))

	txExistDbConfig := &conf.DbConfig{
		Provider:      "leveldb",
		LevelDbConfig: lvlConfig,
	}
	conf1.DisableBlockFileDb = true
	conf1.BlockDbConfig = dbConfig
	conf1.StateDbConfig = statedbConfig
	conf1.HistoryDbConfig = conf.NewHistoryDbConfig(dbConfig)
	conf1.ResultDbConfig = dbConfig
	conf1.ContractEventDbConfig = dbConfig
	conf1.DisableContractEventDB = true
	conf1.TxExistDbConfig = txExistDbConfig
	conf1.LogDBSegmentSize = 20
	return conf1
}
func getSqliteConfig() *conf.StorageConfig {
	conf1, _ := conf.NewStorageConfig(nil)
	conf1.StorePath = filepath.Join(os.TempDir(), fmt.Sprintf("%d", time.Now().Nanosecond()))
	conf1.WriteBlockType = 0

	sqlLite := "sqlite"

	var blockSqlconfig = make(map[string]interface{})
	blockSqlconfig["sqldb_type"] = sqlLite
	blockSqlconfig["dsn"] = filepath.Join(os.TempDir(), fmt.Sprintf("test_memory_%d:memory:",
		time.Now().Nanosecond()))

	var stateSqlconfig = make(map[string]interface{})
	stateSqlconfig["sqldb_type"] = sqlLite
	stateSqlconfig["dsn"] = filepath.Join(os.TempDir(), fmt.Sprintf("test_memory_%d:memory:",
		time.Now().Nanosecond()))

	var historySqlconfig = make(map[string]interface{})
	historySqlconfig["sqldb_type"] = sqlLite
	historySqlconfig["dsn"] = filepath.Join(os.TempDir(), fmt.Sprintf("test_memory_%d:memory:",
		time.Now().Nanosecond()))

	var resultSqlconfig = make(map[string]interface{})
	resultSqlconfig["sqldb_type"] = sqlLite
	resultSqlconfig["dsn"] = filepath.Join(os.TempDir(), fmt.Sprintf("test_memory_%d:memory:",
		time.Now().Nanosecond()))

	var contractSqlconfig = make(map[string]interface{})
	contractSqlconfig["sqldb_type"] = sqlLite
	contractSqlconfig["dsn"] = filepath.Join(os.TempDir(), fmt.Sprintf("test_memory_%d:memory:",
		time.Now().Nanosecond()))

	//dsnPath := fmt.Sprintf("%d:memory:", time.Now().Nanosecond())
	//sqlconfig["dsn"] = ":memory:"
	//sqlconfig["dsn"] = dsnPath

	blockdbConfig := &conf.DbConfig{
		Provider:    "sql",
		SqlDbConfig: blockSqlconfig,
	}
	statedbConfig := &conf.DbConfig{
		Provider:    "sql",
		SqlDbConfig: stateSqlconfig,
	}
	historydbConfig := &conf.DbConfig{
		Provider:    "sql",
		SqlDbConfig: historySqlconfig,
	}
	resultdbConfig := &conf.DbConfig{
		Provider:    "sql",
		SqlDbConfig: resultSqlconfig,
	}
	contractdbConfig := &conf.DbConfig{
		Provider:    "sql",
		SqlDbConfig: contractSqlconfig,
	}

	lvlConfig := make(map[string]interface{})
	lvlConfig["store_path"] = filepath.Join(os.TempDir(), fmt.Sprintf("%d", time.Now().Nanosecond()))

	txExistDbConfig := &conf.DbConfig{
		Provider:      "leveldb",
		LevelDbConfig: lvlConfig,
	}
	conf1.DisableBlockFileDb = true
	conf1.BlockDbConfig = blockdbConfig
	conf1.StateDbConfig = statedbConfig
	conf1.HistoryDbConfig = conf.NewHistoryDbConfig(historydbConfig)
	conf1.ResultDbConfig = resultdbConfig
	conf1.ContractEventDbConfig = contractdbConfig
	conf1.DisableContractEventDB = true
	conf1.TxExistDbConfig = txExistDbConfig
	conf1.LogDBSegmentSize = 20
	//conf1.DisableBigFilter = true
	conf1.RollingWindowCacheCapacity = 1000000

	//增加文件系统配置，支持归档，支持多文件系统，实现横向扩展和冷热处理
	conf1.BlockFileConfig = &conf.BlockFileConfig{
		OnlineFileSystem: fmt.Sprintf("/tmp/online1_test_%d,/tmp/online2_test_%d", time.Now().Nanosecond(),
			time.Now().Nanosecond()),
		ArchiveFileSystem: fmt.Sprintf("/tmp/archive1_test_%d,/tmp/archive2_test_%d", time.Now().Nanosecond(),
			time.Now().Nanosecond()),
		//OnlineFileSystem:  "/tmp/online1,/tmp/online2",
		//ArchiveFileSystem: "/tmp/archive1,/tmp/archive2",
	}
	//增加存储模块中，配置文件的版本号
	conf1.ConfigVersion = &conf.StorageConfigVersion{
		Major: 1,
		Minor: 0,
	}
	return conf1
}

// getConfigWithBigFilterWithMySQLWithSqlKV returns the configuration for storage
//  @Description:  with blockDB,stateDB,historyDB,resultDB,contractEventDB, bigFilterDB, txExistDB
//  provider sqlkv
//  with mysql , mysql port 33670
//  with redis , redis port 7520 7521
// @return: storageConfiguration,tmpDirString
func getConfigWithBigFilterWithMySQLWithSqlKV() (*conf.StorageConfig, string) {
	var cJson = `{
"StorePath":"./ledgerData1",
"DbPrefix":"preA",
"WriteBufferSize":0,
"BloomFilterBits":0,
"BlockWriteBufferSize":0,
"LogDBWriteAsync":false,
"LogDBSegmentSize":1,
"BlockFileConfig":{
	"OnlineFileSystem":"/tmp/online1,/tmp/online2",
	"ArchiveFileSystem":"/tmp/archive1,/tmp/archive2"
},
"ConfigVersion":{
	"Major": 1,
	"Minor": 0
},
"BlockDbConfig":{
	"Provider":"sqlkv",
	"SqlDbConfig":{
		"dsn":"root:mysqlroot@tcp(127.0.0.1:33670)/",
		"sqldb_type":"mysql",
		"max_open_conns":512,
		"db_prefix":"block_db"
	}
},
"TxExistDbConfig":{
	"Provider":"leveldb",
	"LevelDbConfig":{
		"store_path":"./ledgerData1/data/org1/txexist"
	},
	"BadgerDbConfig":null,
	"SqlDbConfig":null
},
"StateDbConfig":{
	"Provider":"sqlkv",
	"SqlDbConfig":{
		"dsn":"root:mysqlroot@tcp(127.0.0.1:33670)/",
		"sqldb_type":"mysql"
	}
},
"HistoryDbConfig":{
	"Provider":"sqlkv",
	"SqlDbConfig":{
		"dsn":"root:mysqlroot@tcp(127.0.0.1:33670)/",
		"sqldb_type":"mysql"
	}
},
"ResultDbConfig":{
	"Provider":"sqlkv",
	"SqlDbConfig":{
		"dsn":"root:mysqlroot@tcp(127.0.0.1:33670)/",
		"sqldb_type":"mysql"
	}
},
"ContractEventDbConfig":{
	"Provider":"sql",
	"SqlDbConfig":{
		"dsn":"root:mysqlroot@tcp(127.0.0.1:33670)/",
		"sqldb_type":"mysql"
	}
},
"EnableBigFilter":true,
"BigFilter":{
	"RedisHosts":"127.0.0.1:7520,127.0.0.1:7521",
    "Pass":"redisPass0",
    "TxCapacity":1000000000,
    "FpRate":0.000000001
},
"UnArchiveBlockHeight":0,
"Encryptor":"sm4",
"EncryptKey":"1234567890123456"
}`
	conf1 := &conf.StorageConfig{}
	err := json.Unmarshal([]byte(cJson), conf1)
	if err != nil {
		panic(err)
	}

	unixTime := time.Now().Unix()
	//create tmpDir for Store path
	storeTmpDir := fmt.Sprintf("tmp_ledgerData1_%d", unixTime)
	conf1.StorePath = storeTmpDir

	//create tmpDir for online file system path and archive file system path
	onlineFSTmpDir := fmt.Sprintf("/tmp/online1_%d,/tmp/online2_%d", unixTime, unixTime)
	archiveFSTmpDir := fmt.Sprintf("/tmp/archive1_%d,/tmp/archive2_%d", unixTime, unixTime)
	conf1.BlockFileConfig.OnlineFileSystem = onlineFSTmpDir
	conf1.BlockFileConfig.ArchiveFileSystem = archiveFSTmpDir

	//conf1.TxExistDbConfig.LevelDbConfig["store_path"] = fmt.Sprintf("./tmp_txExistdb/%d", unixTime)
	conf1.TxExistDbConfig.LevelDbConfig["store_path"] = fmt.Sprintf("./tmp_ledgerData1_%d/txexist", unixTime)

	//conf1.StorePath = filepath.Join(os.TempDir(), fmt.Sprintf("%d", time.Now().Nanosecond()))
	//common write
	conf1.WriteBlockType = 0
	//enable block file
	conf1.DisableBlockFileDb = false
	conf1.LogDBSegmentSize = 20
	conf1.RollingWindowCacheCapacity = 1000000
	//增加文件系统配置，支持归档，支持多文件系统，实现横向扩展和冷热处理
	//增加存储模块中，配置文件的版本号

	return conf1, storeTmpDir
}

// getConfigWithBigFilterWithMySQLWithSqlKV returns the configuration for storage
//  @Description:  with blockDB,stateDB,historyDB,resultDB,contractEventDB, bigFilterDB, txExistDB
//  provider sqlkv
//  with mysql , mysql port 33670
//  with redis , redis port 7520 7521
//  with tikv ,  pd port 2379 2380 , tikv port 20160
// @return: configuration for storage,string of tmp testing directory
func getConfigWithBigFilterWithTikvWithSqlKV() (*conf.StorageConfig, string) {
	var cJson = `{
"StorePath":"./ledgerData1",
"DbPrefix":"preA",
"WriteBufferSize":0,
"BloomFilterBits":0,
"BlockWriteBufferSize":0,
"LogDBWriteAsync":false,
"LogDBSegmentSize":1,
"BlockFileConfig":{
	"OnlineFileSystem":"/tmp/online1,/tmp/online2",
	"ArchiveFileSystem":"/tmp/archive1,/tmp/archive2"
},
"ConfigVersion":{
	"Major": 1,
	"Minor": 0
},
"BlockDbConfig":{
	"Provider":"tikvdb",
	"TikvDbConfig":{
		"db_prefix":"node1_",
      	"endpoints":"127.0.0.1:2379",
      	"max_batch_count":128,
        "grpc_connection_count":512,
        "grpc_keep_alive_time":10,
        "grpc_keep_alive_timeout":3,
        "write_batch_size":128
	}
},
"TxExistDbConfig":{
	"Provider":"leveldb",
	"LevelDbConfig":{
		"store_path":"./ledgerData1/data/org1/txexist"
	},
	"BadgerDbConfig":null,
	"SqlDbConfig":null
},
"StateDbConfig":{
	"Provider":"tikvdb",
	"TikvDbConfig":{
		"db_prefix":"node1_",
      	"endpoints":"127.0.0.1:2379",
      	"max_batch_count":128,
        "grpc_connection_count":512,
        "grpc_keep_alive_time":10,
        "grpc_keep_alive_timeout":3,
        "write_batch_size":128
	}
},
"HistoryDbConfig":{
	"Provider":"sqlkv",
	"SqlDbConfig":{
		"dsn":"root:mysqlroot@tcp(127.0.0.1:33670)/",
		"sqldb_type":"mysql"
	}
},
"ResultDbConfig":{
	"Provider":"tikvdb",
	"TikvDbConfig":{
		"db_prefix":"node1_",
      	"endpoints":"127.0.0.1:2379",
      	"max_batch_count":128,
        "grpc_connection_count":512,
        "grpc_keep_alive_time":10,
        "grpc_keep_alive_timeout":3,
        "write_batch_size":128
	}
},
"ContractEventDbConfig":{
	"Provider":"sql",
	"SqlDbConfig":{
		"dsn":"root:mysqlroot@tcp(127.0.0.1:33670)/",
		"sqldb_type":"mysql"
	}
},
"EnableBigFilter":true,
"BigFilter":{
	"RedisHosts":"127.0.0.1:7520,127.0.0.1:7521",
    "Pass":"redisPass0",
    "TxCapacity":1000000000,
    "FpRate":0.000000001
},
"UnArchiveBlockHeight":0,
"Encryptor":"sm4",
"EncryptKey":"1234567890123456"
}`
	conf1 := &conf.StorageConfig{}
	err := json.Unmarshal([]byte(cJson), conf1)
	if err != nil {
		panic(err)
	}

	unixTime := time.Now().Unix()
	//create tmpDir for Store path
	storeTmpDir := fmt.Sprintf("tmp_ledgerData1_%d", unixTime)
	conf1.StorePath = storeTmpDir

	//create tmpDir for online file system path and archive file system path
	onlineFSTmpDir := fmt.Sprintf("/tmp/online1_%d,/tmp/online2_%d", unixTime, unixTime)
	archiveFSTmpDir := fmt.Sprintf("/tmp/archive1_%d,/tmp/archive2_%d", unixTime, unixTime)
	conf1.BlockFileConfig.OnlineFileSystem = onlineFSTmpDir
	conf1.BlockFileConfig.ArchiveFileSystem = archiveFSTmpDir

	conf1.TxExistDbConfig.LevelDbConfig["store_path"] = fmt.Sprintf("./tmp_ledgerData1_%d/txexist", unixTime)

	//conf1.StorePath = filepath.Join(os.TempDir(), fmt.Sprintf("%d", time.Now().Nanosecond()))
	//quick write
	conf1.WriteBlockType = 1
	//enable block file
	conf1.DisableBlockFileDb = false
	conf1.LogDBSegmentSize = 20
	conf1.RollingWindowCacheCapacity = 1000000
	//增加文件系统配置，支持归档，支持多文件系统，实现横向扩展和冷热处理
	//增加存储模块中，配置文件的版本号

	return conf1, storeTmpDir
}

// getConfigWithBigFilterWithBadgerDBWithSqlKV returns the configuration for storage
//  @Description:  with blockDB,stateDB,historyDB,resultDB,contractEventDB, bigFilterDB, txExistDB
//  provider sqlkv
//  with mysql , mysql port 33670
//  with redis , redis port 7520 7521
//  with badgerdb ,
//  block file , disable mmap enable
// @return: configuration for storage,string of tmp testing directory
func getConfigWithBigFilterWithBadgerDBWithSqlKV() (*conf.StorageConfig, string) {
	var cJson = `{
"StorePath":"./ledgerData1",
"DbPrefix":"preA",
"WriteBufferSize":0,
"BloomFilterBits":0,
"BlockWriteBufferSize":0,
"LogDBWriteAsync":false,
"LogDBSegmentSize":1,
"BlockFileConfig":{
	"OnlineFileSystem":"/tmp/online1,/tmp/online2",
	"ArchiveFileSystem":"/tmp/archive1,/tmp/archive2"
},
"ConfigVersion":{
	"Major": 1,
	"Minor": 0
},
"BlockDbConfig":{
	"Provider":"badgerdb",
	"BadgerDbConfig":{
		"store_path":"./ledgerData1/data/org1/txexist",
		"compression":0,
		"value_threshold":256,
		"write_batch_size":1024
	}
},
"TxExistDbConfig":{
	"Provider":"leveldb",
	"LevelDbConfig":{
		"store_path":"./ledgerData1/data/org1/txexist"
	},
	"BadgerDbConfig":null,
	"SqlDbConfig":null
},
"StateDbConfig":{
	"Provider":"tikvdb",
	"TikvDbConfig":{
		"db_prefix":"node1_",
      	"endpoints":"127.0.0.1:2379",
      	"max_batch_count":128,
        "grpc_connection_count":512,
        "grpc_keep_alive_time":10,
        "grpc_keep_alive_timeout":3,
        "write_batch_size":128
	}
},
"HistoryDbConfig":{
	"Provider":"sqlkv",
	"SqlDbConfig":{
		"dsn":"root:mysqlroot@tcp(127.0.0.1:33670)/",
		"sqldb_type":"mysql"
	}
},
"ResultDbConfig":{
	"Provider":"tikvdb",
	"TikvDbConfig":{
		"db_prefix":"node1_",
      	"endpoints":"127.0.0.1:2379",
      	"max_batch_count":128,
        "grpc_connection_count":512,
        "grpc_keep_alive_time":10,
        "grpc_keep_alive_timeout":3,
        "write_batch_size":128
	}
},
"ContractEventDbConfig":{
	"Provider":"sql",
	"SqlDbConfig":{
		"dsn":"root:mysqlroot@tcp(127.0.0.1:33670)/",
		"sqldb_type":"mysql"
	}
},
"EnableBigFilter":true,
"BigFilter":{
	"RedisHosts":"127.0.0.1:7520,127.0.0.1:7521",
    "Pass":"redisPass0",
    "TxCapacity":1000000000,
    "FpRate":0.000000001
},
"UnArchiveBlockHeight":0,
"Encryptor":"sm4",
"EncryptKey":"1234567890123456"
}`
	conf1 := &conf.StorageConfig{}
	err := json.Unmarshal([]byte(cJson), conf1)
	if err != nil {
		panic(err)
	}

	unixTime := time.Now().Unix()
	//create tmpDir for Store path
	storeTmpDir := fmt.Sprintf("tmp_ledgerData1_%d", unixTime)
	conf1.StorePath = storeTmpDir

	//create tmpDir for online file system path and archive file system path
	onlineFSTmpDir := fmt.Sprintf("/tmp/online1_%d,/tmp/online2_%d", unixTime, unixTime)
	archiveFSTmpDir := fmt.Sprintf("/tmp/archive1_%d,/tmp/archive2_%d", unixTime, unixTime)
	conf1.BlockFileConfig.OnlineFileSystem = onlineFSTmpDir
	conf1.BlockFileConfig.ArchiveFileSystem = archiveFSTmpDir

	conf1.TxExistDbConfig.LevelDbConfig["store_path"] = fmt.Sprintf("./tmp_ledgerData1_%d/txexist", unixTime)

	conf1.BlockDbConfig.BadgerDbConfig["store_path"] = fmt.Sprintf("./tmp_ledgerData1_%d/badgerdb", unixTime)
	//quick write
	conf1.WriteBlockType = 1
	//enable block file
	conf1.DisableBlockFileDb = false

	//disable mmap for wal
	conf1.DisableLogDBMmap = true

	conf1.LogDBSegmentSize = 20

	//enable rwc
	conf1.EnableRWC = true
	conf1.RollingWindowCacheCapacity = 1000000

	//enable bigfilter
	conf1.EnableBigFilter = true
	//增加文件系统配置，支持归档，支持多文件系统，实现横向扩展和冷热处理
	//增加存储模块中，配置文件的版本号

	return conf1, storeTmpDir
}

func getBadgerConfig(path string) *conf.StorageConfig {
	conf1, _ := conf.NewStorageConfig(nil)
	if path == "" {
		path = filepath.Join(os.TempDir(), fmt.Sprintf("%d", time.Now().Nanosecond()))
	}
	conf1.StorePath = path
	conf1.WriteBlockType = 1

	badgerConfig := make(map[string]interface{})
	badgerConfig["store_path"] = path
	dbConfig := &conf.DbConfig{
		Provider:       "badgerdb",
		BadgerDbConfig: badgerConfig,
	}
	conf1.BlockDbConfig = dbConfig
	conf1.StateDbConfig = dbConfig
	conf1.HistoryDbConfig = conf.NewHistoryDbConfig(dbConfig)
	conf1.ResultDbConfig = dbConfig
	conf1.DisableContractEventDB = true
	conf1.TxExistDbConfig = dbConfig
	conf1.StorePath = filepath.Join(os.TempDir(), fmt.Sprintf("%d_wal", time.Now().Nanosecond()))
	return conf1
}
func getlvldbConfig(path string) *conf.StorageConfig {
	conf1, _ := conf.NewStorageConfig(nil)
	if path == "" {
		path = filepath.Join(os.TempDir(), fmt.Sprintf("%d", time.Now().Nanosecond()))
	}

	conf1.StorePath = path
	conf1.WriteBlockType = 0

	lvlConfig := make(map[string]interface{})
	lvlConfig["store_path"] = path

	path2 := filepath.Join(os.TempDir(), fmt.Sprintf("%d", time.Now().Nanosecond()))

	lvlConfig2 := make(map[string]interface{})
	lvlConfig2["store_path"] = path2

	dbConfig := &conf.DbConfig{
		Provider:      "leveldb",
		LevelDbConfig: lvlConfig,
	}
	dbConfig2 := &conf.DbConfig{
		Provider:      "leveldb",
		LevelDbConfig: lvlConfig2,
	}
	conf1.BlockDbConfig = dbConfig
	conf1.StateDbConfig = dbConfig
	conf1.HistoryDbConfig = conf.NewHistoryDbConfig(dbConfig)
	conf1.ResultDbConfig = dbConfig
	conf1.DisableContractEventDB = true
	conf1.TxExistDbConfig = dbConfig2
	conf1.DisableBlockFileDb = false
	//conf1.DisableBigFilter = true
	conf1.StorePath = filepath.Join(os.TempDir(), fmt.Sprintf("%d_wal", time.Now().Nanosecond()))

	//增加文件系统配置，支持归档，支持多文件系统，实现横向扩展和冷热处理
	conf1.BlockFileConfig = &conf.BlockFileConfig{
		OnlineFileSystem: fmt.Sprintf("/tmp/online1_test_%d,/tmp/online2_test_%d", time.Now().Nanosecond(),
			time.Now().Nanosecond()),
		ArchiveFileSystem: fmt.Sprintf("/tmp/archive1_test_%d,/tmp/archive2_test_%d", time.Now().Nanosecond(),
			time.Now().Nanosecond()),
		//OnlineFileSystem:  "/tmp/online1,/tmp/online2",
		//ArchiveFileSystem: "/tmp/archive1,/tmp/archive2",
	}
	//增加存储模块中，配置文件的版本号
	conf1.ConfigVersion = &conf.StorageConfigVersion{
		Major: 1,
		Minor: 0,
	}
	return conf1
}
func generateBlockHash(chainId string, height uint64) []byte {
	blockHash := sha256.Sum256([]byte(fmt.Sprintf("%s-%d", chainId, height)))
	return blockHash[:]
}

func generateTxId(chainId string, height uint64, index int) string {
	txIdBytes := sha256.Sum256([]byte(fmt.Sprintf("%s-%d-%d", chainId, height, index)))
	return hex.EncodeToString(txIdBytes[:])
}

func createConfigBlock(chainId string, height uint64) *commonPb.Block {
	block := &commonPb.Block{
		Header: &commonPb.BlockHeader{
			ChainId:     chainId,
			BlockHeight: height,
			Proposer: &acPb.Member{
				OrgId:      "org1",
				MemberInfo: []byte("User1"),
			},
		},
		Txs: []*commonPb.Transaction{
			{
				Payload: &commonPb.Payload{
					ChainId:      chainId,
					TxType:       commonPb.TxType_INVOKE_CONTRACT,
					ContractName: syscontract.SystemContract_CHAIN_CONFIG.String(),
					TxId:         generateTxId(chainId, height, 0),
				},
				Sender: &commonPb.EndorsementEntry{
					Signer: &acPb.Member{
						OrgId:      "org1",
						MemberInfo: []byte("User1"),
					},
					Signature: []byte("signature1"),
				},
				Result: &commonPb.Result{
					Code: commonPb.TxStatusCode_SUCCESS,
					ContractResult: &commonPb.ContractResult{
						Result: []byte("ok"),
					},
				},
			},
		},
	}

	block.Header.BlockHash = generateBlockHash(chainId, height)
	block.Txs[0].Payload.TxId = generateTxId(chainId, height, 0)
	return block
}

func createBlock(chainId string, height uint64, txNum int) *commonPb.Block {
	block := &commonPb.Block{
		Header: &commonPb.BlockHeader{
			ChainId:     chainId,
			BlockHeight: height,
			Proposer: &acPb.Member{
				OrgId:      "org1",
				MemberInfo: []byte("User1"),
			},
		},
		Txs: []*commonPb.Transaction{},
	}
	for i := 0; i < txNum; i++ {
		tx := &commonPb.Transaction{
			Payload: &commonPb.Payload{
				ChainId: chainId,
				TxType:  commonPb.TxType_INVOKE_CONTRACT,
				TxId:    generateTxId(chainId, height, i),
			},
			Sender: &commonPb.EndorsementEntry{
				Signer: &acPb.Member{
					OrgId:      "org1",
					MemberInfo: []byte("User1"),
				},
				Signature: []byte("signature1"),
			},
			Result: &commonPb.Result{
				Code: commonPb.TxStatusCode_SUCCESS,
				ContractResult: &commonPb.ContractResult{
					Result: []byte("ok"),
				},
			},
		}
		block.Txs = append(block.Txs, tx)
	}
	block.Header.BlockHash = generateBlockHash(chainId, height)
	block.Txs[0].Payload.TxId = generateTxId(chainId, height, 0)

	return block
}

func createBigBlock(chainId string, height uint64, txNum int) *commonPb.Block {
	block := &commonPb.Block{
		Header: &commonPb.BlockHeader{
			ChainId:     chainId,
			BlockHeight: height,
			Proposer: &acPb.Member{
				OrgId:      "org1",
				MemberInfo: []byte("User1"),
			},
		},
		Txs: []*commonPb.Transaction{},
	}
	for i := 0; i < txNum; i++ {
		tx := &commonPb.Transaction{
			Payload: &commonPb.Payload{
				ChainId: chainId,
				TxType:  commonPb.TxType_INVOKE_CONTRACT,
				TxId:    generateTxId(chainId, height, i),
			},
			Sender: &commonPb.EndorsementEntry{
				Signer: &acPb.Member{
					OrgId:      "org1",
					MemberInfo: []byte("User1"),
				},
				Signature: []byte("signature1"),
			},
			Result: &commonPb.Result{
				Code: commonPb.TxStatusCode_SUCCESS,
				ContractResult: &commonPb.ContractResult{
					Result: []byte("ok"),
				},
			},
		}
		var (
			newKey   string
			newValue string
		)
		for i := 0; i < 1; i++ {
			newKey = RandStringBytesMaskImprSrcUnsafe(1024 * 1024)
			newValue = RandStringBytesMaskImprSrcUnsafe(1024 * 1024)
			//fmt.Println("newKey =", newKey, "newValue =", newValue)
			tx.Payload.Parameters = append(
				tx.Payload.Parameters, &commonPb.KeyValuePair{Key: newKey, Value: []byte(newValue)})
		}

		block.Txs = append(block.Txs, tx)
	}
	block.Header.BlockHash = generateBlockHash(chainId, height)
	block.Txs[0].Payload.TxId = generateTxId(chainId, height, 0)

	return block
}

func createConfBlock(chainId string, height uint64) *commonPb.Block {
	block := &commonPb.Block{
		Header: &commonPb.BlockHeader{
			ChainId:     chainId,
			BlockHeight: height,
			Proposer: &acPb.Member{
				OrgId:      "org1",
				MemberInfo: []byte("User1"),
			},
		},
		Txs: []*commonPb.Transaction{
			{
				Payload: &commonPb.Payload{
					ChainId:      chainId,
					TxType:       commonPb.TxType_INVOKE_CONTRACT,
					TxId:         generateTxId(chainId, height, 0),
					ContractName: syscontract.SystemContract_CHAIN_CONFIG.String(),
				},
				Sender: &commonPb.EndorsementEntry{
					Signer: &acPb.Member{
						OrgId:      "org1",
						MemberInfo: []byte("User1"),
					},
					Signature: []byte("signature1"),
				},
				Result: &commonPb.Result{
					Code: commonPb.TxStatusCode_SUCCESS,
					ContractResult: &commonPb.ContractResult{
						Result: []byte("ok"),
					},
				},
			},
		},
	}

	block.Header.BlockHash = generateBlockHash(chainId, height)
	block.Txs[0].Payload.TxId = generateTxId(chainId, height, 0)

	return block
}

func createContractMgrPayload(txId string) *commonPb.Payload {
	p, _ := utils.GenerateInstallContractPayload(userContractName, "2.0",
		commonPb.RuntimeType_WASMER, []byte("byte code!!!"), nil)
	p.TxId = txId
	return p
}
func createInitContractBlockAndRWSets(chainId string, height uint64) (*commonPb.Block, []*commonPb.TxRWSet) {
	block := createBlock(chainId, height, 1)
	block.Header.BlockType = commonPb.BlockType_CONTRACT_MGR_BLOCK
	block.Txs[0].Payload = createContractMgrPayload(generateTxId(chainId, height, 0))

	contract := &commonPb.Contract{
		Name:        userContractName,
		Version:     "2.0",
		RuntimeType: commonPb.RuntimeType_WASMER,
		Status:      commonPb.ContractStatus_NORMAL,
		Creator:     &acPb.MemberFull{MemberInfo: []byte("user1")},
	}
	cdata, err := contract.Marshal()
	if err != nil {
		panic(err)
	}

	var txRWSets []*commonPb.TxRWSet
	//建表脚本在写集
	txRWset := &commonPb.TxRWSet{
		TxId: block.Txs[0].Payload.TxId,
		TxWrites: []*commonPb.TxWrite{
			{
				Key:          utils.GetContractDbKey(userContractName),
				Value:        cdata,
				ContractName: syscontract.SystemContract_CONTRACT_MANAGE.String(),
			},
			{
				Key:          utils.GetContractByteCodeDbKey(userContractName),
				Value:        []byte("byte code!!!"),
				ContractName: syscontract.SystemContract_CONTRACT_MANAGE.String(),
			},
			{
				Key:          nil,
				Value:        []byte("create table t1(name varchar(50) primary key,amount int)"),
				ContractName: userContractName,
			},
		},
	}
	txRWSets = append(txRWSets, txRWset)
	return block, txRWSets
}

func createBlockAndRWSets(chainId string, height uint64, txNum int) (*commonPb.Block, []*commonPb.TxRWSet) {
	block := createBlock(chainId, height, txNum)
	var txRWSets []*commonPb.TxRWSet

	for i := 0; i < txNum; i++ {
		key := fmt.Sprintf("key_%d", i)
		value := fmt.Sprintf("value_%d", i)
		txRWset := &commonPb.TxRWSet{
			TxId: block.Txs[i].Payload.TxId,
			TxWrites: []*commonPb.TxWrite{
				{
					Key:          []byte(key),
					Value:        []byte(value),
					ContractName: userContractName,
				},
			},
		}
		txRWSets = append(txRWSets, txRWset)
	}

	return block, txRWSets
}

// createBigBlockAndRWSets creates a big block ,this block's length is greater than 1MB
func createBigBlockAndRWSets(chainId string, height uint64, txNum int) (*commonPb.Block, []*commonPb.TxRWSet) {
	block := createBigBlock(chainId, height, txNum)
	var txRWSets []*commonPb.TxRWSet

	for i := 0; i < txNum; i++ {
		b := RandStringBytesMaskImprSrcUnsafe(2 * 1024 * 1024)
		//blockfiledb.RandStringBytesMaskImprSrcUnsafe
		key := fmt.Sprintf("key_%d", i)
		value := fmt.Sprintf("value_%s", b)
		txRWset := &commonPb.TxRWSet{
			TxId: block.Txs[i].Payload.TxId,
			TxWrites: []*commonPb.TxWrite{
				{
					Key:          []byte(key),
					Value:        []byte(value),
					ContractName: userContractName,
				},
			},
		}
		txRWSets = append(txRWSets, txRWset)
	}

	return block, txRWSets
}

func createConfBlockAndRWSets(chainId string, height uint64) (*commonPb.Block, []*commonPb.TxRWSet) {
	block := createConfBlock(chainId, height)
	txRWSets := []*commonPb.TxRWSet{
		{
			TxId: block.Txs[0].Payload.TxId,
			TxWrites: []*commonPb.TxWrite{
				{
					Key:          []byte("key_0"),
					Value:        []byte("value_0"),
					ContractName: defaultSysContractName,
				},
			},
		},
	}

	return block, txRWSets
}

//var log = &test.GoLogger{}

//func TestMain(m *testing.M) {
//	fmt.Println("begin")
//	if dbType == types.MySQL {
//		// drop mysql table
//		conf := &localconf.ChainMakerConfig.StorageConfig
//		conf.Provider = "MySQL"
//		conf.MysqlConfig.Dsn = "root:123456@tcp(127.0.0.1:3306)/"
//		db, err := blocksqldb.NewBlockSqlDB(chainId, log)
//		if err != nil {
//			panic("faild to open mysql")
//		}
//		// clear data
//		gormDB := db.(*blocksqldb.BlockSqlDB).GetDB()
//		gormDB.Migrator().DropTable(&blocksqldb.BlockInfo{})
//		gormDB.Migrator().DropTable(&blocksqldb.TxInfo{})
//		gormDB.Migrator().DropTable(&statesqldb.StateInfo{})
//		gormDB.Migrator().DropTable(&historysqldb.HistoryInfo{})
//	}
//	os.RemoveAll(chainId)
//	m.Run()
//	fmt.Println("end")
//}
func Test_blockchainStoreImpl_GetBlockSqlDb(t *testing.T) {
	testBlockchainStoreImpl_GetBlock(t, config1)
}
func Test_blockchainStoreImpl_GetBlockLevelDb(t *testing.T) {
	testBlockchainStoreImpl_GetBlock(t, getlvldbConfig(""))
}

//TODO Devin
//func Test_blockchainStoreImpl_GetBlockBadgerdb(t *testing.T) {
//	testBlockchainStoreImpl_GetBlock(t, getBadgerConfig(""))
//}

func testBlockchainStoreImpl_GetBlock(t *testing.T, config *conf.StorageConfig) {
	var funcName = "get block"
	testChainID := "testBlockchainStoreImpl_GetBlock"
	tests := []struct {
		name  string
		block *commonPb.Block
	}{
		{funcName, createBlock(testChainID, 1, 1)},
		{funcName, createBlock(testChainID, 2, 1)},
		{funcName, createBlock(testChainID, 3, 1)},
		{funcName, createBlock(testChainID, 4, 1)},
	}
	var factory = getFactory()
	s, err := factory.NewStore(testChainID, config, &test.GoLogger{}, nil)
	if err != nil {
		panic(err)
	}
	defer func(s protocol.BlockchainStore) {
		s.Close()
	}(s)
	initGenesis(s, testChainID)
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := s.PutBlock(tt.block, nil); err != nil {
				t.Errorf("blockchainStoreImpl.PutBlock(), error %v", err)
			}
			got, err := s.GetBlockByHash(tt.block.Header.BlockHash)
			assert.Nil(t, err)
			assert.NotNil(t, got)
			assert.Equal(t, tt.block.String(), got.String())
		})
	}
}
func initGenesis(s protocol.BlockchainStore, chainId string) {
	genesis := createConfigBlock(chainId, 0)
	g := &storePb.BlockWithRWSet{Block: genesis, TxRWSets: getTxRWSets()}
	_ = s.InitGenesis(g)
}

func Test_blockchainStoreImpl_PutBlock(t *testing.T) {
	var factory = getFactory()
	//s, err := factory.newStore(chainId, config1)
	testChainID := "PutBlock"
	s, err := factory.NewStore(testChainID, getSqliteConfig(), &test.GoLogger{}, nil)
	initGenesis(s, testChainID)
	txRWSets := getTxRWSets()
	if err != nil {
		panic(err)
	}
	defer func() {
		time.Sleep(1 * time.Second)
		s.Close()
	}()
	block5 := createBlock(testChainID, 5, 1)
	txRWSets[0].TxId = block5.Txs[0].Payload.TxId
	err = s.PutBlock(block5, txRWSets)
	assert.NotNil(t, err)
}

//测试高速写模式,用kvdb
func Test_blockchainStoreImpl_PutBlock2(t *testing.T) {
	var factory = getFactory()
	//高速写
	testConf := getlvldbConfig("")
	testConf.WriteBlockType = 1
	testChainID := "PutBlock2"
	s, err := factory.NewStore(testChainID, testConf, &test.GoLogger{}, nil) //newStore(chainId, testConf)
	initGenesis(s, testChainID)
	txRWSets := getTxRWSets()
	if err != nil {
		panic(err)
	}
	defer func(s protocol.BlockchainStore) {
		time.Sleep(1 * time.Second)
		s.Close()
	}(s)
	block1 := createBlock(testChainID, 1, 1)
	txRWSets[0].TxId = block1.Txs[0].Payload.TxId
	err = s.PutBlock(block1, txRWSets)
	assert.Nil(t, err)
}

//测试配置出错 WriteBlockType 参数
func Test_blockchainStoreImpl_PutBlock3(t *testing.T) {
	var factory = getFactory()
	//高速写, 故意配置成2,模拟配置出错
	var configPutBlock3 = getlvldbConfig("")
	configPutBlock3.WriteBlockType = 2

	testChainID := "PutBlock3"
	s, err := factory.NewStore(testChainID, configPutBlock3, &test.GoLogger{}, nil) //factory.newStore(chainId, configPutBlock3)
	initGenesis(s, testChainID)
	txRWSets := getTxRWSets()
	if err != nil {
		panic(err)
	}
	defer func() {
		time.Sleep(1 * time.Second)
		s.Close()
	}()
	block5 := createBlock(testChainID, 5, 1)
	txRWSets[0].TxId = block5.Txs[0].Payload.TxId
	err = s.PutBlock(block5, txRWSets)
	if err != nil && err.Error() != "config error,write_block_type: "+
		strconv.Itoa(configPutBlock3.WriteBlockType) {
		t.Errorf("err = %v, want %v", err, "config error,write_block_type: 0")
	}

}

func Test_blockchainStoreImpl_HasBlock(t *testing.T) {
	var factory = getFactory()
	testChainID := "HasBlock"

	s, err := factory.NewStore(testChainID, getlvldbConfig(""), &test.GoLogger{}, nil) //factory.newStore(chainId, getlvldbConfig(""))
	if err != nil {
		panic(err)
	}
	defer func() {
		time.Sleep(1 * time.Second)
		s.Close()
	}()
	initGenesis(s, testChainID)
	exist, _ := s.BlockExists(createConfigBlock(testChainID, 0).Header.BlockHash)
	assert.True(t, exist)

	exist, err = s.BlockExists([]byte("not exist"))
	assert.Equal(t, nil, err)
	assert.False(t, exist)
}

//初始化数据库：0创世区块，1合约创建区块，2-5合约调用区块
func init5Blocks(s protocol.BlockchainStore, chainId string) {
	genesis := &storePb.BlockWithRWSet{Block: createConfigBlock(chainId, 0), TxRWSets: getTxRWSets()}
	//fmt.Println("-------------------init5Blocks,InitGenesis")
	err := s.InitGenesis(genesis)
	if err != nil {
		panic(err)
	}
	//fmt.Println("-------------------init5Blocks,createInitContractBlockAndRWSets")
	b, rw := createInitContractBlockAndRWSets(chainId, 1)
	err = s.PutBlock(b, rw)
	if err != nil {
		panic(err)
	}
	//fmt.Println("-------------------init5Blocks,createBlockAndRWSets, 2")
	b, rw = createBlockAndRWSets(chainId, 2, 2)
	err = s.PutBlock(b, rw)
	//fmt.Println("-------------------init5Blocks,createBlockAndRWSets, 2-2")
	if err != nil {
		//fmt.Println("-------------------init5Blocks,createBlockAndRWSets, 2-3",err)
		panic(err)
	}
	//fmt.Println("-------------------init5Blocks,createBlockAndRWSets,3")
	b, rw = createBlockAndRWSets(chainId, 3, 3)
	err = s.PutBlock(b, rw)
	if err != nil {
		panic(err)
	}
	//fmt.Println("-------------------init5Blocks,createBlockAndRWSets,4")
	b, rw = createBlockAndRWSets(chainId, 4, 10)
	err = s.PutBlock(b, rw)
	if err != nil {
		panic(err)
	}
	//fmt.Println("-------------------init5Blocks,createBlockAndRWSets,5")
	b, _ = createBlockAndRWSets(chainId, 5, 1)
	err = s.PutBlock(b, getTxRWSets())
	if err != nil {
		panic(err)
	}
}

// init4BigBlocks creates the big block ,every block's size is more than 1MB
// we use it to testing the hot cold data separate ,
// this testing case ,we need make sure the blocks is saved in several segments of blockFile
// example:  block0 be saved segment0,  block1 be saved segment1, block2 be saved segment2
// 1合约创建区块，2-5合约调用区块,16-25测冷热分离用
func init4BigBlocks(s protocol.BlockchainStore, chainId string) {
	//genesis := &storePb.BlockWithRWSet{Block: createConfigBlock(chainId, 0), TxRWSets: getTxRWSets()}
	////fmt.Println("-------------------init5BigBlocks,InitGenesis")
	//err := s.InitGenesis(genesis)
	//if err != nil {
	//	panic(err)
	//}
	fmt.Println("-------------------init5BigBlocks,createInitContractBlockAndRWSets")
	b, rw := createInitContractBlockAndRWSets(chainId, 1)
	err := s.PutBlock(b, rw)
	if err != nil {
		panic(err)
	}
	fmt.Println("-------------------init5BigBlocks,createBlockAndRWSets, 2")
	b, rw = createBigBlockAndRWSets(chainId, 2, 2)
	err = s.PutBlock(b, rw)
	//fmt.Println("-------------------init5BigBlocks,createBlockAndRWSets, 2-2")
	if err != nil {
		//fmt.Println("-------------------init5Blocks,createBlockAndRWSets, 2-3",err)
		panic(err)
	}
	fmt.Println("-------------------init5BigBlocks,createBlockAndRWSets,3")
	b, rw = createBigBlockAndRWSets(chainId, 3, 3)
	err = s.PutBlock(b, rw)
	if err != nil {
		panic(err)
	}
	fmt.Println("-------------------init5BigBlocks,createBlockAndRWSets,4")
	b, rw = createBigBlockAndRWSets(chainId, 4, 30)
	err = s.PutBlock(b, rw)
	if err != nil {
		panic(err)
	}
	fmt.Println("-------------------init5BigBlocks,createBlockAndRWSets,5")
	b, _ = createBigBlockAndRWSets(chainId, 5, 10)
	err = s.PutBlock(b, getTxRWSets())
	if err != nil {
		panic(err)
	}
	for i := 6; i < 25; i++ {
		fmt.Printf("-------------------init5BigBlocks,createBlockAndRWSets,%d", i)
		b, _ = createBigBlockAndRWSets(chainId, uint64(i), 10)
		err = s.PutBlock(b, getTxRWSets())
		if err != nil {
			panic(err)
		}
	}

}

func init5ContractBlocks(s protocol.BlockchainStore, chainId string) []*commonPb.Block {
	result := []*commonPb.Block{}
	genesis := &storePb.BlockWithRWSet{Block: createConfigBlock(chainId, 0)}
	genesis.TxRWSets = []*commonPb.TxRWSet{
		{
			TxWrites: []*commonPb.TxWrite{
				{
					Key:          []byte("key1"),
					Value:        []byte("value1"),
					ContractName: syscontract.SystemContract_CHAIN_CONFIG.String(),
				},
			},
		},
	}

	_ = s.InitGenesis(genesis)
	result = append(result, genesis.Block)
	b, rw := createInitContractBlockAndRWSets(chainId, 1)
	fmt.Println("Is contract?", utils.IsContractMgmtBlock(b))
	_ = s.PutBlock(b, rw)
	result = append(result, b)
	b, rw = createBlockAndRWSets(chainId, 2, 2)
	_ = s.PutBlock(b, rw)
	result = append(result, b)
	b, rw = createBlockAndRWSets(chainId, 3, 3)
	_ = s.PutBlock(b, rw)
	result = append(result, b)
	b, rw = createBlockAndRWSets(chainId, 4, 10)
	_ = s.PutBlock(b, rw)
	result = append(result, b)
	b, rw = createBlockAndRWSets(chainId, 5, 1)
	_ = s.PutBlock(b, rw)
	result = append(result, b)
	return result
}
func Test_blockchainStoreImpl_GetBlockAt(t *testing.T) {
	var factory = getFactory()
	testChainID := "GetBlockAt"
	//fmt.Println("---------------------------1----------------")
	s, err := factory.NewStore(testChainID, getSqliteConfig(), &test.GoLogger{}, nil)
	//fmt.Println("---------------------------2----------------")
	if err != nil {
		//fmt.Println("---------------------------21----------------")
		fmt.Println("factory.NewStore return error,", err)
		panic(err)
	}
	defer func() {
		time.Sleep(1 * time.Second)
		s.Close()
	}()
	//fmt.Println("---------------------------3----------------")
	init5Blocks(s, testChainID)
	//fmt.Println("---------------------------4----------------")
	block5 := createBlock(testChainID, 5, 1)
	//fmt.Println("---------------------------5----------------")
	got, err := s.GetBlock(block5.Header.BlockHeight)
	//fmt.Println("---------------------------6----------------")
	assert.Equal(t, nil, err)
	//fmt.Println("---------------------------7----------------")
	assert.Equal(t, block5.String(), got.String())
	//fmt.Println("---------------------------8----------------")
}

func Test_blockchainStoreImpl_GetLastBlock(t *testing.T) {
	var factory = getFactory()
	testChainID := "GetLastBlock"
	s, err := factory.NewStore(testChainID, getSqliteConfig(), &test.GoLogger{}, nil)
	if err != nil {
		panic(err)
	}
	defer func(s protocol.BlockchainStore) {

		s.Close()
	}(s)
	init5Blocks(s, testChainID)
	assert.Equal(t, nil, err)
	lastBlock, err := s.GetLastBlock()
	assert.Equal(t, nil, err)
	block5 := createBlock(testChainID, 5, 1)
	assert.Equal(t, block5.Header.BlockHeight, lastBlock.Header.BlockHeight)
}

func Test_blockchainStoreImpl_GetBlockByTx(t *testing.T) {
	var factory = getFactory()
	testChainID := "GetBlockByTx"
	s, err := factory.NewStore(testChainID, getSqliteConfig(), &test.GoLogger{}, nil)
	if err != nil {
		panic(err)
	}
	defer func(s protocol.BlockchainStore) {

		s.Close()
	}(s)
	init5Blocks(s, testChainID)
	block, err := s.GetBlockByTx(generateTxId(testChainID, 3, 0))
	assert.Equal(t, nil, err)
	assert.Equal(t, uint64(3), block.Header.BlockHeight)

	blockNotExist, err := s.GetBlockByTx("not_exist_txid")
	assert.Equal(t, nil, err)
	assert.Equal(t, true, blockNotExist == nil)

}

const HAS_TX = "has tx"

func Test_blockchainStoreImpl_GetTx(t *testing.T) {
	//funcName := HAS_TX
	//tests := []struct {
	//	name  string
	//	block *commonPb.Block
	//}{
	//	{funcName, createBlock(chainId, 1, 1)},
	//	{funcName, createBlock(chainId, 2, 1)},
	//	{funcName, createBlock(chainId, 3, 1)},
	//	{funcName, createBlock(chainId, 4, 1)},
	//	{funcName, createBlock(chainId, 999999, 2)},
	//}

	var factory = getFactory()
	testChainID := "GetTx"

	s, err := factory.NewStore(testChainID, getSqliteConfig(), &test.GoLogger{}, nil)
	if err != nil {
		panic(err)
	}
	defer func(s protocol.BlockchainStore) {

		s.Close()
	}(s)
	//assert.DeepEqual(t, s.GetTx(tests[0].block.Txs[0].TxId, )
	blocks := init5ContractBlocks(s, testChainID)
	tx, err := s.GetTx(blocks[1].Txs[0].Payload.TxId)
	assert.Nil(t, err)
	if tx == nil {
		t.Error("Error, GetTx")
	}
	_, err = s.GetTx(blocks[2].Txs[0].Payload.TxId)
	assert.Nil(t, err)
	_, err = s.GetTx(blocks[3].Txs[0].Payload.TxId)
	assert.Nil(t, err)
	_, err = s.GetTx(blocks[4].Txs[0].Payload.TxId)
	assert.Nil(t, err)
	//assert.Equal(t, tx.Payload.TxId, generateTxId(chainId, 1, 0))
	//
	////chain not exist
	//tx, err = s.GetTx(generateTxId("not exist chain", 1, 0))
	//t.Log(tx)
	//assert.NotNil(t,  err)

}

func Test_blockchainStoreImpl_GetTxWithRWSet(t *testing.T) {
	var factory = getFactory()
	testChainID := "GetTxWithRWSet"

	s, err := factory.NewStore(testChainID, getSqliteConfig(), &test.GoLogger{}, nil)
	if err != nil {
		panic(err)
	}
	defer func(s protocol.BlockchainStore) {
		s.Close()
	}(s)
	//assert.DeepEqual(t, s.GetTx(tests[0].block.Txs[0].TxId, )
	blocks := init5ContractBlocks(s, testChainID)
	tx, err := s.GetTx(blocks[1].Txs[0].Payload.TxId)
	assert.Nil(t, err)
	if tx == nil {
		t.Error("Error, GetTx")
	}
	txwithRwset, err := s.GetTxWithRWSet(blocks[2].Txs[0].Payload.TxId)
	assert.Nil(t, err)
	t.Log(txwithRwset.Transaction)
	t.Log(txwithRwset.RwSet)
	_, err = s.GetTxWithRWSet(blocks[3].Txs[0].Payload.TxId)
	assert.Nil(t, err)
	txinfoWithRWSet, err := s.GetTxInfoWithRWSet(blocks[4].Txs[0].Payload.TxId)
	assert.Nil(t, err)
	assert.EqualValues(t, 4, txinfoWithRWSet.BlockHeight)

}

func Test_blockchainStoreImpl_HasTx(t *testing.T) {
	funcName := HAS_TX
	testChainID := "HasTx"
	tests := []struct {
		name  string
		block *commonPb.Block
	}{
		{funcName, createBlock(testChainID, 1, 1)},
		{funcName, createBlock(testChainID, 2, 1)},
		{funcName, createBlock(testChainID, 3, 1)},
		{funcName, createBlock(testChainID, 4, 1)},
		{funcName, createBlock(testChainID, 999999, 1)},
	}
	var factory = getFactory()

	s, err := factory.NewStore(testChainID, getSqliteConfig(), &test.GoLogger{}, nil)
	if err != nil {
		panic(err)
	}
	defer func(s protocol.BlockchainStore) {

		s.Close()
	}(s)
	init5Blocks(s, testChainID)
	exist, err := s.TxExists(tests[0].block.Txs[0].Payload.TxId)
	assert.Equal(t, nil, err)
	assert.Equal(t, true, exist)
	exist, err = s.TxExists(tests[1].block.Txs[0].Payload.TxId)
	assert.Equal(t, nil, err)
	assert.Equal(t, true, exist)
	exist, err = s.TxExists(tests[2].block.Txs[0].Payload.TxId)
	assert.Equal(t, true, exist)
	assert.Equal(t, nil, err)
	exist, err = s.TxExists(tests[3].block.Txs[0].Payload.TxId)
	assert.Equal(t, true, exist)
	assert.Equal(t, nil, err)
	exist, err = s.TxExists(tests[4].block.Txs[0].Payload.TxId)
	assert.Equal(t, nil, err)
	assert.Equal(t, false, exist)
}
func Test_blockchainStoreImpl_TxExists(t *testing.T) {
	cfg := getSqliteConfig()
	//txExistDbconfig 置为 nil ，这种情况，会使用blockdb
	cfg.TxExistDbConfig = nil
	var factory = getFactory()
	testChainID := "TxExists"
	//fmt.Println("cfg =",cfg)
	s, err := factory.NewStore(testChainID, cfg, &test.GoLogger{}, nil)
	if err != nil {
		panic(err)
	}
	defer func(s protocol.BlockchainStore) {
		s.Close()
	}(s)
	init5Blocks(s, testChainID)
	block1 := createBlock(testChainID, 1, 1)
	exist, err := s.TxExists(block1.Txs[0].Payload.TxId)
	assert.Equal(t, nil, err)
	assert.Equal(t, true, exist)

}
func Test_blockchainStoreImpl_ReadObject(t *testing.T) {
	var factory = getFactory()
	testChainID := "ReadObject"
	s, err := factory.NewStore(testChainID, getSqliteConfig(), &test.GoLogger{}, nil)
	assert.Nil(t, err)
	defer func() { _ = s.Close() }()
	initGenesis(s, testChainID)
	assert.Equal(t, nil, err)
	value, err := s.ReadObject(defaultSysContractName, []byte("key1"))
	assert.Equal(t, nil, err)
	assert.Equal(t, value, []byte("value1"))

	value, err = s.ReadObject(defaultSysContractName, []byte("key2"))
	assert.Equal(t, nil, err)
	assert.Equal(t, value, []byte("value2"))
}

func Test_blockchainStoreImpl_SelectObject(t *testing.T) {
	//t.Run("sql iterator", func(tt *testing.T) {
	//	testSelectStateKeyRange(tt, getSqliteConfig())
	//})
	//TODO Devin: sqlite get error
	t.Run("kvdb iterator", func(tt *testing.T) {
		cf := getlvldbConfig("")
		cf.DisableBlockFileDb = false
		testSelectStateKeyRange(tt, cf)
	})
}
func testSelectStateKeyRange(t *testing.T, storeConfig *conf.StorageConfig) {
	var factory = getFactory()
	testChainID := "testSelectStateKeyRange"
	s, err := factory.NewStore(testChainID, storeConfig, &test.GoLogger{}, nil)
	defer func() { _ = s.Close() }()
	init5ContractBlocks(s, testChainID)
	assert.Equal(t, nil, err)

	iter, err := s.SelectObject(userContractName, []byte("key_2"), []byte("key_4"))
	assert.Nil(t, err)
	defer iter.Release()
	var count = 0
	for iter.Next() {
		count++
		kv, e := iter.Value()
		assert.Nil(t, e)
		t.Logf("key:%s, value:%s\n", string(kv.Key), string(kv.Value))
	}
	assert.Equal(t, 2, count)
}
func Test_blockchainStoreImpl_GetHistoryForKey(t *testing.T) {
	t.Run("kvdb key history iterator", func(tt *testing.T) {
		cf := getlvldbConfig("")
		cf.DisableBlockFileDb = false
		testKeyHistory(tt, cf)
	})
	t.Run("sql history iterator", func(tt *testing.T) {
		testKeyHistory(tt, getSqliteConfig())
	})
}
func testKeyHistory(tt *testing.T, cf *conf.StorageConfig) {
	var factory = getFactory()
	testChainID := "testKeyHistory"
	s, err := factory.NewStore(testChainID, cf, &test.GoLogger{}, nil)
	defer func() { _ = s.Close() }()
	init5ContractBlocks(s, testChainID)
	assert.Equal(tt, nil, err)
	testSelectKeyHistory(tt, s, "key_2", 2)
	testSelectKeyHistory(tt, s, "key_1", 3)
	testSelectKeyHistory(tt, s, "key_0", 4)
}
func testSelectKeyHistory(t *testing.T, s protocol.BlockchainStore, key string, hisCount int) {
	iter, err := s.GetHistoryForKey(userContractName, []byte(key))
	assert.Nil(t, err)
	defer iter.Release()
	var count = 0
	for iter.Next() {
		count++
		kv, e := iter.Value()
		assert.Nil(t, e)
		t.Logf("txid:%s, value:%s,time:%d,block height:%d\n",
			kv.TxId, string(kv.Value), kv.Timestamp, kv.BlockHeight)
	}
	assert.Equal(t, hisCount, count)
}

func Test_blockchainStoreImpl_TxRWSet(t *testing.T) {
	var factory = getFactory()
	testChainID := "TxRWSet"
	s, err := factory.NewStore(testChainID, getSqliteConfig(), &test.GoLogger{}, nil)
	defer func() { _ = s.Close() }()
	init5Blocks(s, testChainID)
	assert.Equal(t, nil, err)
	impl, ok := s.(*BlockStoreImpl)
	assert.Equal(t, true, ok)
	block5 := createBlock(testChainID, 5, 1)
	txid := block5.Txs[0].Payload.TxId
	txRWSetFromDB, err := impl.GetTxRWSet(txid)
	assert.Equal(t, nil, err)
	t.Log(txRWSetFromDB)
	assert.Equal(t, getTxRWSets()[0].String(), txRWSetFromDB.String())
}

/*func TestBlockStoreImpl_Recovery(t *testing.T) {
	fmt.Println("test recover, please delete DB file in 20s")
	time.Sleep(20 * time.Second)
	s, err := Factory{}.NewStore(dbType, chainId)
	defer s.Close()
	assert.Equal(t, nil, err)
	fmt.Println("recover commpleted")
}*/

func Test_blockchainStoreImpl_getLastSavepoint(t *testing.T) {
	var factory = getFactory()
	testChainID := "getLastSavepoint"
	s, err := factory.NewStore(testChainID, getSqliteConfig(), &test.GoLogger{}, nil)
	defer func() { _ = s.Close() }()
	init5Blocks(s, testChainID)
	assert.Equal(t, nil, err)
	impl, ok := s.(*BlockStoreImpl)
	assert.Equal(t, true, ok)
	height, err := impl.getLastFileSavepoint()
	assert.Nil(t, err)
	assert.Equal(t, uint64(5), height)
	height, _ = impl.blockDB.GetLastSavepoint()
	assert.Equal(t, uint64(5), height)
	height, _ = impl.stateDB.GetLastSavepoint()
	assert.Equal(t, uint64(5), height)
	height, _ = impl.resultDB.GetLastSavepoint()
	assert.Equal(t, uint64(5), height)
	height, _ = impl.historyDB.GetLastSavepoint()
	assert.Equal(t, uint64(5), height)
}

func TestBlockStoreImpl_GetTxRWSetsByHeight(t *testing.T) {
	var factory = getFactory()
	testChainID := "GetTxRWSetsByHeight"
	s, err := factory.NewStore(testChainID, getlvldbConfig(""), &test.GoLogger{}, nil)
	defer func() { _ = s.Close() }()
	init5Blocks(s, testChainID)
	txRWSets := getTxRWSets()
	assert.Equal(t, nil, err)
	impl, ok := s.(*BlockStoreImpl)
	assert.Equal(t, true, ok)
	txRWSetsFromDB, err := impl.GetTxRWSetsByHeight(5)
	assert.Equal(t, nil, err)
	assert.Equal(t, len(txRWSets), len(txRWSetsFromDB))
	for index, txRWSet := range txRWSetsFromDB {
		assert.Equal(t, txRWSets[index].String(), txRWSet.String())
	}
}

//func TestBlockStoreImpl_GetDBHandle(t *testing.T) {
//	var factory=getFactory()
//	s, err := factory.newStore(chainId, config1, binlog.NewMemBinlog(), log)
//	defer s.Close()
//	assert.Equal(t, nil, err)
//	dbHandle := s.GetDBHandle("test")
//	dbHandle.Put([]byte("a"), []byte("A"))
//	value, err := dbHandle.Get([]byte("a"))
//	assert.Equal(t, nil, err)
//	assert.Equal(t, []byte("A"), value)
//}

func Test_blockchainStoreImpl_GetBlockWith100Tx(t *testing.T) {
	var factory = getFactory()
	testChainID := "GetBlockWith100Tx"
	s, err := factory.NewStore(testChainID, getSqliteConfig(), &test.GoLogger{}, nil)
	if err != nil {
		panic(err)
	}
	defer func() { _ = s.Close() }()
	init5Blocks(s, testChainID)
	block, txRWSets := createBlockAndRWSets(testChainID, 6, 1)
	_ = s.PutBlock(block, txRWSets)
	block, txRWSets = createBlockAndRWSets(testChainID, 7, 100)
	err = s.PutBlock(block, txRWSets)

	assert.Nil(t, err)
	blockFromDB, err := s.GetBlock(7)
	assert.Equal(t, nil, err)
	assert.Equal(t, block.String(), blockFromDB.String())

	txRWSetsFromDB, err := s.GetTxRWSetsByHeight(7)
	assert.Equal(t, nil, err)
	assert.Equal(t, len(txRWSets), len(txRWSetsFromDB))
	for i := 0; i < len(txRWSets); i++ {
		assert.Equal(t, txRWSets[i].String(), txRWSetsFromDB[i].String())
	}

	blockWithRWSets, err := s.GetBlockWithRWSets(7)
	assert.Equal(t, nil, err)
	assert.Equal(t, block.String(), blockWithRWSets.Block.String())
	for i := 0; i < len(blockWithRWSets.TxRWSets); i++ {
		assert.Equal(t, txRWSets[i].String(), blockWithRWSets.TxRWSets[i].String())
	}
}

func Test_blockchainStoreImpl_recovory(t *testing.T) {
	var factory = getFactory()
	ldbConfig := getlvldbConfig("")
	ldbConfig.DisableBlockFileDb = true
	testChainID := "recovory"
	s, err := factory.NewStore(testChainID, ldbConfig, &test.GoLogger{}, nil)
	//defer s.Close()
	assert.Equal(t, nil, err)
	bs, ok := s.(*BlockStoreImpl)
	assert.Equal(t, true, ok)
	init5Blocks(s, testChainID)
	//
	block6, txRWSets6 := createBlockAndRWSets(testChainID, 6, 100)

	//1. commit wal
	blockWithRWSet := &storePb.BlockWithRWSet{
		Block:    block6,
		TxRWSets: txRWSets6,
	}
	blockWithRWSetBytes, _, err := serialization.SerializeBlock(blockWithRWSet)
	assert.Equal(t, nil, err)
	_, err = bs.writeBlockToFile(block6.Header.BlockHeight, blockWithRWSetBytes)
	if err != nil {
		fmt.Printf("chain[%s] Failed to write wal, block[%d]",
			block6.Header.ChainId, block6.Header.BlockHeight)
		t.Error(err)
	}
	binlogSavepoint, _ := bs.getLastFileSavepoint()
	assert.EqualValues(t, uint64(6), binlogSavepoint)
	blockDBSavepoint, _ := bs.blockDB.GetLastSavepoint()
	assert.Equal(t, uint64(5), blockDBSavepoint)

	stateDBSavepoint, _ := bs.stateDB.GetLastSavepoint()
	assert.Equal(t, uint64(5), stateDBSavepoint)

	historyDBSavepoint, _ := bs.historyDB.GetLastSavepoint()
	assert.Equal(t, uint64(5), historyDBSavepoint)
	resultDBSavepoint, _ := bs.resultDB.GetLastSavepoint()
	assert.Equal(t, uint64(5), resultDBSavepoint)

	s.Close()
	t.Log("start recovery db from bin log")
	//recovory
	s, err = factory.NewStore(testChainID, ldbConfig, &test.GoLogger{}, nil)
	assert.Equal(t, nil, err)
	t.Log("db recovered")
	impl, ok := s.(*BlockStoreImpl)
	assert.Equal(t, true, ok)
	binlogSavepoint, _ = impl.getLastFileSavepoint()
	assert.EqualValues(t, 6, binlogSavepoint)
	blockDBSavepoint, _ = impl.blockDB.GetLastSavepoint()
	assert.EqualValues(t, 6, blockDBSavepoint)

	stateDBSavepoint, _ = impl.stateDB.GetLastSavepoint()
	assert.EqualValues(t, 6, stateDBSavepoint)

	historyDBSavepoint, _ = impl.historyDB.GetLastSavepoint()
	assert.EqualValues(t, 6, historyDBSavepoint)
	resultDBSavepoint, _ = impl.resultDB.GetLastSavepoint()
	assert.EqualValues(t, 6, resultDBSavepoint)
	s.Close()

	//check recover result
	s, err = factory.NewStore(testChainID, ldbConfig, &test.GoLogger{}, nil)
	assert.Nil(t, err)
	blockWithRWSets, err := s.GetBlockWithRWSets(6)
	assert.Equal(t, nil, err)
	assert.Equal(t, block6.String(), blockWithRWSets.Block.String())
	for i := 0; i < len(blockWithRWSets.TxRWSets); i++ {
		assert.Equal(t, txRWSets6[i].String(), blockWithRWSets.TxRWSets[i].String())
	}
	s.Close()
}

//func TestWriteBinlog(t *testing.T) {
//	storePath := filepath.Join(os.TempDir(), fmt.Sprintf("%d", time.Now().Unix()), logPath)
//	opts := blockfiledb.DefaultOptions
//
//	bfdbPath := filepath.Join(storePath, chainId, blockFilePath)
//	bfdb, bfdbErr := blockfiledb.Open(bfdbPath, opts, &test.GoLogger{})
//	if bfdbErr != nil {
//		panic(fmt.Sprintf("open block file db failed, path:%s, error:%s", bfdbPath, bfdbErr))
//	}
//	_, _, _, err := bfdb.Write(1, []byte("100"))
//	assert.Nil(t, err)
//}

//
//func TestLeveldbRange(t *testing.T) {
//	db, err := leveldb.OpenFile("gossip.db", nil)
//	if err != nil {
//		fmt.Println(err)
//		return
//	}
//
//	wo := &opt.WriteOptions{Sync: true}
//	db.Put([]byte("key-1a"), []byte("value-1"), wo)
//	db.Put([]byte("key-3c"), []byte("value-3"), wo)
//	db.Put([]byte("key-4d"), []byte("value-4"), wo)
//	db.Put([]byte("key-5eff"), []byte("value-5"), wo)
//	db.Put([]byte("key-2b"), []byte("value-2"), wo)
//	iter := db.NewIterator(&util.Range{Start: []byte("key-1a"), Limit: []byte("key-3d")}, nil)
//	for iter.Next() {
//		fmt.Println(string(iter.Key()), string(iter.Value()))
//	}
//	iter.Release()
//	err = iter.Error()
//	if err != nil {
//		fmt.Println(err)
//		return
//	}
//	defer db.Close()
//}

func Test_blockchainStoreImpl_Mysql_Archive(t *testing.T) {
	//var factory = getFactory()
	//s, err := factory.NewStore(chainId, getSqliteConfig(), &test.GoLogger{}, nil)
	//assert.Equal(t, nil, err)
	//defer func(s protocol.BlockchainStore) {
	//
	//	s.Close()
	//}(s)
	//
	//err = s.ArchiveBlock(0)
	//assert.Equal(t, nil, err)
	//
	//err = s.RestoreBlocks(nil)
	//assert.Equal(t, nil, err)
	//
	//archivedPivot := s.GetArchivedPivot()
	//assert.True(t, archivedPivot == 0)
}

func Test_blockchainStoreImpl_Archive(t *testing.T) {
	//var factory = getFactory()
	//dbConf := getlvldbConfig("")
	//dbConf.UnArchiveBlockHeight = 10
	//s, err := factory.NewStore(chainId, dbConf, log, nil)
	//assert.Equal(t, nil, err)
	//defer func(s protocol.BlockchainStore) {
	//
	//	s.Close()
	//}(s)
	//
	//totalHeight := 60
	//archiveHeight1 := 27
	//archiveHeight2 := 30
	//archiveHeight3 := 43
	//
	////Prepare block data
	//blocks := make([]*commonPb.Block, 0, totalHeight)
	//txRWSetMp := make(map[uint64][]*commonPb.TxRWSet)
	//for i := 0; i < totalHeight; i++ {
	//	var (
	//		block   *commonPb.Block
	//		txRWSet []*commonPb.TxRWSet
	//	)
	//
	//	if i%5 == 0 {
	//		block, txRWSet = createConfBlockAndRWSets(chainId, uint64(i))
	//	} else {
	//		block, txRWSet = createBlockAndRWSets(chainId, uint64(i), 10)
	//	}
	//
	//	err = s.PutBlock(block, txRWSet)
	//	assert.Equal(t, nil, err)
	//	blocks = append(blocks, block)
	//	txRWSetMp[block.Header.BlockHeight] = txRWSet
	//}
	//
	//verifyArchive(t, 0, blocks, s)
	//
	////archive block height1
	//err = s.ArchiveBlock(uint64(archiveHeight1))
	//assert.Equal(t, nil, err)
	//assert.Equal(t, uint64(archiveHeight1), s.GetArchivedPivot())
	//
	//verifyArchive(t, 10, blocks, s)
	//
	////archive block height2 which is a config block
	//err1 := s.ArchiveBlock(uint64(archiveHeight2))
	//assert.True(t, err1 == archive.ErrConfigBlockArchive)
	//assert.Equal(t, uint64(archiveHeight1), s.GetArchivedPivot())
	//
	//verifyArchive(t, 15, blocks, s)
	//
	////archive block height3
	//err = s.ArchiveBlock(uint64(archiveHeight3))
	//assert.Equal(t, nil, err)
	//assert.Equal(t, uint64(archiveHeight3), s.GetArchivedPivot())
	//
	//verifyArchive(t, 25, blocks, s)
	//
	////Prepare restore data
	//blocksBytes := make([][]byte, 0, archiveHeight3-archiveHeight2+1)
	//for i := archiveHeight2; i <= archiveHeight3; i++ {
	//	blockBytes, _, err5 := serialization.SerializeBlock(&storePb.BlockWithRWSet{
	//		Block:          blocks[i],
	//		TxRWSets:       txRWSetMp[blocks[i].Header.BlockHeight],
	//		ContractEvents: nil,
	//	})
	//
	//	assert.Equal(t, nil, err5)
	//	blocksBytes = append(blocksBytes, blockBytes)
	//}
	//
	////restore block
	//err = s.RestoreBlocks(blocksBytes)
	//assert.Equal(t, nil, err)
	//assert.Equal(t, uint64(archiveHeight2-1), s.GetArchivedPivot())
	//
	//verifyArchive(t, 10, blocks, s)
	//
	////wait kvdb compactrange
	//time.Sleep(5 * time.Second)
}

func verifyArchive(t *testing.T, confHeight uint64, blocks []*commonPb.Block, s protocol.BlockchainStore) {
	archivedPivot := s.GetArchivedPivot()

	if archivedPivot == 0 {
		verifyUnarchivedHeight(t, archivedPivot, blocks, s)
		verifyUnarchivedHeight(t, archivedPivot+1, blocks, s)
		return
	}

	//verify store apis: archived height
	verifyArchivedHeight(t, archivedPivot-1, blocks, s)

	//verify store apis: archivedPivot height
	verifyArchivedHeight(t, archivedPivot, blocks, s)

	//verify store apis: conf block height
	verifyUnarchivedHeight(t, confHeight, blocks, s)

	//verify store apis: unarchived height
	verifyUnarchivedHeight(t, archivedPivot+1, blocks, s)
}

func verifyUnarchivedHeight(t *testing.T, avBlkHeight uint64, blocks []*commonPb.Block, s protocol.BlockchainStore) {
	avBlk := blocks[avBlkHeight]
	vbHeight, err1 := s.GetHeightByHash(avBlk.Header.BlockHash)
	assert.True(t, err1 == nil)
	assert.Equal(t, vbHeight, avBlkHeight)

	header, err2 := s.GetBlockHeaderByHeight(avBlk.Header.BlockHeight)
	assert.True(t, err2 == nil)
	assert.True(t, bytes.Equal(header.BlockHash, avBlk.Header.BlockHash))

	vtHeight, err4 := s.GetTxHeight(avBlk.Txs[0].Payload.TxId)
	assert.True(t, err4 == nil)
	assert.Equal(t, vtHeight, avBlkHeight)

	vtBlk, err5 := s.GetBlockByTx(avBlk.Txs[0].Payload.TxId)
	assert.True(t, err5 == nil)
	assert.Equal(t, avBlk.Header.ChainId, vtBlk.Header.ChainId)

	vttx, err6 := s.GetTx(avBlk.Txs[0].Payload.TxId)
	assert.True(t, err6 == nil)
	assert.Equal(t, avBlk.Header.ChainId, vttx.Payload.ChainId)

	vtBlk2, err7 := s.GetBlockByHash(avBlk.Hash())
	assert.True(t, err7 == nil)
	assert.Equal(t, avBlk.Header.ChainId, vtBlk2.Header.ChainId)

	vtBlkRW, err8 := s.GetBlockWithRWSets(avBlk.Header.BlockHeight)
	assert.True(t, err8 == nil)
	assert.Equal(t, avBlk.Header.ChainId, vtBlkRW.Block.Header.ChainId)

	vtBlkRWs, err9 := s.GetTxRWSetsByHeight(avBlk.Header.BlockHeight)
	assert.True(t, err9 == nil)
	assert.Equal(t, len(avBlk.Txs), len(vtBlkRWs))
	if len(avBlk.Txs) > 0 {
		assert.Equal(t, avBlk.Txs[0].Payload.TxId, vtBlkRWs[0].TxId)
	}
}

func verifyArchivedHeight(t *testing.T, avBlkHeight uint64, blocks []*commonPb.Block, s protocol.BlockchainStore) {
	avBlk := blocks[avBlkHeight]
	vbHeight, err1 := s.GetHeightByHash(avBlk.Header.BlockHash)
	assert.True(t, err1 == nil)
	assert.Equal(t, vbHeight, avBlkHeight)

	header, err2 := s.GetBlockHeaderByHeight(avBlk.Header.BlockHeight)
	assert.True(t, err2 == nil)
	assert.True(t, bytes.Equal(header.BlockHash, avBlk.Header.BlockHash))

	vtHeight, err4 := s.GetTxHeight(avBlk.Txs[0].Payload.TxId)
	assert.True(t, err4 == nil)
	assert.Equal(t, vtHeight, avBlkHeight)

	vtBlk, err5 := s.GetBlockByTx(avBlk.Txs[0].Payload.TxId)
	assert.True(t, archive.ErrArchivedBlock == err5)
	assert.True(t, vtBlk == nil)

	vttx, err6 := s.GetTx(avBlk.Txs[0].Payload.TxId)
	assert.True(t, archive.ErrArchivedTx == err6)
	assert.True(t, vttx == nil)

	vtBlk2, err7 := s.GetBlockByHash(avBlk.Hash())
	assert.True(t, archive.ErrArchivedBlock == err7)
	assert.True(t, vtBlk2 == nil)

	vtBlkRW, err8 := s.GetBlockWithRWSets(avBlk.Header.BlockHeight)
	assert.True(t, archive.ErrArchivedBlock == err8)
	assert.True(t, vtBlkRW == nil)

	vtBlkRWs, err9 := s.GetTxRWSetsByHeight(avBlk.Header.BlockHeight)
	assert.True(t, archive.ErrArchivedRWSet == err9)
	assert.True(t, vtBlkRWs == nil)
}

func TestBlockStoreImpl_GetArchivedPivot(t *testing.T) {
	var factory = getFactory()
	ldbConfig := getlvldbConfig("")
	ldbConfig.DisableBlockFileDb = false
	testChainID := "GetArchivedPivot"
	s, err := factory.NewStore(testChainID, ldbConfig, &test.GoLogger{}, nil)
	//defer s.Close()
	assert.Equal(t, nil, err)
	bs, ok := s.(*BlockStoreImpl)

	assert.Equal(t, true, ok)
	init5Blocks(s, testChainID)

	height := bs.GetArchivedPivot()
	assert.Equal(t, uint64(0), height)

	sqldbConfig := getSqliteConfig()
	s, err = factory.NewStore(testChainID, sqldbConfig, &test.GoLogger{}, nil)
	assert.Equal(t, nil, err)
	bs, ok = s.(*BlockStoreImpl)
	assert.Equal(t, true, ok)
	init5Blocks(s, testChainID)
	height = bs.GetArchivedPivot()
	assert.Equal(t, uint64(0), height)
}

func TestBlockStoreImpl_GetBlockHeaderByHeight(t *testing.T) {
	var factory = getFactory()
	sqldbConfig := getSqliteConfig()
	testChainID := "GetBlockHeaderByHeight"
	s, err := factory.NewStore(testChainID, sqldbConfig, &test.GoLogger{}, nil)
	assert.Equal(t, nil, err)
	bs, ok := s.(*BlockStoreImpl)
	assert.Equal(t, true, ok)
	init5Blocks(s, testChainID)
	header, err := bs.GetBlockHeaderByHeight(1)
	assert.Nil(t, err)
	assert.Equal(t, uint64(1), header.BlockHeight)
	header, err = bs.GetBlockHeaderByHeight(10)
	assert.Nil(t, header)
	assert.Nil(t, err)
}

func TestBlockStoreImpl_GetLastConfigBlock(t *testing.T) {
	var factory = getFactory()
	sqldbConfig := getSqliteConfig()
	testChainID := "GetLastConfigBlock"
	s, err := factory.NewStore(testChainID, sqldbConfig, &test.GoLogger{}, nil)
	assert.Equal(t, nil, err)
	bs, ok := s.(*BlockStoreImpl)
	assert.Equal(t, true, ok)
	init5Blocks(s, testChainID)
	lastConfigBlock, err := bs.GetLastConfigBlock()
	assert.Nil(t, err)
	assert.Equal(t, uint64(0), lastConfigBlock.Header.BlockHeight)
}

func TestBlockStoreImpl_GetLastChainConfig(t *testing.T) {
	var factory = getFactory()
	sqldbConfig := getSqliteConfig()
	testChainID := "GetLastChainConfig"
	s, err := factory.NewStore(testChainID, sqldbConfig, &test.GoLogger{}, nil)
	assert.Equal(t, nil, err)
	bs, ok := s.(*BlockStoreImpl)
	assert.Equal(t, true, ok)
	init5Blocks(s, testChainID)
	chainConfig, err := bs.GetLastChainConfig()
	assert.Nil(t, err)
	assert.Equal(t, chainConfig.ChainId, "")
}

func TestBlockStoreImpl_GetTxHeight(t *testing.T) {
	var factory = getFactory()
	lvldbConfig := getlvldbConfig("")
	lvldbConfig.DisableHistoryDB = true
	testChainID := "GetTxHeight"
	s, err := factory.NewStore(testChainID, lvldbConfig, &test.GoLogger{}, nil)
	assert.Equal(t, nil, err)
	bs, ok := s.(*BlockStoreImpl)
	assert.Equal(t, true, ok)
	//init5Blocks(s,testChainID)
	genesis := &storePb.BlockWithRWSet{Block: createConfigBlock(testChainID, 0), TxRWSets: getTxRWSets()}
	err = s.InitGenesis(genesis)
	assert.Nil(t, err)
	txRWSets := getTxRWSets()
	block1 := createBlock(testChainID, 1, 1)
	txRWSets[0].TxId = block1.Txs[0].Payload.TxId
	err = s.PutBlock(block1, txRWSets)
	assert.Nil(t, err)
	height, err := bs.GetTxHeight(txRWSets[0].TxId)
	assert.Nil(t, err)
	assert.Equal(t, uint64(1), height)
}

// Test_blockchainStoreImpl_Snapshot_HotColdDataSeparate_with_sqlkv test make snapshot and hot cold data separate
// common write pattern for WriteBlockType
// provider:sqlkv ,db: mysql
func Test_blockchainStoreImpl_Snapshot_HotColdDataSeparate_with_sqlkv(t *testing.T) {
	initRedisHosts()
	initMysqlDocker()

	defer destroyRedisHosts()
	defer destroyMysqlDocker()

	funcName := HAS_TX
	testChainID := "HasTxBigFilter"
	tests := []struct {
		name  string
		block *commonPb.Block
	}{
		{funcName, createBigBlock(testChainID, 1, 1)},
		{funcName, createBigBlock(testChainID, 2, 1)},
		{funcName, createBigBlock(testChainID, 3, 1)},
		{funcName, createBigBlock(testChainID, 4, 1)},
		{funcName, createBigBlock(testChainID, 999999, 1)},
	}
	var factory = getFactory()

	sConfig, tmpTestDir := getConfigWithBigFilterWithMySQLWithSqlKV()
	defer destroyFileSystem(tmpTestDir)

	s, err := factory.NewStore(testChainID, sConfig, &test.GoLogger{}, nil)
	if err != nil {
		panic(err)
	}
	defer func(s protocol.BlockchainStore) {

		s.Close()
	}(s)

	// 1. write genesis block , block 0
	genesis := &storePb.BlockWithRWSet{Block: createConfigBlock(testChainID, 0), TxRWSets: getTxRWSets()}
	//fmt.Println("-------------------init5BigBlocks,InitGenesis")
	err = s.InitGenesis(genesis)
	if err != nil {
		panic(err)
	}

	// 2. make snapshot for height 3
	if err = s.MakeSnapshot(3); err != nil {
		fmt.Printf("make snapshot failed:,errInfo:[%s]", err)
		panic(err)
	}

	// 3. write 4 blocks, block2,block3,block4,block5
	init4BigBlocks(s, testChainID)

	// 4. get snapshot status
	snapshotStatus := s.GetSnapshotStatus()
	assert.Equal(t, uint64(1), snapshotStatus)

	// 5. do hot cold data separate for block 3,
	// block 3 is saved in 00000000000000000001.fdb,
	// and the current  segment of bf is 00000000000000000006.fdb.END
	// so we can do a job of separate
	separationJobID, err := s.DoHotColdDataSeparation(2, 3)
	if err != nil {
		fmt.Printf("do hot cold separate error ,errInfo:[%s]", err)
		panic(err)
	}
	separaeJobInfo, err := s.GetHotColdDataSeparationJobByID(separationJobID)
	if err != nil {
		panic(err)
	}
	// get job info until job is finished or time out
	for i := 0; i < 100; i++ {
		separaeJobInfo, _ = s.GetHotColdDataSeparationJobByID(separationJobID)
		time.Sleep(1 * time.Second)
		if separaeJobInfo.Status == 4 {
			break
		}
	}
	// if the job's status is 4,it means the job already finished and successful
	assert.Equal(t, int32(4), separaeJobInfo.Status)

	// 6.test import snapshot

	//
	exist, err := s.TxExists(tests[0].block.Txs[0].Payload.TxId)
	assert.Equal(t, nil, err)
	assert.Equal(t, true, exist)
	exist, err = s.TxExists(tests[1].block.Txs[0].Payload.TxId)
	assert.Equal(t, nil, err)
	assert.Equal(t, true, exist)
	exist, err = s.TxExists(tests[2].block.Txs[0].Payload.TxId)
	assert.Equal(t, true, exist)
	assert.Equal(t, nil, err)
	exist, err = s.TxExists(tests[3].block.Txs[0].Payload.TxId)
	assert.Equal(t, true, exist)
	assert.Equal(t, nil, err)
	exist, err = s.TxExists(tests[4].block.Txs[0].Payload.TxId)
	assert.Equal(t, nil, err)
	assert.Equal(t, false, exist)
}

// Test_blockchainStoreImpl_Snapshot_HotColdDataSeparate_with_tikvdb test make snapshot and hot cold data separate
// quick write pattern for WriteBlockType
// provider:sqlkv  ,tikvdb,  dbs:  mysql,tikv
func Test_blockchainStoreImpl_Snapshot_HotColdDataSeparate_with_tikvdb(t *testing.T) {
	initRedisHosts()
	initMysqlDocker()
	initTikvHosts()

	defer destroyRedisHosts()
	defer destroyMysqlDocker()
	defer destroyTikvHosts()

	funcName := HAS_TX
	testChainID := "HasTxBigFilter2"
	tests := []struct {
		name  string
		block *commonPb.Block
	}{
		{funcName, createBigBlock(testChainID, 1, 1)},
		{funcName, createBigBlock(testChainID, 2, 1)},
		{funcName, createBigBlock(testChainID, 3, 1)},
		{funcName, createBigBlock(testChainID, 4, 1)},
		{funcName, createBigBlock(testChainID, 999999, 1)},
	}
	var factory = getFactory()

	//sConfig := getConfigWithBigFilterWithMySQLWithSqlKV()

	//blockdb ,statedb is tikv
	sConfig, tmpTestDir := getConfigWithBigFilterWithTikvWithSqlKV()

	defer destroyFileSystem(tmpTestDir)

	// set quick write
	sConfig.WriteBlockType = 1

	s, err := factory.NewStore(testChainID, sConfig, &test.GoLogger{}, nil)
	if err != nil {
		panic(err)
	}
	defer func(s protocol.BlockchainStore) {

		s.Close()
	}(s)

	// 1. write genesis block , block 0
	genesis := &storePb.BlockWithRWSet{Block: createConfigBlock(testChainID, 0), TxRWSets: getTxRWSets()}
	//fmt.Println("-------------------init5BigBlocks,InitGenesis")
	err = s.InitGenesis(genesis)
	if err != nil {
		panic(err)
	}

	// 2. make snapshot for height 3
	if err = s.MakeSnapshot(3); err != nil {
		fmt.Printf("make snapshot failed:,errInfo:[%s]", err)
		panic(err)
	}

	// 3. write 4 blocks, block2,block3,block4,block5
	init4BigBlocks(s, testChainID)

	// 4. get snapshot status
	snapshotStatus := s.GetSnapshotStatus()
	assert.Equal(t, uint64(1), snapshotStatus)

	// 5. do hot cold data separate for block 3,
	// block 3 is saved in 00000000000000000001.fdb,
	// and the current  segment of bf is 00000000000000000006.fdb.END
	// so we can do a job of separate
	separationJobID, err := s.DoHotColdDataSeparation(2, 3)
	if err != nil {
		fmt.Printf("do hot cold separate error ,errInfo:[%s]", err)
		panic(err)
	}
	separaeJobInfo, err := s.GetHotColdDataSeparationJobByID(separationJobID)
	if err != nil {
		panic(err)
	}
	// get job info until job is finished or time out
	for i := 0; i < 100; i++ {
		separaeJobInfo, _ = s.GetHotColdDataSeparationJobByID(separationJobID)
		time.Sleep(1 * time.Second)
		if separaeJobInfo.Status == 4 {
			break
		}
	}
	// if the job's status is 4,it means the job already finished and successful
	assert.Equal(t, int32(4), separaeJobInfo.Status)

	// 6.test import snapshot

	//
	exist, err := s.TxExists(tests[0].block.Txs[0].Payload.TxId)
	assert.Equal(t, nil, err)
	assert.Equal(t, true, exist)
	exist, err = s.TxExists(tests[1].block.Txs[0].Payload.TxId)
	assert.Equal(t, nil, err)
	assert.Equal(t, true, exist)
	exist, err = s.TxExists(tests[2].block.Txs[0].Payload.TxId)
	assert.Equal(t, true, exist)
	assert.Equal(t, nil, err)
	exist, err = s.TxExists(tests[3].block.Txs[0].Payload.TxId)
	assert.Equal(t, true, exist)
	assert.Equal(t, nil, err)
	exist, err = s.TxExists(tests[4].block.Txs[0].Payload.TxId)
	assert.Equal(t, nil, err)
	assert.Equal(t, false, exist)
}

// Test_blockchainStoreImpl_Snapshot_HotColdDataSeparate_with_badgerdb test make snapshot and hot cold data separate
// quick write pattern for WriteBlockType
// provider:sqlkv  ,tikvdb, badgerdb  dbs:  mysql,tikv,badgerdb
// blockFile: disable mmap
func Test_blockchainStoreImpl_Snapshot_HotColdDataSeparate_with_badgerdb(t *testing.T) {
	initRedisHosts()
	initMysqlDocker()
	initTikvHosts()

	defer destroyRedisHosts()
	defer destroyMysqlDocker()
	defer destroyTikvHosts()

	funcName := HAS_TX
	testChainID := "HasTxBigFilter3"
	tests := []struct {
		name  string
		block *commonPb.Block
	}{
		{funcName, createBigBlock(testChainID, 1, 1)},
		{funcName, createBigBlock(testChainID, 2, 1)},
		{funcName, createBigBlock(testChainID, 3, 1)},
		{funcName, createBigBlock(testChainID, 4, 1)},
		{funcName, createBigBlock(testChainID, 999999, 1)},
	}
	var factory = getFactory()

	//sConfig := getConfigWithBigFilterWithMySQLWithSqlKV()

	//blockdb  is badgerdb,  statedb is tikv, disable mmap,  set quick write
	sConfig, tmpTestDir := getConfigWithBigFilterWithBadgerDBWithSqlKV()

	defer destroyFileSystem(tmpTestDir)

	// set quick write
	sConfig.WriteBlockType = 1

	s, err := factory.NewStore(testChainID, sConfig, &test.GoLogger{}, nil)
	if err != nil {
		panic(err)
	}
	defer func(s protocol.BlockchainStore) {

		s.Close()
	}(s)

	// 1. write genesis block , block 0
	genesis := &storePb.BlockWithRWSet{Block: createConfigBlock(testChainID, 0), TxRWSets: getTxRWSets()}
	//fmt.Println("-------------------init5BigBlocks,InitGenesis")
	err = s.InitGenesis(genesis)
	if err != nil {
		panic(err)
	}

	// 2. make snapshot for height 3
	if err = s.MakeSnapshot(3); err != nil {
		fmt.Printf("make snapshot failed:,errInfo:[%s]", err)
		panic(err)
	}

	// 3. write 4 blocks, block2,block3,block4,block5,...,block24
	init4BigBlocks(s, testChainID)

	// 4. get snapshot status
	snapshotStatus := s.GetSnapshotStatus()
	assert.Equal(t, uint64(1), snapshotStatus)

	// 5. do hot cold data separate for block 3,
	// block 3 is saved in 00000000000000000001.fdb,
	// and the current  segment of bf is 00000000000000000006.fdb.END
	// so we can do a job of separate
	separationJobID, err := s.DoHotColdDataSeparation(2, 3)
	if err != nil {
		fmt.Printf("do hot cold separate error ,errInfo:[%s]", err)
		panic(err)
	}
	separaeJobInfo, err := s.GetHotColdDataSeparationJobByID(separationJobID)
	if err != nil {
		panic(err)
	}
	// get job info until job is finished or time out
	for i := 0; i < 100; i++ {
		separaeJobInfo, _ = s.GetHotColdDataSeparationJobByID(separationJobID)
		time.Sleep(1 * time.Second)
		if separaeJobInfo.Status == 4 {
			break
		}
	}
	// if the job's status is 4,it means the job already finished and successful
	assert.Equal(t, int32(4), separaeJobInfo.Status)

	// 6.test read, write block25
	b, rw := createInitContractBlockAndRWSets(testChainID, 25)
	err = s.PutBlock(b, rw)
	if err != nil {
		panic(err)
	}

	readStoreImplTest1(t, testChainID, b, rw, s)

	exist, err := s.TxExists(tests[0].block.Txs[0].Payload.TxId)
	assert.Equal(t, nil, err)
	assert.Equal(t, true, exist)
	exist, err = s.TxExists(tests[1].block.Txs[0].Payload.TxId)
	assert.Equal(t, nil, err)
	assert.Equal(t, true, exist)
	exist, err = s.TxExists(tests[2].block.Txs[0].Payload.TxId)
	assert.Equal(t, true, exist)
	assert.Equal(t, nil, err)
	exist, err = s.TxExists(tests[3].block.Txs[0].Payload.TxId)
	assert.Equal(t, true, exist)
	assert.Equal(t, nil, err)
	exist, err = s.TxExists(tests[4].block.Txs[0].Payload.TxId)
	assert.Equal(t, nil, err)
	assert.Equal(t, false, exist)
}

func readStoreImplTest1(t *testing.T, testChainID string, b *commonPb.Block, rw []*commonPb.TxRWSet,
	s protocol.BlockchainStore) {
	// test GetHeightByHash
	getHeight, err := s.GetHeightByHash(b.Hash())
	if err != nil {
		panic(err)
	}
	assert.Equal(t, uint64(25), getHeight)

	// test GetTxWithInfo
	getTxinfo, err := s.GetTxWithInfo(b.Txs[0].Payload.TxId)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%#v", getTxinfo)
	assert.Equal(t, b.Txs[0].Payload.TxId, getTxinfo.Transaction.Payload.TxId)

	// test GetTxInfoOnly
	getTxinfo, err = s.GetTxInfoOnly(b.Txs[0].Payload.TxId)
	if err != nil {
		panic(err)
	}
	assert.Equal(t, b.Header.BlockTimestamp, getTxinfo.BlockTimestamp)

	// test GetTxConfirmedTime
	getTime, err := s.GetTxConfirmedTime(b.Txs[0].Payload.TxId)
	if err != nil {
		panic(err)
	}
	assert.Equal(t, b.Header.BlockTimestamp, getTime)

	// test TxExistsInFullDB
	isExist, currHeight, err := s.TxExistsInFullDB(b.Txs[0].Payload.TxId)
	if err != nil {
		panic(err)
	}
	assert.Equal(t, true, isExist)
	assert.Equal(t, uint64(25), currHeight)

	// test TxExistsInIncrementDB
	isExist, err = s.TxExistsInIncrementDB(b.Txs[0].Payload.TxId, 25)
	if err != nil {
		panic(err)
	}
	assert.Equal(t, true, isExist)

	// test ReadObjects
	inputContractName := b.Txs[0].Payload.ContractName
	inputKeys := [][]byte{}
	expectValues := [][]byte{}

	for i := 0; i < len(rw[0].TxWrites); i++ {
		if rw[0].TxWrites[i].ContractName == inputContractName && rw[0].TxWrites[i].Key != nil {
			inputKeys = append(inputKeys, rw[0].TxWrites[i].Key)
			expectValues = append(expectValues, rw[0].TxWrites[i].Value)
		}

	}
	getValues, err := s.ReadObjects(inputContractName, inputKeys)
	if err != nil {
		panic(err)
	}
	assert.Equal(t, expectValues, getValues)

	// test GetAccountTxHistory
	accountId := b.Txs[0].GetSenderAccountId()
	historyIter, err := s.GetAccountTxHistory(accountId)
	defer historyIter.Release()
	if err != nil {
		panic(err)
	}

	var lastTxHistory *storePb.TxHistory
	expectLastTxHistory := &storePb.TxHistory{
		TxId:        "e1dd76fb41e3fffcbdda11b8592a54749ab40de114abf6489dcbc65d45398f0d",
		BlockHeight: 9,
		Timestamp:   0,
	}
	for historyIter.Next() {
		lastTxHistory, err = historyIter.Value()
		fmt.Printf("lastTxHistory =  [%#v]   \n", lastTxHistory)
	}
	assert.Equal(t, expectLastTxHistory, lastTxHistory)

	// test GetContractTxHistory
	inputContractName = b.Txs[0].Payload.ContractName
	historyIter, err = s.GetContractTxHistory(inputContractName)
	defer historyIter.Release()
	if err != nil {
		panic(err)
	}

	expectLastTxHistory = &storePb.TxHistory{
		TxId:        b.Txs[0].Payload.TxId,
		BlockHeight: b.Header.BlockHeight,
		Timestamp:   b.Header.BlockTimestamp,
	}
	for historyIter.Next() {
		lastTxHistory, err = historyIter.Value()
		fmt.Printf("lastTxHistory =  [%#v]  \n", lastTxHistory)
	}
	assert.Equal(t, expectLastTxHistory, lastTxHistory)

	readStoreImplTest2(t, testChainID, b, rw, s)

}

func readStoreImplTest2(t *testing.T, testChainID string, b *commonPb.Block, rw []*commonPb.TxRWSet,
	s protocol.BlockchainStore) {
	// test GetBlockWithRWSets
	getRWSet, err := s.GetBlockWithRWSets(b.Header.BlockHeight)
	if err != nil {
		panic(err)
	}
	assert.Equal(t, b, getRWSet.Block)
	assert.Equal(t, rw, getRWSet.TxRWSets)

	// test GetDBHandle
	getDBhandle := s.GetDBHandle(testChainID)
	assert.NotNil(t, getDBhandle)

	// test GetContractByName
	contractName := b.Txs[0].Payload.ContractName
	getContract, err := s.GetContractByName(contractName)
	if err != nil {
		panic(err)
	}
	assert.NotNil(t, getContract)

	// test GetContractBytecode
	//contractName := b.Txs[0].Payload.ContractName
	//byteContractName := rw[0].TxWrites[1].ContractName
	//fmt.Errorf("byteContractName:[%s]",byteContractName)
	getContractBytes, err := s.GetContractBytecode(userContractName)
	if err != nil {
		panic(err)
	}
	assert.NotNil(t, getContractBytes)

	/*
		// test GetMemberExtraData
		accessControlMember := b.Header.Proposer
		getAccessControlMember, err := s.GetMemberExtraData(accessControlMember)
		if err != nil {
			panic(err)
		}
		assert.NotNil(t, getAccessControlMember)

	*/

	// test recoverContractEventDB
	blockStoreImpl, ok := s.(*BlockStoreImpl)
	if !ok {
		panic("convert is error")
	}
	//err = blockStoreImpl.recoverContractEventDB(20, 25)
	err = blockStoreImpl.recoverContractEventDB(24, 25)
	if err != nil {
		panic(err)
	}

	// test recoverBigFilterDB
	blockStoreImpl, ok = s.(*BlockStoreImpl)
	if !ok {
		panic("convert is error")
	}
	//err = blockStoreImpl.recoverBigFilterDB(20, 25)
	err = blockStoreImpl.recoverBigFilterDB(24, 25)
	if err != nil {
		panic(err)
	}

	// test recoverAllDB
	blockStoreImpl, ok = s.(*BlockStoreImpl)
	if !ok {
		panic("convert is error")
	}
	err = blockStoreImpl.recoverAllDB(25, 2, 2, 2,
		2, 2, 2, 2, 2)
	if err != nil {
		panic(err)
	}

	// test calculateRecoverHeight
	blockStoreImpl, ok = s.(*BlockStoreImpl)
	if !ok {
		panic("convert is error")
	}
	getRecoverHeight := blockStoreImpl.calculateRecoverHeight(0, 0)
	assert.Equal(t, uint64(0), getRecoverHeight)

	// test getBlockMetaFromFile
	expectTxIDS := []string{}
	for i := 0; i < len(b.Txs); i++ {
		expectTxIDS = append(expectTxIDS, b.Txs[i].Payload.TxId)
	}
	getSerializedBlock, err := blockStoreImpl.getBlockMetaFromFile(25)
	if err != nil {
		panic(err)
	}
	assert.Equal(t, b.Header, getSerializedBlock.Header)
	assert.Equal(t, expectTxIDS, getSerializedBlock.TxIds)
	assert.Equal(t, b.AdditionalData, getSerializedBlock.AdditionalData)

	// test getRWSetFromFile
	getTxRWset, err := blockStoreImpl.getRWSetFromFile(b.Txs[0].Payload.TxId)
	if err != nil {
		panic(err)
	}
	assert.Equal(t, getTxRWset, rw[0])

}

func initMysqlDocker() error {
	//destroyMysqlDocker()

	cmdStart := exec.Command("/bin/bash", "./scripts/start_mysql_docker.sh")
	var stdout, stderr bytes.Buffer
	cmdStart.Stdout = &stdout
	cmdStart.Stderr = &stderr
	err := cmdStart.Run()
	if err != nil {
		fmt.Printf("start mysql error :[%s]\n", err)
		panic(err)
	}
	outStr, errStr := stdout.String(), stderr.String()
	fmt.Printf("cmdStart out:[%s]\n", outStr)
	if errStr != "" && !strings.Contains(errStr, "Warning") {
		fmt.Printf("cmdStart err:[%s]\n", errStr)
		panic(err)
	}

	time.Sleep(50 * time.Second)

	return nil
}

func destroyMysqlDocker() {
	cmd := exec.Command("/bin/bash", "./scripts/stop_mysql_docker.sh")
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()
	if err != nil {
		fmt.Printf("stop mysql error :[%s]", err)
	}
	//outStr, errStr := string(stdout.Bytes()), string(stderr.Bytes())
	outStr, errStr := stdout.String(), stderr.String()
	fmt.Printf("stop mysql out:[%s]\n", outStr)
	if errStr != "" {
		fmt.Printf("stop mysql err:[%s]\n", errStr)
	}
}

func initRedisHosts() {
	cmd := exec.Command("/bin/bash", "./scripts/start_redis.sh")
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()
	if err != nil {
		fmt.Println("start redis error :", err)
	}
	//outStr, errStr := string(stdout.Bytes()), string(stderr.Bytes())
	outStr, errStr := stdout.String(), stderr.String()
	fmt.Printf("out:%s\n", outStr)
	if errStr != "" {
		fmt.Printf("err:%s\n", errStr)
	}
}

func destroyRedisHosts() {
	cmd := exec.Command("/bin/bash", "./scripts/stop_redis.sh")
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()
	if err != nil {
		fmt.Println("stop redis error: ", err)
	}
	//outStr, errStr := string(stdout.Bytes()), string(stderr.Bytes())
	outStr, errStr := stdout.String(), stderr.String()
	fmt.Printf("out:%s\n", outStr)
	if errStr != "" {
		fmt.Printf("err:%s\n", errStr)
	}

}

//生成一个随机字符串，长度为 n 个字符长度
func RandStringBytesMaskImprSrcUnsafe(n int) string {
	src := rand.NewSource(time.Now().UnixNano())
	const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	const letterIdxBits = 6                    // 6 bits to represent a letter index
	const letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	const letterIdxMax = 63 / letterIdxBits    // # of letter indices fitting in 63 bits

	b := make([]byte, n)
	// A src.Int63() generates 63 random bits, enough for letterIdxMax characters!
	for i, cache, remain := n-1, src.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return *(*string)(unsafe.Pointer(&b))
}

func initTikvHosts() error {
	cmd := exec.Command("/bin/bash", "./scripts/start_tikv.sh")
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()
	if err != nil {
		fmt.Println("start tikv error :", err)
	}
	//outStr, errStr := string(stdout.Bytes()), string(stderr.Bytes())
	outStr, errStr := stdout.String(), stderr.String()
	fmt.Printf("out:%s\n", outStr)
	if errStr != "" {
		if !strings.Contains(errStr, "No such container") {
			return fmt.Errorf(errStr)
		}
	}
	if strings.Contains(outStr, "error") {
		return fmt.Errorf(errStr)
	}

	return nil
}

func destroyTikvHosts() {
	cmd := exec.Command("/bin/bash", "./scripts/stop_tikv.sh")
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()
	if err != nil {
		fmt.Println("stop tikv error: ", err)
	}
	//outStr, errStr := string(stdout.Bytes()), string(stderr.Bytes())
	outStr, errStr := stdout.String(), stderr.String()
	fmt.Printf("out:%s\n", outStr)
	if errStr != "" {
		fmt.Printf("err:%s\n", errStr)
	}
}

//删除文件系统
func destroyFileSystem(dirName string) {
	cmd := exec.Command("/bin/bash", "./scripts/rmdir.sh", dirName)
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()
	if err != nil {
		fmt.Printf("stop rmdir [%s] error:[%s] ", dirName, err)
	}
	//outStr, errStr := string(stdout.Bytes()), string(stderr.Bytes())
	outStr, errStr := stdout.String(), stderr.String()
	fmt.Printf("out:%s\n", outStr)
	if errStr != "" {
		fmt.Printf("destroy tmp dir :[%s] err:%s\n", dirName, errStr)
	}
	fmt.Printf("destroy tmp test dir success tmp_test_dir[%s],out[%s]\n", dirName, outStr)
}

// constructAcctTxHistKey construct account tx history key
// key format : a{accountId}#{blockHeight}#{txId}
// @Description:
// @param accountId
// @param blockHeight
// @param txId
// @return []byte
func constructAcctTxHistKey(accountId []byte, blockHeight uint64, txId string) []byte {
	key := fmt.Sprintf("a"+"%x"+"#"+"%d"+"#"+"%s", accountId, blockHeight, txId)
	return []byte(key)
}

// constructAcctTxHistKeyPrefix account tx history key prefix
// key prefix : a{accountId}#
// @Description:
// @param accountId
// @return []byte
func constructAcctTxHistKeyPrefix(accountId []byte) []byte {
	key := fmt.Sprintf("c"+"%x"+"#", accountId)
	return []byte(key)
}
