#!/bin/bash
count=`docker ps -a | grep mysql33670|grep -v grep | wc -l`;
if [ $count -gt 0 ];
then
  echo 'mysql docker mysql33670 exists,we will stop it and rm it,then we run a new docker container of mysql for unit testing';
  docker stop mysql33670
  docker rm   mysql33670
fi
# use mysql:5.7 replace with mysql:8.0
docker run -d -p 33670:3306 --name mysql33670 -e MYSQL_ROOT_PASSWORD=mysqlroot -d mysql:5.7  --max_connections=10000