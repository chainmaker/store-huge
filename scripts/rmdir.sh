#!/bin/bash
#delete tmp dir if it exists
testdir="$1"
rmdir(){
  if [ -d "$1" ]; then
    echo  "delete dir $1 "
    rm -rf "$1"
  else
    echo  "dir $1 not exist "
  fi
}
rmdir $testdir