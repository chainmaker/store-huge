//+build !windows

package blockfiledb

import (
	"encoding/binary"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"sync"
	"time"

	"chainmaker.org/chainmaker/store-huge/v3/meta/metadb"

	"chainmaker.org/chainmaker/store-huge/v3/meta"
	"github.com/shirou/gopsutil/v3/disk"

	lwsf "chainmaker.org/chainmaker/lws/file"
	storePb "chainmaker.org/chainmaker/pb-go/v3/store"
	"chainmaker.org/chainmaker/protocol/v3"
	"chainmaker.org/chainmaker/store-huge/v3/utils"
	"github.com/tidwall/tinylru"
)

var (
	// ErrCorrupt is returns when the log is corrupt.
	ErrCorrupt = errors.New("log corrupt")

	// ErrClosed is returned when an operation cannot be completed because
	// the log is closed.
	ErrClosed = errors.New("log closed")

	// ErrNotFound is returned when an entry is not found.
	ErrNotFound = errors.New("not found")

	// ErrOutOfOrder is returned from Write() when the index is not equal to
	// LastIndex()+1. It's required that log monotonically grows by one and has
	// no gaps. Thus, the series 10,11,12,13,14 is valid, but 10,11,13,14 is
	// not because there's a gap between 11 and 13. Also, 10,12,11,13 is not
	// valid because 12 and 11 are out of order.
	ErrOutOfOrder = errors.New("out of order")

	// ErrInvalidateIndex is returned when entry file is not legal
	ErrInvalidateIndex = errors.New("invalidate rfile index")

	// ErrBlockWrite is returned when write error
	ErrBlockWrite = errors.New("write block wfile size invalidate")
)

const (
	dbFileSuffix          = ".fdb"
	lastFileSuffix        = ".END"
	dbFileNameLen         = 20
	blockFilenameTemplate = "%020d"
	blockFilePath         = "bfdb"
)

// Options for BlockFile
type Options struct {
	// NoSync disables fsync after writes. This is less durable and puts the
	// log at risk of data loss when there's a server crash.
	NoSync bool
	// SegmentSize of each segment. This is just a target value, actual size
	// may differ. Default is 20 MB.
	SegmentSize int
	// SegmentCacheSize is the maximum number of segments that will be held in
	// memory for caching. Increasing this value may enhance performance for
	// concurrent read operations. Default is 1
	SegmentCacheSize int
	// NoCopy allows for the Read() operation to return the raw underlying data
	// slice. This is an optimization to help minimize allocations. When this
	// option is set, do not modify the returned data because it may affect
	// other Read calls. Default false
	NoCopy bool
	// UseMmap It is a method of memory-mapped rfile I/O. It implements demand
	// paging because rfile contents are not read from disk directly and initially
	// do not use physical RAM at all
	UseMmap bool
}

// DefaultOptions for Open().
var DefaultOptions = &Options{
	NoSync:           false,    // Fsync after every write
	SegmentSize:      67108864, // 64 MB log segment files.
	SegmentCacheSize: 25,       // Number of cached in-memory segments
	NoCopy:           false,    // Make a new copy of data for every Read call.
	UseMmap:          true,     // use mmap for faster write block to file.
}

// BlockFile represents a block to rfile
type BlockFile struct {
	mu              sync.RWMutex
	path            string        // absolute path to log directory
	opts            Options       // log options
	closed          bool          // log is closed
	corrupt         bool          // log may be corrupt
	lastSegment     *segment      // last log segment
	lastIndex       uint64        // index of the last entry in log
	sfile           *lockableFile // tail segment rfile handle
	wbatch          Batch         // reusable write batch
	logger          protocol.Logger
	bclock          time.Time
	cachedBuf       []byte
	openedFileCache tinylru.LRU // openedFile entries cache
	meta            meta.MetaData
}

type lockableFile struct {
	sync.RWMutex
	//blkWriter BlockWriter
	wfile lwsf.WalFile
	rfile *os.File
}

// segment represents a single segment rfile.
type segment struct {
	path  string // path of segment rfile
	name  string // name of segment rfile
	index uint64 // first index of segment
	ebuf  []byte // cached entries buffer, storage format of one log entry: checksum|data_size|data
	epos  []bpos // cached entries positions in buffer
}

type bpos struct {
	pos       int // byte position
	end       int // one byte past pos
	prefixLen int
}

// Open a new write ahead log
//  @Description:
//  @param height
//  @param opts
//  @param logger
//  @param meta
//  @return *BlockFile
//  @return error
//func Open(path string, opts *Options, logger protocol.Logger) (*BlockFile, error) {
func Open(path string, opts *Options, logger protocol.Logger, meta meta.MetaData) (*BlockFile, error) {
	logger.Debugf("Open,path:[%s]", path)
	if opts == nil {
		opts = DefaultOptions
	}
	if opts.SegmentCacheSize <= 0 {
		opts.SegmentCacheSize = DefaultOptions.SegmentCacheSize
	}
	if opts.SegmentSize <= 0 {
		opts.SegmentSize = DefaultOptions.SegmentSize
	}
	//var err error
	//从meta 中取 最后写入的文件，所在的文件系统
	//然后 计算该文件系统是否 磁盘可用，可用则直接用这个文件系统
	//如果不够用，则再切换生成新文件时，重新获取文件系统
	_, info, b, err := meta.GetLastFileInfo()
	if err != nil {
		return nil, err
	}
	//存在，用最后写入的文件，所在文件系统作为 path
	if b {
		path = info.OnlineFileSystem
		path = filepath.Join(path, meta.GetChainID(), blockFilePath)
	} else { //不存在，说明是第一次启动
		var fs storePb.MetaFileSystem
		fs, err = meta.GetMetaFS()
		if err != nil {
			return nil, err
		}
		//用第一个文件系统作为 path
		path = fs.OnlineFileSystem[0]
		path = filepath.Join(path, meta.GetChainID(), blockFilePath)
	}

	/*
		if path, err = filepath.Abs(path); err != nil {
			return nil, err
		}
	*/
	//fmt.Println("filepath.Abs,path =", path)

	l := &BlockFile{
		path:      path,
		opts:      *opts,
		logger:    logger,
		cachedBuf: make([]byte, 0, int(float32(opts.SegmentSize)*float32(1.5))),
		meta:      meta,
	}

	l.openedFileCache.Resize(l.opts.SegmentCacheSize)
	if err = os.MkdirAll(path, 0777); err != nil {
		return nil, err
	}
	if err = l.load(); err != nil {
		return nil, err
	}
	return l, nil
}

// ImportOpen , create block file by snapshot data
//  @Description:
//  @param opts
//  @param logger
//  @param meta
//  @param height
//  @return *BlockFile
//  @return error
func ImportOpen(opts *Options, logger protocol.Logger, meta meta.MetaData, height uint64) (*BlockFile, error) {
	//logger.Debugf("Open,path:[%s]", path)
	var path string
	if opts == nil {
		opts = DefaultOptions
	}
	if opts.SegmentCacheSize <= 0 {
		opts.SegmentCacheSize = DefaultOptions.SegmentCacheSize
	}
	if opts.SegmentSize <= 0 {
		opts.SegmentSize = DefaultOptions.SegmentSize
	}
	var err error

	//第一次启动
	var fs storePb.MetaFileSystem
	fs, err = meta.GetMetaFS()
	if err != nil {
		return nil, err
	}
	//用第一个文件系统作为 path
	path = fs.OnlineFileSystem[0]
	path = filepath.Join(path, meta.GetChainID(), blockFilePath)

	l := &BlockFile{
		path:      path,
		opts:      *opts,
		logger:    logger,
		cachedBuf: make([]byte, 0, int(float32(opts.SegmentSize)*float32(1.5))),
		meta:      meta,
	}

	l.openedFileCache.Resize(l.opts.SegmentCacheSize)
	if err = os.MkdirAll(path, 0777); err != nil {
		return nil, err
	}
	if err = l.importLoad(height); err != nil {
		return nil, err
	}
	return l, nil
}

func (l *BlockFile) pushCache(path string, segFile *lockableFile) {
	if strings.HasSuffix(path, lastFileSuffix) {
		// has lastFileSuffix mean this is an writing rfile, only happened when add new block entry to rfile,
		// we do not use ofile in other place, since we read new added block entry from memory (l.lastSegment.ebuf),
		// so we should not cache this rfile object
		return
	}
	_, _, _, v, evicted :=
		l.openedFileCache.SetEvicted(path, segFile)
	if evicted {
		// nolint
		if v == nil {
			return
		}
		if lfile, ok := v.(*lockableFile); ok {
			lfile.Lock()
			_ = lfile.rfile.Close()
			lfile.Unlock()
		}
	}
}

// TODO: load只是打开最后一个segment，支持多文件系统，需要从这儿修改
// nolint load all the segments. This operation also cleans up any START/END segments.
func (l *BlockFile) load() error {
	var err error
	if err = l.loadFromPath(l.path); err != nil {
		return err
	}

	// for the first time to start,use first onlineFS
	if l.lastSegment == nil {
		// Create a new log
		segName := l.segmentName(1)
		//fmt.Println("segName =", segName)
		//上报meta ,创建新文件
		metaFS, err := l.meta.GetMetaFS()
		if err != nil {
			return err
		}
		nextFS := metaFS.OnlineFileSystem[0]
		err = l.meta.CreateNewFile(segName, nextFS, 1)
		if err != nil {
			return err
		}

		l.lastSegment = &segment{
			name:  segName,
			index: 1,
			path:  l.segmentPathWithENDSuffix(segName),
			ebuf:  l.cachedBuf[:0],
		}
		//fmt.Println("l.lastSegment = ", *l.lastSegment)
		l.lastIndex = 0
		//创建或打开文件
		l.sfile, err = l.openWriteFile(l.lastSegment.path)
		if err != nil {
			return err
		}
		return err
	}

	// Open the last segment for appending
	// 如果meta中有元数据信息，但是 bfdb中，还没创建好这个文件，这时还会创建文件
	if l.sfile, err = l.openWriteFile(l.lastSegment.path); err != nil {
		return err
	}

	// Customize part start
	// Load the last segment, only load uncorrupted log entries
	if err = l.loadSegmentEntriesForRestarting(l.lastSegment); err != nil {
		return err
	}
	// Customize part end
	l.lastIndex = l.lastSegment.index + uint64(len(l.lastSegment.epos)) - 1
	return nil
}

// nolint importLoad all the segments. This operation also cleans up any START/END segments.
func (l *BlockFile) importLoad(height uint64) error {
	var err error

	startEntryIndex := height + 2

	// for the first time to start,use first onlineFS

	// 1.Create a new log
	segName := l.segmentName(startEntryIndex)
	//fmt.Println("segName =", segName)
	// 2.上报meta ,创建新文件
	metaFS, err := l.meta.GetMetaFS()
	if err != nil {
		return err
	}
	nextFS := metaFS.OnlineFileSystem[0]
	err = l.meta.CreateNewFile(segName, nextFS, startEntryIndex)
	if err != nil {
		return err
	}

	l.lastSegment = &segment{
		name:  segName,
		index: startEntryIndex,
		path:  l.segmentPathWithENDSuffix(segName),
		ebuf:  l.cachedBuf[:0],
	}
	//fmt.Println("l.lastSegment = ", *l.lastSegment)
	l.lastIndex = height + 1
	// 3.创建或打开文件
	l.sfile, err = l.openWriteFile(l.lastSegment.path)
	if err != nil {
		return err
	}
	return err

}

func (l *BlockFile) loadFromPath(path string) error {
	//直接调用 meta ，获得最后一个写入的文件信息
	fileName, _, b, err2 := l.meta.GetLastFileInfo()
	if err2 != nil {
		return err2
	}
	if !b {
		return nil
	}
	var index uint64

	segName := fileName
	index, err := strconv.ParseUint(fileName[1:dbFileNameLen], 10, 64)
	if err != nil {
		return err
	}
	l.lastSegment = &segment{
		name:  segName,
		index: index,
		path:  l.segmentPathWithENDSuffix(segName),
		ebuf:  l.cachedBuf[:0],
	}
	return nil
}

func (l *BlockFile) openReadFile(path string) (*lockableFile, error) {
	// Open the appropriate rfile as read-only.
	var (
		err     error
		isExist bool
		rfile   *os.File
		ofile   *lockableFile
	)

	fileV, isOK := l.openedFileCache.Get(path)
	l.logger.Debugf("openFile ,path=[%s]", path)
	if isOK && fileV != nil {
		if isExist, _ = utils.PathExists(path); isExist {
			ofil, ok := fileV.(*lockableFile)
			if ok {
				l.pushCache(path, ofil)
				return ofil, nil
			}
		}
	}

	if isExist, err = utils.PathExists(path); err != nil {
		return nil, err
	} else if !isExist {
		return nil, fmt.Errorf("bfdb rfile:%s missed", path)
	}

	rfile, err = os.OpenFile(path, os.O_RDONLY, 0666)
	if err != nil {
		return nil, err
	}
	if _, err = rfile.Seek(0, 2); err != nil {
		return nil, err
	}

	ofile = &lockableFile{rfile: rfile}
	l.pushCache(path, ofile)
	return ofile, nil
}

func (l *BlockFile) openWriteFile(path string) (*lockableFile, error) {
	// Open the appropriate rfile as read-only.
	var (
		err     error
		isExist bool
		wfile   lwsf.WalFile
		ofile   *lockableFile
	)
	if isExist, err = utils.PathExists(path); err != nil {
		return nil, err
	}

	if !isExist && !strings.HasSuffix(path, lastFileSuffix) {
		return nil, fmt.Errorf("bfdb wfile:%s missed", path)
	}

	if l.opts.UseMmap {
		if wfile, err = lwsf.NewMmapFile(path, l.opts.SegmentSize); err != nil {
			return nil, err
		}
	} else {
		if wfile, err = lwsf.NewFile(path); err != nil {
			return nil, err
		}
	}
	ofile = &lockableFile{wfile: wfile}
	return ofile, nil
}

// segmentName returns a 20-byte textual representation of an index
// for lexical ordering. This is used for the rfile names of log segments.
func (l *BlockFile) segmentName(index uint64) string {
	return fmt.Sprintf(blockFilenameTemplate, index)
}

func (l *BlockFile) segmentPath(name string) string {
	return fmt.Sprintf("%s%s", filepath.Join(l.path, name), dbFileSuffix)
}

func (l *BlockFile) segmentPathWithENDSuffix(name string) string {
	if strings.HasSuffix(name, lastFileSuffix) {
		return name
	}

	return fmt.Sprintf("%s%s", l.segmentPath(name), lastFileSuffix)
}

// Close the log.
func (l *BlockFile) Close() error {
	l.mu.Lock()
	defer l.mu.Unlock()
	if l.closed {
		if l.corrupt {
			return ErrCorrupt
		}
		return ErrClosed
	}
	if err := l.sfile.wfile.Sync(); err != nil {
		return err
	}
	if err := l.sfile.wfile.Close(); err != nil {
		return err
	}
	l.closed = true
	if l.corrupt {
		return ErrCorrupt
	}

	l.openedFileCache.Resize(l.opts.SegmentCacheSize)
	return nil
}

// Write an entry to the block rfile db.
func (l *BlockFile) Write(index uint64, data []byte) (fileName string, offset, blkLen uint64, err error) {
	l.mu.Lock()
	defer l.mu.Unlock()
	l.bclock = time.Now()
	if l.corrupt {
		return "", 0, 0, ErrCorrupt
	} else if l.closed {
		return "", 0, 0, ErrClosed
	}
	l.wbatch.Write(index, data)
	bwi, err := l.writeBatch(&l.wbatch)
	if err != nil {
		return "", 0, 0, err
	}
	//fmt.Println("Write,fileName=", bwi.FileName)
	return bwi.FileName, bwi.Offset, bwi.ByteLen, nil
}

//func (l *BlockFile) appendEntry(dst []byte, index uint64, data []byte) (out []byte,
//	epos bpos) {
//	return appendBinaryEntry(dst, data)
//}

// Cycle the old segment for a new segment.
func (l *BlockFile) cycle() error {
	if l.opts.NoSync {
		if err := l.sfile.wfile.Sync(); err != nil {
			return err
		}
	}
	if err := l.sfile.wfile.Close(); err != nil {
		return err
	}

	// remove pre last segment name's  end suffix
	orgPath := l.lastSegment.path
	if !strings.HasSuffix(orgPath, lastFileSuffix) {
		return fmt.Errorf("last segment rfile dot not end with %s", lastFileSuffix)
	}
	finalPath := orgPath[:len(orgPath)-len(lastFileSuffix)]
	// TODO: 这里可能有bug，如果这里rename成功，但是磁盘已经写满了，要生成新的segment，这时会panic，重启之后最后写入的文件，没有.END后缀名
	if err := os.Rename(orgPath, finalPath); err != nil {
		return err
	}
	// cache the previous lockfile
	//l.pushCache(finalPath, l.sfile)

	// TODO 切换文件系统，在这儿修改
	// 1.先确定是否要切换文件系统，获得可用的文件系统
	// 2.再更新 l.path 为新的绝对路径
	// 3.新文件名 先上报 meta
	// TODO power down issue
	//先判断当前文件系统是否空间够用，够用则直接用，不够用则meta中获取当前文件系统的下一个文件系统来用
	_, lastFSInfo, _, err2 := l.meta.GetLastFileInfo()
	if err2 != nil {
		return err2
	}
	fs, err2 := l.meta.GetMetaFS()
	if err2 != nil {
		return err2
	}

	var nextPath, lastPath, nextFS string
	nextPath = l.path
	lastPath = filepath.Join(lastFSInfo.OnlineFileSystem, l.meta.GetChainID(), blockFilePath)
	nextFS = lastFSInfo.OnlineFileSystem

	fsUsageInfo, err3 := disk.Usage(nextFS)
	if err3 != nil {
		return err3
	}

	currPath := filepath.Join(fs.OnlineFileSystem[len(fs.OnlineFileSystem)-1], l.meta.GetChainID(), blockFilePath)
	//currentFS  is last FS, disk full
	if currPath == l.path && fsUsageInfo.Free-metadb.ReserveSpaceSize <= uint64(l.opts.SegmentSize) {
		l.logger.Errorf("error,online FS disk full")
		panic(metadb.ErrOnlineFSSpaceNotEnough)
	}
	//当前文件系统 剩余空间不够用，换下一个(每个文件系统保留256MB空间,因为bf一个segment可能会大于segmentSize)
	if fsUsageInfo.Free-metadb.ReserveSpaceSize <= uint64(l.opts.SegmentSize) {
		for i := 0; i < len(fs.OnlineFileSystem); i++ {
			currPath := filepath.Join(fs.OnlineFileSystem[i], l.meta.GetChainID(), blockFilePath)
			//if lastPath == l.path && i+1 < len(fs.OnlineFileSystem) {
			if lastPath == currPath && i+1 < len(fs.OnlineFileSystem) {
				nextFS = fs.OnlineFileSystem[i+1]
				nextPath = filepath.Join(nextFS, l.meta.GetChainID(), blockFilePath)
				break
			}
		}
	}

	l.path = nextPath

	segName := l.segmentName(l.lastIndex + 1)
	//上报meta ,上报旧文件的区块块高，
	err2 = l.meta.SetFileEndHeight(l.lastSegment.name, l.lastIndex)
	if err2 != nil {
		return err2
	}
	//上报meta ,创建新文件
	err2 = l.meta.CreateNewFile(segName, nextFS, l.lastIndex+1)
	if err2 != nil {
		return err2
	}

	s := &segment{
		name:  segName,
		index: l.lastIndex + 1,
		path:  l.segmentPathWithENDSuffix(segName),
		ebuf:  l.cachedBuf[:0],
	}
	//fmt.Println("cycle(),segment =", *s)
	var err error
	if l.sfile, err = l.openWriteFile(s.path); err != nil {
		return err
	}
	l.lastSegment = s
	return nil
}

func (l *BlockFile) appendBinaryEntry(dst []byte, data []byte) (out []byte, epos bpos) {
	// checksum + data_size + data
	pos := len(dst)
	// Customize part start
	dst = appendChecksum(dst, NewCRC(data).Value())
	// Customize part end
	dst = appendUvarint(dst, uint64(len(data)))
	prefixLen := len(dst) - pos
	dst = append(dst, data...)
	return dst, bpos{pos, len(dst), prefixLen}
}

// Customize part start
func appendChecksum(dst []byte, checksum uint32) []byte {
	dst = append(dst, []byte("0000")...)
	binary.LittleEndian.PutUint32(dst[len(dst)-4:], checksum)
	return dst
}

// Customize part end

func appendUvarint(dst []byte, x uint64) []byte {
	var buf [10]byte
	n := binary.PutUvarint(buf[:], x)
	dst = append(dst, buf[:n]...)
	return dst
}

// Batch of entries. Used to write multiple entries at once using WriteBatch().
type Batch struct {
	entry batchEntry
	data  []byte
}

type batchEntry struct {
	index uint64
	size  int
}

// Write an entry to the batch
func (b *Batch) Write(index uint64, data []byte) {
	b.entry = batchEntry{index, len(data)}
	b.data = data
}

// WriteBatch writes the entries in the batch to the log in the order that they
// were added to the batch. The batch is cleared upon a successful return.
func (l *BlockFile) WriteBatch(b *Batch) (*storePb.StoreInfo, error) {
	l.mu.Lock()
	defer l.mu.Unlock()
	if l.corrupt {
		return nil, ErrCorrupt
	} else if l.closed {
		return nil, ErrClosed
	}
	if b.entry.size == 0 {
		return nil, nil
	}
	return l.writeBatch(b)
}

func (l *BlockFile) writeBatch(b *Batch) (*storePb.StoreInfo, error) {
	// check that indexes in batch are same
	if b.entry.index != l.lastIndex+uint64(1) {
		l.logger.Errorf(fmt.Sprintf("out of order, b.entry.index: %d and l.lastIndex+uint64(1): %d",
			b.entry.index, l.lastIndex+uint64(1)))
		if l.lastIndex == 0 {
			l.logger.Errorf("your block rfile db is damaged or not use this feature before, " +
				"please check your disable_block_file_db setting in chainmaker.yml")
		}
		return nil, ErrOutOfOrder
	}
	// load the tail segment
	s := l.lastSegment
	l.logger.Debugf("s.ebuf size:[%v],segmentsize:[%v]", len(s.ebuf), l.opts.SegmentSize)
	if len(s.ebuf) > l.opts.SegmentSize {
		// tail segment has reached capacity. Close it and create a new one.
		if err := l.cycle(); err != nil {
			return nil, err
		}
		s = l.lastSegment
	}

	var epos bpos
	s.ebuf, epos = l.appendBinaryEntry(s.ebuf, b.data)
	s.epos = append(s.epos, epos)

	startTime := time.Now()
	l.sfile.Lock()
	if _, err := l.sfile.wfile.WriteAt(s.ebuf[epos.pos:epos.end], int64(epos.pos)); err != nil {
		l.logger.Errorf("write rfile: %s in %d err: %v", s.path, s.index+uint64(len(s.epos)), err)
		return nil, err
	}
	l.lastIndex = b.entry.index
	l.sfile.Unlock()
	l.logger.Debugf("writeBatch block[%d] rfile.WriteAt time: %v", l.lastIndex, utils.ElapsedMillisSeconds(startTime))

	if !l.opts.NoSync {
		if err := l.sfile.wfile.Sync(); err != nil {
			return nil, err
		}
	}
	if epos.end-epos.pos != b.entry.size+epos.prefixLen {
		return nil, ErrBlockWrite
	}

	//fmt.Println("writeBatch,l.lastSegment.name=", l.lastSegment.name)
	//fmt.Println("writeBatch,l.lastSegment.name[:dbFileNameLen]=", l.lastSegment.name[:dbFileNameLen])
	//fmt.Println("writeBatch,FileName=", l.lastSegment.name[:dbFileNameLen])
	//fmt.Println("writeBatch,l.lastSegment =", *l.lastSegment)
	return &storePb.StoreInfo{
		FileName: l.lastSegment.name[:dbFileNameLen],
		Offset:   uint64(epos.pos + epos.prefixLen),
		ByteLen:  uint64(b.entry.size),
	}, nil
}

// LastIndex returns the index of the last entry in the log. Returns zero when
// log has no entries.
func (l *BlockFile) LastIndex() (index uint64, err error) {
	l.mu.RLock()
	defer l.mu.RUnlock()
	if l.corrupt {
		return 0, ErrCorrupt
	} else if l.closed {
		return 0, ErrClosed
	}
	if l.lastIndex == 0 {
		return 0, nil
	}
	return l.lastIndex, nil
}

// loadSegmentEntriesForRestarting loads ebuf and epos in the segment when restarting
func (l *BlockFile) loadSegmentEntriesForRestarting(s *segment) error {
	data, err := ioutil.ReadFile(s.path)
	if err != nil {
		return err
	}

	var (
		epos []bpos
		pos  int
	)
	ebuf := data
	for exidx := s.index; len(data) > 0; exidx++ {
		var n, prefixLen int
		n, prefixLen, err = loadNextBinaryEntry(data)
		// if there are corrupted log entries, the corrupted and subsequent data are discarded
		if err != nil {
			break
		}
		data = data[n:]
		epos = append(epos, bpos{pos, pos + n, prefixLen})
		pos += n
		// load uncorrupted data
		s.ebuf = ebuf[0:pos]
		s.epos = epos
	}

	return nil
}

func loadNextBinaryEntry(data []byte) (n, prefixLen int, err error) {
	// Customize part start
	// checksum + data_size + data
	// checksum read
	checksum := binary.LittleEndian.Uint32(data[:4])
	// binary read
	data = data[4:]
	// Customize part end
	size, n := binary.Uvarint(data)
	if n <= 0 {
		return 0, 0, ErrCorrupt
	}
	if uint64(len(data)-n) < size {
		return 0, 0, ErrCorrupt
	}
	// Customize part start
	// verify checksum
	if checksum != NewCRC(data[n:uint64(n)+size]).Value() {
		return 0, 0, ErrCorrupt
	}
	prefixLen = 4 + n
	return prefixLen + int(size), prefixLen, nil
	// Customize part end
}

// ReadLastSegSection read an entry from the log. Returns a byte slice containing the data entry.
// todo：这里BF支持了多文件系统，以及集成meta，这个函数需要修改，不然有bug
// todo: 应该根据 index也就是区块块高，查找该区块对应的metaFileInfo，找到这个区块对应的bf的文件名和文件系统
// todo: 以及这个区块是否被归档了。
// 如果要找的区块，正好，不在最近写入的文件系统比如 /tmp/online2 中，在 /tmp/online1中，有bug
// 如果要找的区块，正好被归档了，那么有bug，这时这个区块，对应的索引，没有写入db
// todo: 所以归档时，需要保证归档的区块，对应的索引被写入到了db中了，而异步写区块的buffer大小是channel大小10,所以要保证最后10个区块不能被归档
// 同时，要找到的这个区块，必须在最后写入的文件中，才行。
func (l *BlockFile) ReadLastSegSection(index uint64) (data []byte,
	fileName string, offset uint64, byteLen uint64, err error) {
	l.mu.RLock()
	defer l.mu.RUnlock()
	if l.corrupt {
		return nil, "", 0, 0, ErrCorrupt
	} else if l.closed {
		return nil, "", 0, 0, ErrClosed
	}

	s := l.lastSegment
	// 1. index is out of range
	if index == 0 || index > l.lastIndex {
		l.logger.Debugf("index:[%d],s.index:[%d],l.lastIndex:[%d]", index, s.index, l.lastIndex)
		return nil, "", 0, 0, ErrNotFound
	}
	// 2. index is less than starting index of the last segment
	// we need to find the segment, load it into tmp segment and get data from it
	if index == 0 || index < s.index || index > l.lastIndex {
		return l.readSectionByIndex(index)
	}

	// 3. read from last segment
	epos := s.epos[index-s.index]
	edata := s.ebuf[epos.pos:epos.end]

	// Customize part start
	// checksum read
	checksum := binary.LittleEndian.Uint32(edata[:4])
	// binary read
	edata = edata[4:]
	// Customize part end
	size, n := binary.Uvarint(edata)
	if n <= 0 {
		return nil, "", 0, 0, ErrCorrupt
	}
	if uint64(len(edata)-n) < size {
		return nil, "", 0, 0, ErrCorrupt
	}
	// Customize part start
	if checksum != NewCRC(edata[n:]).Value() {
		return nil, "", 0, 0, ErrCorrupt
	}
	// Customize part end
	if l.opts.NoCopy {
		data = edata[n : uint64(n)+size]
	} else {
		data = make([]byte, size)
		copy(data, edata[n:])
	}
	fileName = l.lastSegment.name[:dbFileNameLen]
	offset = uint64(epos.pos + epos.prefixLen)
	byteLen = uint64(len(data))
	return data, fileName, offset, byteLen, nil
}

// readSectionByIndex read an entry from the log. Returns a byte slice containing the data entry.
func (l *BlockFile) readSectionByIndex(index uint64) (data []byte,
	fileName string, offset uint64, byteLen uint64, err error) {
	var filePath string
	// 1. get metaFileInfo
	fileName, fileInfo, exist, err := l.meta.GetFileInfoByHeight(index)
	l.logger.Debugf("get fileName: %v", fileName)
	if (err != nil) || (!exist) {
		return nil, "", 0, 0, ErrNotFound
	}

	// 2. check it whether archived or not
	isArchive, err := l.meta.IsArchive(fileName)
	if err != nil {
		return nil, "", 0, 0, ErrNotFound
	}
	// 3. get file path
	chainID := l.meta.GetChainID()
	if isArchive {
		//filePath = path.Join(fileInfo.ArchiveFileSystem ,fileName)
		filePath = fmt.Sprintf("%s%s",
			filepath.Join(fileInfo.ArchiveFileSystem, chainID, blockFilePath, fileName), dbFileSuffix)
	} else {
		filePath = fmt.Sprintf("%s%s",
			filepath.Join(fileInfo.OnlineFileSystem, chainID, blockFilePath, fileName), dbFileSuffix)
	}

	// 4. read data into tmp segment
	segName := fileName
	segmengStartIndex, err := strconv.ParseUint(fileName[1:dbFileNameLen], 10, 64)
	l.logger.Debugf("parse uint,index:[%d]", segmengStartIndex)
	if err != nil {
		l.logger.Errorf("failed to parse segment name: %v, err: %v", fileName, err)
		return nil, "", 0, 0, ErrNotFound
	}
	tmpSegment := &segment{
		name:  segName,
		index: segmengStartIndex,
		path:  filePath,
		ebuf:  []byte{},
	}
	err = l.loadSegmentEntriesForRestarting(tmpSegment)
	if err != nil {
		l.logger.Errorf("failed to load segment name: %v, err: %v", fileName, err)
	}
	// 5. read data from tmp segment and return
	epos := tmpSegment.epos[index-tmpSegment.index]
	edata := tmpSegment.ebuf[epos.pos:epos.end]
	// Customize part start
	// checksum read
	checksum := binary.LittleEndian.Uint32(edata[:4])
	// binary read
	edata = edata[4:]
	// Customize part end
	size, n := binary.Uvarint(edata)
	if n <= 0 {
		return nil, "", 0, 0, ErrCorrupt
	}
	if uint64(len(edata)-n) < size {
		return nil, "", 0, 0, ErrCorrupt
	}
	// Customize part start
	if checksum != NewCRC(edata[n:]).Value() {
		return nil, "", 0, 0, ErrCorrupt

	}
	// Customize part end
	if l.opts.NoCopy {
		data = edata[n : uint64(n)+size]
	} else {
		data = make([]byte, size)
		copy(data, edata[n:])
	}
	offset = uint64(epos.pos + epos.prefixLen)
	byteLen = uint64(len(data))

	return data, fileName, offset, byteLen, nil

}

// ReadFileSection an entry from the log. Returns a byte slice containing the data entry.
// 如果文件未归档，直接读取文件数据
// 如果已归档，调用归档系统接口，读归档数据
func (l *BlockFile) ReadFileSection(fiIndex *storePb.StoreInfo) ([]byte, error) {
	l.mu.RLock()
	defer l.mu.RUnlock()

	if fiIndex == nil || len(fiIndex.FileName) != dbFileNameLen || fiIndex.ByteLen == 0 {
		l.logger.Warnf("invalidate rfile index: %s", FileIndexToString(fiIndex))
		return nil, ErrInvalidateIndex
	}

	//拼绝对路径,并加.fdb结尾
	path := l.segmentPath(fiIndex.FileName)
	//如果文件是wal当前的最新最后写入的文件,则文件名是以.END结尾
	if fiIndex.FileName == l.lastSegment.name[:dbFileNameLen] {
		path = l.segmentPathWithENDSuffix(fiIndex.FileName)
	}

	//调 meta 返回文件是否归档
	//metaFileName := fiIndex.FileName + dbFileSuffix
	metaFileName := fiIndex.FileName
	l.logger.Debugf("get file by filename from meta,fileName:[%s]", metaFileName)
	isArchived, err2 := l.meta.IsArchive(metaFileName)
	if err2 != nil {
		l.logger.Errorf("is archived,fileName:[%s],errInfo:[%s]", metaFileName, err2)
		return nil, err2
	}
	//如果已归档，调归档系统，读已归档文件的数据
	if isArchived {
		l.logger.Debugf("read archive")
		data := make([]byte, fiIndex.ByteLen)
		n, err1 := l.meta.ReadArchiveFileDataByOffset(fiIndex.FileName, data, int64(fiIndex.Offset))
		if err1 != nil {
			return nil, err1
		}
		if uint64(n) != fiIndex.ByteLen {
			errMsg := fmt.Sprintf("read block rfile size invalidate, wanted: %d, actual: %d", fiIndex.ByteLen, n)
			return nil, errors.New(errMsg)
		}
		return data, nil
	}

	//未归档，则读wal数据
	l.logger.Debugf("read unarchived")
	lfile, err := l.openReadFile(path)
	if err != nil {
		return nil, err
	}

	data := make([]byte, fiIndex.ByteLen)
	lfile.RLock()
	n, err1 := lfile.rfile.ReadAt(data, int64(fiIndex.Offset))
	lfile.RUnlock()
	if err1 != nil {
		return nil, err1
	}
	if uint64(n) != fiIndex.ByteLen {
		errMsg := fmt.Sprintf("read block rfile size invalidate, wanted: %d, actual: %d", fiIndex.ByteLen, n)
		return nil, errors.New(errMsg)
	}
	return data, nil
}

// ClearCache clears the segment cache
func (l *BlockFile) ClearCache() error {
	l.mu.Lock()
	defer l.mu.Unlock()
	if l.corrupt {
		return ErrCorrupt
	} else if l.closed {
		return ErrClosed
	}
	l.clearCache()
	return nil
}

func (l *BlockFile) clearCache() {
	l.openedFileCache.Range(func(_, v interface{}) bool {
		// nolint
		if v == nil {
			return true
		}
		if s, ok := v.(*lockableFile); ok {
			s.Lock()
			_ = s.rfile.Close()
			s.Unlock()
		}
		return true
	})
	l.openedFileCache = tinylru.LRU{}
	l.openedFileCache.Resize(l.opts.SegmentCacheSize)
}

// Sync performs an fsync on the log. This is not necessary when the
// NoSync option is set to false.
func (l *BlockFile) Sync() error {
	l.mu.Lock()
	defer l.mu.Unlock()
	if l.corrupt {
		return ErrCorrupt
	} else if l.closed {
		return ErrClosed
	}
	return l.sfile.wfile.Sync()
}

// TruncateFront truncate the block from the front
//  @Description:
//  @receiver l
//  @param index
//  @return error
func (l *BlockFile) TruncateFront(index uint64) error {
	return nil
}

// FileIndexToString format store info into a string
//  @Description:
//  @param fiIndex
//  @return string
func FileIndexToString(fiIndex *storePb.StoreInfo) string {
	if fiIndex == nil {
		return "rfile index is nil"
	}

	return fmt.Sprintf("fileIndex: fileName: %s, offset: %d, byteLen: %d",
		fiIndex.GetFileName(), fiIndex.GetOffset(), fiIndex.GetByteLen())
}
