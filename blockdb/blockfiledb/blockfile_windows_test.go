//+build windows

package blockfiledb

import (
	"errors"
	"strconv"

	"chainmaker.org/chainmaker/store-huge/v3/meta"

	//"github.com/stretchr/testify/assert"
	//"reflect"
	"testing"

	"chainmaker.org/chainmaker/common/v3/crypto"

	storePb "chainmaker.org/chainmaker/pb-go/v3/store"
	"chainmaker.org/chainmaker/protocol/v3"
	"chainmaker.org/chainmaker/protocol/v3/test"

	"fmt"
	"math/rand"
	"os"
	"path/filepath"
	"time"
	"unsafe"

	"chainmaker.org/chainmaker/store-huge/v3/conf"
	"chainmaker.org/chainmaker/store-huge/v3/meta/metadb"
	leveldbprovider "chainmaker.org/chainmaker/store-leveldb/v3"
	"github.com/mitchellh/mapstructure"
)

const (
	letterBytes   = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits

	sqlLite   = "sqlite"
	memoryStr = ":memory:"
)

var (
	src               = rand.NewSource(time.Now().UnixNano())
	globalConfig      = getSqliteConfig()
	globalTestChainID = "test-bfdb-global-test-chain-id"
	//testChainID       = "test-bfdb-chain-id"
	testDBHandler = getDBHandle(globalTestChainID, "test-metadb", "metadb", &test.GoLogger{}, nil)
	//metadbTest, _     = NewMetaDataDB(*globalConfig, globalTestChainID, testDBHandler, &test.GoLogger{})
	testLogger = &test.GoLogger{}

	//metaFSGTest = &storePb.MetaFileSystem{
	//	OnlineFileSystem:  []string{"/tmp/online1,/tmp/online2"},
	//	ArchiveFileSystem: []string{"/tmp/archive1,/tmp/archive2"},
	//}
	//bfdbFilesTest = map[string]storePb.MetaFileInfo{
	//	"000000000000000001001": {
	//		OnlineFileSystem:  "/tmp/online1",
	//		Status:            0,
	//		ArchiveFileSystem: "/tmp/archive1",
	//		StartHeight:       1001,
	//		EndHeight:         1100,
	//	},
	//	"00000000000000000001101": {
	//		OnlineFileSystem:  "/tmp/online1",
	//		Status:            0,
	//		ArchiveFileSystem: "/tmp/archive1",
	//		StartHeight:       1101,
	//		EndHeight:         1200,
	//	},
	//}
	//multVersionsTest = &storePb.MetaMultVersions{
	//	Version: []*storePb.MetaVersion{
	//		{
	//			MajorVersion: 1,
	//			MinorVersion: 1,
	//			UpdateTime:   0,
	//		},
	//		//&storePb.MetaVersion{
	//		//	MajorVersion:2,
	//		//	MinorVersion: 1,
	//		//	UpdateTime: 0,
	//		//},
	//	},
	//}
)

//
//import (
//	"encoding/binary"
//	"fmt"
//	"io/ioutil"
//	"math/rand"
//	"os"
//	"strings"
//	"sync/atomic"
//	"testing"
//)
//
//func dataStr(index uint64) string {
//	if index%2 == 0 {
//		return fmt.Sprintf("data-\"%d\"", index)
//	}
//	return fmt.Sprintf("data-'%d'", index)
//}
//
//// nolint
//func testLog(t *testing.T, opts *Options, N int) {
//	logPath := "testlog/" + strings.Join(strings.Split(t.Name(), "/")[1:], "/")
//	l, err := Open(logPath, opts)
//	if err != nil {
//		t.Fatal(err)
//	}
//	defer l.Close()
//
//	// ======================================normal==================================
//	// FirstIndex - should be zero
//	n, err := l.FirstIndex()
//	if err != nil {
//		t.Fatal(err)
//	}
//	if n != 0 {
//		t.Fatalf("expected %d, got %d", 0, n)
//	}
//
//	// LastIndex - should be zero
//	n, err = l.LastIndex()
//	if err != nil {
//		t.Fatal(err)
//	}
//	if n != 0 {
//		t.Fatalf("expected %d, got %d", 0, n)
//	}
//
//	var data []byte
//	for i := 1; i <= N; i++ {
//		// Write - try to append previous index, should fail
//		err = l.Write(uint64(i-1), nil)
//		if !strings.Contains(err.Error(), ErrOutOfOrder.Error()) {
//			t.Fatalf("expected %v, got %v", ErrOutOfOrder, err)
//		}
//		// Write - append next item
//		err = l.Write(uint64(i), []byte(dataStr(uint64(i))))
//		if err != nil {
//			t.Fatalf("expected %v, got %v", nil, err)
//		}
//		// Write - get next item
//		data, err = l.Read(uint64(i))
//		if err != nil {
//			t.Fatalf("expected %v, got %v", nil, err)
//		}
//		if string(data) != dataStr(uint64(i)) {
//			t.Fatalf("expected %s, got %s", dataStr(uint64(i)), data)
//		}
//	}
//
//	// Read -- should fail, not found
//	_, err = l.Read(0)
//	if err != ErrNotFound {
//		t.Fatalf("expected %v, got %v", ErrNotFound, err)
//	}
//	// Read -- read back all entries
//	for i := 1; i <= N; i++ {
//		data, err = l.Read(uint64(i))
//		if err != nil {
//			t.Fatalf("error while getting %d", i)
//		}
//		if string(data) != dataStr(uint64(i)) {
//			t.Fatalf("expected %s, got %s", dataStr(uint64(i)), data)
//		}
//	}
//	// Read -- read back first half entries
//	for i := 1; i <= N/2; i++ {
//		data, err = l.Read(uint64(i))
//		if err != nil {
//			t.Fatalf("error while getting %d", i)
//		}
//		if string(data) != dataStr(uint64(i)) {
//			t.Fatalf("expected %s, got %s", dataStr(uint64(i)), data)
//		}
//	}
//	// Read -- read second third entries
//	for i := N / 3; i <= N/3+N/3; i++ {
//		data, err = l.Read(uint64(i))
//		if err != nil {
//			t.Fatalf("error while getting %d", i)
//		}
//		if string(data) != dataStr(uint64(i)) {
//			t.Fatalf("expected %s, got %s", dataStr(uint64(i)), data)
//		}
//	}
//	// Read -- random access
//	for _, v := range rand.Perm(N) {
//		index := uint64(v + 1)
//		data, err = l.Read(index)
//		if err != nil {
//			t.Fatal(err)
//		}
//		if dataStr(index) != string(data) {
//			t.Fatalf("expected %v, got %v", dataStr(index), string(data))
//		}
//	}
//
//	// FirstIndex/LastIndex -- check valid first and last indexes
//	n, err = l.FirstIndex()
//	if err != nil {
//		t.Fatal(err)
//	}
//	if n != 1 {
//		t.Fatalf("expected %d, got %d", 1, n)
//	}
//	n, err = l.LastIndex()
//	if err != nil {
//		t.Fatal(err)
//	}
//	if n != uint64(N) {
//		t.Fatalf("expected %d, got %d", N, n)
//	}
//
//	// ======================================close==================================
//	// Close -- close the log
//	if err = l.Close(); err != nil {
//		t.Fatal(err)
//	}
//	// Write - try while closed
//	err = l.Write(1, nil)
//	if err != ErrClosed {
//		t.Fatalf("expected %v, got %v", ErrClosed, err)
//	}
//	// WriteBatch - try while closed
//	err = l.WriteBatch(nil)
//	if err != ErrClosed {
//		t.Fatalf("expected %v, got %v", ErrClosed, err)
//	}
//	// FirstIndex - try while closed
//	_, err = l.FirstIndex()
//	if err != ErrClosed {
//		t.Fatalf("expected %v, got %v", ErrClosed, err)
//	}
//	// LastIndex - try while closed
//	_, err = l.LastIndex()
//	if err != ErrClosed {
//		t.Fatalf("expected %v, got %v", ErrClosed, err)
//	}
//	// Get - try while closed
//	_, err = l.Read(0)
//	if err != ErrClosed {
//		t.Fatalf("expected %v, got %v", ErrClosed, err)
//	}
//	// TruncateFront - try while closed
//	err = l.TruncateFront(0)
//	if err != ErrClosed {
//		t.Fatalf("expected %v, got %v", ErrClosed, err)
//	}
//	// TruncateBack - try while closed
//	err = l.TruncateBack(0)
//	if err != ErrClosed {
//		t.Fatalf("expected %v, got %v", ErrClosed, err)
//	}
//
//	// ======================================restart==================================
//	// Open -- reopen log
//	l, err = Open(logPath, opts)
//	if err != nil {
//		t.Fatal(err)
//	}
//	defer l.Close()
//
//	// Read -- read back all entries
//	for i := 1; i <= N; i++ {
//		data, err = l.Read(uint64(i))
//		if err != nil {
//			t.Fatalf("error while getting %d, err=%s", i, err)
//		}
//		if string(data) != dataStr(uint64(i)) {
//			t.Fatalf("expected %s, got %s", dataStr(uint64(i)), data)
//		}
//	}
//	// FirstIndex/LastIndex -- check valid first and last indexes
//	n, err = l.FirstIndex()
//	if err != nil {
//		t.Fatal(err)
//	}
//	if n != 1 {
//		t.Fatalf("expected %d, got %d", 1, n)
//	}
//	n, err = l.LastIndex()
//	if err != nil {
//		t.Fatal(err)
//	}
//	if n != uint64(N) {
//		t.Fatalf("expected %d, got %d", N, n)
//	}
//	// Write -- add 50 more items
//	for i := N + 1; i <= N+50; i++ {
//		index := uint64(i)
//		if err = l.Write(index, []byte(dataStr(index))); err != nil {
//			t.Fatal(err)
//		}
//		data, err = l.Read(index)
//		if err != nil {
//			t.Fatal(err)
//		}
//		if string(data) != dataStr(index) {
//			t.Fatalf("expected %v, got %v", dataStr(index), string(data))
//		}
//	}
//	N += 50
//	// FirstIndex/LastIndex -- check valid first and last indexes
//	n, err = l.FirstIndex()
//	if err != nil {
//		t.Fatal(err)
//	}
//	if n != 1 {
//		t.Fatalf("expected %d, got %d", 1, n)
//	}
//	n, err = l.LastIndex()
//	if err != nil {
//		t.Fatal(err)
//	}
//	if n != uint64(N) {
//		t.Fatalf("expected %d, got %d", N, n)
//	}
//	// Batch -- test batch writes
//	b := new(Batch)
//	b.Write(1, nil)
//	b.Write(2, nil)
//	b.Write(3, nil)
//	// WriteBatch -- should fail out of order
//	err = l.WriteBatch(b)
//	if !strings.Contains(err.Error(), ErrOutOfOrder.Error()) {
//		t.Fatalf("expected %v, got %v", ErrOutOfOrder, nil)
//	}
//	// Clear -- clear the batch
//	b.Clear()
//	// WriteBatch -- should succeed
//	err = l.WriteBatch(b)
//	if err != nil {
//		t.Fatal(err)
//	}
//	// Write 100 entries in batches of 10
//	for i := 0; i < 10; i++ {
//		for i := N + 1; i <= N+10; i++ {
//			index := uint64(i)
//			b.Write(index, []byte(dataStr(index)))
//		}
//		err = l.WriteBatch(b)
//		if err != nil {
//			t.Fatal(err)
//		}
//		N += 10
//	}
//	// Read -- read back all entries
//	for i := 1; i <= N; i++ {
//		data, err = l.Read(uint64(i))
//		if err != nil {
//			t.Fatalf("error while getting %d", i)
//		}
//		if string(data) != dataStr(uint64(i)) {
//			t.Fatalf("expected %s, got %s", dataStr(uint64(i)), data)
//		}
//	}
//
//	// Write -- one entry, so the buffer might be activated
//	err = l.Write(uint64(N+1), []byte(dataStr(uint64(N+1))))
//	if err != nil {
//		t.Fatal(err)
//	}
//	N++
//	// Read -- one random read, so there is an opened reader
//	data, err = l.Read(uint64(N / 2))
//	if err != nil {
//		t.Fatal(err)
//	}
//	if string(data) != dataStr(uint64(N/2)) {
//		t.Fatalf("expected %v, got %v", dataStr(uint64(N/2)), string(data))
//	}
//
//	// TruncateFront -- should fail, out of range
//	for _, i := range []int{0, N + 1} {
//		index := uint64(i)
//		if err = l.TruncateFront(index); err != ErrOutOfRange {
//			t.Fatalf("expected %v, got %v", ErrOutOfRange, err)
//		}
//		testFirstLast(t, l, uint64(1), uint64(N), nil)
//	}
//
//	// TruncateBack -- should fail, out of range
//	err = l.TruncateFront(0)
//	if err != ErrOutOfRange {
//		t.Fatalf("expected %v, got %v", ErrOutOfRange, err)
//	}
//	testFirstLast(t, l, uint64(1), uint64(N), nil)
//
//	// TruncateFront -- Remove no entries
//	if err = l.TruncateFront(1); err != nil {
//		t.Fatal(err)
//	}
//	testFirstLast(t, l, uint64(1), uint64(N), nil)
//
//	// TruncateFront -- Remove first 80 entries
//	if err = l.TruncateFront(81); err != nil {
//		t.Fatal(err)
//	}
//	testFirstLast(t, l, uint64(81), uint64(N), nil)
//
//	// Write -- one entry, so the buffer might be activated
//	err = l.Write(uint64(N+1), []byte(dataStr(uint64(N+1))))
//	if err != nil {
//		t.Fatal(err)
//	}
//	N++
//	testFirstLast(t, l, uint64(81), uint64(N), nil)
//
//	// Read -- one random read, so there is an opened reader
//	data, err = l.Read(uint64(N / 2))
//	if err != nil {
//		t.Fatal(err)
//	}
//	if string(data) != dataStr(uint64(N/2)) {
//		t.Fatalf("expected %v, got %v", dataStr(uint64(N/2)), string(data))
//	}
//
//	// TruncateBack -- should fail, out of range
//	for _, i := range []int{0, 80} {
//		index := uint64(i)
//		if err = l.TruncateBack(index); err != ErrOutOfRange {
//			t.Fatalf("expected %v, got %v", ErrOutOfRange, err)
//		}
//		testFirstLast(t, l, uint64(81), uint64(N), nil)
//	}
//
//	// TruncateBack -- Remove no entries
//	if err = l.TruncateBack(uint64(N)); err != nil {
//		t.Fatal(err)
//	}
//	testFirstLast(t, l, uint64(81), uint64(N), nil)
//	// TruncateBack -- Remove last 80 entries
//	if err = l.TruncateBack(uint64(N - 80)); err != nil {
//		t.Fatal(err)
//	}
//	N -= 80
//	testFirstLast(t, l, uint64(81), uint64(N), nil)
//
//	// Read -- read back all entries
//	for i := 81; i <= N; i++ {
//		data, err = l.Read(uint64(i))
//		if err != nil {
//			t.Fatalf("error while getting %d", i)
//		}
//		if string(data) != dataStr(uint64(i)) {
//			t.Fatalf("expected %s, got %s", dataStr(uint64(i)), data)
//		}
//	}
//
//	// Close -- close log after truncating
//	if err = l.Close(); err != nil {
//		t.Fatal(err)
//	}
//
//	// Open -- open log after truncating
//	l, err = Open(logPath, opts)
//	if err != nil {
//		t.Fatal(err)
//	}
//	defer l.Close()
//
//	testFirstLast(t, l, uint64(81), uint64(N), nil)
//
//	// Read -- read back all entries
//	for i := 81; i <= N; i++ {
//		data, err = l.Read(uint64(i))
//		if err != nil {
//			t.Fatalf("error while getting %d", i)
//		}
//		if string(data) != dataStr(uint64(i)) {
//			t.Fatalf("expected %s, got %s", dataStr(uint64(i)), data)
//		}
//	}
//
//	// TruncateFront -- truncate all entries but one
//	if err = l.TruncateFront(uint64(N)); err != nil {
//		t.Fatal(err)
//	}
//	testFirstLast(t, l, uint64(N), uint64(N), nil)
//
//	// Write -- write on entry
//	err = l.Write(uint64(N+1), []byte(dataStr(uint64(N+1))))
//	if err != nil {
//		t.Fatal(err)
//	}
//	N++
//	testFirstLast(t, l, uint64(N-1), uint64(N), nil)
//
//	// TruncateBack -- truncate all entries but one
//	if err = l.TruncateBack(uint64(N - 1)); err != nil {
//		t.Fatal(err)
//	}
//	N--
//	testFirstLast(t, l, uint64(N), uint64(N), nil)
//
//	if err = l.Write(uint64(N+1), []byte(dataStr(uint64(N+1)))); err != nil {
//		t.Fatal(err)
//	}
//	N++
//
//	l.Sync()
//	testFirstLast(t, l, uint64(N-1), uint64(N), nil)
//}
//
//func testFirstLast(t *testing.T, l *BlockFile, expectFirst, expectLast uint64, data func(index uint64) []byte) {
//	t.Helper()
//	fi, err := l.FirstIndex()
//	if err != nil {
//		t.Fatal(err)
//	}
//	li, err := l.LastIndex()
//	if err != nil {
//		t.Fatal(err)
//	}
//	if fi != expectFirst || li != expectLast {
//		t.Fatalf("expected %v/%v, got %v/%v", expectFirst, expectLast, fi, li)
//	}
//	for i := fi; i <= li; i++ {
//		dt1, err := l.Read(i)
//		if err != nil {
//			t.Fatal(err)
//		}
//		if data != nil {
//			dt2 := data(i)
//			if string(dt1) != string(dt2) {
//				t.Fatalf("mismatch '%s' != '%s'", dt2, dt1)
//			}
//		}
//	}
//
//}
//
//func testGenBinaryData(buf []byte, withErrChecksum, withErrLen, withErrData bool) []byte {
//	data := make([]byte, 0)
//	// write checksum
//	checksum := NewCRC(buf).Value()
//	data = append(data, []byte("0000")...)
//	if withErrChecksum {
//		binary.LittleEndian.PutUint32(data[len(data)-4:], checksum+1)
//	} else {
//		binary.LittleEndian.PutUint32(data[len(data)-4:], checksum)
//	}
//	// write data_size
//	if withErrLen {
//		data = appendUvarint(data, uint64(len(buf)+1))
//	} else {
//		data = appendUvarint(data, uint64(len(buf)))
//	}
//
//	// write data
//	if withErrData {
//		data = append(data, buf[0:len(buf)-1]...)
//	} else {
//		data = append(data, buf...)
//	}
//	return data
//}
//
//func TestLog(t *testing.T) {
//	os.RemoveAll("testlog")
//	defer os.RemoveAll("testlog")
//
//	t.Run("nil-opts", func(t *testing.T) {
//		testLog(t, nil, 100)
//	})
//	t.Run("no-sync", func(t *testing.T) {
//		t.Run("json", func(t *testing.T) {
//			testLog(t, makeOpts(512, true, JSON), 100)
//		})
//		t.Run("binary", func(t *testing.T) {
//			testLog(t, makeOpts(512, true, Binary), 100)
//		})
//	})
//	t.Run("sync", func(t *testing.T) {
//		t.Run("json", func(t *testing.T) {
//			testLog(t, makeOpts(512, false, JSON), 100)
//		})
//		t.Run("binary", func(t *testing.T) {
//			testLog(t, makeOpts(512, false, Binary), 100)
//		})
//	})
//}
//
//// nolint
//func TestOutliers(t *testing.T) {
//	os.RemoveAll("testlog")
//	defer os.RemoveAll("testlog")
//
//	// Create some scenarios where the log has been corrupted, operations
//	// fail, or various weirdnesses.
//	t.Run("fail-in-memory", func(t *testing.T) {
//		if l, err := Open(":memory:", nil); err == nil {
//			l.Close()
//			t.Fatal("expected error")
//		}
//	})
//	t.Run("fail-not-a-directory", func(t *testing.T) {
//		defer os.RemoveAll("testlog/rfile")
//		if err := os.MkdirAll("testlog", 0777); err != nil {
//			t.Fatal(err)
//		} else if f, err := os.Create("testlog/rfile"); err != nil {
//			t.Fatal(err)
//		} else if err := f.Close(); err != nil {
//			t.Fatal(err)
//		} else if l, err := Open("testlog/rfile", nil); err == nil {
//			l.Close()
//			t.Fatal("expected error")
//		}
//	})
//	t.Run("load-with-junk-files", func(t *testing.T) {
//		// junk should be ignored
//		defer os.RemoveAll("testlog/junk")
//		if err := os.MkdirAll("testlog/junk/other1", 0777); err != nil {
//			t.Fatal(err)
//		}
//		f, err := os.Create("testlog/junk/other2")
//		if err != nil {
//			t.Fatal(err)
//		}
//		f.Close()
//		f, err = os.Create("testlog/junk/" + strings.Repeat("A", 20))
//		if err != nil {
//			t.Fatal(err)
//		}
//		f.Close()
//		l, err := Open("testlog/junk", nil)
//		if err != nil {
//			t.Fatal(err)
//		}
//		l.Close()
//	})
//	// Customize part start
//	// When opening, corrupted log entries will not be loaded
//	t.Run("corrupted-tail-json", func(t *testing.T) {
//		defer os.RemoveAll("testlog/corrupt-tail")
//		opts := makeOpts(512, true, JSON)
//		os.MkdirAll("testlog/corrupt-tail", 0777)
//		ioutil.WriteFile(
//			"testlog/corrupt-tail/00000000000000000001",
//			[]byte("\n"), 0600)
//		if l, err := Open("testlog/corrupt-tail", opts); err != nil {
//			l.Close()
//			t.Fatalf("expected %v, got %v", "not err", err)
//		}
//		ioutil.WriteFile(
//			"testlog/corrupt-tail/00000000000000000001",
//			[]byte(`{}`+"\n"), 0600)
//		if l, err := Open("testlog/corrupt-tail", opts); err != nil {
//			l.Close()
//			t.Fatalf("expected %v, got %v", "not err", err)
//		}
//		ioutil.WriteFile(
//			"testlog/corrupt-tail/00000000000000000001",
//			[]byte(`{"index":"1"}`+"\n"), 0600)
//		if l, err := Open("testlog/corrupt-tail", opts); err != nil {
//			l.Close()
//			t.Fatalf("expected %v, got %v", "not err", err)
//		}
//		ioutil.WriteFile(
//			"testlog/corrupt-tail/00000000000000000001",
//			[]byte(`{"index":"1","checksum":"2687495478"}`+"\n"), 0600)
//		if l, err := Open("testlog/corrupt-tail", opts); err != nil {
//			l.Close()
//			t.Fatalf("expected %v, got %v", "not err", err)
//		}
//		ioutil.WriteFile(
//			"testlog/corrupt-tail/00000000000000000001",
//			[]byte(`{"index":"1","data":"?"}`+"\n"), 0600)
//		if l, err := Open("testlog/corrupt-tail", opts); err != nil {
//			l.Close()
//			t.Fatalf("expected %v, got %v", "not err", err)
//		}
//	})
//	t.Run("corrupted-tail-binary", func(t *testing.T) {
//		defer os.RemoveAll("testlog/corrupt-tail")
//		opts := makeOpts(512, true, Binary)
//		os.MkdirAll("testlog/corrupt-tail", 0777)
//		ioutil.WriteFile(
//			"testlog/corrupt-tail/00000000000000000001",
//			testGenBinaryData([]byte("string"), true, false, false), 0600) // modify checksum
//		if l, err := Open("testlog/corrupt-tail", opts); err != nil {
//			l.Close()
//			t.Fatalf("expected %v, got %v", "not err", err)
//		}
//
//		ioutil.WriteFile(
//			"testlog/corrupt-tail/00000000000000000001",
//			testGenBinaryData([]byte("string"), false, true, false), 0600) // modify data_size
//		if l, err := Open("testlog/corrupt-tail", opts); err != nil {
//			l.Close()
//			t.Fatalf("expected %v, got %v", "not err", err)
//		}
//
//		ioutil.WriteFile(
//			"testlog/corrupt-tail/00000000000000000001",
//			testGenBinaryData([]byte("string"), false, false, true), 0600) // modify data
//		if l, err := Open("testlog/corrupt-tail", opts); err != nil {
//			l.Close()
//			t.Fatalf("expected %v, got %v", "not err", err)
//		}
//	})
//	t.Run("uncorrupted-tail-json", func(t *testing.T) {
//		defer os.RemoveAll("testlog/uncorrupted-tail")
//		opts := makeOpts(512, true, JSON)
//		os.MkdirAll("testlog/uncorrupted-tail", 0777)
//		ioutil.WriteFile(
//			"testlog/uncorrupted-tail/00000000000000000001",
//			[]byte(`{"index":"1","checksum":"2867159285","data":"+string"}`+"\n"), 0600)
//		l, err := Open("testlog/uncorrupted-tail", opts)
//		if err != nil {
//			l.Close()
//			t.Fatalf("expected %v, got %v", "not err", err)
//		}
//		if l.firstIndex != 1 || l.lastIndex != 1 {
//			l.Close()
//			t.Fatalf("expected[1,1], got [%d,%d]", l.firstIndex, l.lastIndex)
//		}
//	})
//	t.Run("uncorrupted-tail-binary", func(t *testing.T) {
//		defer os.RemoveAll("testlog/uncorrupted-tail")
//		opts := makeOpts(512, true, Binary)
//		os.MkdirAll("testlog/uncorrupted-tail", 0777)
//		ioutil.WriteFile(
//			"testlog/uncorrupted-tail/00000000000000000001",
//			testGenBinaryData([]byte("string"), false, false, false), 0600)
//		l, err := Open("testlog/uncorrupted-tail", opts)
//		if err != nil {
//			l.Close()
//			t.Fatalf("expected %v, got %v", "not err", err)
//		}
//		if l.firstIndex != 1 || l.lastIndex != 1 {
//			l.Close()
//			t.Fatalf("expected[1,1], got [%d,%d]", l.firstIndex, l.lastIndex)
//		}
//	})
//	// Customize part end
//	t.Run("start-marker-rfile", func(t *testing.T) {
//		lpath := "testlog/start-marker"
//		opts := makeOpts(512, true, JSON)
//		l := must(Open(lpath, opts)).(*BlockFile)
//		defer l.Close()
//		for i := uint64(1); i <= 100; i++ {
//			must(nil, l.Write(i, []byte(dataStr(i))))
//		}
//		path := l.segments[l.findSegment(35)].path
//		firstIndex := l.segments[l.findSegment(35)].index
//		must(nil, l.Close())
//		data := must(ioutil.ReadFile(path)).([]byte)
//		must(nil, ioutil.WriteFile(path+".START", data, 0600))
//		l = must(Open(lpath, opts)).(*BlockFile)
//		defer l.Close()
//		testFirstLast(t, l, firstIndex, 100, nil)
//	})
//}
//
//func makeOpts(segSize int, noSync bool, lf LogFormat) *Options {
//	opts := *DefaultOptions
//	opts.SegmentSize = segSize
//	opts.NoSync = noSync
//	opts.LogFormat = lf
//	return &opts
//}
//
//// https://github.com/tidwall/wal/issues/1
//func TestIssue1(t *testing.T) {
//	in := []byte{0, 0, 0, 0, 0, 0, 0, 1, 37, 108, 131, 178, 151, 17, 77, 32,
//		27, 48, 23, 159, 63, 14, 240, 202, 206, 151, 131, 98, 45, 165, 151, 67,
//		38, 180, 54, 23, 138, 238, 246, 16, 0, 0, 0, 0}
//	opts := *DefaultOptions
//	opts.LogFormat = JSON
//	os.RemoveAll("testlog")
//	defer os.RemoveAll("testlog")
//	l, err := Open("testlog", &opts)
//	if err != nil {
//		t.Fatal(err)
//	}
//	defer l.Close()
//	if err = l.Write(1, in); err != nil {
//		t.Fatal(err)
//	}
//	out, err := l.Read(1)
//	if err != nil {
//		t.Fatal(err)
//	}
//	if string(in) != string(out) {
//		t.Fatal("data mismatch")
//	}
//}
//
//func TestSimpleTruncateFront(t *testing.T) {
//	os.RemoveAll("testlog")
//	defer os.RemoveAll("testlog")
//
//	opts := &Options{
//		NoSync:      true,
//		LogFormat:   JSON,
//		SegmentSize: 100,
//	}
//
//	l, err := Open("testlog", opts)
//	if err != nil {
//		t.Fatal(err)
//	}
//	defer func() {
//		l.Close()
//	}()
//
//	makeData := func(index uint64) []byte {
//		return []byte(fmt.Sprintf("data-%d", index))
//	}
//
//	valid := func(t *testing.T, first, last uint64) {
//		t.Helper()
//		var index uint64
//		index, err = l.FirstIndex()
//		if err != nil {
//			t.Fatal(err)
//		}
//		if index != first {
//			t.Fatalf("expected %v, got %v", first, index)
//		}
//		index, err = l.LastIndex()
//		if err != nil {
//			t.Fatal(err)
//		}
//		if index != last {
//			t.Fatalf("expected %v, got %v", last, index)
//		}
//		var data []byte
//		for i := first; i <= last; i++ {
//			data, err = l.Read(i)
//			if err != nil {
//				t.Fatal(err)
//			}
//			if string(data) != string(makeData(i)) {
//				t.Fatalf("expcted '%s', got '%s'", makeData(i), data)
//			}
//		}
//	}
//	validReopen := func(t *testing.T, first, last uint64) {
//		t.Helper()
//		valid(t, first, last)
//		if err = l.Close(); err != nil {
//			t.Fatal(err)
//		}
//		l, err = Open("testlog", opts)
//		if err != nil {
//			t.Fatal(err)
//		}
//		valid(t, first, last)
//	}
//	for i := 1; i <= 100; i++ {
//		err = l.Write(uint64(i), makeData(uint64(i)))
//		if err != nil {
//			t.Fatal(err)
//		}
//	}
//	validReopen(t, 1, 100)
//
//	if err = l.TruncateFront(1); err != nil {
//		t.Fatal(err)
//	}
//	validReopen(t, 1, 100)
//
//	if err = l.TruncateFront(2); err != nil {
//		t.Fatal(err)
//	}
//	validReopen(t, 2, 100)
//
//	if err = l.TruncateFront(4); err != nil {
//		t.Fatal(err)
//	}
//	validReopen(t, 4, 100)
//
//	if err = l.TruncateFront(5); err != nil {
//		t.Fatal(err)
//	}
//	validReopen(t, 5, 100)
//
//	if err = l.TruncateFront(99); err != nil {
//		t.Fatal(err)
//	}
//	validReopen(t, 99, 100)
//
//	if err = l.TruncateFront(100); err != nil {
//		t.Fatal(err)
//	}
//	validReopen(t, 100, 100)
//
//}
//
//func TestSimpleTruncateBack(t *testing.T) {
//	os.RemoveAll("testlog")
//	defer os.RemoveAll("testlog")
//
//	opts := &Options{
//		NoSync:      true,
//		LogFormat:   JSON,
//		SegmentSize: 100,
//	}
//
//	l, err := Open("testlog", opts)
//	if err != nil {
//		t.Fatal(err)
//	}
//	defer func() {
//		l.Close()
//	}()
//
//	makeData := func(index uint64) []byte {
//		return []byte(fmt.Sprintf("data-%d", index))
//	}
//
//	valid := func(t *testing.T, first, last uint64) {
//		t.Helper()
//		var index uint64
//		index, err = l.FirstIndex()
//		if err != nil {
//			t.Fatal(err)
//		}
//		if index != first {
//			t.Fatalf("expected %v, got %v", first, index)
//		}
//		index, err = l.LastIndex()
//		if err != nil {
//			t.Fatal(err)
//		}
//		if index != last {
//			t.Fatalf("expected %v, got %v", last, index)
//		}
//		var data []byte
//		for i := first; i <= last; i++ {
//			data, err = l.Read(i)
//			if err != nil {
//				t.Fatal(err)
//			}
//			if string(data) != string(makeData(i)) {
//				t.Fatalf("expcted '%s', got '%s'", makeData(i), data)
//			}
//		}
//	}
//	validReopen := func(t *testing.T, first, last uint64) {
//		t.Helper()
//		valid(t, first, last)
//		if err = l.Close(); err != nil {
//			t.Fatal(err)
//		}
//		l, err = Open("testlog", opts)
//		if err != nil {
//			t.Fatal(err)
//		}
//		valid(t, first, last)
//	}
//	for i := 1; i <= 100; i++ {
//		err = l.Write(uint64(i), makeData(uint64(i)))
//		if err != nil {
//			t.Fatal(err)
//		}
//	}
//	validReopen(t, 1, 100)
//
//	/////////////////////////////////////////////////////////////
//	if err = l.TruncateBack(100); err != nil {
//		t.Fatal(err)
//	}
//	validReopen(t, 1, 100)
//	if err = l.Write(101, makeData(101)); err != nil {
//		t.Fatal(err)
//	}
//	validReopen(t, 1, 101)
//
//	/////////////////////////////////////////////////////////////
//	if err = l.TruncateBack(99); err != nil {
//		t.Fatal(err)
//	}
//	validReopen(t, 1, 99)
//	if err = l.Write(100, makeData(100)); err != nil {
//		t.Fatal(err)
//	}
//	validReopen(t, 1, 100)
//
//	if err = l.TruncateBack(94); err != nil {
//		t.Fatal(err)
//	}
//	validReopen(t, 1, 94)
//
//	if err = l.TruncateBack(93); err != nil {
//		t.Fatal(err)
//	}
//	validReopen(t, 1, 93)
//
//	if err = l.TruncateBack(92); err != nil {
//		t.Fatal(err)
//	}
//	validReopen(t, 1, 92)
//
//}
//
//func TestConcurrency(t *testing.T) {
//	os.RemoveAll("testlog")
//	defer os.RemoveAll("testlog")
//
//	l, err := Open("testlog", &Options{
//		NoSync: true,
//		NoCopy: true,
//	})
//	if err != nil {
//		t.Fatal(err)
//	}
//	defer l.Close()
//
//	// Write 1000 entries
//	for i := 1; i <= 1000; i++ {
//		err = l.Write(uint64(i), []byte(dataStr(uint64(i))))
//		if err != nil {
//			t.Fatal(err)
//		}
//	}
//
//	// Perform 100,000 reads (over 100 threads)
//	finished := int32(0)
//	maxIndex := int32(1000)
//	numReads := int32(0)
//	for i := 0; i < 100; i++ {
//		go func() {
//			defer atomic.AddInt32(&finished, 1)
//
//			for j := 0; j < 1_000; j++ {
//				// nolint
//				index := rand.Int31n(atomic.LoadInt32(&maxIndex)) + 1
//				if _, err = l.Read(uint64(index)); err != nil {
//					// nolint
//					t.Fatal(err)
//				}
//				atomic.AddInt32(&numReads, 1)
//			}
//		}()
//	}
//
//	// continue writing
//	for index := maxIndex + 1; atomic.LoadInt32(&finished) < 100; index++ {
//		err = l.Write(uint64(index), []byte(dataStr(uint64(index))))
//		if err != nil {
//			t.Fatal(err)
//		}
//		atomic.StoreInt32(&maxIndex, index)
//	}
//
//	// confirm total reads
//	if exp := int32(100_000); numReads != exp {
//		t.Fatalf("expected %d reads, but god %d", exp, numReads)
//	}
//}

//func initBlockFileDb(t *testing.T) *BlockFile {
//	var (
//		bfdbErr error
//		bfdb    *BlockFile
//	)
//	opts := DefaultOptions
//
//	bfdbPath := filepath.Join(os.TempDir(), fmt.Sprintf("%d", time.Now().Nanosecond()))
//	bfdb, bfdbErr = Open(bfdbPath, opts, &test.GoLogger{})
//	assert.Nil(t, bfdbErr)
//	return bfdb
//}

//func TestBlockFile_Write(t *testing.T) {
//	bfdb := initBlockFileDb(t)
//	fileName, offset, bytelen, err := bfdb.Write(1, []byte("1234567890"))
//	assert.Nil(t, err)
//	assert.Equal(t, fileName, "00000000000000000001")
//	assert.Equal(t, offset, uint64(5))
//	assert.Equal(t, bytelen, uint64(len([]byte("1234567890"))))
//
//	data, fileName, offset, bytelen, err := bfdb.ReadLastSegSection(1)
//	assert.Nil(t, err)
//	assert.Equal(t, fileName, "00000000000000000001")
//	assert.Equal(t, offset, uint64(5))
//	assert.Equal(t, bytelen, uint64(len([]byte("1234567890"))))
//	assert.Equal(t, data, []byte("1234567890"))
//}

func resetMetaDBData(db protocol.DBHandle) {
	iter, _ := db.NewIteratorWithPrefix([]byte("m"))
	for iter.Next() {
		k := iter.Key()
		db.Delete(k)
	}

}

// 将block写入到filedb中
func writeBlockToFile(bs *BlockFile, blockHeight uint64, bytes []byte) (*storePb.StoreInfo, error) {

	// wal log, index increase from 1, while blockHeight increase form 0
	fileName, offset, bytesLen, err := bs.Write(blockHeight+1, bytes)
	if err != nil {
		return nil, err
	}

	return &storePb.StoreInfo{
		FileName: fileName,
		Offset:   offset,
		ByteLen:  bytesLen,
	}, nil
}

func getDBHandle(chainId, providerName, dbFolder string,
	logger protocol.Logger, encryptor crypto.SymmetricKey) protocol.DBHandle {
	timeRand := time.Now().Unix()
	dbFolder = dbFolder + strconv.Itoa(int(timeRand))

	//使用全局配置
	inputDBConfig := globalConfig.BlockDbConfig
	configMap := inputDBConfig.LevelDbConfig

	dbConfig := &leveldbprovider.LevelDbConfig{}

	err := mapstructure.Decode(configMap, dbConfig)
	if err != nil {
		return nil
	}
	input := &leveldbprovider.NewLevelDBOptions{
		Config:    dbConfig,
		Logger:    logger,
		Encryptor: encryptor,
		ChainId:   chainId,
		DbFolder:  dbFolder,
	}
	return leveldbprovider.NewLevelDBHandle(input)
}
func getSqliteConfig() *conf.StorageConfig {
	conf1, _ := conf.NewStorageConfig(nil)
	conf1.StorePath = filepath.Join(os.TempDir(), fmt.Sprintf("%d", time.Now().Nanosecond()))
	conf1.WriteBlockType = 0
	var sqlconfig = make(map[string]interface{})

	sqlconfig["sqldb_type"] = sqlLite
	sqlconfig["dsn"] = memoryStr

	dbConfig := &conf.DbConfig{
		Provider:    "sql",
		SqlDbConfig: sqlconfig,
	}
	statedbConfig := &conf.DbConfig{
		Provider:    "sql",
		SqlDbConfig: sqlconfig,
	}

	lvlConfig := make(map[string]interface{})
	lvlConfig["store_path"] = filepath.Join(os.TempDir(), fmt.Sprintf("%d", time.Now().Nanosecond()))

	txExistDbConfig := &conf.DbConfig{
		Provider:      "leveldb",
		LevelDbConfig: lvlConfig,
	}
	conf1.DisableBlockFileDb = true
	conf1.BlockDbConfig = dbConfig
	conf1.StateDbConfig = statedbConfig
	conf1.HistoryDbConfig = conf.NewHistoryDbConfig(dbConfig)
	conf1.ResultDbConfig = dbConfig
	conf1.ContractEventDbConfig = dbConfig
	conf1.DisableContractEventDB = true
	conf1.TxExistDbConfig = txExistDbConfig
	conf1.LogDBSegmentSize = 20
	//conf1.DisableBigFilter = true
	conf1.RollingWindowCacheCapacity = 1000000
	conf1.BlockFileConfig = &conf.BlockFileConfig{
		OnlineFileSystem:  "/tmp/online1,/tmp/online2",
		ArchiveFileSystem: "/tmp/archive1,/tmp/archive2",
	}
	conf1.ConfigVersion = &conf.StorageConfigVersion{
		Major: 1,
		Minor: 1,
	}
	return conf1
}

//生成一个随机字符串，长度为 n 个字符长度
func RandStringBytesMaskImprSrcUnsafe(n int) string {
	b := make([]byte, n)
	// A src.Int63() generates 63 random bits, enough for letterIdxMax characters!
	for i, cache, remain := n-1, src.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return *(*string)(unsafe.Pointer(&b))
}

func TestOpen1(t *testing.T) {
	//1.创建metadb
	metadb, err := metadb.NewMetaDataDB(*globalConfig, globalTestChainID, testDBHandler, &test.GoLogger{})
	if err != nil {
		fmt.Println("create metadatadb error ", err)
		return
	}
	defer resetMetaDBData(testDBHandler)

	type args struct {
		path   string
		opts   *Options
		logger protocol.Logger
		meta   meta.MetaData
		idx    uint64
	}
	tests := []struct {
		name string
		args args
		//want    *BlockFile
		wantErr bool
	}{
		{
			name: "test-open-no-mmap",
			args: args{
				path: "test",
				opts: &Options{
					NoSync:           false,
					SegmentSize:      67108864,
					SegmentCacheSize: 2,
					NoCopy:           false,
					UseMmap:          false,
				},
				logger: testLogger,
				meta:   metadb,
				idx:    0,
			},
			//want:initBlockFileDb(),
			wantErr: false,
		},
		{
			name: "test-open-mmap",
			args: args{
				path: "test",
				opts: &Options{
					NoSync:           false,
					SegmentSize:      67108864,
					SegmentCacheSize: 2,
					NoCopy:           false,
					UseMmap:          true,
				},
				logger: testLogger,
				meta:   metadb,
				idx:    6,
			},
			//want:initBlockFileDb(),
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			//先清理元数据，保持为空
			resetMetaDBData(testDBHandler)

			if tt.name == "test-open-no-mmap" {

				//1.创建metadb
				//2.创建bfdb
				//3.写入一些数据
				//4.发起归档，调用metadb
				//5.读归档数据,读未归档数据，读不同文件系统的数据

				idx := tt.args.idx
				//block2 用于归档读
				block2Byte := []byte{}
				block2StoreFile := &storePb.StoreInfo{}

				//block4 不归档，读不归档数据
				block4Byte := []byte{}
				block4StoreFile := &storePb.StoreInfo{}

				//block5 不归档，读不归档数据，模拟对应的wal文件为last segment,后缀名带 .END
				block5Byte := []byte{}
				block5StoreFile := &storePb.StoreInfo{}

				//1.打开bfdb
				bfDB, err := Open(tt.args.path, tt.args.opts, tt.args.logger, tt.args.meta)
				if (err != nil) != tt.wantErr {
					t.Errorf("Open() error = %v, wantErr %v", err, tt.wantErr)
					return
				}

				//2.写入一些数据
				//模拟写入block0-block3  4个区块, 对应bfdb 的文件为 00000000000000000001.fdb.END
				//模拟写入 block4-block5 2个区块，对应bfdb 的文件为 00000000000000000005.fdb.END
				//在写 block4 时触发了 文件切换,生成了第二个 fdb 文件
				block2Byte, block4Byte, block5Byte, block2StoreFile, block4StoreFile, block5StoreFile, idx, err = writeBlock(bfDB, idx)
				if err != nil {
					t.Errorf("write block error = %v, wantErr %v", err, tt.wantErr)
				}

				//3.发起归档,对1-2区块块高的 2个区块 进行归档，对应的文件为 00000000000000000001.fdb
				//正常情况下，会把 00000000000000000001.fdb 进行归档
				archiveID, err := metadb.DoArchive(1, 2)
				if err != nil {
					t.Errorf("do archive error = %v, wantErr %v", err, tt.wantErr)
					return
				}

				//4.读数据,读正在归档的数据和已完成归档的数据
				//4.1读 block2
				err = readArchiveData(bfDB, block2Byte, block2StoreFile, metadb, archiveID)
				if err != nil {
					t.Errorf("readArchiveData ,error = %v, wantErr %v", err, tt.wantErr)
					return
				}

				//4.2再读未归档数据 block4, block5
				//block5 对应的bfdb文件后缀包含.END
				err = readUnArchiveData(bfDB, block4Byte, block4StoreFile, block5Byte, block5StoreFile)
				if err != nil {
					t.Errorf("readUnArchiveData ,error = %v, wantErr %v", err, tt.wantErr)
					return
				}

				//5.其它函数测试
				fmt.Println("--------------test  ClearCache()--------------")
				if err = bfDB.ClearCache(); err != nil {
					t.Errorf("clear cache error =%v, wantErr %v", err, tt.wantErr)
				}

				fmt.Println("--------------test  Sync()--------------")
				if err = bfDB.Sync(); err != nil {
					t.Errorf("sync error =%v, wantErr %v", err, tt.wantErr)
				}

				fmt.Println("--------------test  LastIndex()--------------")
				lastIdx, err := bfDB.LastIndex()
				if err != nil {
					t.Errorf("sync error =%v, wantErr %v", err, tt.wantErr)
				}
				if lastIdx != 6 {
					t.Errorf("LastIndex() want =%v, got %v", 6, lastIdx)
				}

				fmt.Println("--------------test  Close()--------------")
				if err = bfDB.Close(); err != nil {
					t.Errorf("close error =%v, wantErr %v", err, tt.wantErr)
				}
			}
			if tt.name == "test-open-mmap" {
				//1.创建metadb
				//2.创建bfdb
				//3.写入一些数据
				//4.发起归档，调用metadb
				//5.读归档数据,读未归档数据，读不同文件系统的数据

				idx := tt.args.idx

				bfDB, err := Open(tt.args.path, tt.args.opts, tt.args.logger, tt.args.meta)
				if (err != nil) != tt.wantErr {
					t.Errorf("Open() error = %v, wantErr %v", err, tt.wantErr)
					return
				}

				//3.写入一些数据
				//模拟写入block0-block3  4个区块, 对应bfdb 的文件为 00000000000000000001.fdb.END
				var storeFile *storePb.StoreInfo
				for idx < 8 {
					b2 := RandStringBytesMaskImprSrcUnsafe(20000000)
					b1 := []byte(b2)
					storeFile, err = writeBlockToFile(bfDB, idx, b1)
					fmt.Println("current block height = ", idx)
					idx++
					if err != nil {
						t.Errorf("write block to file error = %v, wantErr %v", err, tt.wantErr)
						return
					}
					fmt.Println("writeBlockToFile, return ,storeFile=", *storeFile)
				}

				fmt.Println("--------------test WriteBatch()---------------")
				tbatch := []byte("test-batch")
				testBatch := &Batch{
					entry: batchEntry{
						index: idx + 1,
						size:  len(tbatch),
					},
					data: tbatch,
				}
				_, err = bfDB.WriteBatch(testBatch)
				if err != nil {
					t.Errorf("WriteBatch err =%v,", err)
					return
				}

				fmt.Println("--------------test out of order, b.entry.index---------------")
				_, err = writeBlockToFile(bfDB, 100, []byte("out-index"))
				if err == nil {
					t.Errorf("want error,get nil")
					return
				}

			}

		})
	}
}

func readBlock(bfDB *BlockFile, storeFile *storePb.StoreInfo, blockByte []byte) error {
	fmt.Println("read block2 fileName=", storeFile.FileName)
	section, err := bfDB.ReadFileSection(storeFile)
	if err != nil {
		fmt.Println("read file Section error ", err, storeFile)
		return errors.New("read block error")
	}
	if string(section) != string(blockByte) {
		fmt.Println("read error,not equal", storeFile)
		return errors.New("read block error,read != write")
		//return
	}
	return nil
}

func writeBlock(bfDB *BlockFile, idx uint64) ([]byte, []byte, []byte,
	*storePb.StoreInfo, *storePb.StoreInfo, *storePb.StoreInfo, uint64, error) {
	//var err2 error
	//block2 用于归档读
	block2Byte := []byte{}
	block2StoreFile := &storePb.StoreInfo{}

	//block4 不归档，读不归档数据
	block4Byte := []byte{}
	block4StoreFile := &storePb.StoreInfo{}

	//block5 不归档，读不归档数据，模拟对应的wal文件为last segment,后缀名带 .END
	block5Byte := []byte{}
	block5StoreFile := &storePb.StoreInfo{}

	//写入一些数据

	//模拟写入block0-block3  4个区块, 对应bfdb 的文件为 00000000000000000001.fdb.END
	//模拟写入 block4-block5 2个区块，对应bfdb 的文件为 00000000000000000005.fdb.END
	//在写 block4 时触发了 文件切换,生成了第二个 fdb 文件
	for idx < 6 {
		//b2 :=RandStringBytesMaskImprSrcUnsafe(1000000)
		b2 := RandStringBytesMaskImprSrcUnsafe(20000000)
		//b2 := RandStringBytesMaskImprSrcUnsafe(2)
		//fmt.Println("RandStringBytesMaskImprSrcUnsafe =",string(b2))
		//b1 := []byte("1a")
		b1 := []byte(b2)
		storeFile, err := writeBlockToFile(bfDB, idx, b1)
		if idx == 2 {
			block2Byte = b1
			block2StoreFile = storeFile
		}
		if idx == 4 {
			block4Byte = b1
			block4StoreFile = storeFile
		}
		if idx == 5 {
			block5Byte = b1
			block5StoreFile = storeFile
		}
		fmt.Println("current block height = ", idx)
		idx++
		if err != nil {
			//t.Errorf("write block to file error = %v, wantErr %v", err, tt.wantErr)
			fmt.Printf("writeBlockToFile error:%v", err)
			return block2Byte, block4Byte, block5Byte, block2StoreFile, block4StoreFile, block5StoreFile, idx, err
			//return
		}
		//return block2Byte, block4Byte, block5Byte, block2StoreFile, block4StoreFile, block5StoreFile, idx,nil

	}
	return block2Byte, block4Byte, block5Byte, block2StoreFile, block4StoreFile, block5StoreFile, idx, nil
}

func readArchiveData(bfDB *BlockFile, block2Byte []byte, block2StoreFile *storePb.StoreInfo,
	metadb meta.MetaData, archiveID string) error {

	errChan := make(chan error, 100)

	//6.读数据
	//6.0读正在归档的数据，不停的读
	go func() {
		for {
			err := readBlock(bfDB, block2StoreFile, block2Byte)
			if err != nil {
				errChan <- err
				//t.Errorf("get archive job by id ,error = %v, wantErr %v", err, tt.wantErr)
				fmt.Printf("readBlock error = %v", err)
			}
		}
	}()

	//6.1读已归档数据 block2,先保证归档已完成
	//获得归档状态
	for {
		archiveJobInfo, err := metadb.GetArchiveJobByID(archiveID)
		if err != nil {
			errChan <- err
			fmt.Printf("get archive job by id ,error = %v", err)
			//t.Errorf("get archive job by id ,error = %v, wantErr %v", err, tt.wantErr)
			return <-errChan
		}
		if archiveJobInfo.Status != 4 {
			fmt.Println("archiveJobInfo=", archiveJobInfo)
			time.Sleep(100 * time.Millisecond)
			continue
		}
		break
	}

	//确定归档后，先读已归档数据 block2
	fmt.Println("read block2 fileName=", block2StoreFile.FileName)
	section, err := bfDB.ReadFileSection(block2StoreFile)
	if err != nil {
		errChan <- err
		fmt.Printf("read block2 file Section error= %v", err)
		return <-errChan
	}
	if string(section) != string(block2Byte) {
		errChan <- err
		fmt.Printf("read block2 error,not equal")
		//fmt.Println("read block2 error,not equal")
		return <-errChan
	}
	if len(errChan) > 0 {
		return <-errChan
	}
	return nil
}

func readUnArchiveData(bfDB *BlockFile, block4Byte []byte, block4StoreFile *storePb.StoreInfo,
	block5Byte []byte, block5StoreFile *storePb.StoreInfo) error {
	//6.2再读未归档数据 block4
	fmt.Println("read block4 fileName=", block4StoreFile.FileName)
	section, err := bfDB.ReadFileSection(block4StoreFile)
	if err != nil {
		fmt.Printf("read file Section error= %v", err)
		//fmt.Println("read file Section error ",err)
		return err
	}

	if string(section) != string(block4Byte) {
		fmt.Printf("read block4 error,not equal")
		//t.Errorf("read block4 error,not equal")
		return errors.New("read block4 error,not equal")
	}

	//6.3再读未归档数据 block5, block5对应的file 带后缀名 .END
	fmt.Println("read block5 fileName=", block5StoreFile.FileName)
	section, err = bfDB.ReadFileSection(block5StoreFile)
	if err != nil {
		fmt.Printf("read file Section error= %v", err)
		//fmt.Println("read file Section error ",err)
		return err
	}
	if string(section) != string(block5Byte) {
		fmt.Printf("read block5 error,not equal")
		return errors.New("read block5 error,not equal")
	}
	fmt.Println("--------------test  FileIndexToString()--------------")
	wantStr := "fileIndex: fileName: 00000000000000000005, offset: 20000016, byteLen: 20000000"
	toString := FileIndexToString(block5StoreFile)
	if toString != wantStr {
		fmt.Printf(" FileIndexToString  error,not equal,want :%v,got:%v", toString, wantStr)
		return errors.New("FileIndexToString  error,not equal")
	}

	fmt.Println("--------------test  ReadLastSegSection()--------------")
	segSection, name, offset, byteLen, err := bfDB.ReadLastSegSection(5 + 1)
	if byteLen != block5StoreFile.ByteLen {
		fmt.Printf("read LastSegSection  byteLen error,not equal,want :%v,got:%v",
			block5StoreFile.ByteLen, byteLen)
		return errors.New("read LastSegSection byteLen error,not equal")
	}
	if string(segSection) != string(block5Byte) {
		fmt.Printf("read LastSegSection error,segSection not equal")
		return errors.New("read LastSegSection error,segSection not equal")
	}
	if name != block5StoreFile.FileName {
		fmt.Printf("read LastSegSection fileName error,not equal,want :%v,got:%v",
			block5StoreFile.FileName, name)
		return errors.New("read LastSegSection fileName error,not equal")
	}

	if offset != block5StoreFile.Offset {
		fmt.Printf("read LastSegSection offset error,not equal,want :%v,got:%v",
			block5StoreFile.Offset, offset)
		return errors.New("read LastSegSection offset error,not equal")
	}
	return nil
}
