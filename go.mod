module chainmaker.org/chainmaker/store-huge/v3

go 1.16

require (
	chainmaker.org/chainmaker/common/v3 v3.0.0
	chainmaker.org/chainmaker/lws v1.1.0
	chainmaker.org/chainmaker/pb-go/v3 v3.0.0
	chainmaker.org/chainmaker/protocol/v3 v3.0.0
	chainmaker.org/chainmaker/store-badgerdb/v3 v3.0.0
	chainmaker.org/chainmaker/store-leveldb/v3 v3.0.0
	chainmaker.org/chainmaker/store-sqldb/v3 v3.0.0
	chainmaker.org/chainmaker/store-tikv/v3 v3.0.0
	chainmaker.org/chainmaker/utils/v3 v3.0.0
	github.com/RedisBloom/redisbloom-go v1.0.0
	github.com/allegro/bigcache/v3 v3.0.2
	github.com/gogo/protobuf v1.3.2
	github.com/golang/groupcache v0.0.0-20200121045136-8c9f03a8e57e
	github.com/golang/mock v1.6.0
	github.com/google/flatbuffers v2.0.0+incompatible // indirect
	github.com/google/uuid v1.1.2
	github.com/magiconair/properties v1.8.5
	github.com/mitchellh/mapstructure v1.4.2
	github.com/pkg/errors v0.9.1
	github.com/shirou/gopsutil/v3 v3.22.10
	github.com/spaolacci/murmur3 v1.1.0
	github.com/stretchr/testify v1.8.1
	github.com/tidwall/tinylru v1.1.0
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
)

replace google.golang.org/grpc => google.golang.org/grpc v1.26.0

replace github.com/RedisBloom/redisbloom-go => chainmaker.org/third_party/redisbloom-go v1.0.0

replace github.com/oleiade/reflections => github.com/oleiade/reflections v1.0.1
