/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package store

import (
	"chainmaker.org/chainmaker/common/v3/wal"
	"chainmaker.org/chainmaker/store-huge/v3/snapshot"

	//"chainmaker.org/chainmaker/logger/v3"
	"encoding/hex"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"time"

	"chainmaker.org/chainmaker/store-huge/v3/binlog"
	"chainmaker.org/chainmaker/store-huge/v3/meta"
	"chainmaker.org/chainmaker/store-huge/v3/meta/metadb"

	"chainmaker.org/chainmaker/store-huge/v3/rolling_window_cache"

	"chainmaker.org/chainmaker/store-huge/v3/bigfilterdb"
	"chainmaker.org/chainmaker/store-huge/v3/bigfilterdb/bigfilterkvdb"

	"chainmaker.org/chainmaker/common/v3/container"
	"chainmaker.org/chainmaker/common/v3/crypto"
	"chainmaker.org/chainmaker/common/v3/crypto/pkcs11"
	"chainmaker.org/chainmaker/common/v3/crypto/sym/aes"
	"chainmaker.org/chainmaker/common/v3/crypto/sym/sm4"
	"chainmaker.org/chainmaker/protocol/v3"
	"chainmaker.org/chainmaker/store-huge/v3/blockdb"
	"chainmaker.org/chainmaker/store-huge/v3/blockdb/blockfiledb"
	"chainmaker.org/chainmaker/store-huge/v3/blockdb/blockkvdb"
	"chainmaker.org/chainmaker/store-huge/v3/blockdb/blocksqldb"
	"chainmaker.org/chainmaker/store-huge/v3/cache"
	"chainmaker.org/chainmaker/store-huge/v3/conf"
	"chainmaker.org/chainmaker/store-huge/v3/contracteventdb"
	"chainmaker.org/chainmaker/store-huge/v3/contracteventdb/eventsqldb"
	"chainmaker.org/chainmaker/store-huge/v3/dbprovider"
	"chainmaker.org/chainmaker/store-huge/v3/historydb"
	"chainmaker.org/chainmaker/store-huge/v3/historydb/historykvdb"
	"chainmaker.org/chainmaker/store-huge/v3/historydb/historysqldb"
	"chainmaker.org/chainmaker/store-huge/v3/resultdb"
	"chainmaker.org/chainmaker/store-huge/v3/resultdb/resultfiledb"
	"chainmaker.org/chainmaker/store-huge/v3/resultdb/resultkvdb"
	"chainmaker.org/chainmaker/store-huge/v3/resultdb/resultsqldb"
	"chainmaker.org/chainmaker/store-huge/v3/serialization"
	"chainmaker.org/chainmaker/store-huge/v3/statedb"
	"chainmaker.org/chainmaker/store-huge/v3/statedb/statekvdb"
	"chainmaker.org/chainmaker/store-huge/v3/statedb/statesqldb"
	"chainmaker.org/chainmaker/store-huge/v3/test"
	"chainmaker.org/chainmaker/store-huge/v3/txexistdb"
	"chainmaker.org/chainmaker/store-huge/v3/txexistdb/txexistkvdb"
	"github.com/allegro/bigcache/v3"
)

// nolint
const (
	//StoreBlockDBDir blockdb folder name
	StoreBlockDBDir = "store_block"
	//StoreStateDBDir statedb folder name
	StoreStateDBDir = "store_state"
	//StoreHistoryDBDir historydb folder name
	StoreHistoryDBDir = "store_history"
	//StoreResultDBDir resultdb folder name
	StoreResultDBDir   = "store_result"
	StoreEventLogDBDir = "store_event_log"
	StoreLocalDBDir    = "localdb"
	StoreTxExistDbDir  = "store_txexist"

	DBName_BlockDB   = "blockdb"
	DBName_StateDB   = "statedb"
	DBName_HistoryDB = "historydb"
	DBName_ResultDB  = "resultdb"
	DBName_EventDB   = "eventdb"
	DBName_LocalDB   = "localdb"
	DBName_TxExistDB = "txexistdb"

	DBHandleBlockName    = "newDBHandleName" + DBName_BlockDB
	SqlDBHandleBlockName = "newSqlDBhandleName" + DBName_BlockDB

	DBHandleStateName    = "newDBHandleName" + DBName_StateDB
	SqlDBHandleStateName = "newSqlDBhandleName" + DBName_StateDB

	DBHandleHistoryName    = "newDBHandleName" + DBName_HistoryDB
	SqlDBHandleHistoryName = "newSqlDBhandleName" + DBName_HistoryDB

	DBHandleResultName    = "newDBHandleName" + DBName_ResultDB
	SqlDBHandleResultName = "newSqlDBhandleName" + DBName_ResultDB

	DBHandleEventName    = "newDBHandleName" + DBName_EventDB
	SqlDBHandleEventName = "newSqlDBhandleName" + DBName_EventDB

	DBHandleTxExistName    = "newDBHandleName" + DBName_TxExistDB
	SqlDBHandleTxExistName = "newSqlDBhandleName" + DBName_TxExistDB

	DBHandleLocalName    = "newDBHandleName" + DBName_LocalDB
	SqlDBHandleLocalName = "newSqlDBhandleName" + DBName_LocalDB

	BlockKvDB    = "NewBlockKvDB"
	BlockSQLDB   = "NewBlockSqlDB"
	BlockFileDB  = "NewBlockFileDB"
	StateKvDB    = "NewStateKvDB"
	StateSQLDB   = "NewStateSqlDB"
	HistoryKvDB  = "NewHistoryKvDB"
	HistorySQLDB = "NewHistorySqlDB"
	ResultKvDB   = "NewResultKvDB"
	ResultSQLDB  = "NewResultSqlDB"
	ResultFileDB = "NewResultFileDB"

	WrapTxExistDB = "NewExistDB"
	TxExistKvDB   = "NewExistKvDB"

	EventSQLDB = "NewEventSqlDB"

	WAL       = "NewWAL"
	BlockFile = "NewBF"

	META      = "NewMeta"
	BigFilter = "NewBigFilter"
	RWC       = "NewRollingWindowCache"
	Encryptor = "NewEncryptor"

	LedgerSnapshot       = "NewLedgerSnapshot"
	ImportLedgerSnapshot = "NewImportLedgerSnapshot"

	//newDBHandleName = "newDBHandleName" + name
	//newSqlDBhandleName = "newSqlDBhandleName" + name
)

// nolint
var (
	ErrConversionInterface = errors.New("interface conversion error")
)

// Factory is a factory function to create an instance of the block store
// which commits block into the ledger.
type Factory struct {
	ioc       *container.Container
	dbFactory *dbprovider.DBFactory
	//first-level properties, include wal,rwc,bigfilter, encryptor,dbhandle,sqlhandle
	firstAttrSet map[string]interface{}

	//second-level properties, include metadb
	secondAttrSet map[string]interface{}

	//third-level properties,  include blockFile,blockDB,stateDB,resultDB,HistoryDB,ContractEventDB,
	//TxExistDB,ledgerSnapshot,importLedgerSnapshot
	thirdAttrSet map[string]interface{}
}

// NewFactory create a factory of storage
func NewFactory() *Factory {
	ioc := container.NewContainer()
	return &Factory{
		ioc:           ioc,
		dbFactory:     dbprovider.NewDBFactory(),
		firstAttrSet:  make(map[string]interface{}),
		secondAttrSet: make(map[string]interface{}),
		thirdAttrSet:  make(map[string]interface{}),
	}
}

// createAll creates various objects ,include metadb,blockDB,stateDB,resultDB,historyDB,block file db and so on.
// 注册函数，存储对象中，所有成员变量 初始化入口
// firstAttrSet 保存 wal，rwc,bigfilter, dbhandle,sqlhandle 等对象
// secondAttrSet 保存 了 metadb
// thirdAttrSet 保存了 BlockFile,blockDB，stateDB，resultDB,HistoryDB,ContractEventDB,TxExistDB,ledgerSnapshot
// 基本依赖关系: 第三个集合 依赖第二个集合，第二个集合 依赖第一个集合
// 比如:先创建好 dbhandle，才能创建 metadb ，进而 才能创建bf，进而才能创建blockDB
// 因为：blockDB 包含 blockFile， blockFile 包含 metadb ，metadb 包含dbhandle
func (m *Factory) createALL(chainId string, storeConfig *conf.StorageConfig, logger protocol.Logger,
	p11Handle *pkcs11.P11Handle) error {
	var err error
	// 1.创建对象，保存在 firstAttrSet 中
	//1.1 创建wal
	if err = m.createWAL(chainId, storeConfig, logger); err != nil {
		logger.Errorf("create wal error,errInfo:[%s]", err)
		return err
	}

	//1.2 创建 bigFilter
	if storeConfig.EnableBigFilter {
		if err = m.createBigFilter(chainId, storeConfig.BigFilter, logger); err != nil {
			logger.Errorf("create bigfilter error,errInfo:[%s]", err)
			return err
		}
	}
	//1.3 创建 rollingWindowCache
	if err = m.createRollingWindowCache(storeConfig, logger); err != nil {
		logger.Errorf("create rwc error,errInfo:[%s]", err)
		return err
	}

	//1.4 创建 数据落盘对称数据加密对象
	if err = m.createEncryptor(chainId, storeConfig, logger, p11Handle); err != nil {
		logger.Errorf("create encryptor error,errInfo:[%s]", err)
		return err
	}

	//1.5 创建 dbhandle
	if err = m.createAllDbHandle(chainId, storeConfig, logger); err != nil {
		logger.Errorf("create all dbhandle error,errInfo:[%s]", err)
		return err
	}

	// 2.创建对象，保存在 secondAttrSet 中
	//2.1 创建 meta
	if err = m.createMeta(chainId, storeConfig, logger); err != nil {
		logger.Errorf("create meta error,errInfo:[%s]", err)
		return err
	}

	// 3.创建对象，保存在 thirdAttrSet 中
	//3.1 创建blockFile(bf)
	if err = m.createBF(chainId, storeConfig, logger); err != nil {
		logger.Errorf("create block file error,errInfo:[%s]", err)
		return err
	}

	//3.2 创建 BusinessDB 包括blockdb,resultdb,statedb,eventdb,historydb,txExistdb 等
	if err = m.createBusinessDB(chainId, storeConfig, logger); err != nil {
		logger.Errorf("create businessdb error,errInfo:[%s]", err)
		return err
	}

	//3.3 创建 ledgerSnapshot
	if err = m.createLedgerSnapshot(logger, chainId, storeConfig); err != nil {
		logger.Errorf("create ledgerSnapshot error,errInfo:[%s]", err)
		return err
	}

	return nil
}

// createImportALL creates various  objects those are used to import snapshot data from snapshot files.
func (m *Factory) createImportALL(chainId string, storeConfig *conf.StorageConfig, logger protocol.Logger,
	p11Handle *pkcs11.P11Handle, height uint64) error {
	var err error
	// 1.创建对象，保存在 firstAttrSet 中
	//1.1 创建 bigFilter
	if storeConfig.EnableBigFilter {
		if err = m.createBigFilter(chainId, storeConfig.BigFilter, logger); err != nil {
			logger.Errorf("create bigfilter error,errInfo:[%s]", err)
			return err
		}
	}

	//1.2 创建 数据落盘对称数据加密对象
	if err = m.createEncryptor(chainId, storeConfig, logger, p11Handle); err != nil {
		logger.Errorf("create encryptor error,errInfo:[%s]", err)
		return err
	}

	//1.3 创建 dbhandle
	if err = m.createAllDbHandle(chainId, storeConfig, logger); err != nil {
		logger.Errorf("create all dbhandle error,errInfo:[%s]", err)
		return err
	}

	// 2.创建对象，保存在 secondAttrSet 中
	//2.1 创建 meta
	if err = m.createMeta(chainId, storeConfig, logger); err != nil {
		logger.Errorf("create meta error,errInfo:[%s]", err)
		return err
	}

	// 3.创建对象，保存在 thirdAttrSet 中
	//3.1 创建blockFile(bf)
	if err = m.createImportBF(chainId, storeConfig, logger, height); err != nil {
		logger.Errorf("create block file error,errInfo:[%s]", err)
		return err
	}

	//3.2 创建 BusinessDB 包括blockdb,resultdb,statedb,eventdb,historydb,txExistdb 等
	if err = m.createBusinessDB(chainId, storeConfig, logger); err != nil {
		logger.Errorf("create businessdb error,errInfo:[%s]", err)
		return err
	}

	////3.3 创建 ImportLedgerSnapshot
	//if err = m.createImportLedgerSnapshot(logger, chainId, storeConfig); err != nil {
	//	logger.Errorf("create ledgerSnapshot error,errInfo:[%s]", err)
	//	return err
	//}

	return nil
}

// createWAL creates a new WAL
//创建wal对象
func (m *Factory) createWAL(chainId string, storeConfig *conf.StorageConfig, logger protocol.Logger) error {
	var (
		err    error
		walLog *wal.Log
	)
	// block file and wal ,there is only one option
	// if block file is enabled, then walLog is nil
	if !storeConfig.DisableBlockFileDb {
		m.firstAttrSet[WAL] = walLog
		return nil
	}

	opts := wal.DefaultOptions
	opts.NoCopy = true
	opts.NoSync = storeConfig.LogDBSegmentAsync
	if storeConfig.LogDBSegmentSize > 0 { // LogDBSegmentSize default is 20MB
		opts.SegmentSize = storeConfig.LogDBSegmentSize * 1024 * 1024
	}

	walPath := filepath.Join(storeConfig.StorePath, chainId, walLogPath)
	walLog, err = wal.Open(walPath, opts)
	if err != nil {
		panic(fmt.Sprintf("open wal log failed, path:%s, error:%s", walPath, err))
	}
	m.firstAttrSet[WAL] = walLog
	return nil
}

// createCache creates a new cache for the given cache config
// 创建Cache
func (m *Factory) createCache(config *conf.CacheConfig) (cache.Cache, error) {
	if config == nil || len(config.Provider) == 0 || strings.ToLower(config.Provider) == "bigcache" {
		newCache, err := newBigCache(config)
		if err != nil {
			return nil, err
		}
		return newCache, nil
	}

	panic("cache provider[" + config.Provider + "] not support")
}

// createAllDbHandle creates all kinds of db handles.
//创建所有db的对应handle，并将结果保存在 firstAttrSet 中
//不同类型的db，按照 createDbHandle 返回的第一个返回值，区分是 kv型，还是sql型 dbhandle
func (m *Factory) createAllDbHandle(chainId string, storeConfig *conf.StorageConfig, logger protocol.Logger) error {
	//创建block 对应 dbhandle
	bHName, bHandle, err := m.createDbHandle(storeConfig, chainId, storeConfig.BlockDbConfig, StoreBlockDBDir,
		DBName_BlockDB, storeConfig.DbPrefix, false, logger)
	if err != nil {
		return err
	}
	m.firstAttrSet[bHName] = bHandle

	//创建state 对应 dbhandle
	//sHName, sHandle, err := m.createDbHandle(storeConfig, chainId, storeConfig.BlockDbConfig, StoreStateDBDir,
	sHName, sHandle, err := m.createDbHandle(storeConfig, chainId, storeConfig.StateDbConfig, StoreStateDBDir,
		DBName_StateDB, storeConfig.DbPrefix, !storeConfig.DisableStateCache, logger)
	if err != nil {
		return err
	}
	m.firstAttrSet[sHName] = sHandle

	//创建txExist 对应 dbhandle
	if storeConfig.TxExistDbConfig != nil {
		var tHName string
		var tHandle interface{}
		tHName, tHandle, err = m.createDbHandle(storeConfig, chainId, storeConfig.TxExistDbConfig, StoreTxExistDbDir,
			DBName_TxExistDB, storeConfig.DbPrefix, false, logger)
		if err != nil {
			return err
		}
		m.firstAttrSet[tHName] = tHandle
	}

	//创建history 对应 dbhandle
	if !storeConfig.DisableHistoryDB {
		var hHName string
		var hHandle interface{}
		hHName, hHandle, err = m.createDbHandle(storeConfig, chainId, &storeConfig.HistoryDbConfig.DbConfig,
			StoreHistoryDBDir, DBName_HistoryDB, storeConfig.DbPrefix, false, logger)
		if err != nil {
			return err
		}
		m.firstAttrSet[hHName] = hHandle
	}
	//创建result 对应 dbhandle
	if !storeConfig.DisableResultDB {
		var rHName string
		var rHandle interface{}
		rHName, rHandle, err = m.createDbHandle(storeConfig, chainId, storeConfig.ResultDbConfig, StoreResultDBDir,
			DBName_ResultDB, storeConfig.DbPrefix, false, logger)
		if err != nil {
			return err
		}
		m.firstAttrSet[rHName] = rHandle
	}
	//创建contractEvent 对应 dbhandle
	if !storeConfig.DisableContractEventDB {
		var eHName string
		var eHandle interface{}
		eHName, eHandle, err = m.createDbHandle(storeConfig, chainId, storeConfig.ContractEventDbConfig, StoreEventLogDBDir,
			DBName_EventDB, storeConfig.DbPrefix, false, logger)
		if err != nil {
			return err
		}
		m.firstAttrSet[eHName] = eHandle
	}

	//创建common 对应 dbhandle，只支持kvdb
	var cHName string
	var cHandle interface{}
	cHName, cHandle, err = m.createDbHandle(storeConfig, chainId, storeConfig.GetDefaultDBConfig(), StoreLocalDBDir,
		DBName_LocalDB, storeConfig.DbPrefix, false, logger)
	if err != nil {
		return err
	}
	//key为 DBHandleLocalName
	m.firstAttrSet[cHName] = cHandle

	return nil
}

// createMeta creates a new meta db .
//创建wal对象,meta使用的dbhandle ,用common dbhandle
//创建好的meta保存在 secondAttrSet 中
func (m *Factory) createMeta(chainID string, storeConfig *conf.StorageConfig, logger protocol.Logger) error {
	localDBHandle := m.firstAttrSet[DBHandleLocalName]
	commonDB, ok := localDBHandle.(protocol.DBHandle)
	if !ok {
		return ErrConversionInterface
	}

	metadb, err := metadb.NewMetaDataDB(*storeConfig, chainID, commonDB, logger)
	if err != nil {
		return err
	}
	m.secondAttrSet[META] = metadb
	return nil
}

// createBF creates a new block file db .
//创建blockfile对象,使用已经创建好的的meta ,meta 在secondAttrSet 中
//bf创建好后，保存在 thirdAttrSet 中
func (m *Factory) createBF(chainID string, storeConfig *conf.StorageConfig, logger protocol.Logger) error {
	var (
		err error
		bf  *blockfiledb.BlockFile
	)
	if !storeConfig.DisableBlockFileDb {
		mdb := m.secondAttrSet[META]
		metadb, ok := mdb.(meta.MetaData)
		if !ok {
			return ErrConversionInterface
		}

		opts := blockfiledb.DefaultOptions
		opts.NoCopy = true
		opts.NoSync = storeConfig.LogDBSegmentAsync
		if storeConfig.LogDBSegmentSize > 64 { // LogDBSegmentSize default is 64MB
			opts.SegmentSize = storeConfig.LogDBSegmentSize * 1024 * 1024
		}
		if storeConfig.DisableLogDBMmap {
			opts.UseMmap = false
		}
		bfPath := filepath.Join(storeConfig.StorePath, chainID, blockFilePath)
		bf, err = blockfiledb.Open(bfPath, opts, logger, metadb)

		if err != nil {
			panic(fmt.Sprintf("open block file db failed, path:%s, error:%s", bfPath, err))
		}
		m.thirdAttrSet[BlockFile] = bf
	}
	return nil
}

// createImportBF creates a new block file db which is used to import the snapshot data from snapshot files
// into block file db. createImportBF does not need to be executed if the node was not started with a snapshot.
//bf创建好后，保存在 thirdAttrSet 中
func (m *Factory) createImportBF(chainID string, storeConfig *conf.StorageConfig, logger protocol.Logger,
	height uint64) error {
	var (
		err error
		bf  *blockfiledb.BlockFile
	)
	if !storeConfig.DisableBlockFileDb {
		mdb := m.secondAttrSet[META]
		metadb, ok := mdb.(meta.MetaData)
		if !ok {
			return ErrConversionInterface
		}

		opts := blockfiledb.DefaultOptions
		opts.NoCopy = true
		opts.NoSync = storeConfig.LogDBSegmentAsync
		if storeConfig.LogDBSegmentSize > 64 { // LogDBSegmentSize default is 64MB
			opts.SegmentSize = storeConfig.LogDBSegmentSize * 1024 * 1024
		}
		if storeConfig.DisableLogDBMmap {
			opts.UseMmap = false
		}
		bfPath := filepath.Join(storeConfig.StorePath, chainID, blockFilePath)
		//importOpen会初始化
		bf, err = blockfiledb.ImportOpen(opts, logger, metadb, height)

		if err != nil {
			panic(fmt.Sprintf("open block file db failed, path:%s, error:%s", bfPath, err))
		}
		m.thirdAttrSet[BlockFile] = bf
	}
	return nil
}

// NewStore constructs a new BlockStore
// 创建 BlockchainStore 就像堆积木一样，先把 底层对象创建好，再创建中间对象，再创建上层对象
func (m *Factory) NewStore(chainId string, storeConfig *conf.StorageConfig,
	logger protocol.Logger, p11Handle *pkcs11.P11Handle) (protocol.BlockchainStore, error) {

	var err error

	dbConfig := storeConfig.BlockDbConfig
	if strings.ToLower(dbConfig.Provider) == "simple" {
		logger.Debugf("new store ,block dbconfig simple")
		var db protocol.DBHandle
		db, err = m.dbFactory.NewKvDB(chainId, conf.DbconfigProviderLeveldb, StoreBlockDBDir,
			dbConfig.LevelDbConfig, logger, nil)

		if err != nil {
			return nil, err
		}

		return test.NewDebugStore(logger, storeConfig, db), nil
	}
	if strings.ToLower(dbConfig.Provider) == "memory" {
		logger.Debugf("new store ,block dbconfig memory")
		var db protocol.DBHandle
		db, err = m.dbFactory.NewKvDB(chainId, conf.DbconfigProviderMemdb, StoreBlockDBDir,
			dbConfig.LevelDbConfig, logger, nil)

		if err != nil {
			return nil, err
		}

		return test.NewDebugStore(logger, storeConfig, db), nil
	}

	//创建所有需要的基础对象，保存在 3个集合 firstAttrSet、secondAttrSet、thirdAttrSet 中
	if err = m.createALL(chainId, storeConfig, logger, p11Handle); err != nil {
		logger.Errorf("create all error, errInfo:[%s]", err)
		return nil, err
	}

	//利用已创建好的基础对象，再创建BlockStoreImpl
	var store protocol.BlockchainStore
	if store, err = m.createBlockStoreImpl(chainId, storeConfig, logger); err != nil {
		logger.Errorf("create blockStoreImpl error, errInfo:[%s]", err)
		return nil, err
	}

	//返回最后结果
	return store, err
}

// NewImportStore constructs a new ImportBlockStore
// 创建 ImportLedgerSnapshot 就像堆积木一样，先把 底层对象创建好，再创建中间对象，再创建上层对象
func (m *Factory) NewImportStore(chainId string, storeConfig *conf.StorageConfig,
	logger protocol.Logger, p11Handle *pkcs11.P11Handle, height uint64) (protocol.ImportLedgerSnapshot, error) {

	var err error

	//创建所有需要的基础对象，保存在 3个集合 firstAttrSet、secondAttrSet、thirdAttrSet 中
	if err = m.createImportALL(chainId, storeConfig, logger, p11Handle, height); err != nil {
		logger.Errorf("create all error, errInfo:[%s]", err)
		return nil, err
	}

	return m.createImportLedgerSnapshot(logger, chainId, storeConfig)

}

// buildEncryptor creates a new encryptor  with the given key, encryptor type and pkcs11
//创建 加解密套件
func buildEncryptor(encryptor, key string, p11Handle *pkcs11.P11Handle) (crypto.SymmetricKey, error) {
	switch strings.ToLower(encryptor) {
	case "sm4":
		if p11Handle != nil {
			return pkcs11.NewSecretKey(p11Handle, key, crypto.SM4)
		}
		return &sm4.SM4Key{Key: readEncryptKey(key)}, nil
	case "aes":
		if p11Handle != nil {
			return pkcs11.NewSecretKey(p11Handle, key, crypto.AES)
		}
		return &aes.AESKey{Key: readEncryptKey(key)}, nil
	default:
		return nil, errors.New("unsupported encryptor:" + encryptor)
	}
}

// readEncryptKey reads a key and decode it into a original byte slice.
// 读加密数据
func readEncryptKey(key string) []byte {
	reg := regexp.MustCompile("^0[xX][0-9a-fA-F]+$") //is hex
	if reg.Match([]byte(key)) {
		b, _ := hex.DecodeString(key[2:])
		return b
	}
	if key[0] == '/' && pathExists(key) {
		f, err := ioutil.ReadFile(key)
		if err == nil {
			return f
		}
	}
	//对于windows系统，临时文件以C开头
	if key[0] == 'C' && pathExists(key) {
		f, err := ioutil.ReadFile(key)
		if err == nil {
			return f
		}
	}
	return []byte(key)
}

// pathExists returns true / false if the given path exists or not.
//判断path是否存在
func pathExists(path string) bool {
	_, err := os.Stat(path)
	if err == nil {
		return true
	}
	return os.IsExist(err)
}

// newBigCache creates a new big cache for the given config
//创建 bigCache
func newBigCache(cacheConfig *conf.CacheConfig) (cache.Cache, error) {
	bigCacheDefaultConfig := bigcache.Config{
		// number of shards (must be a power of 2)
		Shards: 1024,
		// time after which entry can be evicted
		LifeWindow: 10 * time.Minute,
		// rps * lifeWindow, used only in initial memory allocation
		CleanWindow: 10 * time.Second,
		// max entry size in bytes, used only in initial memory allocation
		MaxEntrySize: 500,
		// prints information about additional memory allocation
		Verbose: true,
		// cache will not allocate more memory than this limit, value in MB
		// if value is reached then the oldest entries can be overridden for the new ones
		// 0 value means no size limit
		HardMaxCacheSize: 128,
		// callback fired when the oldest entry is removed because of its
		// expiration time or no space left for the new entry. Default value is nil which
		// means no callback and it prevents from unwrapping the oldest entry.
		OnRemove: nil,
	}
	if cacheConfig != nil {
		bigCacheDefaultConfig.LifeWindow = cacheConfig.LifeWindow
		bigCacheDefaultConfig.CleanWindow = cacheConfig.CleanWindow
		bigCacheDefaultConfig.MaxEntrySize = cacheConfig.MaxEntrySize
		bigCacheDefaultConfig.HardMaxCacheSize = cacheConfig.HardMaxCacheSize
	}
	return bigcache.NewBigCache(bigCacheDefaultConfig)
}

// createBigFilter creates a new filter for the given config
// 创建 bigfilter
func (m *Factory) createBigFilter(chainId string, config *conf.BigFilterConfig, logger protocol.Logger) error {
	filter, err := newBigFilter(chainId, config, logger)
	if err != nil {
		return err
	}
	m.firstAttrSet[BigFilter] = filter

	return nil
}

// newBigFilter 创建一个bigFilter
func newBigFilter(chainId string, bigFilterConfig *conf.BigFilterConfig,
	logger protocol.Logger) (bigfilterdb.BigFilterDB, error) {
	redisHosts := strings.Split(bigFilterConfig.RedisHosts, ",")
	filterNum := len(redisHosts)
	if bigFilterConfig.Pass == "" {
		b, err := bigfilterkvdb.NewBigFilterKvDB(filterNum, bigFilterConfig.TxCapacity, bigFilterConfig.FpRate,
			logger, redisHosts, nil, chainId)
		if err != nil {
			return b, err
		}
	}
	b, err := bigfilterkvdb.NewBigFilterKvDB(filterNum, bigFilterConfig.TxCapacity, bigFilterConfig.FpRate,
		logger, redisHosts, &bigFilterConfig.Pass, chainId)
	if err != nil {
		return b, err
	}
	return b, nil

}

// createRollingWindowCache 注册newRollingWindowCache 函数，主要 将 rwCache中的 rolling_window_cache_capacity 初始化赋值
// txIdCount 通过 配置文件中的rolling_window_cache_capacity 赋值
func (m *Factory) createRollingWindowCache(storeConfig *conf.StorageConfig, logger protocol.Logger) error {
	windowCache, err := newRollingWindowCache(storeConfig, logger)
	if err != nil {
		return err
	}

	m.firstAttrSet[RWC] = windowCache

	return nil
}

// newRollingWindowCache 创建一个 RWCache
func newRollingWindowCache(storeConfig *conf.StorageConfig,
	logger protocol.Logger) (rolling_window_cache.RollingWindowCache, error) {
	rollingWindowCacheCapacity := storeConfig.RollingWindowCacheCapacity
	// 未配置或者配置为0，则给默认 1000000
	if storeConfig.RollingWindowCacheCapacity == 0 {
		rollingWindowCacheCapacity = 1000000
	}
	r := rolling_window_cache.NewRollingWindowCacher(rollingWindowCacheCapacity,
		0, 0, 0, 0, logger)
	return r, nil

}

// createEncryptor 创建加密套件
func (m *Factory) createEncryptor(chainId string, storeConfig *conf.StorageConfig, logger protocol.Logger,
	p11Handle *pkcs11.P11Handle) error {
	//注册数据落盘对称数据加密对象
	if len(storeConfig.Encryptor) > 0 && len(storeConfig.EncryptKey) > 0 {
		//encryptor, err = buildEncryptor(storeConfig.Encryptor, storeConfig.EncryptKey, p11Handle)
		logger.Debugf("ioc register store encryptor")
		encryptor, err := buildEncryptor(storeConfig.Encryptor, storeConfig.EncryptKey, p11Handle)
		if err != nil {
			return err
		}

		m.firstAttrSet[Encryptor] = encryptor
	}
	return nil
}

// createDbHandle 创建一个 dbhandle
// 创建 dbhandle 需要先创建好 加密套件 encryptor
// 返回 名字，对象，error; 对象可能是 DBHandle或SqlDBHandle 类型
func (m *Factory) createDbHandle(storeConfig *conf.StorageConfig, chainId string, dbConfig *conf.DbConfig,
	dbDir, name, dbPrefix string, enableCache bool, logger protocol.Logger) (string, interface{}, error) {
	logger.Debugf("create dbhandle ,dbDir:[%s]", dbDir)

	var (
		encryptor      crypto.SymmetricKey
		newDBHandle    protocol.DBHandle
		newSqlDBhandle protocol.SqlDBHandle

		newDBHandleName    string
		newSqlDBhandleName string
	)

	newDBHandleName = "newDBHandleName" + name
	newSqlDBhandleName = "newSqlDBhandleName" + name

	//使用已创建好的encryptor,放在了一级map中
	if len(storeConfig.Encryptor) > 0 && len(storeConfig.EncryptKey) > 0 {
		encryptorInterface := m.firstAttrSet[Encryptor]
		var ok bool
		encryptor, ok = encryptorInterface.(crypto.SymmetricKey)
		if !ok {
			return "", nil, ErrConversionInterface
		}
	} else {
		encryptor = nil
	}

	if dbConfig.IsKVDB() {
		dbName := dbDir
		if dbConfig.Provider == conf.DbconfigProviderSqlKV {
			dbName = getDbName(dbPrefix, name, chainId)
		}
		config := dbConfig.GetDbConfig()
		logger.Debugf("new kvdb ,providerName:[%s]", strings.ToLower(dbConfig.Provider))
		innerDb, err := m.dbFactory.NewKvDB(chainId, dbConfig.Provider, dbName, config, logger, encryptor)
		if err != nil {
			return newDBHandleName, nil, err
		}
		//如果配置了开启缓存，则要加一个缓存
		if enableCache {
			c, err := m.createCache(storeConfig.StateCache)
			if err != nil {
				return newDBHandleName, nil, err
			}
			newDBHandle = cache.NewCacheWrapToDBHandle(c, innerDb, logger)

			return newDBHandleName, newDBHandle, nil
		}
		return newDBHandleName, innerDb, nil

	}
	var err error
	dbName := getDbName(dbPrefix, name, chainId)
	logger.Debugf("new sqldb ,dbName:[%s]", dbName)
	newSqlDBhandle, err = m.dbFactory.NewSqlDB(chainId, dbName, dbConfig.SqlDbConfig, logger)

	if err != nil {
		return newSqlDBhandleName, nil, err
	}
	return newSqlDBhandleName, newSqlDBhandle, nil

}

// getDbName returns the name of the database with dbPrefix , to support multiple chain and database
//为了支持多链和多个db
func getDbName(dbPrefix, dbName, chainId string) string {
	return dbPrefix + dbName + "_" + chainId
}

// createBlockDB creates a new block db
// 创建blockdb，利用blockFile ，DBHandleBlock/SqlDBHandleBlock
// 创建好的blockdb 保存在 thirdAttrSet中
func (m *Factory) createBlockDB(chainId string, storeConfig *conf.StorageConfig, logger protocol.Logger) error {

	dbPrefix := storeConfig.DbPrefix
	//如果是kvdb
	if storeConfig.BlockDbConfig.IsKVDB() {

		bDBH := m.firstAttrSet[DBHandleBlockName]
		blockDBHandle, ok := bDBH.(protocol.DBHandle)
		if !ok {
			return ErrConversionInterface
		}

		if storeConfig.DisableBlockFileDb {
			db := blockkvdb.NewBlockKvDB(chainId, blockDBHandle, logger, storeConfig)
			m.thirdAttrSet[BlockKvDB] = db

		} else {
			blockFile := m.thirdAttrSet[BlockFile]
			bf, ok := blockFile.(binlog.BinLogger)
			if !ok {
				return ErrConversionInterface
			}
			db := blockfiledb.NewBlockFileDB(chainId, blockDBHandle, logger, storeConfig, bf)
			m.thirdAttrSet[BlockFileDB] = db
		}
	} else { //如果是sqldb
		dbName := getDbName(dbPrefix, DBName_BlockDB, chainId)
		sqlDBH := m.firstAttrSet[SqlDBHandleBlockName]
		blockDBHandle, ok := sqlDBH.(protocol.SqlDBHandle)
		if !ok {
			return ErrConversionInterface
		}

		db := blocksqldb.NewBlockSqlDB(dbName, blockDBHandle, logger)
		m.thirdAttrSet[BlockSQLDB] = db
	}
	return nil
}

// createStateDB creates a state db
// 创建statedb，使用已创建好的DBHandle
func (m *Factory) createStateDB(chainId string, storeConfig *conf.StorageConfig, logger protocol.Logger) error {

	//如果是kvdb
	if storeConfig.StateDbConfig.IsKVDB() {
		sDBH := m.firstAttrSet[DBHandleStateName]
		stateDBHandle, ok := sDBH.(protocol.DBHandle)
		if !ok {
			return ErrConversionInterface
		}
		db := statekvdb.NewStateKvDB(chainId, stateDBHandle, logger, storeConfig)
		m.thirdAttrSet[StateKvDB] = db

	} else { //sqldb
		sqlDBH := m.firstAttrSet[SqlDBHandleStateName]
		stateSQLDBHandle, ok := sqlDBH.(protocol.SqlDBHandle)
		if !ok {
			return ErrConversionInterface
		}

		newDbFunc := func(dbName string) (protocol.SqlDBHandle, error) {
			return stateSQLDBHandle, nil
			//var db protocol.SqlDBHandle
			//err = m.ioc.Resolve(&db, container.ResolveName(DBName_StateDB),
			//	container.Arguments(map[int]interface{}{1: dbName}))
			//return db, err
		}
		connPoolSize := 90
		if maxConnSize, ok := storeConfig.StateDbConfig.SqlDbConfig["max_open_conns"]; ok {
			connPoolSize, _ = maxConnSize.(int)
		}

		db, err := statesqldb.NewStateSqlDB(storeConfig.DbPrefix, chainId, stateSQLDBHandle, newDbFunc, logger, connPoolSize)

		if err != nil {
			return err
		}
		m.thirdAttrSet[StateSQLDB] = db

	}
	return nil
}

// createHistoryDB creates a new history db
// 创建historydb,利用已创建好的handle
// 创建好的historydb 保存在 thirdAttrSet中
func (m *Factory) createHistoryDB(chainId string, storeConfig *conf.StorageConfig, logger protocol.Logger) error {

	if storeConfig.HistoryDbConfig.IsKVDB() {
		hDBH := m.firstAttrSet[DBHandleHistoryName]
		historyDBHandle, ok := hDBH.(protocol.DBHandle)
		if !ok {
			return ErrConversionInterface
		}
		db := historykvdb.NewHistoryKvDB(chainId, storeConfig.HistoryDbConfig, historyDBHandle, logger)
		m.thirdAttrSet[HistoryKvDB] = db

	} else {
		sqlDBH := m.firstAttrSet[SqlDBHandleHistoryName]
		historySQLDBHandle, ok := sqlDBH.(protocol.SqlDBHandle)
		if !ok {
			return ErrConversionInterface
		}

		dbName := getDbName(storeConfig.DbPrefix, DBName_HistoryDB, chainId)
		db := historysqldb.NewHistorySqlDB(dbName, storeConfig.HistoryDbConfig, historySQLDBHandle, logger)
		m.thirdAttrSet[HistorySQLDB] = db
	}
	return nil
}

// createResultDB creates a new result db
// 创建resultdb，利用已经创建好的dbhandle
// 将创建好的resultdb保存在 thirdAttrSet中
func (m *Factory) createResultDB(chainId string, storeConfig *conf.StorageConfig, logger protocol.Logger) error {
	//如果是kvdb
	if storeConfig.ResultDbConfig.IsKVDB() {
		rDBH := m.firstAttrSet[DBHandleResultName]
		resultDBHandle, ok := rDBH.(protocol.DBHandle)
		if !ok {
			return ErrConversionInterface
		}

		if storeConfig.DisableBlockFileDb {
			db := resultkvdb.NewResultKvDB(chainId, resultDBHandle, logger, storeConfig)
			m.thirdAttrSet[ResultKvDB] = db

		} else { //需要用到blockFile (bf)
			blockFile := m.thirdAttrSet[BlockFile]
			bf, ok := blockFile.(binlog.BinLogger)
			if !ok {
				return ErrConversionInterface
			}
			db := resultfiledb.NewResultFileDB(chainId, resultDBHandle, logger, storeConfig, bf)
			m.thirdAttrSet[ResultFileDB] = db
		}
	} else { //如果是sqldb
		dbName := getDbName(storeConfig.DbPrefix, DBName_ResultDB, chainId)
		sqlDBH := m.firstAttrSet[SqlDBHandleResultName]
		resultDBHandle, ok := sqlDBH.(protocol.SqlDBHandle)
		if !ok {
			return ErrConversionInterface
		}

		db := resultsqldb.NewResultSqlDB(dbName, resultDBHandle, logger)
		m.thirdAttrSet[ResultSQLDB] = db
	}
	return nil
}

// createContractDB creates a contract event db
// 创建contractEventdb，利用已经创建好的dbhandle
// 将创建好的contractEventdb保存在 thirdAttrSet中
// contractdb 只是 sql类型
func (m *Factory) createContractDB(chainId string, storeConfig *conf.StorageConfig, logger protocol.Logger) error {

	dbName := getDbName(storeConfig.DbPrefix, DBName_EventDB, chainId)
	sqlDBH := m.firstAttrSet[SqlDBHandleEventName]
	eventDBHandle, ok := sqlDBH.(protocol.SqlDBHandle)
	if !ok {
		return ErrConversionInterface
	}

	db, err := eventsqldb.NewContractEventDB(dbName, eventDBHandle, logger, storeConfig, chainId)
	if err != nil {
		return err
	}
	m.thirdAttrSet[EventSQLDB] = db

	return nil
}

// createTxExistDB creates a txExistDB
// 创建txExistdb
func (m *Factory) createTxExistDB(chainId string, storeConfig *conf.StorageConfig, logger protocol.Logger) error {
	var blockDB blockdb.BlockDB
	//如果没有配置TxExistDB，那么就使用BlockDB来判断TxExist
	if storeConfig.TxExistDbConfig == nil {
		//1.先读取blockdb对应的对象
		//如果是kvdb
		if storeConfig.BlockDbConfig.IsKVDB() {
			if storeConfig.DisableBlockFileDb {
				bDB := m.thirdAttrSet[BlockKvDB]
				var ok bool
				blockDB, ok = bDB.(*blockkvdb.BlockKvDB)
				if !ok {
					return ErrConversionInterface
				}
			} else {
				bDB := m.thirdAttrSet[BlockFileDB]
				var ok bool
				blockDB, ok = bDB.(*blockfiledb.BlockFileDB)
				if !ok {
					return ErrConversionInterface
				}
			}
		} else { //如果是sql db
			bDB := m.thirdAttrSet[BlockSQLDB]
			var ok bool
			blockDB, ok = bDB.(*blocksqldb.BlockSqlDB)
			if !ok {
				return ErrConversionInterface
			}
		}

		//2.创建 noTxExistDB ,noTxExistDB中使用blockDB
		db := WrapBlockDB2TxExistDB(blockDB, logger)
		m.thirdAttrSet[WrapTxExistDB] = db

		// 单独配置了txExistdb,那么使用独立的db，不使用BlockDB
	} else if storeConfig.TxExistDbConfig.IsKVDB() {
		tDBH, ok := m.firstAttrSet[DBHandleTxExistName]
		if !ok {
			return ErrConversionInterface
		}
		txExistDBHandle, ok := tDBH.(protocol.DBHandle)
		if !ok {
			return ErrConversionInterface
		}
		db := txexistkvdb.NewTxExistKvDB(chainId, txExistDBHandle, logger)
		m.thirdAttrSet[TxExistKvDB] = db

	}
	return nil
}

// createBusinessDB creates a new business db using the  db handle created earlier.
// 创建blockdb,statedb,historydb,resultdb,contractEventdb,txExistdb
// 利用已创建好的dbhandle
func (m *Factory) createBusinessDB(chainId string, storeConfig *conf.StorageConfig, logger protocol.Logger) error {
	//blockdb
	if err := m.createBlockDB(chainId, storeConfig, logger); err != nil {
		return err
	}
	//statedb
	if err := m.createStateDB(chainId, storeConfig, logger); err != nil {
		return err
	}
	//historydb
	if !storeConfig.DisableHistoryDB {
		if err := m.createHistoryDB(chainId, storeConfig, logger); err != nil {
			return err
		}
	}
	//resultdb
	if !storeConfig.DisableResultDB {
		if err := m.createResultDB(chainId, storeConfig, logger); err != nil {
			return err
		}
	}
	//contractEventdb
	if !storeConfig.DisableContractEventDB {
		if err := m.createContractDB(chainId, storeConfig, logger); err != nil {
			return err
		}
	}
	//TxExistDB
	return m.createTxExistDB(chainId, storeConfig, logger)
}

// 创建blockdb,statedb,historydb,resultdb,contractEventdb,txExistdb
// 利用已创建好的dbhandle
func (m *Factory) createLedgerSnapshot(logger protocol.Logger, chainID string, storeConfig *conf.StorageConfig) error {
	var bdb, sdb, edb interface{}
	var ok bool

	//1.从thirdAttrSet 得到blockDB
	//如果是kvdb
	if storeConfig.BlockDbConfig.IsKVDB() {
		if storeConfig.DisableBlockFileDb {
			bdb = m.thirdAttrSet[BlockKvDB]
		} else {
			bdb = m.thirdAttrSet[BlockFileDB]
		}
	} else { //如果是sqldb
		bdb = m.thirdAttrSet[BlockSQLDB]
	}
	blockDB, ok := bdb.(blockdb.BlockDB)
	if !ok {
		logger.Errorf("blockdb conversion err")
		err := ErrConversionInterface
		return err
	}

	//2.从thirdAttrSet 得到stateDB
	if storeConfig.StateDbConfig.IsKVDB() {
		sdb = m.thirdAttrSet[StateKvDB]
	} else {
		sdb = m.thirdAttrSet[StateSQLDB]
	}
	stateDB, ok := sdb.(statedb.StateDB)
	if !ok {
		logger.Errorf("statedb conversion err")
		err := ErrConversionInterface
		return err
	}

	//3.从thirdAttrSet 得到eventDB
	var eventDB contracteventdb.ContractEventDB
	if !storeConfig.DisableContractEventDB {
		edb = m.thirdAttrSet[EventSQLDB]
		eventDB, ok = edb.(contracteventdb.ContractEventDB)
		if !ok {
			logger.Errorf("eventdb conversion err")
			err := ErrConversionInterface
			return err
		}
	} else {
		eventDB = nil
	}

	//4.创建ledgerSnapshot，存到thirdAttrSet中
	newLedgerSnapshot := snapshot.NewSnapshot(blockDB, stateDB, eventDB, logger, chainID, storeConfig)
	m.thirdAttrSet[LedgerSnapshot] = newLedgerSnapshot
	return nil

}

// createImportLedgerSnapshot finish create import ledger snapshot
// 创建blockdb,statedb,historydb,resultdb,contractEventdb,txExistdb
// 利用已创建好的dbhandle
func (m *Factory) createImportLedgerSnapshot(logger protocol.Logger, chainID string,
	storeConfig *conf.StorageConfig) (protocol.ImportLedgerSnapshot, error) {
	var bfdb binlog.BinLogger
	var metadb meta.MetaData
	var ok bool

	//get db
	blockDB, stateDB, historyDB, resultDB, contractDB, existDB, commonDB, err := m.getBusinessDB(storeConfig, logger)
	if err != nil {
		return nil, err
	}

	//get bf
	if !storeConfig.DisableBlockFileDb {
		bf := m.thirdAttrSet[BlockFile]
		bfdb, ok = bf.(binlog.BinLogger)
		if !ok {
			logger.Errorf("blockdb conversion err")
			err = ErrConversionInterface
			return nil, err
		}
	}

	//get meta
	if !storeConfig.DisableBlockFileDb {
		mdb := m.secondAttrSet[META]
		metadb, ok = mdb.(meta.MetaData)
		if !ok {
			return nil, ErrConversionInterface
		}
	}

	//get bigfilter
	var bigFilter bigfilterdb.BigFilterDB
	if storeConfig.EnableBigFilter {
		bFilter := m.firstAttrSet[BigFilter]
		bigFilter, ok = bFilter.(bigfilterdb.BigFilterDB)
		if !ok {
			return nil, ErrConversionInterface
		}
	}

	//create importLedgerSnapshot
	importSnapshot, err := snapshot.NewStoreImportSnapshot(chainID, storeConfig, blockDB, stateDB, historyDB,
		contractDB, resultDB, existDB, commonDB, logger, bfdb, bigFilter, metadb)
	if err != nil {
		panic(err)
	}
	m.thirdAttrSet[ImportLedgerSnapshot] = importSnapshot

	return importSnapshot, nil

}

// getBusinessDB  returns	all kinds of business db
// 返回所有bussinessdb,包括 blockdb，statedb，historydb,resultdb,contractdb,txExistdb,commondbhandle
// 除了common dbhandle 从firstAttrSet中获得，其它都从 thirdAttrSet获得
func (m *Factory) getBusinessDB(storeConfig *conf.StorageConfig,
	logger protocol.Logger) (blockDB blockdb.BlockDB, stateDB statedb.StateDB, historyDB historydb.HistoryDB,
	resultDB resultdb.ResultDB, contractDB contracteventdb.ContractEventDB, txExistDB txexistdb.TxExistDB,
	commonDBHandle protocol.DBHandle, err error) {

	var bdb, sdb, hdb, rdb, cdb, tdb interface{}
	var ok bool

	//1.从thirdAttrSet 得到blockDB
	//如果是kvdb
	if storeConfig.BlockDbConfig.IsKVDB() {
		if storeConfig.DisableBlockFileDb {
			bdb = m.thirdAttrSet[BlockKvDB]
		} else {
			bdb = m.thirdAttrSet[BlockFileDB]
		}
	} else { //如果是sqldb
		bdb = m.thirdAttrSet[BlockSQLDB]
	}
	blockDB, ok = bdb.(blockdb.BlockDB)
	if !ok {
		logger.Errorf("blockdb conversion err")
		err = ErrConversionInterface
	}

	//2.从thirdAttrSet 得到stateDB
	if storeConfig.StateDbConfig.IsKVDB() {
		sdb = m.thirdAttrSet[StateKvDB]
	} else {
		sdb = m.thirdAttrSet[StateSQLDB]
	}
	stateDB, ok = sdb.(statedb.StateDB)
	if !ok {
		logger.Errorf("statedb conversion err")
		err = ErrConversionInterface
	}

	//3.从thirdAttrSet 得到historyDB
	//var historyDB historydb.HistoryDB
	if !storeConfig.DisableHistoryDB {
		if storeConfig.HistoryDbConfig.IsKVDB() {
			hdb = m.thirdAttrSet[HistoryKvDB]
		} else {
			hdb = m.thirdAttrSet[HistorySQLDB]
		}
		historyDB, ok = hdb.(historydb.HistoryDB)
		if !ok {
			logger.Errorf("historydb conversion err")
			err = ErrConversionInterface
		}
	}

	//4.从thirdAttrSet 得到resultDB
	//var resultDB resultdb.ResultDB
	if !storeConfig.DisableResultDB {
		if storeConfig.ResultDbConfig.IsKVDB() {
			if storeConfig.DisableBlockFileDb {
				rdb = m.thirdAttrSet[ResultKvDB]
			} else { //需要用到blockFile (bf)
				rdb = m.thirdAttrSet[ResultFileDB]
			}
		} else { //如果是sqldb
			rdb = m.thirdAttrSet[ResultSQLDB]
		}
		resultDB, ok = rdb.(resultdb.ResultDB)
		if !ok {
			logger.Errorf("resultdb conversion err")
			err = ErrConversionInterface
		}
	}

	//5.从thirdAttrSet 得到ContractEventDB
	//var contractDB contracteventdb.ContractEventDB

	if !storeConfig.DisableContractEventDB {
		cdb = m.thirdAttrSet[EventSQLDB]
		contractDB, ok = cdb.(contracteventdb.ContractEventDB)
		if !ok {
			logger.Errorf("contractevent db conversion err")
			err = ErrConversionInterface
		}
	}

	//6.从thirdAttrSet 得到txExistDB
	if storeConfig.TxExistDbConfig == nil {
		tdb = m.thirdAttrSet[WrapTxExistDB]
		// 单独配置了txExistdb
	} else if storeConfig.TxExistDbConfig.IsKVDB() {
		tdb = m.thirdAttrSet[TxExistKvDB]

	}
	txExistDB, ok = tdb.(txexistdb.TxExistDB)
	if !ok {
		logger.Errorf("txexistdb conversion err")
		err = ErrConversionInterface
	}

	//7.从firstAttrSet 得到commonDB
	commonDB := m.firstAttrSet[DBHandleLocalName]
	commonDBHandle, ok = commonDB.(protocol.DBHandle)
	if !ok {
		logger.Errorf("commondb conversion err")
		err = ErrConversionInterface
	}
	return
}

// createBlockStoreImpl creates a new block chain store using the chain id ,storeConfig and logger
// 创建BlockStoreImpl
// 先从3个集合AttrSet中获得基础的对象，然后作为参数，传入，完成创建
func (m *Factory) createBlockStoreImpl(chainId string, storeConfig *conf.StorageConfig,
	logger protocol.Logger) (protocol.BlockchainStore, error) {
	var (
		blockDB        blockdb.BlockDB
		stateDB        statedb.StateDB
		historyDB      historydb.HistoryDB
		resultDB       resultdb.ResultDB
		contractDB     contracteventdb.ContractEventDB
		txExistDB      txexistdb.TxExistDB
		commonDBHandle protocol.DBHandle
		ok             bool
		err            error
	)
	//1.获得各个db对象
	blockDB, stateDB, historyDB, resultDB, contractDB, txExistDB, commonDBHandle, err = m.getBusinessDB(storeConfig,
		logger)
	if err != nil {
		return nil, err
	}

	//2.获得bf
	var blockFile binlog.BinLogger
	if !storeConfig.DisableBlockFileDb {
		bf := m.thirdAttrSet[BlockFile]
		blockFile, ok = bf.(binlog.BinLogger)
		if !ok {
			return nil, ErrConversionInterface
		}
	}

	//3.获得wal
	var waler *wal.Log
	if storeConfig.DisableBlockFileDb {
		wAL := m.firstAttrSet[WAL]
		waler, ok = wAL.(*wal.Log)
		if !ok {
			return nil, ErrConversionInterface
		}
	}

	//4.获得bigfilter
	var bigFilter bigfilterdb.BigFilterDB
	if storeConfig.EnableBigFilter {
		bFilter := m.firstAttrSet[BigFilter]
		bigFilter, ok = bFilter.(bigfilterdb.BigFilterDB)
		if !ok {
			return nil, ErrConversionInterface
		}
	}

	//5.获得rwCache(rollingWindowCache)
	rwc := m.firstAttrSet[RWC]
	rWC, ok := rwc.(rolling_window_cache.RollingWindowCache)
	if !ok {
		return nil, ErrConversionInterface
	}

	//6.获得meta
	metadb := m.secondAttrSet[META]
	metaDB, ok := metadb.(meta.MetaData)
	if !ok {
		return nil, ErrConversionInterface
	}

	//7.获得ledgerSnapshot
	snapshoter := m.thirdAttrSet[LedgerSnapshot]
	ledgerSnapshot, ok := snapshoter.(snapshot.LedgerSnapshot)
	if !ok {
		return nil, ErrConversionInterface
	}

	//创建同步存储对象，blockStoreImpl
	impl, err := NewBlockStoreImpl(chainId, storeConfig, blockDB, stateDB, historyDB, contractDB, resultDB, txExistDB,
		commonDBHandle, logger, blockFile, waler, bigFilter, rWC, metaDB, ledgerSnapshot)
	if err != nil {
		return nil, err
	}
	//创建异步存储对象
	if storeConfig.Async {
		return NewAsyncBlockStoreImpl(impl, logger), err
	}
	return impl, nil
}

// 如果txExistDB不单独创建handle
// 使用这个 noTxExistDB，里面用blockdb
type noTxExistDB struct {
	db blockdb.BlockDB
}

// InitGenesis 初始化
func (n noTxExistDB) InitGenesis(genesisBlock *serialization.BlockWithSerializedInfo) error {
	return nil
}

// CommitBlock 提交
func (n noTxExistDB) CommitBlock(blockWithRWSet *serialization.BlockWithSerializedInfo, isCache bool) error {
	return nil
}

// GetLastSavepoint 返回savepoint
func (n noTxExistDB) GetLastSavepoint() (uint64, error) {
	return n.db.GetLastSavepoint()
}

// TxExists 判断交易是否存在
func (n noTxExistDB) TxExists(txId string) (bool, error) {
	return n.db.TxExists(txId)
}

// ImportTxID  import txid from snapshot data to db
func (n noTxExistDB) ImportTxID(key, value []byte) error {
	return n.db.ImportTxID(key, value)
}

// ImportSetSavePoint  set up savePoint when import snapshot
func (n noTxExistDB) ImportSetSavePoint(height uint64) error {
	return n.db.ImportSetSavePoint(height)
}

// Close 关闭
func (n noTxExistDB) Close() {

}

// WrapBlockDB2TxExistDB 创建一个使用了blockDB的 txExistDB
func WrapBlockDB2TxExistDB(db blockdb.BlockDB, log protocol.Logger) txexistdb.TxExistDB {
	log.Info("no TxExistDB config, use BlockDB to replace TxExistDB")
	return &noTxExistDB{db: db}
}
